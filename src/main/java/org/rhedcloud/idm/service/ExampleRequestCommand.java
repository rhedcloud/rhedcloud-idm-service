package org.rhedcloud.idm.service;

import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.AppConfig;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.consumer.commands.RequestCommandImpl;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

public class ExampleRequestCommand extends RequestCommandImpl implements RequestCommand {

	private static String LOGTAG = "[" + ExampleRequestCommand.class.getSimpleName() + "]";
	private static Logger LOG = org.apache.logging.log4j.LogManager.getLogger(ExampleRequestCommand.class);
	private static AppConfig appConfig;
	private Document responseDoc = null; // the primed XML response document

	@SuppressWarnings("unchecked")
	private <T> T getObjectByType(Class<T> t) {
		T moaIn;
		try {
			moaIn = (T) appConfig.getObjectByType(t.getName());
		}
		catch (EnterpriseConfigurationObjectException e) {
			LOG.fatal(LOGTAG, e);
			throw new RuntimeException(e);
		}
		return moaIn;
	}

	public ExampleRequestCommand(CommandConfig cConfig) throws InstantiationException {
		super(cConfig);
		System.out.println(LOGTAG + "init");
		appConfig = getAppConfig();
		PropertyConfig pConfig = new PropertyConfig();
		try {
			pConfig = (PropertyConfig) getAppConfig().getObject("ExampleRequestCommandProperties");
		}
		catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from " + "AppConfig: The exception is: "
					+ eoce.getMessage();
			LOG.fatal(LOGTAG + errMsg, eoce);
			throw new InstantiationException(errMsg);
		}
		setProperties(pConfig.getProperties());

		// Initialize response document.
		XmlDocumentReader xmlReader = new XmlDocumentReader();
		try {
			LOG.debug(LOGTAG + "responseDocumentUri: " + getProperties().getProperty("responseDocumentUri"));
			responseDoc = xmlReader.initializeDocument(getProperties().getProperty("responseDocumentUri"),
					getOutboundXmlValidation());
			if (responseDoc == null) {
				String errMsg = "Missing 'responseDocumentUri' "
						+ "property in the deployment descriptor.  Can't continue.";
				LOG.fatal(LOGTAG + errMsg);
				throw new InstantiationException(errMsg);
			}
		}
		catch (XmlDocumentReaderException e) {
			LOG.fatal(LOGTAG, e);
			e.printStackTrace();
			throw new InstantiationException(e.getMessage());
		}

		LOG.info(LOGTAG + " instantiated successfully.");
	}

	@SuppressWarnings("unchecked")
	public final Message execute(int messageNumber, Message aMessage) throws CommandException {
		Document localResponseDoc = (Document) responseDoc.clone();

		// Convert the JMS Message to an XML Document
		Document inDoc = null;
		try {
			inDoc = initializeInput(messageNumber, aMessage);
		}
		catch (Exception e) {
			String errMsg = "Exception occurred processing input message in "
					+ "org.openeai.jms.consumer.commands.Command.  Exception: " + e.getMessage();
			throw new CommandException(errMsg, e);
		}

		// Retrieve text portion of message.
		TextMessage msg = (TextMessage) aMessage;
		try {
			// Clear the message body for the reply, so we do not
			// have to do it later.
			msg.clearBody();
		}
		catch (Exception e) {
			String errMsg = "Error clearing the message body.";
			throw new CommandException(errMsg + ". The exception is: " + e.getMessage(), e);
		}
		Element eControlArea = getControlArea(inDoc.getRootElement());
		String msgAction = eControlArea.getAttribute("messageAction").getValue();
		String msgObject = "edu.emory.moa.jmsobjects.identity.v1_0."
				+ eControlArea.getAttribute("messageObject").getValue();

		Class<ActionableEnterpriseObject> moaClass = null;
		try {
			moaClass = (Class<ActionableEnterpriseObject>) Class.forName(msgObject);
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			LOG.fatal(LOGTAG, e);
			throw new CommandException(e);
		}
		ActionableEnterpriseObject moa = getObjectByType(moaClass);
		Document doc = null;
		if (msgAction.equalsIgnoreCase("Generate")) {
			doc = moa.getResponseDoc();
		}
		else if (msgAction.equalsIgnoreCase("Query")) {
			doc = moa.getProvideDoc();
		}
		else {
			doc = localResponseDoc;
		}
		return getMessage(msg, buildReplyDocument(eControlArea, doc));
	}
}

package org.rhedcloud.idm.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.openeai.OpenEaiException;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.consumer.commands.RequestCommandImpl;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.EnterpriseObjectSyncException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.moa.XmlEnterpriseObjectImpl;
import org.openeai.moa.objects.resources.Authentication;
import org.openeai.transport.SyncService;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;
import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.ProviderProcessingResponse;
import org.rhedcloud.idm.service.provider.ResourceProvisioningProvider;
import org.rhedcloud.idm.service.provider.RoleAssignmentProvider;
import org.rhedcloud.idm.service.provider.RoleProvisioningProvider;
import org.rhedcloud.idm.service.provider.netiq.TransactionScopeManager;
import org.rhedcloud.idm.service.provider.netiq.util.IdmValues;
import org.rhedcloud.idm.service.util.MessageControl;
import org.rhedcloud.idm.service.util.PropertyNames;

import edu.emory.moa.jmsobjects.identity.v1_0.Resource;
import edu.emory.moa.jmsobjects.identity.v1_0.Role;
import edu.emory.moa.jmsobjects.identity.v1_0.RoleAssignment;
import edu.emory.moa.objects.resources.v1_0.ResourceQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.ResourceRequisition;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentRequisition;
import edu.emory.moa.objects.resources.v1_0.RoleQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.RoleRequisition;

/**
 * This request command handles Request messages for IDM RoleAssignment, Role
 * and Resource objects. Specifically, it handles a Query-Request, a
 * Generate-Request, and a Delete-Request for RoleAssignment object; a
 * Query-Request, a Generate-Request, a Update-Request and a Delete-Request for
 * Role object; a Query-Request, a Generate-Request and a Delete-Request for
 * Resource object. 
 * 
 * If a Query-Request messages is received, the command evaluates the query
 * parameters to identify the query object in its DataArea, then it invokes the
 * specific query method of the configured service provider to process business
 * logic. For RoleAssignment query object, the command evaluates either
 * retrieving all identities assigned to a role or querying all roles assigned
 * to an identity; for Role query object, the command uses the RoleDN as
 * querying key and identifies the ResourceAssociation object linked to Role and
 * Resource objects on NetIQ system, once confirmed, it retrieves Role and all
 * associated Resource objects based on their associations, then it builds the
 * Provide-Reply message from the results of the query operation. If no matching
 * message objects can be identified by the application processing, the
 * Provide-Reply returned to the requesting application will contain an empty
 * DataArea element.
 * 
 * If a Generate-Request is received, the command builds the message object and
 * dispatches the submission to the NetIQ system to create a new object by
 * processing the request in its DataArea. If the request creation process
 * contains multiple creation steps, for example, several Resources and
 * ResourceAssociation objects need to be created at any given time when Role
 * object is created, for each Create operation call to complete, the command
 * will follow up with a Get operation to make sure the object won't be created
 * without the previous creation step complete successfully. After the creation
 * steps are complete without error, the application polls the asynchronous
 * process to confirm the process is complete using waitForCompletion() method
 * and build the new RoleAssignment, Role and Resource object with the persisted
 * source.
 * 
 * If a Delete-Request is received, first of all, the command use the state of
 * the object found in the DeleteData to locate the object in NetIQ system and
 * to perform a comparison element by element with the new and baseline objects.
 * If the object for which a delete is requested is found in NetIQ system and
 * its state matches the state of the object in the DeleteData element of the
 * Delete-Request, the command applies further business logic to verify if
 * delete action should be performed. For example, if Role object is passed in
 * for deletion, after element comparison in the command class, the business
 * methods will apply further logic to validate if there are assigned identities
 * attached to the Role object. The delete action will not be performed if there
 * are identities attached to the Role object. After all the message and
 * business logics are applied, the command will attempt to process the deletion
 * of Role, Resource and ResourceAssociation objects and complete the process
 * with the calling of waitForCompletion() method.
 * 
 * There are no Update-Request to be performed for Resource and RoleAssignment
 * message objects. If a Update-Request is received for Role Object, the command
 * uses the state of the object found in the BaselineData element to locate the
 * object on NetIQ to modify and to perform a baseline comparison. It also
 * compares the baseline element with the new data element. If the baseline is
 * stale comparing with the new data element then it logs with a 'baseline
 * stale' error. If the new data state is identical with the NetIQ data store,
 * or new data state is identical with baseline data store, there is no update
 * needed thus command returning a message to the calling application. If there
 * is new data not present or data redundancy found in NetIQ data store, the
 * command will apply the business rule and process to add associations to the
 * new Resource or perform the deletion of the stale ResourseAssociation object.
 * It worths to mention that Update-Request for Role object is not intended for
 * the creation of Resource or Role object, there are specific Resource or Role
 * generation methods the calling application can use if the creation of new
 * Role or Resource object is the business purpose. Rather Role.Update-Request
 * is used to create or remove ResourceAssocation relationship among Role and
 * Resource so ResourceAssociation object can be hide from the end users.
 * 
 * In Generate-Request or Update-Request processing, this command employs a
 * transaction scope manager which tracks all the completed create or update
 * calls, and necessarily make sure that all work is complete in its entirety
 * that is, multiple operations are performed successfully, or if
 * unsuccessfully, that no operations have been performed. If there is a
 * processing step failed, in Role.Generate-Request, for example, where
 * ResourceAssocation object creation process is failed and an exception be
 * thrown, the command will call rollback() method from the transaction scope
 * manager and remove all the Role and Resource objects already created.
 * However, in Delete-Request, once the data is deleted, it won't be able to
 * recover as NetIQ will not allow us re-creating a new object with the deleted
 * key.
 * 
 * In cases of error, the implementation-specific exceptions like Axis2's
 * RemoteException or NetIQ's NrfServiceExceptionException will not be escalated
 * to the command class directly. Instead the exception will be caught and
 * converted to a custom ProviderException in the business methods and be
 * re-thrown to the command class, thus the command class can create meaningful
 * reply message and send back to the users.
 * 
 * @author Tom Cervenka (tcerven@emory.edu)
 * @author Richard Xing (rxing2@emory.edu)
 * @author Steve Wheat (swheat@emory.edu)
 * 
 * @version 1.0 - 11 Feburary 2017
 * @version 1.1 - 6 Oct 2023
 */
public class RoleServiceRequestCommand extends RequestCommandImpl implements RequestCommand {
	private static String serviceUserId = "";
	private static String servicePassword = "";
	private static String searchServiceEndpoint = "";
	private static int connectionTimeout = 12000;
	private ProducerPool producerPool = null;
	private static int socketTimeout = 12000;
	private static String authUserId = null;
	private Document responseDoc = null;
	private boolean verbose = false;
	private RoleAssignmentProvider roleAssignmentProvider = null;
	private RoleProvisioningProvider roleProvisioningProvider = null;
	private ResourceProvisioningProvider resourceProvisioningProvider = null;
	private static final String LOGTAG = "[" + RoleServiceRequestCommand.class.getSimpleName() + "]";;

	@SuppressWarnings({ "deprecation" })
	public RoleServiceRequestCommand(CommandConfig cConfig)
			throws InstantiationException, ProviderException, IllegalAccessException, ClassNotFoundException {
		super(cConfig);

		logger.info(LOGTAG + "RoleServiceRequestCommand is initializing...");
		// Initialize a command-specific logger if it exists.
		try {
			ProducerPool pool = (ProducerPool) getAppConfig().getObject("SyncPublisher");
			setProducerPool(pool);
		}
		catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a ProducerPool object " + "from AppConfig. The exception is: "
					+ eoce.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		try {
			PropertyConfig pConfig = (PropertyConfig) getAppConfig().getObject("GeneralProperties");
			setProperties(pConfig.getProperties());
		}
		catch (EnterpriseConfigurationObjectException ecoe) {
			String errMsg = "An error occurred retrieving a property config from AppConfig. The exception is: "
					+ ecoe.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		// Initialize response document.
		XmlDocumentReader xmlReader = new XmlDocumentReader();
		try {
			logger.debug(LOGTAG + "responseDocumentUri: " + getProperties().getProperty("responseDocumentUri"));
			responseDoc = xmlReader.initializeDocument(getProperties().getProperty("responseDocumentUri"),
					getOutboundXmlValidation());
			if (responseDoc == null) {
				String errMsg = LOGTAG + "Missing 'responseDocumentUri' "
						+ "property in the deployment descriptor.  Can't continue.";
				logger.fatal(LOGTAG + errMsg);
				throw new InstantiationException(errMsg);
			}
			setResponseDoc(responseDoc);
		}
		catch (XmlDocumentReaderException e) {
			String errMsg = "Error initializing the primed documents.";
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(e.getMessage());
		}

		String roleServiceClassName = getProperties()
				.getProperty(PropertyNames.ROLE_SERVICE_PROVIDER_CLASSNAME.getName());
		logger.info(LOGTAG + "Getting RoleService class for name: " + roleServiceClassName);

		if (getProperties().getProperty("verbose", "false").equalsIgnoreCase("true")) {
			setVerbose(true);
		}

		if (roleServiceClassName == null || roleServiceClassName.equals("")) {
			String errMsg = "No RoleServiceProviderClassName property specified. Can't continue. Please define provider class name from AppConfig.";
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		String resourceServiceClassName = getProperties()
				.getProperty(PropertyNames.RESOURCE_SERVICE_PROVIDER_CLASSNAME.getName());
		logger.info(LOGTAG + "Getting ResourceService class for name: " + resourceServiceClassName);

		if (resourceServiceClassName == null || resourceServiceClassName.equals("")) {
			String errMsg = "No ResourceServiceProviderClassName property specified. Can't continue. Please define provider class name from AppConfig.";
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		String roleAssignmentClassName = getProperties()
				.getProperty(PropertyNames.ROLE_ASSIGNMENT_PROVIDER_CLASSNAME.getName());
		logger.info(LOGTAG + "Getting RoleAssignment class for name: " + roleAssignmentClassName);


		if (resourceServiceClassName == null || resourceServiceClassName.equals("")) {
			String errMsg = "No ResourceServiceProviderClassName property specified. Can't continue. Please define provider class name from AppConfig.";
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		logger.info(LOGTAG + "ResourceServiceProviderClassName is: " + resourceServiceClassName);

		this.roleProvisioningProvider = (RoleProvisioningProvider) Class.forName(roleServiceClassName).newInstance();
		try {
			this.roleProvisioningProvider.init(getAppConfig());
			setRoleProvisioningProvider(roleProvisioningProvider);
		}
		catch (ProviderException pe) {
			String errMsg = "Error initializing the IdmRoleProvisioningProvider object from AppConfig: The exception is: "
					+ pe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		this.roleAssignmentProvider = (RoleAssignmentProvider) Class.forName(roleAssignmentClassName).newInstance();
		try {
			this.roleAssignmentProvider.init(getAppConfig());
			setRoleAssignmentProvider(roleAssignmentProvider);
		}
		catch (ProviderException pe) {
			String errMsg = "Error initializing the IdmRoleAssignmentProvider object from AppConfig: The exception is: "
					+ pe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}
	}

	@Override
	public final Message execute(int messageNumber, Message aMessage) throws CommandException {

		logger.debug(LOGTAG + "In execute()");
		long startTime = System.currentTimeMillis();
		Document requestFromEai = new Document();
		TextMessage msg = (TextMessage) aMessage;
		try {
			requestFromEai = initializeInput(messageNumber, aMessage);
			if (isVerbose()) {
				logger.info(LOGTAG + "Processing message: " + msg.getText());
			}
			msg.clearBody();
		}
		catch (JMSException je) {
			String errMsg = "Exception occurred processing input message in "
					+ "org.openeai.jms.consumer.commands.Command.  Exception is: " + je.getMessage();
			throw new CommandException(errMsg);
		}

		// Get Control area from XML object
		Element eControlArea = getControlArea(requestFromEai.getRootElement());
		String msgAction = this.getMessageAction(requestFromEai);
		String msgObject = this.getMessageObject(requestFromEai);
		String msgRelease = this.getMessageRelease(requestFromEai);

		// parse the text from the JMS message into a JDOM document.
		logger.debug(LOGTAG + "msgAction: " + this.getMessageAction(requestFromEai));
		logger.debug(LOGTAG + "msgType: " + this.getMessageType(requestFromEai));
		logger.debug(LOGTAG + "msgObject: " + this.getMessageObject(requestFromEai));
		logger.debug(LOGTAG + "msgRelease: " + this.getMessageRelease(requestFromEai));

		Element eAuthUserId = eControlArea.getChild("Sender").getChild("Authentication").getChild("AuthUserId");
		authUserId = eAuthUserId.getValue();

		Document responseXmlObject = (Document) getResponseXml(msgAction, msgObject, msgRelease).clone();

		/***********************************************************************************************************************************
		 * 
		 * ROLE_ASSIGNMENT
		 * 		
		 ***********************************************************************************************************************************/

		if (MessageControl.ROLE_ASSIGNMENT_OBJECT.toString().equalsIgnoreCase(msgObject)) {

			RoleAssignment roleAssignment = null;
			RoleAssignmentQuerySpecification queryData = null;
			try {
				roleAssignment = this.getObject(RoleAssignment.class);
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "Error retrieving an object from AppConfig. The exception is: " + ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new CommandException(errMsg, ecoe);
			}

			try {

				String aRoleDn = null;
				List<XmlEnterpriseObject> roleAssignmentObjects = null;

				if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					logger.debug(LOGTAG + " Start to process RoleAssignment.Query-Request.");
					logger.debug(LOGTAG + " Provider name is: " + getRoleAssignmentProvider().getClass().getName());
					Element queryElement = null;
					queryElement = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.ROLE_ASSIGNMENT_QUERY_SPECIFICATION.toString());
					XMLOutputter outputter = new XMLOutputter();

					logger.info(LOGTAG + "Query messages: " + outputter.outputString(queryElement));
					if (queryElement != null) {
						queryData = this.getObject(RoleAssignmentQuerySpecification.class);
						queryData.buildObjectFromInput(queryElement);
						logger.debug(LOGTAG + "queryData is build: " + queryData.getIdentityType());
					}

					// Retrieves all roles assigned to an identity
					if (queryData.getUserDN() != null && queryData.getUserDN().length() > 0) {

						String identityType = null;
						if (queryData.getIdentityType() != null && queryData.getIdentityType().length() > 0) {
							identityType = queryData.getIdentityType();
						}
						if (queryData.getRoleDN() != null && queryData.getRoleDN().length() > 0) {
							roleAssignmentObjects = getRoleAssignmentProvider().query(queryData.getUserDN(),
									queryData.getRoleDN(), identityType);
						}
						else {
							roleAssignmentObjects = getRoleAssignmentProvider().query(queryData.getUserDN(),
									identityType);
						}
					}
					else {
						roleAssignmentObjects = getRoleAssignmentProvider().query(queryData);
					}

					logger.debug(LOGTAG + "In execute(). Completed returning response oEAI object for Query-Request.");

				}

				if (MessageControl.GENERATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					logger.debug(LOGTAG + " Start to process RoleAssignment.Generate-Request. requestFromEai: "
							+ requestFromEai.toString());
					logger.debug(LOGTAG + " Provider name is: " + getRoleAssignmentProvider().getClass().getName());
					Element eRequisition = null;
					eRequisition = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.ROLE_ASSIGNMENT_REQUISITION.toString());
					if (eRequisition == null) {
						String errMsg = "Invalid element found in the Generate-Request message. This action expects a RoleAssignmentRequisition object";
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "error");
					}

					XMLOutputter outputter = new XMLOutputter();
					logger.info(LOGTAG + "Generate messages: " + outputter.outputString(eRequisition));

					RoleAssignmentRequisition roleAssignmentRequisition = new RoleAssignmentRequisition();
					RoleAssignment confirmedRoleAssignment = null;
					roleAssignmentRequisition = (RoleAssignmentRequisition) getAppConfig()
							.getObject("RoleAssignmentRequisition" + "." + generateRelease(msgRelease));
					if (roleAssignmentRequisition != null) {
						roleAssignmentRequisition.buildObjectFromInput(eRequisition);
					}

					if (getRoleAssignmentProvider().isUserRoleBeingGranted(roleAssignmentRequisition)) {
						throw new CommandException(
								"Data is already in NetIQ, the user or the group has already being granted for the Role. RoleDN [Request XML Value = "
										+ roleAssignmentRequisition.getRoleDNs().getDistinguishedName(0) + "] | "
										+ "IdentityDN [Request XML Value = " + roleAssignmentRequisition.getIdentityDN()
										+ "]");
					}

					try {
						long generateStartTime = System.currentTimeMillis();
						confirmedRoleAssignment = getRoleAssignmentProvider().generate(roleAssignmentRequisition,
								msgAction);
						long generateTime = System.currentTimeMillis() - generateStartTime;
						logger.info(LOGTAG + "Generated RoleAssignment in " + generateTime + " ms.");
					} catch (ProviderException pe) {
						String errMsg = "An error occurred generating Role in Generate-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "warn");
					}

					// Issue a create sync only on Generate-Request
					if (confirmedRoleAssignment != null) {
						try {
							MessageProducer producer = getProducerPool().getProducer();
							logger.info(LOGTAG + "Publishing Role Assignment Create-Sync");
							Authentication auth = new Authentication();
							auth.setAuthUserId(authUserId);
							auth.setAuthUserSignature("none");
							confirmedRoleAssignment.setAuthentication(auth);
							confirmedRoleAssignment.createSync((SyncService) producer);
							logger.info(LOGTAG + "Published RoleAssignment.Create-Sync" + " message.");
						} catch (EnterpriseObjectSyncException eose) {
							String errMsg = "An error occurred publishing the Identity.Create-Sync message after generating an identity.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(eose);
						} catch (JMSException jmse) {
							String errMsg = "An error occurred publishing the Identity.Create-Sync message after generating an identity.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(jmse);
						}
					}

					responseXmlObject.getRootElement().getChild("DataArea").removeContent();
					logger.debug(LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync.");
					Element elementRole = null;
					try {
						elementRole = (Element) confirmedRoleAssignment.buildOutputFromObject();
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred serializing a Role object to an XML element. The exception is: "
								+ ele.getMessage();
						logger.error(LOGTAG + errMsg, ele);
						throw new CommandException(errMsg, ele);
					}
					responseXmlObject.getRootElement().getChild("DataArea").addContent(elementRole);
					String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
					// Log execution time.
					long executionTime = System.currentTimeMillis() - startTime;
					logger.info(LOGTAG + "RoleServiceRequestcommand execution complete in " + executionTime + " ms.");
					// Return the response with status success.
					return getMessage(msg, replyContents);
				}

				if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					logger.debug(LOGTAG + " Start to process RoleAssignment.Delete-Request ");
					logger.debug(LOGTAG + " Provider name is: " + getRoleAssignmentProvider().getClass().getName());
					Element deleteElement = null;
					deleteElement = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.DELETE_DATA.toString())
							.getChild(MessageControl.ROLE_ASSIGNMENT.toString());
					XMLOutputter outputter = new XMLOutputter();
					logger.info(LOGTAG + "Delete messages: " + outputter.outputString(deleteElement));

					if (deleteElement == null) {
						String errMsg = "Invalid element found in the Delete-Request message. This action expects a RoleAssignment object";
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
					}

					try {
						roleAssignment.buildObjectFromInput(deleteElement);
					}
					catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred building the delete object from the DataArea element in the Delete-Request message. The exception is: "
								+ ele.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-1000", eControlArea, msgRelease, msg, "error");
					}
					RoleAssignment confirmedDelete = null;
					try {

						long deleteStartTime = System.currentTimeMillis();
						confirmedDelete = getRoleAssignmentProvider().delete(roleAssignment);
						long deleteTime = System.currentTimeMillis() - deleteStartTime;
						logger.info(LOGTAG + "Deleted RoleAssignment in " + deleteTime + " ms.");
					}
					catch (ProviderException pe) {
						String errMsg = "An error occurred deleting RoleAssignment in Generate-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "warn");
					}

					logger.debug(LOGTAG + "In excute(). RoleDN response string returned from delete(): " + aRoleDn);
					// Issue a delete sync only we confirm the role is deleted.
					if (confirmedDelete == null) {
						try {
							MessageProducer producer = getProducerPool().getProducer();
							logger.info(LOGTAG + "Publishing Role Assignment Delete-Sync");
							roleAssignment.deleteSync("purge", (SyncService) producer);
							logger.info(LOGTAG + "Published RoleAssignment.Delete-Sync" + " message.");
						}
						catch (EnterpriseObjectSyncException eose) {
							String errMsg = "An error occurred publishing the Identity.Delete-Sync message after revoking an identity.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(eose);
						}
						catch (JMSException jmse) {
							String errMsg = "An error occurred publishing the Identity.Delete-Sync message after revoking an identity.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(jmse);
						}
					}
					logger.debug(LOGTAG + "In execute(). Completed returning oEAI object and publishing Delete-Sync.");
				}

				// Send response
				Element roleAssignmentElement = null;
				List<Element> elementList = new ArrayList<Element>();
				Element responseDataArea = responseXmlObject.getRootElement().getChild(DATA_AREA);
				responseDataArea.removeContent();
				if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
					if (roleAssignmentObjects != null && roleAssignmentObjects.size() > 0) {
						for (XmlEnterpriseObject eObj : roleAssignmentObjects) {
							roleAssignmentElement = getElementFromEnterpriseObject(eObj);
							elementList.add(roleAssignmentElement);
						}
						if (!elementList.isEmpty()) {
							logger.debug(LOGTAG + "In excute(). Setting data area elements. Elements to set: "
									+ elementList.size());
							for (Element element : elementList) {
								responseDataArea.addContent(element);
							}
						}
					}
				} 

				if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
					responseDoc.getRootElement().getChild("DataArea").removeContent();
					String replyContents = buildReplyDocument(eControlArea, responseDoc);
					// Return the response with status success.
					// Log execution time.
					long executionTime = System.currentTimeMillis() - startTime;
					logger.info(
							LOGTAG + "RoleServiceRequestcommand execution complete in " + executionTime + " ms.");
					return getMessage(msg, replyContents);
				}

				String responseMessage = buildReplyDocument(eControlArea, responseXmlObject);
				Message responseToEAI = getMessage(msg, responseMessage);
				// Log execution time.
				long executionTime = System.currentTimeMillis() - startTime;
				logger.info(LOGTAG + "RoleServiceRequestcommand execution complete in " + executionTime + " ms.");
				return responseToEAI;
			} catch (OpenEaiException o) {
				logger.error(LOGTAG+"An error occurred starting the producer. The exception is: "+ o.getMessage());
				return getExceptionResponse(o, "IDMService-100", eControlArea, msgRelease, msg, "error");
			} catch (ProviderException pe) {
				String errMsg = "An error occurred quering RoleAssignment in Query-Request. The exception is: "
						+ pe.getMessage();
				OpenEaiException e = new OpenEaiException(errMsg + msgObject);
				e.printStackTrace();
				return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "error");
			}
		}

		/*********************************************************************************************************
		 * 
		 * ROLE
		 * 		
		 */

		else if (MessageControl.ROLE_OBJECT.toString().equalsIgnoreCase(msgObject)) {

			Role role = new Role();
			try {
				role = this.getObject(Role.class);
			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "Error retrieving an object from AppConfig. The exception is: " + ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new CommandException(errMsg, ecoe);
			}

			if (MessageControl.GENERATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

				logger.debug(LOGTAG + " Start to process Role.Generate-Request ");
				logger.debug(LOGTAG + " Provider name is: " + this.getRoleProvisioningProvider().getClass().getName());
				ProviderProcessingResponse result = new ProviderProcessingResponse();
				TransactionScopeManager transactionManager = new TransactionScopeManager();
				Element eRequisition = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
						.getChild(MessageControl.ROLE_REQUISITION.toString());
				if (eRequisition == null) {
					String errMsg = "Invalid element found in the Generate-Request message. This action expects a RoleRequisition object";
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "error");
				}

				RoleRequisition roleRequisition = new RoleRequisition();
				try {
					roleRequisition = (RoleRequisition) getAppConfig()
							.getObject(MessageControl.ROLE_REQUISITION.toString() + "." + generateRelease(msgRelease));
					if (roleRequisition != null) {
						roleRequisition.buildObjectFromInput(eRequisition);
					}
					try {
						long generateStartTime = System.currentTimeMillis();
						result = getRoleProvisioningProvider().generate(roleRequisition);
						long generateTime = System.currentTimeMillis() - generateStartTime;
						logger.info(LOGTAG + "Generated Role in " + generateTime + " ms.");
					} catch (ProviderException pe) {
						String errMsg = "An error occurred generating Role in Generate-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "warn");
					}

					// Issue a create sync only on Generate-Request
					if (result != null && result.getResponse() != null) {
						try {
							MessageProducer producer = getProducerPool().getProducer();
							logger.info(LOGTAG + "Publishing Role Object Create-Sync");
							Authentication auth = new Authentication();
							auth.setAuthUserId(authUserId);
							auth.setAuthUserSignature("none");
							((Role) result.getResponse()).setAuthentication(auth);
							((Role) result.getResponse()).createSync((SyncService) producer);
							logger.info(LOGTAG + "Published Role.Create-Sync" + " message.");
						} catch (EnterpriseObjectSyncException eose) {
							// in case publishing failed remove all the objects
							// created from NetIQ
							if (result != null && result.getSavedChanges() != null
									&& result.getSavedChanges().size() > 0) {
								try {
									transactionManager.rollback(result.getSavedChanges(), result.getRoleService(),
											result.getResourceService());
								}
								catch (ProviderException pe) {
									String errMsg = "An error occurred in rollback(). The exception is: "
											+ pe.getMessage();
									OpenEaiException e = new OpenEaiException(errMsg + msgObject);
									return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg,
											"error");
								}
							}
							String errMsg = "An error occurred publishing the Role.Create-Sync message after generating an identity.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(eose);
						} catch (JMSException jmse) {
							// always check if there are changes being made in
							// case of exceptions so as to rollback
							if (result != null && result.getSavedChanges() != null
									&& result.getSavedChanges().size() > 0) {
								try {
									transactionManager.rollback(result.getSavedChanges(), result.getRoleService(),
											result.getResourceService());
								}
								catch (ProviderException pe) {
									String errMsg = "An error occurred in rollback(). The exception is: "
											+ pe.getMessage();
									OpenEaiException e = new OpenEaiException(errMsg + msgObject);
									return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg,
											"error");
								}
							}
							String errMsg = "An error occurred publishing the Role.Create-Sync message after generating an identity.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(jmse);
						}
					}
					logger.debug(LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync.");
				} catch (EnterpriseConfigurationObjectException ecoe) {
					String errMsg = "Error retrieving an object from AppConfig: The exception" + "is: "
							+ ecoe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ecoe);
				} catch (EnterpriseLayoutException ele) {
					String errMsg = "An error occurred serializing Role object to an XML element. The exception is: "
							+ ele.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ele);
				}
				responseXmlObject.getRootElement().getChild("DataArea").removeContent();
				logger.debug(LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync.");
				Element elementRole = null;
				try {
					elementRole = (Element) ((Role) result.getResponse()).buildOutputFromObject();
				} catch (EnterpriseLayoutException ele) {
					// always check if there are changes being made in case of
					// exceptions so as to rollback
					if (result != null && result.getSavedChanges() != null && result.getSavedChanges().size() > 0) {
						try {
							transactionManager.rollback(result.getSavedChanges(), result.getRoleService(),
									result.getResourceService());
						}
						catch (ProviderException pe) {
							String errMsg = "An error occurred in rollback() during Role.Generate-Request. The exception is: "
									+ pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "error");
						}
					}
					String errMsg = "An error occurred serializing a Role object to an XML element. The exception is: "
							+ ele.getMessage();
					logger.error(LOGTAG + errMsg, ele);
					throw new CommandException(errMsg, ele);
				}
				responseXmlObject.getRootElement().getChild("DataArea").addContent(elementRole);
				String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
				// Log execution time.
				long executionTime = System.currentTimeMillis() - startTime;
				logger.info(LOGTAG + "RoleServiceRequestcommand execution complete in " + executionTime + " ms.");
				// Return the response with status success.
				return getMessage(msg, replyContents);
			}

			if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

				logger.debug(LOGTAG + " Start to process Role.Query-Request ");
				logger.debug(LOGTAG + " Provider name is: " + this.getRoleProvisioningProvider().getClass().getName());
				Element eQuerySpec = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
						.getChild(MessageControl.ROLE_QUERY_SPECIFICATION.toString());

				XMLOutputter outputter = new XMLOutputter();
				logger.info("Query messages: " + outputter.outputString(eQuerySpec));
				RoleQuerySpecification queryData = new RoleQuerySpecification();

				if (eQuerySpec != null) {
					try {
						queryData = this.getObject(RoleQuerySpecification.class);
						if (queryData != null) {
							queryData.buildObjectFromInput(eQuerySpec);
						}
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred serializing Role object to an XML element. The exception is: "
								+ ele.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, ele);
					} catch (EnterpriseConfigurationObjectException e) {
						String errMsg = "Error retrieving an object from AppConfig: " + "The exception" + "is: "
								+ e.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg);
					}
					logger.info(LOGTAG + "queryData is build: " + queryData.getRoleDN());
					try {

						String roleDn = queryData.getRoleDN();
						role = getRoleProvisioningProvider().query(roleDn, null, null);
					} catch (ProviderException pe) {
						String errMsg = "An error occurred quering Role in Query-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "warn");
					}
				}

				// Send response
				Element responseDataArea = responseXmlObject.getRootElement().getChild(DATA_AREA);
				responseDataArea.removeContent();
				try {
					if (role != null) {
						eQuerySpec = (Element) role.buildOutputFromObject();
						responseXmlObject.getRootElement().getChild("DataArea").addContent(eQuerySpec);
						String responseMessage = buildReplyDocument(eControlArea, responseXmlObject);
						Message responseToEAI = getMessage(msg, responseMessage);
						// Log execution time.
						long executionTime = System.currentTimeMillis() - startTime;
						logger.info(
								LOGTAG + "RoleServiceRequestcommand execution complete in " + executionTime + " ms.");
						return responseToEAI;
					}
				} catch (EnterpriseLayoutException ele) {
					String errMsg = "An error occurred serializing Role object to an XML element. The exception is: "
							+ ele.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ele);
				}
			}
			if (MessageControl.UPDATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

				logger.debug(LOGTAG + " Start to process Role.Update-Request ");
				logger.debug(LOGTAG + " Provider name is: " + this.getRoleProvisioningProvider().getClass().getName());
				ProviderProcessingResponse result = new ProviderProcessingResponse();
				TransactionScopeManager transactionManager = new TransactionScopeManager();
				Element eBaselineData = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
						.getChild("BaselineData").getChild("Role");

				Element eNewData = requestFromEai.getRootElement().getChild("DataArea").getChild("NewData")
						.getChild("Role");

				if (eNewData == null || eBaselineData == null) {
					String errMsg = "Either the baseline or new data state of the Role object is null. Can't continue.";
					throw new CommandException(errMsg);
				}

				Role newRole = new Role();
				Role baselineRole = new Role();
				try {
					baselineRole = (Role) getAppConfig().getObjectByType(baselineRole.getClass().getName());
					newRole = (Role) getAppConfig().getObjectByType(newRole.getClass().getName());
				} catch (EnterpriseConfigurationObjectException ecoe) {
					String errMsg = "An error occurred retrieving an object from " + "AppConfig. The exception is: "
							+ ecoe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ecoe);
				}

				try {
					baselineRole.buildObjectFromInput(eBaselineData);
					newRole.buildObjectFromInput(eNewData);
				} catch (EnterpriseLayoutException ele) {
					String errMsg = "An error occurred building the baseline and newdata states of the Role object passed in. The exception is: "
							+ ele.getMessage();
					throw new CommandException(errMsg, ele);
				}

				// Perform the baseline check.
				Role idmRole = null;
				try {
					idmRole = getRoleProvisioningProvider().query(newRole.getRoleDN(), null, null);

					if (idmRole == null) {
						// TODO: just turn it into a create???
						String errDesc = "Baseline is stale and no identity from IDM matches the new data. No update operation may be performed, please use Generate to create.";
						logger.warn(LOGTAG + errDesc);
						OpenEaiException e = new OpenEaiException(errDesc + msgObject);
						return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "warn");
					}

					if (baselineRole.equals(idmRole)) {
						String errDesc = "Baseline matches the current state of the Role in the IDM service.";
						logger.debug(LOGTAG + errDesc);
					} else {
						String errDesc = "Baseline does not matches the current state of the Role in the IDM service.";
						logger.warn(LOGTAG + errDesc);
					}
				} catch (XmlEnterpriseObjectException xeoe) {
					String errMsg = "An error occurred comparing the baseline and new data. The exception is: "
							+ xeoe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, xeoe);
				} catch (ProviderException pe) {
					String errMsg = "An error occurred Querying in Update-Request. The exception is: "
							+ pe.getMessage();
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "warn");
				}

				try {
					long updateStartTime = System.currentTimeMillis();
					result = getRoleProvisioningProvider().update(newRole, idmRole);
					long updateTime = System.currentTimeMillis() - updateStartTime;
					logger.info(LOGTAG + "Update Role in " + updateTime + " ms.");
				} catch (ProviderException pe) {
					String errMsg = "An error occurred in Update-Request. The exception is: " + pe.getMessage();
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "error");
				}

				try {
					newRole.setBaseline(baselineRole);
					MessageProducer producer = getProducerPool().getProducer();
					newRole.updateSync((SyncService) producer);
				} catch (EnterpriseObjectSyncException eose) {
					// if there is an exception, we need rollback all the
					// changes made from previous processing in
					// RoleProvisioningProvider
					if (result != null && result.getSavedChanges() != null && result.getSavedChanges().size() > 0) {
						try {
							transactionManager.rollback(result.getSavedChanges(), result.getRoleService(),
									result.getResourceService());
						} catch (ProviderException pe) {
							String errMsg = "An error occurred in rollback(). The exception is: " + pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "error");
						}
					}
					else {
						// TODO: can't roll back the changes, NOP??
					}
					String errMsg = "An error occurred publishing the Role.Update-Sync message after updating Role. The exception is: "
							+ eose.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, eose);
				} catch (JMSException jmse) {
					if (result != null && result.getSavedChanges() != null && result.getSavedChanges().size() > 0) {
						try {
							transactionManager.rollback(result.getSavedChanges(), result.getRoleService(),
									result.getResourceService());
						} catch (ProviderException pe) {
							String errMsg = "An error occurred in rollback() during Role.Update-Request. The exception is: "
									+ pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "error");
						}
					}
					String errMsg = "An error occurred publishing the Role.Update-Sync message after updating Role. The exception is: "
							+ jmse.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, jmse);
				}
			}

			if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

				logger.debug(LOGTAG + " Start to process Role.Delete-Request ");
				logger.debug(LOGTAG + " Provider name is: " + this.getRoleProvisioningProvider().getClass().getName());
				Element eDeleteData = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
						.getChild(MessageControl.DELETE_DATA.toString()).getChild(IdmValues.ROLE.toString());

				if (eDeleteData == null) {
					String errMsg = "Invalid element found in the Delete-Request message. This action expects a Role object";
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
				}

				try {
					role.buildObjectFromInput(eDeleteData);
				} catch (EnterpriseLayoutException ele) {

					String errMsg = "An error occurred building the delete object from the DataArea element in the Delete-Request message. The exception is: "
							+ ele.getMessage();
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, "IDMService-1000", eControlArea, msgRelease, msg, "error");
				}

				try {
					long deleteStartTime = System.currentTimeMillis();
					getRoleProvisioningProvider().delete(role);
					long deleteTime = System.currentTimeMillis() - deleteStartTime;
					logger.info(LOGTAG + "Update Role in " + deleteTime + " ms.");
				} catch (ProviderException pe) {
					String errMsg = "An error occurred deleting Role in Delete-Request message. The exception is: "
							+ pe.getMessage();
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, "IDMService-1000", eControlArea, msgRelease, msg, "warn");
				}

				try {
					MessageProducer producer = getProducerPool().getProducer();
					logger.info(LOGTAG + "Publishing Role Delete-Sync");
					role.deleteSync("purge", (SyncService) producer);
					logger.info(LOGTAG + "Published Role.Delete-Sync" + " message.");
				} catch (EnterpriseObjectSyncException eose) {
					String errMsg = "An error occurred publishing the Role.Delete-Sync message after deleting Role.";
					logger.error(LOGTAG + errMsg);
					throw new CommandException(eose);
				} catch (JMSException jmse) {
					String errMsg = "An error occurred publishing the Role.Delete-Sync message after deleting Role.";
					logger.error(LOGTAG + errMsg);
					throw new CommandException(jmse);
				}

				logger.debug(LOGTAG + "In execute(). Completed returning oEAI object and publishing Delete-Sync.");
			}

			responseXmlObject.getRootElement().getChild("DataArea").removeContent();
			String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
			// Log execution time.
			long executionTime = System.currentTimeMillis() - startTime;
			logger.info(LOGTAG + "RoleServiceRequestcommand execution complete in " + executionTime + " ms.");
			// Return the response with status success.
			return getMessage(msg, replyContents);
		}

		/*********************************************************************************************************
		 * 
		 * RESOURCE
		 * 		
		 */		

		else if (MessageControl.RESOURCE_OBJECT.toString().equalsIgnoreCase(msgObject)) {

			Resource resource = new Resource();
			try {
				resource = this.getObject(Resource.class);
			}
			catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "Error retrieving an object from AppConfig. The exception is: " + ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new CommandException(errMsg, ecoe);
			}

			Element eResource = null;
			if (MessageControl.GENERATE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

				logger.debug(LOGTAG + " Start to process Resource.Generate-Request ");
				logger.debug(LOGTAG + " Provider name is: " + this.getRoleProvisioningProvider().getClass().getName());
				ProviderProcessingResponse result = new ProviderProcessingResponse();
				TransactionScopeManager transactionManager = new TransactionScopeManager();
				eResource = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
						.getChild(MessageControl.RESOURCE_REQUISITION.toString());
				if (eResource == null) {
					String errMsg = "Invalid element found in the Generate-Request message. This action expects a ResourceRequisition object";
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "error");
				}

				ResourceRequisition resourceRequisition = new ResourceRequisition();
				try {
					resourceRequisition = (ResourceRequisition) getAppConfig()
							.getObject("ResourceRequisition" + "." + generateRelease(msgRelease));
					if (resourceRequisition != null) {
						resourceRequisition.buildObjectFromInput(eResource);
					}
					try {
						long generateStartTime = System.currentTimeMillis();
						result = getResourceProvisioningProvider().generate(resourceRequisition);
						long generateTime = System.currentTimeMillis() - generateStartTime;
						logger.info(LOGTAG + "Generated Resource in " + generateTime + " ms.");
					}
					catch (ProviderException pe) {
						String errMsg = "An error occurred generating Resource in Generate-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "warn");
					}
				} catch (EnterpriseConfigurationObjectException ecoe) {
					String errMsg = "Error retrieving an object from AppConfig: The exception" + "is: "
							+ ecoe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ecoe);
				} catch (EnterpriseLayoutException ele) {
					String errMsg = "An error occurred serializing Role object to an XML element. The exception is: "
							+ ele.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ele);
				}

				if (result != null && result.getResponse() != null) {
					try {

						MessageProducer producer = getProducerPool().getProducer();
						logger.info(LOGTAG + "Publishing Resource Create-Sync");
						Authentication auth = new Authentication();
						auth.setAuthUserId(authUserId);
						auth.setAuthUserSignature("none");
						((Resource) result.getResponse()).setAuthentication(auth);
						((Resource) result.getResponse()).createSync((SyncService) producer);
						logger.info(LOGTAG + "Published Resource.Create-Sync" + " message.");
					}
					catch (EnterpriseObjectSyncException eose) {
						// In case publishing Create-Sync failed, we rollback
						// the changes
						if (result != null && result.getSavedChanges() != null && result.getSavedChanges().size() > 0) {
							try {
								transactionManager.rollback(result.getSavedChanges(), result.getRoleService(),
										result.getResourceService());
							}
							catch (ProviderException pe) {
								String errMsg = "An error occurred in rollback(). The exception is: " + pe.getMessage();
								OpenEaiException e = new OpenEaiException(errMsg + msgObject);
								return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg,
										"error");
							}
						}
						String errMsg = "An error occurred publishing the Resource.Create-Sync message after generating Resource.";
						logger.error(LOGTAG + errMsg);
						throw new CommandException(eose);
					}
					catch (JMSException jmse) {
						if (result != null && result.getSavedChanges() != null && result.getSavedChanges().size() > 0) {
							try {
								transactionManager.rollback(result.getSavedChanges(), result.getRoleService(),
										result.getResourceService());
							}
							catch (ProviderException pe) {
								String errMsg = "An error occurred in rollback(). The exception is: " + pe.getMessage();
								OpenEaiException e = new OpenEaiException(errMsg + msgObject);
								return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg,
										"error");
							}
						}
						String errMsg = "An error occurred publishing the Resource.Create-Sync message after generating Resource.";
						logger.error(LOGTAG + errMsg);
						throw new CommandException(jmse);
					}
				}

				responseXmlObject.getRootElement().getChild("DataArea").removeContent();
				logger.debug(LOGTAG + "In execute(). Completed returning oEAI object and publishing Create-Sync.");
				Element elementResource = null;

				try {
					elementResource = (Element) ((Resource) result.getResponse()).buildOutputFromObject();
				} catch (EnterpriseLayoutException ele) {
					// always check if there are changes being made in case of
					// exceptions so as to rollback
					if (result != null && result.getSavedChanges() != null && result.getSavedChanges().size() > 0) {
						try {
							transactionManager.rollback(result.getSavedChanges(), result.getRoleService(),
									result.getResourceService());
						}
						catch (ProviderException pe) {
							String errMsg = "An error occurred in rollback(). The exception is: " + pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "error");
						}
					}
					String errMsg = "An error occurred serializing a Role object to an XML element. The exception is: "
							+ ele.getMessage();
					logger.error(LOGTAG + errMsg, ele);
					throw new CommandException(errMsg, ele);
				}
				responseXmlObject.getRootElement().getChild("DataArea").addContent(elementResource);
				String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
				// Log execution time.
				long executionTime = System.currentTimeMillis() - startTime;
				logger.info(LOGTAG + "RoleServiceRequestcommand execution complete in " + executionTime + " ms.");
				// Return the response with status success.
				return getMessage(msg, replyContents);

			}

			if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

				logger.debug(LOGTAG + " Start to process Resource.Query-Request ");
				logger.debug(LOGTAG + " Provider name is: " + this.getRoleProvisioningProvider().getClass().getName());
				eResource = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
						.getChild(MessageControl.RESOURCE_QUERY_SPECIFICATION.toString());
				ResourceQuerySpecification queryData = new ResourceQuerySpecification();

				if (eResource != null) {
					try {
						queryData = this.getObject(ResourceQuerySpecification.class);

						if (queryData != null) {
							queryData.buildObjectFromInput(eResource);
						}
					}
					catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred serializing Role object to an XML element. The exception is: "
								+ ele.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg, ele);
					}
					catch (EnterpriseConfigurationObjectException e) {
						String errMsg = "Error retrieving an object from AppConfig: " + "The exception" + "is: "
								+ e.getMessage();
						logger.error(LOGTAG + errMsg);
						throw new CommandException(errMsg);
					}

					logger.info(LOGTAG + "queryData is build: " + queryData.getResourceDN());
					try {
						resource = getResourceProvisioningProvider().query(queryData.getResourceDN(), null);
					}
					catch (ProviderException pe) {
						String errMsg = "An error occurred querying Resource in Generate-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-100", eControlArea, msgRelease, msg, "warn");
					}
				}

				// Send response
				Element responseDataArea = responseXmlObject.getRootElement().getChild(DATA_AREA);
				responseDataArea.removeContent();
				try {
					if (resource != null && resource.getResourceDN() != null && resource.getResourceDN().length() > 0) {
						eResource = (Element) resource.buildOutputFromObject();
						responseXmlObject.getRootElement().getChild("DataArea").addContent(eResource);
						String responseMessage = buildReplyDocument(eControlArea, responseXmlObject);
						// Log execution time.
						long executionTime = System.currentTimeMillis() - startTime;
						logger.info(
								LOGTAG + "RoleServiceRequestcommand execution complete in " + executionTime + " ms.");
						Message responseToEAI = getMessage(msg, responseMessage);
						return responseToEAI;
					}
				} catch (EnterpriseLayoutException ele) {
					String errMsg = "An error occurred serializing Role object to an XML element. The exception is: "
							+ ele.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ele);
				}
			}

			if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

				logger.debug(LOGTAG + " Start to process Resource.Delete-Request ");
				logger.debug(LOGTAG + " Provider name is: " + this.getRoleProvisioningProvider().getClass().getName());
				eResource = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
						.getChild(MessageControl.DELETE_DATA.toString()).getChild(IdmValues.RESOURCE.toString());

				if (eResource == null) {
					String errMsg = "Invalid element found in the Delete-Request message. This action expects a Resource object";
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
				}

				try {
					resource.buildObjectFromInput(eResource);
				} catch (EnterpriseLayoutException ele) {
					String errMsg = "An error occurred serializing Role object to an XML element. The exception is: "
							+ ele.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new CommandException(errMsg, ele);
				}
				logger.info(LOGTAG + "queryData is build: " + eResource.getText());

				try {
					long deleteStartTime = System.currentTimeMillis();
					getResourceProvisioningProvider().delete(resource);
					long deleteTime = System.currentTimeMillis() - deleteStartTime;
					logger.info(LOGTAG + "Delete Resource in " + deleteTime + " ms.");
				} catch (ProviderException pe) {
					String errMsg = "An error occurred deleting Role in Delete-Request message. The exception is: "
							+ pe.getMessage();
					OpenEaiException e = new OpenEaiException(errMsg + msgObject);
					return getExceptionResponse(e, "IDMService-1000", eControlArea, msgRelease, msg, "warn");
				}

				try {
					MessageProducer producer = getProducerPool().getProducer();
					logger.info(LOGTAG + "Publishing Resource Delete-Sync");
					resource.deleteSync("purge", (SyncService) producer);
					logger.info(LOGTAG + "Published Resource.Delete-Sync" + " message.");
				} catch (EnterpriseObjectSyncException eose) {
					String errMsg = "An error occurred publishing the Resource.Delete-Sync message after deleting Resource.";
					logger.error(LOGTAG + errMsg);
					throw new CommandException(eose);
				} catch (JMSException jmse) {
					String errMsg = "An error occurred publishing the Resource.Delete-Sync message after deleting Resource.";
					logger.error(LOGTAG + errMsg);
					throw new CommandException(jmse);
				}
			}

			responseXmlObject.getRootElement().getChild(DATA_AREA).removeContent();
			String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
			// Log execution time.
			long executionTime = System.currentTimeMillis() - startTime;
			logger.info(LOGTAG + "RoleServiceRequestcommand execution complete in " + executionTime + " ms.");
			// Return the response with status success.
			return getMessage(msg, replyContents);
		} else {
			String errMsg = "The Role service doesn't support the input message object. Verify that the sending application is sending appropriate message objects. Input message object: ";
			OpenEaiException e = new OpenEaiException(errMsg + msgObject);
			return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
		}
	}

	/**
	 * Converts object to XmlEnterpriseObject to add to return replyDoc message in
	 * XML format.
	 * 
	 * @param eObject
	 * @return
	 * @throws OpenEaiException
	 */
	private <T> Element getElementFromEnterpriseObject(T eObject) throws OpenEaiException {
		// logger.info(
		// LOGTAG + "In getElementFromEnterpriseObject(). Attempting to convert
		// Enterpirse Object to Element.");
		if (!(eObject instanceof org.openeai.moa.XmlEnterpriseObject)) {
			String errMsg = "Error while attempting to convert a MOA Enterprise Object to JDOM Element. Input object not of type 'org.openeai.moa.XmlEnterpriseObject'.";
			throw new OpenEaiException(errMsg);
		}

		XmlEnterpriseObject enterpriseObj = (XmlEnterpriseObject) eObject;
		Element eOutput = null;
		if (enterpriseObj != null) {
			org.openeai.layouts.EnterpriseLayoutManager outElm = enterpriseObj
					.getOutputLayoutManager(MessageControl.XML_OUTPUT.toString());
			enterpriseObj.setOutputLayoutManager(outElm);
			eOutput = (Element) enterpriseObj.buildOutputFromObject();
		}
		logger.debug(LOGTAG + "In getElementFromEnterpriseObject(). Object conversion complete.");
		return eOutput;
	}

	/**
	 * Creates and returns an XML object that is ready to receive input element
	 * data.
	 * 
	 * @param msgAction  - Action that this object is a response to
	 * @param msgObject  - The type of action this object responds to
	 * @param msgRelease - Version information.
	 * @return Document - Ready JDOM XML object
	 * @throws CommandException
	 */
	protected Document getResponseXml(final String msgAction, final String msgObject, final String msgRelease)
			throws CommandException {
		try {
			XmlEnterpriseObject xmlObject = (XmlEnterpriseObject) getAppConfig()
					.getObject(msgObject + "." + generateRelease(msgRelease));
			Document responseObject = null;
			responseObject = xmlObject.getProvideDoc();
			if (responseObject == null) {
				String errMsg = "Could not find an appropriate reply document for the following request: ";
				throw new CommandException(errMsg + msgObject + "-" + msgAction);
			}
			return responseObject;
		}
		catch (Exception e) {
			throw new CommandException(e.getMessage(), e);
		}
	}

	/**
	 * This method wraps the response exception passed in to the Error elements as
	 * specified in the OpenEAI protocal.
	 * 
	 * @param e
	 * @param errorId
	 * @param eControlArea
	 * @param msgRelease
	 * @param msg
	 * @return
	 * @throws CommandException
	 */
	protected Message getExceptionResponse(Exception e, String errorId, Element eControlArea, String msgRelease,
			TextMessage msg, String logLevel) throws CommandException {
		Document xmlObject = getResponseXml(MessageControl.PROVIDE_MSG_ACTION.toString(),
				eControlArea.getAttributeValue(MESSAGE_OBJECT), msgRelease);

		List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<org.openeai.moa.objects.resources.Error>();
		errors.add(buildError(e.getClass().getName(), "EaiException" + errorId, e.getMessage()));

		String responseContents = buildReplyDocumentWithErrors(eControlArea, xmlObject, errors);
		if (logLevel.equals("error")) {
			logger.error(LOGTAG + "Exception Thrown: \nException Message: " + e.getMessage() + "\nStack Trace: "
					+ this.getExceptionStackTrace(e));
		}
		else {
			logger.warn(LOGTAG + "Exception Thrown: \nException Message: " + e.getMessage());
		}
		return getMessage(msg, responseContents);
	}

	private String getExceptionStackTrace(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}

	public static int getConnectionTimeout() {
		return connectionTimeout;
	}

	public static void setConnectionTimeout(int connectionTimeout) {
		RoleServiceRequestCommand.connectionTimeout = connectionTimeout;
	}

	public static int getSocketTimeout() {
		return socketTimeout;
	}

	public static void setSocketTimeout(int socketTimeout) {
		RoleServiceRequestCommand.socketTimeout = socketTimeout;
	}

	protected static void setServicePassword(String password) {
		servicePassword = password;
	}

	public static String getServicePassword() {
		return servicePassword;
	}

	protected static void setServiceUserId(String id) {
		serviceUserId = id;
	}

	public static String getServiceUserId() {
		return serviceUserId;
	}

	protected static void setWebServiceEndpoint(String endpoint) {
		searchServiceEndpoint = endpoint;
	}

	protected static String getWebServiceEndpoint() {
		return searchServiceEndpoint;
	}

	public ProducerPool getProducerPool() {
		return this.producerPool;
	}

	public void setProducerPool(ProducerPool producerPool) {
		this.producerPool = producerPool;
	}

	public Document getResponseDoc() {
		return responseDoc;
	}

	public void setResponseDoc(Document responseDoc) {
		this.responseDoc = responseDoc;
	}

	@SuppressWarnings("unchecked")
	private <T extends XmlEnterpriseObjectImpl> T getObject(Class<T> aClass)
			throws EnterpriseConfigurationObjectException {
		return (T) getAppConfig().getObjectByType(aClass.getName());
	}

	public RoleAssignmentProvider getRoleAssignmentProvider() {
		return roleAssignmentProvider;
	}

	public RoleProvisioningProvider getRoleProvisioningProvider() {
		return roleProvisioningProvider;
	}

	public void setRoleProvisioningProvider(RoleProvisioningProvider roleProvisioningProvider) {
		this.roleProvisioningProvider = roleProvisioningProvider;
	}

	public ResourceProvisioningProvider getResourceProvisioningProvider() {
		return resourceProvisioningProvider;
	}

	public void setResourceProvisioningProvider(ResourceProvisioningProvider resourceProvisioningProvider) {
		this.resourceProvisioningProvider = resourceProvisioningProvider;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
	
	private void setRoleAssignmentProvider(RoleAssignmentProvider roleAssignmentProvider) {
		this.roleAssignmentProvider = roleAssignmentProvider;
	}



}

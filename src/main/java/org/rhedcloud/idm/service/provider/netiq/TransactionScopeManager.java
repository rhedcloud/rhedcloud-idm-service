package org.rhedcloud.idm.service.provider.netiq;

import java.rmi.RemoteException;
import java.util.List;
import org.apache.axis2.client.NrfServiceExceptionException;
import org.apache.axis2.client.ResourceServiceStub;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import com.novell.www.resource.service.RemoveResourceResponseDocument;
import com.novell.www.role.service.DeleteResourceAssociationResponseDocument;
import com.novell.www.role.service.RemoveRolesResponseDocument;

import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.netiq.client.ResourceServiceClient;
import org.rhedcloud.idm.service.provider.netiq.client.RoleServiceClient;
import org.rhedcloud.idm.service.util.SavedChanges;

import edu.emory.idm.service.axis2.RoleServiceStub;

/**
 * This class ensures that if any of the create operation fails, the application
 * will rollback all the create calls. However we can roll back the create
 * operation, but unable to roll back some of the delete operations.
 *
 * @author RXING2
 *
 */
public class TransactionScopeManager extends OpenEaiObject {

	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(TransactionScopeManager.class);
	private String LOGTAG = "[TransactionScopeManager] ";

	public void rollback(List<SavedChanges<String, String>> changedList, RoleServiceStub roleService,
			ResourceServiceStub resourceService) throws ProviderException {

		for (SavedChanges<String, String> change : changedList) {
			if (change.key.equals("Role")) {
				RemoveRolesResponseDocument removeRolesResponseDocument = null;
				try {
					removeRolesResponseDocument = RoleServiceClient.doRemoveRole(roleService, change.value);
				}
				catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
					if (nsee != null && nsee.getFaultMessage() != null
							&& nsee.getFaultMessage().getNrfServiceException() != null
							&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
						String errMsg = "An error occurred deleting Role object from NetIQ. Error Message: "
								+ nsee.getFaultMessage().getNrfServiceException().getReason();
						logger.warn(LOGTAG + errMsg);
						throw new ProviderException(errMsg, nsee);
					}
				}
				catch (RemoteException re) {
					String errMsg = "Error calling web service to NetIQ from rollback(). Error Message: "
							+ re.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg, re);
				}

				if (removeRolesResponseDocument != null
						&& removeRolesResponseDocument.getRemoveRolesResponse() != null) {
					logger.info(LOGTAG + "Role is removed from rollback() for " + change.value);
				}
			}

			if (change.key.equals("Resource")) {

				RemoveResourceResponseDocument removeResourceResponseDocument = null;
				try {
					removeResourceResponseDocument = ResourceServiceClient.doRemoveResource(resourceService,
							change.value);
				}
				catch (RemoteException re) {
					String errMsg = "Error making web service calls to NetIQ from delete(). Error Message: "
							+ re.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg, re);
				}
				catch (NrfServiceExceptionException nsee) {
					if (nsee != null && nsee.getFaultMessage() != null
							&& nsee.getFaultMessage().getNrfServiceException() != null
							&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

						String errMsg = "An error occurred deleting Resource object from NetIQ. Error Message: "
								+ nsee.getFaultMessage().getNrfServiceException().getReason();
						logger.warn(LOGTAG + errMsg);
						throw new ProviderException(errMsg, nsee);
					}
				}

				if (removeResourceResponseDocument != null
						&& removeResourceResponseDocument.getRemoveResourceResponse() != null) {
					logger.info(LOGTAG + "Resource is removed from rollback() for " + change.value);
				}
			}

			if (change.key.equals("ResourceAssociation")) {

				DeleteResourceAssociationResponseDocument deleteResourceAssociationResponseDocument = DeleteResourceAssociationResponseDocument.Factory
						.newInstance();
				try {
					deleteResourceAssociationResponseDocument = RoleServiceClient
							.doDeleteResourceAssociation(roleService, change.value);
				}
				catch (RemoteException re) {
					String errMsg = "Error making web service calls doDeleteResourceAssociation() during rollback(). Error Message: "
							+ re.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg, re);
				}
				catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
					// if (nsee != null && nsee.getFaultMessage() != null
					// && nsee.getFaultMessage().getNrfServiceException() != null
					// && nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
//
					// String errMsg = "An error occurred deleting ResourceAssociation object during
					// rollback(). Error Message: "
					// + nsee.getFaultMessage().getNrfServiceException().getReason();
					// logger.warn(LOGTAG + errMsg);
					// throw new ProviderException(errMsg, nsee);
					// }
				}
				if (deleteResourceAssociationResponseDocument != null
						&& deleteResourceAssociationResponseDocument.getDeleteResourceAssociationResponse() != null) {
					logger.info(LOGTAG + "ResourceAssocation is removed from rollback() for " + change.value);
				}
			}
		}
	}
}
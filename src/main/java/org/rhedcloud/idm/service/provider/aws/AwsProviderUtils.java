package org.rhedcloud.idm.service.provider.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Datetime;
import org.rhedcloud.idm.service.provider.ProviderException;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.IdentityCenterUser;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.PermissionSet;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Policy;
import com.amazon.aws.moa.objects.resources.v1_0.EmailAddress;
import com.amazon.aws.moa.objects.resources.v1_0.IdentityCenterUserQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.NameInfo;
import com.amazon.aws.moa.objects.resources.v1_0.PhysicalAddress;
import com.amazon.aws.moa.objects.resources.v1_0.PolicyQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.Tag;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.model.GetPolicyRequest;
import com.amazonaws.services.identitymanagement.model.GetPolicyResult;
import com.amazonaws.services.identitymanagement.model.GetPolicyVersionRequest;
import com.amazonaws.services.identitymanagement.model.GetPolicyVersionResult;
import com.amazonaws.services.identitymanagement.model.ListPoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListPoliciesResult;
import com.amazonaws.services.identitymanagement.model.PolicyScopeType;
import com.amazonaws.services.identitystore.AWSIdentityStoreClient;
import com.amazonaws.services.identitystore.model.Address;
import com.amazonaws.services.identitystore.model.CreateUserRequest;
import com.amazonaws.services.identitystore.model.CreateUserResult;
import com.amazonaws.services.identitystore.model.Email;
import com.amazonaws.services.identitystore.model.Filter;
import com.amazonaws.services.identitystore.model.ListUsersRequest;
import com.amazonaws.services.identitystore.model.ListUsersResult;
import com.amazonaws.services.identitystore.model.Name;
import com.amazonaws.services.identitystore.model.PhoneNumber;
import com.amazonaws.services.identitystore.model.User;
import com.amazonaws.services.ssoadmin.AWSSSOAdminClient;
import com.amazonaws.services.ssoadmin.model.AccountAssignment;
import com.amazonaws.services.ssoadmin.model.AttachedManagedPolicy;
import com.amazonaws.services.ssoadmin.model.CustomerManagedPolicyReference;
import com.amazonaws.services.ssoadmin.model.DeleteAccountAssignmentRequest;
import com.amazonaws.services.ssoadmin.model.DeleteAccountAssignmentResult;
import com.amazonaws.services.ssoadmin.model.DescribePermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.DescribePermissionSetResult;
import com.amazonaws.services.ssoadmin.model.GetInlinePolicyForPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.GetInlinePolicyForPermissionSetResult;
import com.amazonaws.services.ssoadmin.model.ListAccountAssignmentsRequest;
import com.amazonaws.services.ssoadmin.model.ListAccountAssignmentsResult;
import com.amazonaws.services.ssoadmin.model.ListAccountsForProvisionedPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.ListAccountsForProvisionedPermissionSetResult;
import com.amazonaws.services.ssoadmin.model.ListCustomerManagedPolicyReferencesInPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.ListCustomerManagedPolicyReferencesInPermissionSetResult;
import com.amazonaws.services.ssoadmin.model.ListManagedPoliciesInPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.ListManagedPoliciesInPermissionSetResult;
import com.amazonaws.services.ssoadmin.model.ListPermissionSetsRequest;
import com.amazonaws.services.ssoadmin.model.ListPermissionSetsResult;
import com.amazonaws.services.ssoadmin.model.ListTagsForResourceRequest;
import com.amazonaws.services.ssoadmin.model.ListTagsForResourceResult;

public class AwsProviderUtils {
	static Logger logger = LogManager.getLogger(AwsProviderUtils.class); 
	static final String LOGTAG = "[AwsProviderUtils] ";
	public static Integer maxResults = 100; //this is the maximum as documented in https://docs.aws.amazon.com/singlesignon/latest/APIReference/API_ListPermissionSets.html


	static protected boolean useCache;
	static protected HashMap<String,String> cachePsByName = new HashMap<>();
	static protected HashMap<String,PermissionSet> cachePsByArn = new HashMap<>();

	class CacheFreshener extends Thread {

		private AWSSSOAdminClient clientSSO;
		private AppConfig appConfig;
		private String instanceArn;
		private long cacheRefreshIntervalInMillis = 60 * 24 * 1000; // every 24 hours
		private final String LOGTAG = "[CacheFreshener] ";

		@Override
		public void run() {
			super.run();
			if (AwsProviderUtils.useCache) {
				while (true) {
					AwsProviderUtils.useCache = false; //TODO: Does this need to be atomic?
					cachePsByName.clear();
					cachePsByArn.clear();
					logger.info(LOGTAG+"Filling up the query cache of permission set descriptions...");
					long start = System.currentTimeMillis();
					List<ActionableEnterpriseObject> permissionSets = null;
					try {
						permissionSets = AwsProviderUtils.getPermissionSet(
								null, // name 
								clientSSO, 
								appConfig, 
								instanceArn, 
								true, // tags
								true, // policies
								true  // provisioned account IDs
								);
					} catch (ProviderException e) {
						logger.error(LOGTAG+"Can't get permission sets: Exception: "+e);
						e.printStackTrace();
					}
					for(ActionableEnterpriseObject aeo: permissionSets) {
						PermissionSet psi = (PermissionSet) aeo;
						AwsProviderUtils.cachePsByName.put(psi.getName(),psi.getArn());
						AwsProviderUtils.cachePsByArn.put(psi.getArn(), psi);
					}
					AwsProviderUtils.useCache = true;
					logger.info(LOGTAG+AwsProviderUtils.cachePsByName.size()+" entries added to the permission set cache in "+(System.currentTimeMillis()-start)+" ms");
					try {
						logger.info(LOGTAG+"CacheFreshener is sleeping for "+cacheRefreshIntervalInMillis+" ms");
						Thread.sleep(cacheRefreshIntervalInMillis);
					} catch (InterruptedException e) {
						logger.info(LOGTAG+"CacheFreshener is interrupted. Returning.");
						return;
					}

				}
			}

		}

		public CacheFreshener(AWSSSOAdminClient clientSSO, AppConfig appConfig, String instanceArn,
				long cacheRefreshIntervalInMillis) {
			super();
			this.clientSSO = clientSSO;
			this.appConfig = appConfig;
			this.instanceArn = instanceArn;
			this.cacheRefreshIntervalInMillis = cacheRefreshIntervalInMillis;
		}



	}

	static protected void deleteAllAccountAssignmentsForPermissionSet(PermissionSet ps, AWSSSOAdminClient clientSSO, String instanceArn, Integer maxThreads) throws ProviderException {


		logger.info(LOGTAG+"Deleting account assignments.");

		List<AccountAssignmentDeleteWaiter> aaDeleteThreads = new ArrayList<>();

		ListAccountsForProvisionedPermissionSetRequest lafppsQ = new ListAccountsForProvisionedPermissionSetRequest()
				.withInstanceArn(instanceArn)
				.withPermissionSetArn(ps.getArn())
				.withMaxResults(100);
		ListAccountsForProvisionedPermissionSetResult lafppsS;
		do {
			lafppsS = clientSSO.listAccountsForProvisionedPermissionSet(lafppsQ);
			lafppsQ.setNextToken(lafppsS.getNextToken());

			logger.info(LOGTAG+"There are "+lafppsS.getAccountIds().size()+" accounts provisioned to "+ps.getName());

			for (String accountId: lafppsS.getAccountIds()) {

				logger.info(LOGTAG+"Listing account assignments for accountId = "+accountId);

				//aws sso-admin list-account-assignments --account-id 057372381290 --instance-arn "arn:aws:sso:::instance/ssoins-72238aa0982f098e" --permission-set-arn arn:aws:sso:::permissionSet/ssoins-72238aa0982f098e/ps-27a030a837227e97
				ListAccountAssignmentsRequest laaQ = new ListAccountAssignmentsRequest()
						.withInstanceArn(instanceArn)
						.withPermissionSetArn(ps.getArn())
						.withAccountId(accountId)
						.withMaxResults(maxThreads);
				ListAccountAssignmentsResult laaS;
				do {
					laaS = clientSSO.listAccountAssignments(laaQ);;
					laaQ.setNextToken(laaS.getNextToken());
					logger.info(LOGTAG+"There are "+laaS.getAccountAssignments().size()+" accounts assignments to delete");

					for ( AccountAssignment aa: laaS.getAccountAssignments()) {

						DeleteAccountAssignmentRequest daaQ = new DeleteAccountAssignmentRequest()
								.withInstanceArn(instanceArn)
								.withPermissionSetArn(aa.getPermissionSetArn())
								.withPrincipalId(aa.getPrincipalId())
								.withPrincipalType(aa.getPrincipalType())
								.withTargetId(accountId)
								.withTargetType("AWS_ACCOUNT");

						logger.info(LOGTAG+"Deleting acccount assignment with request: "+daaQ);

						DeleteAccountAssignmentResult deleteAccountAssignmentResult = clientSSO.deleteAccountAssignment(daaQ);
						deleteAccountAssignmentResult.getAccountAssignmentDeletionStatus().getRequestId();
						String accountAssignmentDeletionRequestId = deleteAccountAssignmentResult.getAccountAssignmentDeletionStatus().getRequestId();

						aaDeleteThreads.add(new AccountAssignmentDeleteWaiter(accountAssignmentDeletionRequestId, clientSSO, 5000, instanceArn, logger));
					}
				} while (laaS.getNextToken() != null);
			}
		} while(lafppsS.getNextToken() != null);

		logger.info(LOGTAG+"Deleting "+aaDeleteThreads.size()+" accounts assignments");
		List<Thread> threadList = new ArrayList<>();
		for ( AccountAssignmentDeleteWaiter aadestroy: aaDeleteThreads) {
			Thread t = new Thread(aadestroy);
			threadList.add(t);
			t.start();
		}
		logger.info(LOGTAG+"Waiting for "+threadList.size()+" accounts assignment deletions to finish...");
		for (Thread t: threadList) {
			try {
				t.join();
			} catch (InterruptedException e) {
				logger.error(LOGTAG+"");
			}
		}



	}

	protected static List<ActionableEnterpriseObject> getPermissionSet(
			String permissionSetName,
			AWSSSOAdminClient clientSSO, 
			AppConfig appConfig,
			String instanceArn,
			boolean withTags,
			boolean withPolicies, 
			Boolean withProvisionedAccountIds) throws ProviderException {

		if (useCache) {
			ArrayList<ActionableEnterpriseObject> r = new ArrayList<ActionableEnterpriseObject>();
			HashMap<String,PermissionSet> psByArn = null;

			if (permissionSetName != null) {
				psByArn = new HashMap<>();
				String candidateArn = cachePsByName.get(permissionSetName);
				if (candidateArn != null) {
					PermissionSet candidate = cachePsByArn.get(candidateArn);
					logger.info(LOGTAG+"Permission set cache hit on "+permissionSetName+" ("+candidateArn+")");
					psByArn.put(candidateArn, candidate);
				} else {
					logger.warn(LOGTAG+"Permission set description cache miss on "+permissionSetName);
					return r;
				}
			} else {
				psByArn = cachePsByArn;
			}
			for (PermissionSet ps:psByArn.values()) {
				PermissionSet result;
				try {
					result = (PermissionSet) ps.clone();
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
					throw new ProviderException("Can't clone ps from cache",e);
				}
				r.add(result);
			}
			return r;
		}

		// cacheless query

		try {

			List<ActionableEnterpriseObject> results = new ArrayList<>();
			ListPermissionSetsResult listPermissionSetsResult;
			ListPermissionSetsRequest listPermissionSetsRequest  = new ListPermissionSetsRequest()
					.withInstanceArn(instanceArn)
					.withMaxResults(maxResults);		
			do {

				listPermissionSetsResult = clientSSO.listPermissionSets(listPermissionSetsRequest);
				logger.info(LOGTAG+"listPermissionSets returned "+listPermissionSetsResult.getPermissionSets().size()+" permission sets");
				listPermissionSetsRequest.setNextToken(listPermissionSetsResult.getNextToken());


				for (String candidateArn: listPermissionSetsResult.getPermissionSets()) {
					logger.debug(LOGTAG+"permission set arn is "+candidateArn);
					PermissionSet permissionSet = null;	


					permissionSet = (PermissionSet) appConfig.getObject("PermissionSet.v1_0");
					DescribePermissionSetRequest reqx = new DescribePermissionSetRequest()
							.withInstanceArn(instanceArn)
							.withPermissionSetArn(candidateArn);

					DescribePermissionSetResult repx = clientSSO.describePermissionSet(reqx);
					com.amazonaws.services.ssoadmin.model.PermissionSet candidate = repx.getPermissionSet();
					permissionSet.setName(candidate.getName());
					permissionSet.setDescription(candidate.getDescription());
					permissionSet.setArn(candidateArn);
					permissionSet.setSessionDuration(candidate.getSessionDuration());
					permissionSet.setCreateDatetime(new Datetime(candidate.getCreatedDate()));

					if (permissionSetName != null) {
						if (!candidate.getName().equals(permissionSetName)) {
							logger.debug(LOGTAG+"Skipping permissionSet named '"+candidate.getName()+"' because it doesn't match the name passed in the query spec.");
							continue;
						}
					}

					logger.info(LOGTAG+"adding permission set named "+candidate.getName()+" to results");


					if (permissionSetName != null) {
						if (!permissionSet.getName().equals(permissionSetName)) {
							logger.debug(LOGTAG+"Skipping permissionSet named '"+permissionSet.getName()+"' because it doesn't match the name passed in the query spec.");
							continue;
						} 
						try {
							permissionSet = (PermissionSet) permissionSet.clone();
						} catch (CloneNotSupportedException e) {
							e.printStackTrace();
							throw new ProviderException("Can't clone ps",e);
						}

					}


					if (withTags || useCache) {
						addTags(permissionSet, instanceArn, clientSSO);
					}

					if (withProvisionedAccountIds || useCache) {
						addProvisionedAccountIds(permissionSet, instanceArn, clientSSO);
					}

					if (withPolicies || useCache) {									
						addPolicies(permissionSet, instanceArn, clientSSO);
					}

					results.add(permissionSet);

					if (useCache) {
						logger.info(LOGTAG + "Adding "+candidate.getName()+" to cache");
						cachePsByName.put(permissionSet.getName(), permissionSet.getArn());
						cachePsByArn.put(permissionSet.getArn(), permissionSet);
					}
					if (permissionSetName != null) {
						break;
					}

				}

				if (permissionSetName != null && results.size() == 1) {
					logger.info(LOGTAG+"Found desired permission set");
					break;
				}


			} while ( listPermissionSetsResult.getNextToken() != null );

			return results;

		} catch (RuntimeException e) {
			e.printStackTrace();
			String errMsg = "An error occurred while trying to query for permission sets. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg,e);       	      			
		}  catch (EnterpriseFieldException e) {
			String errmsg="EnterpriseFieldException thrown";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg="Can't get PermissionSet from the config. ";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		}

	}

	private static void addTags(PermissionSet permissionSet, String instanceArn, AWSSSOAdminClient clientSSO) throws ProviderException {
		ListTagsForResourceRequest listTagsForResourceRequest = new ListTagsForResourceRequest()
				.withInstanceArn(instanceArn)
				.withResourceArn(permissionSet.getArn());
		ListTagsForResourceResult listTagsForResourceResult;
		List<com.amazonaws.services.ssoadmin.model.Tag> amzTags = new ArrayList<>();
		do {

			listTagsForResourceResult = clientSSO.listTagsForResource(listTagsForResourceRequest);
			listTagsForResourceRequest.setNextToken(listTagsForResourceResult.getNextToken());
			if (listTagsForResourceResult.getTags() != null ) {
				amzTags.addAll(listTagsForResourceResult.getTags());
			}

		} while (listTagsForResourceRequest.getNextToken() != null);
		if (amzTags.size() > 0) {
			permissionSet.setTag(awsSsoTags2moaTags(amzTags,permissionSet.newTag()));
		}

	}

	private static void addProvisionedAccountIds(PermissionSet permissionSet, String instanceArn, AWSSSOAdminClient clientSSO) {
		ListAccountsForProvisionedPermissionSetRequest lafppsQ = new ListAccountsForProvisionedPermissionSetRequest()
				.withInstanceArn(instanceArn)
				.withPermissionSetArn(permissionSet.getArn())
				.withMaxResults(100);
		ListAccountsForProvisionedPermissionSetResult lafppsS;
		do {
			lafppsS = clientSSO.listAccountsForProvisionedPermissionSet(lafppsQ);
			lafppsQ.setNextToken(lafppsS.getNextToken());
			for(String accountId:lafppsS.getAccountIds()) {
				permissionSet.addProvisionedAccountId(accountId);
			}
		} while (lafppsS.getNextToken() != null);	
	}

	private static void addPolicies(PermissionSet permissionSet, String instanceArn, AWSSSOAdminClient clientSSO) throws ProviderException {

		ListCustomerManagedPolicyReferencesInPermissionSetRequest req = 
				new ListCustomerManagedPolicyReferencesInPermissionSetRequest()
				.withInstanceArn(instanceArn)
				.withPermissionSetArn(permissionSet.getArn());
		ListCustomerManagedPolicyReferencesInPermissionSetResult res;
		do {
			try {
				res = clientSSO.listCustomerManagedPolicyReferencesInPermissionSet(req);
				req.setNextToken(res.getNextToken());
				for (CustomerManagedPolicyReference cmpr: res.getCustomerManagedPolicyReferences()) {
					permissionSet.addCustomerManagedPolicyReference(cmpr.getPath()+cmpr.getName());
				}	
			} catch (RuntimeException e) {
				String errMsg = "An error occurred trying to list the customer managed policies attached to the permission set. " +
						e.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);  
			}

		} while(res.getNextToken() !=  null);

		ListManagedPoliciesInPermissionSetRequest lmpq = new ListManagedPoliciesInPermissionSetRequest()
				.withInstanceArn(instanceArn)
				.withPermissionSetArn(permissionSet.getArn());

		ListManagedPoliciesInPermissionSetResult lmps;
		do {
			try {
				lmps = clientSSO.listManagedPoliciesInPermissionSet(lmpq);
				lmpq.setNextToken(lmps.getNextToken());
				for (AttachedManagedPolicy mp: lmps.getAttachedManagedPolicies()) {
					permissionSet.addManagedPolicyArn(mp.getArn());
				}	
			} catch (RuntimeException e) {
				String errMsg = "An error occurred trying to list the managed policies attached to the permission set. " +
						e.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);  
			}
		} while (lmps.getNextToken() != null);

		GetInlinePolicyForPermissionSetRequest inlineq = new GetInlinePolicyForPermissionSetRequest()
				.withInstanceArn(instanceArn)
				.withPermissionSetArn(permissionSet.getArn());
		try {
			GetInlinePolicyForPermissionSetResult inlines = clientSSO.getInlinePolicyForPermissionSet(inlineq);
			permissionSet.setInlinePolicyDocument(inlines.getInlinePolicy());
		} catch (RuntimeException | EnterpriseFieldException e) {
			String errMsg = "An error occurred trying to list the inline policy attached to the permission set. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);  
		}
	}

	@SuppressWarnings("unchecked")
	protected static IdentityCenterUser createIdentityCenterUser(
			IdentityCenterUser icu, 
			AppConfig appConfig, 
			Properties awsUtilsProps, 
			String identityStoreId, 
			AWSIdentityStoreClient clientIS) throws ProviderException {

		try {

			String userName = AwsProviderUtils.composeUserName(icu.getUserName(),awsUtilsProps);
			Name name = new Name()
					.withFamilyName(icu.getNameInfo().getFamilyName())
					.withFormatted(icu.getNameInfo().getFormattedName())
					.withGivenName(icu.getNameInfo().getGivenName())
					.withHonorificPrefix(icu.getNameInfo().getHonorificPrefix())
					.withHonorificSuffix(icu.getNameInfo().getHonorificSuffix())
					.withMiddleName(icu.getNameInfo().getMiddleName());

			List<Email> awsemails = icu.getEmailAddressLength() == 0 ? null : new ArrayList<Email>();
			for (EmailAddress moaemail: (List<EmailAddress>)icu.getEmailAddress()) {
				awsemails.add(new Email()
						.withPrimary(Boolean.parseBoolean(moaemail.getPrimary()))
						.withType(moaemail.getType())
						.withValue(moaemail.getValue()));
			}

			List<Address> awsaddresses = icu.getPhysicalAddressLength() == 0 ? null : new ArrayList<Address>();
			for (PhysicalAddress moaaddress: (List<PhysicalAddress>)icu.getPhysicalAddress()) {
				awsaddresses.add(new Address()
						.withPrimary(Boolean.parseBoolean(moaaddress.getPrimary()))
						.withType(moaaddress.getType())
						.withCountry(moaaddress.getCountry())
						.withFormatted(moaaddress.getFormattedAddress())
						.withLocality(moaaddress.getLocality())
						.withPostalCode(moaaddress.getPostalCode())
						.withPrimary(Boolean.parseBoolean(moaaddress.getPrimary()))
						.withRegion(moaaddress.getRegion())
						.withStreetAddress(moaaddress.getStreetAddress())
						.withType(moaaddress.getType()));
			}

			List<PhoneNumber> awsphones = icu.getPhoneNumberLength() == 0 ? null : new ArrayList<PhoneNumber>();
			for (com.amazon.aws.moa.objects.resources.v1_0.PhoneNumber phoneNumber: 
				(List<com.amazon.aws.moa.objects.resources.v1_0.PhoneNumber>)icu.getPhoneNumber()) {
				awsphones.add(new PhoneNumber()
						.withPrimary(Boolean.parseBoolean(phoneNumber.getPrimary()))
						.withType(phoneNumber.getType())
						.withValue(phoneNumber.getValue()));
			}

			CreateUserRequest createreq = new CreateUserRequest()
					.withIdentityStoreId(identityStoreId)
					.withUserName(userName)
					.withName(name)
					.withDisplayName(icu.getDisplayName())
					.withTitle(icu.getTitle())
					.withNickName(icu.getNickName())
					.withProfileUrl(icu.getProfileUrl())
					.withEmails(awsemails)
					.withAddresses(awsaddresses)
					.withPhoneNumbers(awsphones)
					.withLocale(icu.getLocale())
					.withUserType(icu.getType())
					.withPreferredLanguage(icu.getPreferredLanguage())
					.withTimezone(icu.getTimezone())
					.withUserType(icu.getType());


			logger.debug(LOGTAG+"createreq = "+createreq.toString());
			CreateUserResult createres = clientIS.createUser(createreq);
			logger.debug(LOGTAG+"Reply from create user: "+createres);
			icu.setUserId(createres.getUserId());

			return icu;

		} catch (RuntimeException e) {
			e.printStackTrace();
			String errMsg = "An error occurred trying to create the IdentityCenterUser. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg,e);       	      			
		} catch (EnterpriseFieldException e) {
			String errmsg="EnterpriseFieldException thrown";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		}

	}

	protected static List<ActionableEnterpriseObject> getIdentityCenterUser (
			IdentityCenterUserQuerySpecification identityCenterUserQuerySpecification,
			AWSIdentityStoreClient clientIS, 
			AppConfig appConfig,
			String identityStoreId,
			Properties props) throws ProviderException {

		IdentityCenterUserQuerySpecification querySpec = (IdentityCenterUserQuerySpecification) identityCenterUserQuerySpecification;

		try {

			List<Filter> filters = new ArrayList<>();
			String composed = null;

			if (querySpec.getUserName() != null ) {
				composed = composeUserName(querySpec.getUserName(), props);
				logger.info(LOGTAG+"Filtering for Username = "+composed);
			}

			List<ActionableEnterpriseObject> results = new ArrayList<>();
			ListUsersResult listUsersResult;
			@SuppressWarnings("deprecation")
			ListUsersRequest listUsersRequest  = new ListUsersRequest()
			.withIdentityStoreId(identityStoreId)
			.withFilters(filters.size()==0?null:filters)
			.withMaxResults(maxResults);		
			do {

				listUsersResult = clientIS.listUsers(listUsersRequest);
				logger.info(LOGTAG+"listUsers returned "+listUsersResult.getUsers().size()+" users");
				listUsersRequest.setNextToken(listUsersResult.getNextToken());


				for (User candidate: listUsersResult.getUsers()) {
					logger.debug(LOGTAG+"username is "+candidate.getUserName());
					logger.debug(LOGTAG+"userid is "+candidate.getUserId());

					if (querySpec.getUserName() != null) {
						if (!candidate.getUserName().equals(composed)) {
							logger.debug(LOGTAG+"Skipping userName '"+candidate.getUserName()+"' because it doesn't match the userName passed in the query spec.");
							continue;
						}
					}

					if (querySpec.getUserId() != null) {
						if (!candidate.getUserId().equals(querySpec.getUserId())) {
							logger.debug(LOGTAG+"Skipping user with ID '"+candidate.getUserId()+"' because it doesn't match the ID passed in the query spec.");
							continue;
						}
					}

					logger.info(LOGTAG+"adding user named "+composed+" to results");

					IdentityCenterUser icu = (IdentityCenterUser) appConfig.getObject("IdentityCenterUser.v1_0");							
					icu.setUserName(candidate.getUserName());
					icu.setUserId(candidate.getUserId());
					NameInfo nameInfo = icu.newNameInfo();
					nameInfo.setFamilyName(candidate.getName().getFamilyName());
					nameInfo.setFormattedName(candidate.getName().getFormatted());
					nameInfo.setGivenName(candidate.getName().getGivenName());
					nameInfo.setMiddleName(candidate.getName().getMiddleName());
					nameInfo.setHonorificPrefix(candidate.getName().getHonorificPrefix());
					nameInfo.setHonorificSuffix(candidate.getName().getHonorificSuffix());
					icu.setNameInfo(nameInfo);
					icu.setDisplayName(candidate.getDisplayName());
					icu.setTitle(candidate.getTitle());
					icu.setNickName(candidate.getNickName());
					icu.setProfileUrl(candidate.getProfileUrl());
					if (candidate.getEmails() != null) {
						for(Email email: candidate.getEmails()) {
							EmailAddress emailAddress = icu.newEmailAddress();
							emailAddress.setType(email.getType());
							emailAddress.setValue(email.getValue());
							emailAddress.setPrimary(email.getPrimary()+"");
							icu.addEmailAddress(emailAddress);
						}
					}
					if (candidate.getAddresses() != null) {
						for(Address address: candidate.getAddresses()) {
							PhysicalAddress physicalAddress = icu.newPhysicalAddress();
							physicalAddress.setStreetAddress(address.getStreetAddress());
							physicalAddress.setLocality(address.getLocality());
							physicalAddress.setRegion(address.getRegion());
							physicalAddress.setPostalCode(address.getPostalCode());
							physicalAddress.setCountry(address.getCountry());
							physicalAddress.setFormattedAddress(address.getFormatted());
							physicalAddress.setPrimary(address.getPrimary()+"");
							physicalAddress.setType(address.getType());
							icu.addPhysicalAddress(physicalAddress);
						}
					}
					if (candidate.getPhoneNumbers() != null) {
						for(PhoneNumber phone: candidate.getPhoneNumbers()) {
							com.amazon.aws.moa.objects.resources.v1_0.PhoneNumber phoneNumber = icu.newPhoneNumber();
							phoneNumber.setValue(phone.getValue());
							phoneNumber.setType(phone.getType());
							phoneNumber.setPrimary(phone.getPrimary()+"");
							icu.addPhoneNumber(phoneNumber);
						}
					}
					results.add(icu);

					if ( querySpec.getUserName() != null || querySpec.getUserId() != null) {
						break;
					}

				}
				if ((querySpec.getUserName() != null || querySpec.getUserId() != null) && results.size() == 1) {
					logger.info(LOGTAG+"Found desired user");
					break;
				}

			} while ( listUsersResult.getNextToken() != null );

			return results;

		} catch (RuntimeException e) {
			e.printStackTrace();
			String errMsg = "An error occurred while trying to query for identity center users. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg,e);       	      			
		}  catch (EnterpriseFieldException e) {
			String errmsg="EnterpriseFieldException thrown";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg="Can't get PermissionSet from the config. ";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		}

	}

	protected static List<ActionableEnterpriseObject> getPolicy(XmlEnterpriseObject querySpec1, AmazonIdentityManagement iam, AppConfig appConfig) throws ProviderException {
		PolicyQuerySpecification querySpec = (PolicyQuerySpecification) querySpec1;
		logger.info(LOGTAG+"queryspec = "+querySpec);
		logger.info(LOGTAG+"queryspec.getName() = "+querySpec.getName());

		try {


			Boolean withPolicyDocument = Boolean.parseBoolean(querySpec.getWithPolicyDocument());
			Boolean withTags = Boolean.parseBoolean(querySpec.getWithTags());
			PolicyScopeType scope = querySpec.getWithScope() == null ? PolicyScopeType.All
					: querySpec.getWithScope().equalsIgnoreCase("ALL") ? PolicyScopeType.All
							: querySpec.getWithScope().equalsIgnoreCase("LOCAL") ? PolicyScopeType.Local 
									: querySpec.getWithScope().equalsIgnoreCase("AWS") ? PolicyScopeType.AWS 
											: null;
			if (scope == null) {
				String errmsg="invalid scope specified. valid scopes are 'ALL' 'Local' and 'AWS'";
				logger.error(LOGTAG+errmsg);
				throw new ProviderException(errmsg);			        		
			}

			List<ActionableEnterpriseObject> results = new ArrayList<>();
			ListPoliciesResult listPoliciesResult;
			ListPoliciesRequest listPoliciesRequest  = new ListPoliciesRequest()
					.withMaxItems(maxResults)
					.withScope(scope);		
			do {

				listPoliciesResult = iam.listPolicies(listPoliciesRequest);
				logger.info(LOGTAG+"listPolicies returned "+listPoliciesResult.getPolicies().size()+" customer managed policies");
				logger.info(LOGTAG+"listPoliciesResult.getMarker()="+listPoliciesResult.getMarker());
				listPoliciesRequest.setMarker(listPoliciesResult.getMarker());


				for (com.amazonaws.services.identitymanagement.model.Policy candidate: listPoliciesResult.getPolicies()) {

					String candidateName = candidate.getPolicyName();
					if (querySpec.getName() != null 
							&& !candidateName.equals(querySpec.getName())) {
						continue;
					}
					String candidateArn = candidate.getArn();

					logger.info(LOGTAG+"policy tags are "+candidate.getTags());

					Policy policy = (Policy) appConfig.getObject("Policy.v1_0");
					policy.setAccountId(querySpec.getAccountId());
					policy.setName(candidateName);
					policy.setArn(candidateArn);
					policy.setDescription(candidate.getDescription());
					policy.setPath(candidate.getPath());
					policy.setAttachmentCount(candidate.getAttachmentCount()+"");
					policy.setDefaultVersionId(candidate.getDefaultVersionId());
					policy.setIsAttachable(candidate.getIsAttachable()+"");
					policy.setPermissionsBoundaryUsageCount(candidate.getPermissionsBoundaryUsageCount()+"");
					policy.setTag(awsIamTags2moaTags(candidate.getTags(), policy.newTag()));
					policy.setLastUpdateDatetime(new Datetime(candidate.getUpdateDate()));
					policy.setCreateDatetime(new Datetime(candidate.getCreateDate()));

					if (withPolicyDocument) {
						GetPolicyVersionRequest getPolicyVersionRequest = new GetPolicyVersionRequest()
								.withPolicyArn(candidateArn)
								.withVersionId(candidate.getDefaultVersionId());
						GetPolicyVersionResult getPolicyVersionResult = iam.getPolicyVersion(getPolicyVersionRequest);
						policy.setPolicyDocument(getPolicyVersionResult.getPolicyVersion().getDocument());
					}

					if (withTags) {
						GetPolicyRequest getPolicyRequest = new GetPolicyRequest()
								.withPolicyArn(candidateArn);
						GetPolicyResult getPolicyResult = iam.getPolicy(getPolicyRequest);
						policy.setTag(awsIamTags2moaTags(getPolicyResult.getPolicy().getTags(), policy.newTag()));
					}

					results.add(policy);

					if (querySpec.getName() != null) {
						break;
					}

				}
				if (querySpec.getName() != null && results.size() == 1) {
					logger.info(LOGTAG+"Found desired policy");
					break;
				}

			} while ( listPoliciesResult.getMarker() != null );

			return results;

		} catch (RuntimeException | EnterpriseFieldException | EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			String errMsg = "An error occurred while trying to query for policies. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg,e);       	      			
		}	

	}

	protected static List<com.amazon.aws.moa.objects.resources.v1_0.Tag> awsIamTags2moaTags(
			List<com.amazonaws.services.identitymanagement.model.Tag> list, 
			com.amazon.aws.moa.objects.resources.v1_0.Tag blankTag) throws ProviderException {

		List<com.amazon.aws.moa.objects.resources.v1_0.Tag> moatags = new ArrayList<>();
		for (com.amazonaws.services.identitymanagement.model.Tag awstag:list) {
			try {
				com.amazon.aws.moa.objects.resources.v1_0.Tag moatag = (Tag) blankTag.clone();
				moatag.setKey(awstag.getKey());
				moatag.setValue(awstag.getValue());
				moatags.add(moatag);
			} catch (EnterpriseFieldException | CloneNotSupportedException e) {
				throw new ProviderException("Exception thrown",e);
			}
		}
		return moatags;
	}

	protected static List<com.amazon.aws.moa.objects.resources.v1_0.Tag> awsSsoTags2moaTags(
			List<com.amazonaws.services.ssoadmin.model.Tag> list, 
			com.amazon.aws.moa.objects.resources.v1_0.Tag blankTag) throws ProviderException {

		List<com.amazon.aws.moa.objects.resources.v1_0.Tag> moatags = new ArrayList<>();
		for (com.amazonaws.services.ssoadmin.model.Tag awstag:list) {
			try {
				com.amazon.aws.moa.objects.resources.v1_0.Tag moatag = (Tag) blankTag.clone();
				moatag.setKey(awstag.getKey());
				moatag.setValue(awstag.getValue());
				moatags.add(moatag);
			} catch (EnterpriseFieldException | CloneNotSupportedException e) {
				throw new ProviderException("Exception thrown",e);
			}
		}
		return moatags;
	}

	protected static String composeUserName(String userId, Properties props) {
		String userNamePrefix = props.getProperty("userNamePrefix");
		String userNameSuffix = props.getProperty("userNameSuffix");
		String username = userNamePrefix != null ? userNamePrefix + userId : userId;
		username = userNameSuffix != null ? username + userNameSuffix : username;
		return username;
	}

	protected static String decomposeUserName(String username, Properties props) {
		String userNamePrefix = props.getProperty("userNamePrefix");
		String userNameSuffix = props.getProperty("userNameSuffix");
		String userId = userNamePrefix == null ? username : username.replace(userNamePrefix, "");
		userId = userNameSuffix == null ? userId : userId.replace(userNameSuffix, "");
		return userId;
	}

}

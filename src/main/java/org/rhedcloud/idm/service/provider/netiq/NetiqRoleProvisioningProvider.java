package org.rhedcloud.idm.service.provider.netiq;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.NrfServiceExceptionException;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ResourceServiceStub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.moa.XmlEnterpriseObjectImpl;
import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.ProviderProcessingResponse;
import org.rhedcloud.idm.service.provider.RoleAssignmentProvider;
import org.rhedcloud.idm.service.provider.RoleProvisioningProvider;
import org.rhedcloud.idm.service.provider.netiq.client.ResourceServiceClient;
import org.rhedcloud.idm.service.provider.netiq.client.RoleServiceClient;
import org.rhedcloud.idm.service.provider.netiq.util.IdmValues;
import org.rhedcloud.idm.service.util.ESBJsonObject;
import org.rhedcloud.idm.service.util.JsonUtils;
import org.rhedcloud.idm.service.util.MessageControl;
import org.rhedcloud.idm.service.util.PropertyNames;
import org.rhedcloud.idm.service.util.SavedChanges;
import org.rhedcloud.idm.service.util.StringUtil;

import com.novell.www.resource.service.CreateResourceResponseDocument;
import com.novell.www.resource.service.GetResourceResponse;
import com.novell.www.resource.service.GetResourceResponseDocument;
import com.novell.www.resource.service.NrfEntitlementRef;
import com.novell.www.role.service.CategoryKey;
import com.novell.www.role.service.CreateResourceAssociationResponseDocument;
import com.novell.www.role.service.CreateRoleResponseDocument;
import com.novell.www.role.service.GetAssignedIdentitiesResponse;
import com.novell.www.role.service.GetAssignedIdentitiesResponseDocument;
import com.novell.www.role.service.GetResourceAssociationsResponseDocument;
import com.novell.www.role.service.GetRoleResponse;
import com.novell.www.role.service.GetRoleResponseDocument;
import com.novell.www.role.service.RemoveRolesResponseDocument;
import com.novell.www.role.service.ResourceAssociation;
import com.novell.www.role.service.RoleAssignmentType;

import edu.emory.idm.service.axis2.RoleServiceStub;
import edu.emory.moa.jmsobjects.identity.v1_0.Entitlement;
import edu.emory.moa.jmsobjects.identity.v1_0.Resource;
import edu.emory.moa.jmsobjects.identity.v1_0.Role;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.RoleRequisition;

/**
 * Provider implementation for RoleProvisioningProvider
 * 
 * @author RXING2
 *
 */
public class NetiqRoleProvisioningProvider extends OpenEaiObject implements RoleProvisioningProvider {
	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(NetiqRoleAssignmentProvider.class);
	private AppConfig appConfig;
	private String serviceUserId = "";
	private String servicePassword = "";
	private String codeMapKey = "";
	private String idvCodeMapKey = "";
	private String categoryKey = "";
	private String eadEntitlementDn = "";
	private String hmdEntitlementDn = "";
	private String idvEntitlementDn = "";
	private String umdEntitlementDn = "";
	private static long maxWaitTime = 30000;
	private String roleServiceEndpoint = "";
	private String resourceServiceEndpoint = "";
	private int connectionTimeout = 12000;
	private int socketTimeout = 12000;
	private String LOGTAG = "[IdmRoleServiceProvider] ";
	// TJ 6/25/2021
	private int maximumRetries=4;				// default to 4
	private int retrySleepTimeInMillis=500;		// default to 500

	/**
	 * @throws EnterpriseConfigurationObjectException
	 * @see RoleAssignmentProvider.java
	 */
	public void init(AppConfig aConfig) throws ProviderException {

		logger.info(LOGTAG + "Initializing...");
		setAppConfig(aConfig);
		// Get the provider properties
		PropertyConfig pConfig = new PropertyConfig();
		try {
			pConfig = (PropertyConfig) aConfig.getObject("RoleProvisioningProviderProperties");
			Properties props = pConfig.getProperties();
			setProperties(props);
		}
		catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from AppConfig: The exception is:";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, eoce);
		}

		if (getProperties() != null && !getProperties().isEmpty()) {

			// TJ 6/25/2021
			maximumRetries = Integer.parseInt(getProperties().getProperty("maximumRetries", "4"));
			retrySleepTimeInMillis = Integer.parseInt(getProperties().getProperty("retrySleepTimeInMillis", "500"));
			
			if (getProperties().getProperty(PropertyNames.ROLE_WEB_SERVICE_ENDPOINT.toString()) != null) {
				setRoleServiceEndpoint(getProperties().getProperty(PropertyNames.ROLE_WEB_SERVICE_ENDPOINT.toString()));
				logger.info(LOGTAG + PropertyNames.ROLE_WEB_SERVICE_ENDPOINT.toString() + " = " + roleServiceEndpoint);
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.ROLE_WEB_SERVICE_ENDPOINT.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.RESOURCE_WEB_SERVICE_ENDPOINT.toString()) != null) {
				setResourceServiceEndpoint(
						getProperties().getProperty(PropertyNames.RESOURCE_WEB_SERVICE_ENDPOINT.toString()));
				logger.info(LOGTAG + PropertyNames.RESOURCE_WEB_SERVICE_ENDPOINT.toString() + " = "
						+ getResourceServiceEndpoint());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.RESOURCE_WEB_SERVICE_ENDPOINT.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the user id to log into IDM with from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()) != null) {
				setServiceUserId(getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()));
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_USER_ID.toString() + " = " + getServiceUserId());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_USER_ID.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the password to log into IDM with from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()) != null) {
				setServicePassword(getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()));
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_PASSWORD.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.EAD_ENTITLEMENT_DN.toString()) != null) {
				setEadEntitlementDn(getProperties().getProperty(PropertyNames.EAD_ENTITLEMENT_DN.toString()));
				logger.info(LOGTAG + PropertyNames.EAD_ENTITLEMENT_DN.toString() + " = " + getEadEntitlementDn());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.EAD_ENTITLEMENT_DN.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.HMD_ENTITLEMENT_DN.toString()) != null) {
				setHmdEntitlementDn(getProperties().getProperty(PropertyNames.HMD_ENTITLEMENT_DN.toString()));
				logger.info(LOGTAG + PropertyNames.HMD_ENTITLEMENT_DN.toString() + " = " + getHmdEntitlementDn());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.HMD_ENTITLEMENT_DN.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.IDV_ENTITLEMENT_DN.toString()) != null) {
				setIdvEntitlementDn(getProperties().getProperty(PropertyNames.IDV_ENTITLEMENT_DN.toString()));
				logger.info(LOGTAG + PropertyNames.IDV_ENTITLEMENT_DN.toString() + " = " + getIdvEntitlementDn());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.IDV_ENTITLEMENT_DN.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.UMD_ENTITLEMENT_DN.toString()) != null) {
				setUmdEntitlementDn(getProperties().getProperty(PropertyNames.UMD_ENTITLEMENT_DN.toString()));
				logger.info(LOGTAG + PropertyNames.UMD_ENTITLEMENT_DN.toString() + " = " + getUmdEntitlementDn());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.UMD_ENTITLEMENT_DN.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.CODE_MAP_KEY.toString()) != null) {
				setCodeMapKey(getProperties().getProperty(PropertyNames.CODE_MAP_KEY.toString()));
				logger.info(LOGTAG + PropertyNames.CODE_MAP_KEY.toString() + " = " + getCodeMapKey());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.CODE_MAP_KEY.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.IDV_CODE_MAP_KEY.toString()) != null) {
				setIdvCodeMapKey(getProperties().getProperty(PropertyNames.IDV_CODE_MAP_KEY.toString()));
				logger.info(LOGTAG + PropertyNames.IDV_CODE_MAP_KEY.toString() + " = " + getIdvCodeMapKey());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.IDV_CODE_MAP_KEY.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.CATEGORY_KEY.toString()) != null) {
				setCategoryKey(getProperties().getProperty(PropertyNames.CATEGORY_KEY.toString()));
				logger.info(LOGTAG + PropertyNames.CATEGORY_KEY.toString() + " = " + getCategoryKey());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.CATEGORY_KEY.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.WAIT_INTERVAL.toString()) != null) {
				setWaitInterval(Long.valueOf(getProperties().getProperty(PropertyNames.WAIT_INTERVAL.toString())));
				logger.info(LOGTAG + PropertyNames.WAIT_INTERVAL.toString() + " = " + waitInterval);
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.WAIT_INTERVAL.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.MAX_WAIT_TIME.toString()) != null) {
				setMaxWaitTime(Long.valueOf(getProperties().getProperty(PropertyNames.MAX_WAIT_TIME.toString())));
				logger.info(LOGTAG + PropertyNames.MAX_WAIT_TIME.toString() + " = " + maxWaitTime);
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.MAX_WAIT_TIME.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString()) != null) {
				setConnectionTimeout(
						Integer.parseInt(getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString())));
				logger.info(LOGTAG + PropertyNames.CONNECTION_TIMEOUT.toString() + " = " + getConnectionTimeout());
			}
			else {
				logger.info(
						LOGTAG + "OPTIONAL Property " + PropertyNames.CONNECTION_TIMEOUT.toString() + " not defined");
			}

			if (getProperties().getProperty(PropertyNames.SOCKET_TIMEOUT.toString()) != null) {
				setSocketTimeout(
						Integer.parseInt(getProperties().getProperty(PropertyNames.SOCKET_TIMEOUT.toString())));
				logger.info(LOGTAG + PropertyNames.SOCKET_TIMEOUT.toString() + " = " + socketTimeout);
			}
			else {
				logger.info(LOGTAG + "OPTIONAL Property " + PropertyNames.SOCKET_TIMEOUT.toString() + " not defined");
			}
		}
		else {
			String errMsg = "REQUIRED Properties have not been defined. Please define required properties within AppConfig.";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg);
		}
	}

	@Override
	public Role query(String roleDn, List<SavedChanges<String, String>> changedList, String fromAction)
			throws ProviderException {

		logger.info(LOGTAG + "In query(). Ready to execute request.");
		TransactionScopeManager transactionManager = new TransactionScopeManager();
		GetRoleResponseDocument getRoleRsponseDocument = null;

		RoleServiceStub roleService = null;
		ResourceServiceStub resourceService = null;
		try {
			roleService = getRoleServiceStub(getServiceUserId(), getServicePassword(), getRoleServiceEndpoint(),
					getConnectionTimeout(), getSocketTimeout());
			if (roleService != null) {
				resourceService = getResourceServiceStub(getServiceUserId(), getServicePassword(),
						getResourceServiceEndpoint(), getConnectionTimeout(), getSocketTimeout());
			}
		}
		catch (RemoteException re) {
			String errMsg = "The Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		try {
			getRoleRsponseDocument = RoleServiceClient.doGetRoleSearchByDn(roleService, roleDn);
		}
		catch (RemoteException re) {
			if (changedList != null && changedList.size() > 0) {
				transactionManager.rollback(changedList, roleService, resourceService);
			}
			String errMsg = "An error occurred calling IDM NetIQ from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
				if (changedList != null && changedList.size() > 0) {
					transactionManager.rollback(changedList, roleService, resourceService);
				}
				String errMsg = "A server error occurred retrieving Role object from IDM NetIQ during query(). Error message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}

		Role responseMoa = null;
		if (getRoleRsponseDocument != null && getRoleRsponseDocument.getGetRoleResponse() != null
				&& getRoleRsponseDocument.getGetRoleResponse().getResult() != null) {

			GetRoleResponse roleResultData = getRoleRsponseDocument.getGetRoleResponse();
			GetResourceAssociationsResponseDocument getResourceAssociationsResponseDocument = null;
			List<String> resourceDnList = null;
			try {
				responseMoa = this.getObject(Role.class);
			}
			catch (EnterpriseConfigurationObjectException ecoe) {
				if (changedList != null && changedList.size() > 0) {
					transactionManager.rollback(changedList, roleService, resourceService);
				}
				String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: "
						+ ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, ecoe);
			}
			if (roleResultData != null && roleResultData.getResult() != null) {
				try {
					if (roleResultData.getResult().getDescription() != null) {
						responseMoa.setRoleDescription(roleResultData.getResult().getDescription());
					}
					if (roleResultData.getResult().getName() != null) {
						responseMoa.setRoleName(roleResultData.getResult().getName());
					}

					for (CategoryKey categoryKey : roleResultData.getResult().getRoleCategoryKeys()
							.getCategorykeyArray()) {
						responseMoa.setRoleCategoryKey(categoryKey.getCategoryKey());
						break;
					}
					responseMoa.setRoleDN(roleDn);
					getResourceAssociationsResponseDocument = RoleServiceClient.doGetResourceAssociationsSearch(
							roleService, roleResultData.getResult().getEntityKey(), null);
				}
				catch (EnterpriseFieldException efe) {
					if (changedList != null && changedList.size() > 0) {
						transactionManager.rollback(changedList, roleService, resourceService);
					}
					String errMsg = "An error occurred setting the field values of the Role. The exception is: "
							+ efe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg, efe);
				}
				catch (RemoteException re) {
					if (changedList != null && changedList.size() > 0) {
						transactionManager.rollback(changedList, roleService, resourceService);
					}
					String errMsg = "Error making web service calls to NetIQ in query(). Error Message: "
							+ re.getMessage();
					logger.error(errMsg);
					throw new ProviderException(errMsg, re);
				}
				catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
					if (nsee != null && nsee.getFaultMessage() != null
							&& nsee.getFaultMessage().getNrfServiceException() != null
							&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
						String errMsg = "Server error in query() retrieving RoleAssociation object from NetIQ, please check log for more detail. Error Message: "
								+ nsee.getFaultMessage().getNrfServiceException().getReason();
						logger.error(errMsg);
						if (changedList != null && changedList.size() > 0) {
							transactionManager.rollback(changedList, roleService, resourceService);
						}
						throw new ProviderException(errMsg, nsee);
					}
				}

				resourceDnList = new ArrayList<String>();
				if (getResourceAssociationsResponseDocument != null && getResourceAssociationsResponseDocument
						.getGetResourceAssociationsResponse().getResult().sizeOfResourceassociationArray() > 0) {
					for (ResourceAssociation resourceAssociation : getResourceAssociationsResponseDocument
							.getGetResourceAssociationsResponse().getResult().getResourceassociationArray()) {
						if (resourceAssociation != null) {
							resourceDnList.add(resourceAssociation.getResource());
							logger.info(LOGTAG + "Resource added to the DN list: " + resourceAssociation.getResource());
						}
					}
					// getResourceAssociationsResponseDocument.getGetResourceAssociationsResponse().getResult().getResourceassociationArray();
					logger.info(LOGTAG + "resoucedn1 retrieved: "
							+ getResourceAssociationsResponseDocument.getGetResourceAssociationsResponse().getResult()
									.getResourceassociationArray(0).getResource());
				}
			}

			if (resourceDnList != null && !resourceDnList.isEmpty()) {
				for (String resourceDn : resourceDnList) {

					Resource resource = new Resource();
					List<Entitlement> list = new ArrayList<Entitlement>();
					GetResourceResponseDocument getResourceResponseDocument = null;
					try {
						getResourceResponseDocument = ResourceServiceClient.doResourceSearchByDN(resourceService,
								resourceDn);
					}
					catch (RemoteException re) {
						if (changedList != null && changedList.size() > 0) {
							transactionManager.rollback(changedList, roleService, resourceService);
						}
						String errMsg = "An error occurred calling IDM NetIQ from query(), please check the log for more detail "
								+ re.getMessage();
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, re);
					}
					catch (org.apache.axis2.client.NrfServiceExceptionException nsee) {
						if (nsee != null && nsee.getFaultMessage() != null
								&& nsee.getFaultMessage().getNrfServiceException() != null
								&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
							String errMsg = "A server error occurred retrieving Resource object from IDM NetIQ during query(). Error message: "
									+ nsee.getFaultMessage().getNrfServiceException().getReason();
							logger.error(errMsg);
							if (changedList != null && changedList.size() > 0) {
								transactionManager.rollback(changedList, roleService, resourceService);
							}
							throw new ProviderException(errMsg, nsee);
						}
					}

					GetResourceResponse getResourceResponse = getResourceResponseDocument.getGetResourceResponse();
					if (getResourceResponse != null && getResourceResponse.getResult() != null
							&& getResourceResponse.getResult().getEntityKey() != null) {
						String resourceName = getResourceResponse.getResult().getName();
						try {
							resource.setResourceName(resourceName);
							resource.setResourceDN(resourceDn);
							resource.setResourceDescription(getResourceResponse.getResult().getDescription());
							for (com.novell.www.resource.service.CategoryKey categoryKey : getResourceResponse
									.getResult().getResourceCategoryKeys().getCategorykeyArray()) {
								resource.setResourceCategoryKey(categoryKey.getCategoryKey());
								break;
							}
						}
						catch (EnterpriseFieldException efe) {
							if (changedList != null && changedList.size() > 0) {
								transactionManager.rollback(changedList, roleService, resourceService);
							}
							String errMsg = "An error occurred setting the field values of the Role. The exception is: "
									+ efe.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new ProviderException(errMsg, efe);
						}
						for (NrfEntitlementRef ref : getResourceResponse.getResult().getEntitlementRef()
								.getNrfentitlementrefArray()) {
							Entitlement aEntitlement = new Entitlement();
							try {

								logger.info("entitlementDn: " + ref.getEntitlementDn());
								if (ref.getEntitlementParameters() != null) {

									logger.info("entitlementGuid: "
											+ StringUtil.getUUIDFromString(ref.getEntitlementParameters()));
									String dnString = JsonUtils.convertJSONObjectToString(
											ref.getEntitlementParameters(), IdmValues.ID2.getName());
									if (dnString != null) {
										aEntitlement.setEntitlementDN(dnString);
									}

									if (aEntitlement.getEntitlementDN() != null
											&& aEntitlement.getEntitlementDN().length() > 0) {
										aEntitlement.setEntitlementGuid(JsonUtils.convertJSONObjectToString(
												ref.getEntitlementParameters(), IdmValues.ID.getName()));
									}
									else {
										aEntitlement.setEntitlementDN(JsonUtils.convertJSONObjectToString(
												ref.getEntitlementParameters(), IdmValues.ID.getName()));
									}
								}
							}
							catch (EnterpriseFieldException efe) {
								if (changedList != null && changedList.size() > 0) {
									transactionManager.rollback(changedList, roleService, resourceService);
								}
								String errMsg = "An error occurred setting the field values of the Role. The exception is: "
										+ efe.getMessage();
								logger.error(LOGTAG + errMsg);
								throw new ProviderException(errMsg, efe);
							}
							list.add(aEntitlement);
						}
						resource.setEntitlement(list.get(0));
					}
					responseMoa.addResource(resource);
				}
			}
		}
		else {
			String errDesc = null;
			if (fromAction != null && fromAction.equalsIgnoreCase(MessageControl.DELETE_MSG_ACTION.getName())) {
				errDesc = "The Role is no longer existed after query(). It's successfully removed from NetIQ. RoleDn: "
						+ roleDn;
			}
			else {
				errDesc = "The Role does not exist for the requested RoleDn: " + roleDn;
			}
			logger.info(LOGTAG + errDesc);
		}

		try {
			roleService._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		try {
			resourceService._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return responseMoa;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ProviderProcessingResponse generate(RoleRequisition roleRequisition) throws ProviderException {

		try {
			logger.info(LOGTAG + "Ready to execute Generate-Request. "+roleRequisition.toXmlString());
		} catch (XmlEnterpriseObjectException e1) {
			e1.printStackTrace();
		}
		CreateRoleResponseDocument createRoleResponseDocument = null;
		ProviderProcessingResponse providerProcessingResponse = new ProviderProcessingResponse();
		String aRoleDn = null;
		String aResourceDn = null;
		TransactionScopeManager transactionManager = new TransactionScopeManager();
		List<SavedChanges<String, String>> changedList = new ArrayList<SavedChanges<String, String>>();

		RoleServiceStub roleService = null;
		ResourceServiceStub resourceService = null;
		try {
			roleService = getRoleServiceStub(getServiceUserId(), getServicePassword(), getRoleServiceEndpoint(),
					getConnectionTimeout(), getSocketTimeout());
			if (roleService != null) {
				resourceService = getResourceServiceStub(getServiceUserId(), getServicePassword(),
						getResourceServiceEndpoint(), getConnectionTimeout(), getSocketTimeout());
			}
		}
		catch (RemoteException re) {
			String errMsg = "The Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		if (roleRequisition.getRoleCategoryKey() != null && roleRequisition.getRoleCategoryKey().length() > 0) {
			if (getCategoryKey().indexOf(roleRequisition.getRoleCategoryKey().toLowerCase()) == -1) {
				String errMsg = "RoleCategory field passed in from the request message is not valid. The valid fields are: "
						+ getCategoryKey();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}
		}
		else {
			try {
				roleRequisition.setRoleCategoryKey(IdmValues.DEFAULT.getName());
			}
			catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred setting RoleCategory field in generate(). The exception is: "
						+ efe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, efe);
			}
		}

		try {

			createRoleResponseDocument = RoleServiceClient.doRoleCreate(roleService, roleRequisition);
			if ((createRoleResponseDocument != null && createRoleResponseDocument.getCreateRoleResponse() != null
					&& createRoleResponseDocument.getCreateRoleResponse().getResult() != null
					&& createRoleResponseDocument.getCreateRoleResponse().getResult().getDn() != null
					&& !createRoleResponseDocument.getCreateRoleResponse().getResult().getDn().isEmpty())) {

				aRoleDn = createRoleResponseDocument.getCreateRoleResponse().getResult().getDn();
			}
		}
		catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

				String errMsg = "Server error from NetIQ, please check log for more detail. Error Message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.warn(errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}
		catch (RemoteException re) {
			String errMsg = "Error making web service calls to NetIQ. Error Message: " + re.getMessage();
			logger.error(errMsg);
			throw new ProviderException(errMsg, re);
		}

		GetRoleResponseDocument getRoleResponseDocument = null;
		Role responseMoa = new Role();
		if (aRoleDn != null && aRoleDn.length() > 0) {

			try {
				getRoleResponseDocument = RoleServiceClient.doGetRoleSearchByDn(roleService, aRoleDn);
			}
			catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
					String errMsg = "A server error occurred retrieving Role object from IDM NetIQ during query(). Error message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.warn(errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}
			catch (RemoteException re) {
				String errMsg = "Error making web service calls to NetIQ. Error Message: " + re.getMessage();
				logger.error(errMsg);
				if (changedList.size() > 0) {
					transactionManager.rollback(changedList, roleService, resourceService);
				}
				throw new ProviderException(errMsg, re);
			}
		}

		if ((getRoleResponseDocument != null && getRoleResponseDocument.getGetRoleResponse() != null
				&& getRoleResponseDocument.getGetRoleResponse().getResult() != null
				&& getRoleResponseDocument.getGetRoleResponse().getResult().getEntityKey() != null
				&& !getRoleResponseDocument.getGetRoleResponse().getResult().getEntityKey().isEmpty())) {

			// now create resource using Resource web services
			try {
				changedList.add(new SavedChanges<String, String>(IdmValues.ROLE.getName(),
						getRoleResponseDocument.getGetRoleResponse().getResult().getEntityKey()));
				responseMoa.setRoleDN(getRoleResponseDocument.getGetRoleResponse().getResult().getEntityKey());
			}
			catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: "
						+ efe.getMessage();
				logger.error(LOGTAG + errMsg);
				if (changedList.size() > 0) {
					transactionManager.rollback(changedList, roleService, resourceService);
				}
				throw new ProviderException(errMsg, efe);
			}

			List<String> resourceDnList = new ArrayList<String>();
			Iterator<Resource> iterator = roleRequisition.getResource().iterator();
			while (iterator.hasNext()) {
				Resource resource = iterator.next();
				if (resource.getResourceName() != null) {
					logger.info(LOGTAG + "Resources to be processed: " + resource.getResourceName());
					CreateResourceResponseDocument createResourceResponseDoc = CreateResourceResponseDocument.Factory
							.newInstance();
					CreateResourceAssociationResponseDocument createResourceAssociationResponseDocument = CreateResourceAssociationResponseDocument.Factory
							.newInstance();
					boolean isResourceAssociationCreated = true;
					try {
						if (resource.getEntitlement().getEntitlementApplication()
								.equalsIgnoreCase(IdmValues.EAD.getName())) {

							String paramGuidValue = ResourceServiceClient.doGetCodeMapValue(resourceService,
									resource.getEntitlement().getEntitlementDN(), getCodeMapKey());
							if (paramGuidValue != null && paramGuidValue.length() > 0) {
								resource.getEntitlement().setEntitlementGuid(paramGuidValue);
								resource.getEntitlement().setEntitlementDN(getEadEntitlementDn());
							}
							else {
								String errMsg = "The entitlement value for EAD codeMapKey " + getCodeMapKey()
										+ " was not found in the NetIQ CodeMap. Please contact IDM for additional assistance.";
								logger.error(errMsg);
								if (changedList.size() > 0) {
									transactionManager.rollback(changedList, roleService, resourceService);
								}
								throw new ProviderException(errMsg);
							}
						}
						else if (resource.getEntitlement().getEntitlementApplication()
								.equalsIgnoreCase(IdmValues.IDV.getName())) {

							String paramGuidValue = ResourceServiceClient.doGetCodeMapValue(resourceService,
									resource.getEntitlement().getEntitlementDN(), getIdvCodeMapKey());
							if (paramGuidValue != null && paramGuidValue.length() > 0) {
								resource.getEntitlement().setEntitlementGuid(paramGuidValue);
								resource.getEntitlement().setEntitlementDN(getIdvEntitlementDn());
							}
							else {
								String errMsg = "The entitlement value for IDV codeMapKey " + getCodeMapKey()
										+ " was not found in the NetIQ CodeMap. Please contact IDM for additional assistance.";
								logger.error(errMsg);
								if (changedList.size() > 0) {
									transactionManager.rollback(changedList, roleService, resourceService);
								}
								throw new ProviderException(errMsg);
							}
						}
						else if (resource.getEntitlement().getEntitlementApplication()
								.equalsIgnoreCase(IdmValues.UMD.getName())) {

							if (resource.getEntitlement().getEntitlementGuid().toLowerCase().chars()
									.filter(e -> e == '-').count() != 4
									|| resource.getEntitlement().getEntitlementGuid().length() != 38) {

								String errMsg = "The entitlement GUID values passed in is invalid. Please verify in the request message the proper UMD entitlement GUID values are provided. ";
								logger.error(errMsg);
								if (changedList.size() > 0) {
									transactionManager.rollback(changedList, roleService, resourceService);
								}
								throw new ProviderException(errMsg);
							}
							else {

								String convertedGuid = StringUtil
										.convertGuidFromString(resource.getEntitlement().getEntitlementGuid());
								/// JSONObject object = JsonUtils.convertStringToJSONObject(convertedGuid,
								// resource.getEntitlement().getEntitlementDN());
								ESBJsonObject object = JsonUtils.convertStringToJSONObject(convertedGuid,
										resource.getEntitlement().getEntitlementDN());
								resource.getEntitlement().setEntitlementGuid(object.toJSONString());
								resource.getEntitlement().setEntitlementDN(getUmdEntitlementDn());
							}
						}
						else if (resource.getEntitlement().getEntitlementApplication()
								.equalsIgnoreCase(IdmValues.HMD.getName())) {

							if (resource.getEntitlement().getEntitlementGuid().toLowerCase().chars()
									.filter(e -> e == '-').count() != 4
									|| resource.getEntitlement().getEntitlementGuid().length() != 38) {

								String errMsg = "The entitlement GUID values passed in is invalid. Please verify in the request message the proper HMD entitlement GUID values are provided. ";
								logger.error(errMsg);
								if (changedList.size() > 0) {
									transactionManager.rollback(changedList, roleService, resourceService);
								}
								throw new ProviderException(errMsg);
							}
							else {

								String convertedGuid = StringUtil
										.convertGuidFromString(resource.getEntitlement().getEntitlementGuid());
								ESBJsonObject object = JsonUtils.convertStringToJSONObject(convertedGuid,
										resource.getEntitlement().getEntitlementDN());
								resource.getEntitlement().setEntitlementGuid(object.toJSONString());
								resource.getEntitlement().setEntitlementDN(getHmdEntitlementDn());
							}
						}
						else {

							String errMsg = "The entitlement application values can only be EAD, IDV, HMD or UMD. Please verify in the request message the proper values are provided. ";
							logger.error(errMsg);
							if (changedList.size() > 0) {
								transactionManager.rollback(changedList, roleService, resourceService);
							}
							throw new ProviderException(errMsg);
						}

						createResourceResponseDoc = ResourceServiceClient.doResourceCreate(resourceService, resource);
						if ((createResourceResponseDoc != null
								&& createResourceResponseDoc.getCreateResourceResponse() != null
								&& createResourceResponseDoc.getCreateResourceResponse().getResult() != null
								&& createResourceResponseDoc.getCreateResourceResponse().getResult().length() > 0)) {

							aResourceDn = createResourceResponseDoc.getCreateResourceResponse().getResult().toString();
							changedList
									.add(new SavedChanges<String, String>(IdmValues.RESOURCE.getName(), aResourceDn));

							try {
								createResourceAssociationResponseDocument = RoleServiceClient
										.doResourceAssociationCreate(roleService, aRoleDn, aResourceDn);
							}
							catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
								String errMsg = "Server error from NetIQ during generate(), please check log for more detail. Error Message: "
										+ nsee.getFaultMessage().getNrfServiceException().getReason();

								if (!StringUtil.isResourceAlreadyExist(
										nsee.getFaultMessage().getNrfServiceException().getReason())) {
									logger.error(LOGTAG + errMsg);
									if (changedList.size() > 0) {
										transactionManager.rollback(changedList, roleService, resourceService);
									}
									throw new ProviderException(errMsg, nsee);
								}
								logger.warn(errMsg);
							}
							catch (RemoteException re) {
								String errMsg = "Error making web service calls to NetIQ. Error Message: "
										+ re.getMessage();
								logger.error(errMsg);
								if (changedList.size() > 0) {
									transactionManager.rollback(changedList, roleService, resourceService);
								}
								throw new ProviderException(errMsg, re);
							}

							if ((createResourceAssociationResponseDocument != null
									&& createResourceAssociationResponseDocument
											.getCreateResourceAssociationResponse() != null
									&& createResourceAssociationResponseDocument.getCreateResourceAssociationResponse()
											.getResult() != null)) {
								String resourceAssociationDn = createResourceAssociationResponseDocument
										.getCreateResourceAssociationResponse().getResult().getEntityKey();
								resourceDnList.add(aResourceDn);
								changedList.add(new SavedChanges<String, String>(
										IdmValues.RESOURCE_ASSOCIATION.getName(), resourceAssociationDn));
								String desc = "ResourceAssociation object created for ResourceDN:  " + aResourceDn;
								logger.info(LOGTAG + desc);
							}
						}
					}
					catch (NrfServiceExceptionException nsee) {

						if (nsee != null && nsee.getFaultMessage() != null
								&& nsee.getFaultMessage().getNrfServiceException() != null
								&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

							String errMsg = "Error returned from NetIQ in generate(), please check log for more detail. Error Message: "
									+ nsee.getFaultMessage().getNrfServiceException().getReason();
							if (!StringUtil.isResourceAlreadyExist(
									nsee.getFaultMessage().getNrfServiceException().getReason())) {
								if (changedList.size() > 0) {
									transactionManager.rollback(changedList, roleService, resourceService);
								}
								throw new ProviderException(errMsg, nsee);
							}
							logger.warn(errMsg);
							isResourceAssociationCreated = false;
							aResourceDn = StringUtil.getRoleResourceDn(
									nsee.getFaultMessage().getNrfServiceException().getReason(),
									IdmValues.RESOURCE.getName());
						}
					}
					catch (RemoteException re) {
						String errMsg = "Error making web service calls to NetIQ in Role.generate() Error Message: "
								+ re.getMessage();
						logger.error(errMsg);
						if (changedList.size() > 0) {
							transactionManager.rollback(changedList, roleService, resourceService);
						}
						throw new ProviderException(errMsg, re);
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred setting the field values of the Role in generate(). The exception is: "
								+ efe.getMessage();
						logger.error(LOGTAG + errMsg);
						if (changedList.size() > 0) {
							transactionManager.rollback(changedList, roleService, resourceService);
						}
						throw new ProviderException(errMsg, efe);
					}
					catch (ProviderException pe) {
						String errMsg = "Error in processing Entitlement in IdmRoleProvisioningProvider(). Error Message: "
								+ pe.getMessage();
						logger.error(errMsg);
						if (changedList.size() > 0) {
							transactionManager.rollback(changedList, roleService, resourceService);
						}
						throw new ProviderException(errMsg);
					}

					if (isResourceAssociationCreated == false) {
						try {
							createResourceAssociationResponseDocument = RoleServiceClient
									.doResourceAssociationCreate(roleService, aRoleDn, aResourceDn);
							changedList.add(new SavedChanges<String, String>(IdmValues.RESOURCE_ASSOCIATION.getName(),
									aResourceDn));
							resource.setResourceDN(aResourceDn);
							responseMoa.addResource(resource);
						}
						catch (RemoteException re) {
							String errMsg = "Error making web service calls to NetIQ. Error Message: "
									+ re.getMessage();
							logger.error(errMsg);
							if (changedList.size() > 0) {
								transactionManager.rollback(changedList, roleService, resourceService);
							}
							throw new ProviderException(errMsg, re);
						}
						catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
							if (nsee != null && nsee.getFaultMessage() != null
									&& nsee.getFaultMessage().getNrfServiceException() != null
									&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
								String errMsg = "A server error occurred creating ResourceAssoication object from IDM NetIQ during generate(). Error message: "
										+ nsee.getFaultMessage().getNrfServiceException().getReason();
								logger.warn(errMsg);
								if (changedList.size() > 0) {
									transactionManager.rollback(changedList, roleService, resourceService);
								}
								throw new ProviderException(errMsg, nsee);
							}
						}
						catch (EnterpriseFieldException e) {
							String errMsg = "An error occurred retrieving an object from "
									+ "AppConfig. The exception is: " + e.getMessage();
							logger.error(LOGTAG + errMsg);
							if (changedList.size() > 0) {
								transactionManager.rollback(changedList, roleService, resourceService);
							}
							throw new ProviderException(errMsg, e);
						}
					}
				}
			}

			if (aRoleDn != null && aRoleDn.length() > 0) {
				responseMoa = (Role) waitForCompletion(responseMoa, MessageControl.GENERATE_MSG_ACTION.getName(),
						changedList, roleService, resourceService);
				providerProcessingResponse.setSavedChanges(changedList);
				providerProcessingResponse.setResponse(responseMoa);
				providerProcessingResponse.setRoleService(roleService);
				providerProcessingResponse.setResourceService(resourceService);
				logger.info("Role.Generate-Request complete.");
			}
		}

		try {
			roleService._getServiceClient().cleanupTransport();
			resourceService._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return providerProcessingResponse;
	}

	@Override
	public Role delete(Role role) throws ProviderException {

		logger.info(LOGTAG + "In delete(). Ready to execute delete request.");
		GetAssignedIdentitiesResponseDocument getAssignedIdentitiesResponseDocument = null;
		RemoveRolesResponseDocument removeRolesResponseDocument = null;
		Role responseMoa = new Role();
		String aRoleDn = null;
		RoleServiceStub roleService = null;
		ResourceServiceStub resourceService = null;

		try {
			roleService = getRoleServiceStub(getServiceUserId(), getServicePassword(), getRoleServiceEndpoint(),
					getConnectionTimeout(), getSocketTimeout());
			if (roleService != null) {
				resourceService = getResourceServiceStub(getServiceUserId(), getServicePassword(),
						getResourceServiceEndpoint(), getConnectionTimeout(), getSocketTimeout());
			}
		}
		catch (RemoteException re) {
			String errMsg = "The Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}
        
		RoleAssignmentQuerySpecification querySpec = new RoleAssignmentQuerySpecification();
		try {
			//RoleAssignmentQuerySpecification querySpec = new RoleAssignmentQuerySpecification();
			querySpec.setRoleDN(role.getRoleDN());
			querySpec.setIdentityType("USER");
			getAssignedIdentitiesResponseDocument = RoleServiceClient.doRoleAssignedIdentitiesSearch(roleService,
					querySpec);
		}
		catch (RemoteException re) {
			String errMsg = "Error making web service calls doGetUserSearch() to NetIQ during delete(). Error Message: "
					+ re.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, re);
		}
		catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
				String errMsg = "An error occurred querying Role object from NetIQ. Error Message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}
		catch (EnterpriseFieldException efe) {
			String errMsg = "An error occurred setting the field values of the Role in Delete(). The exception is: "
					+ efe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, efe);
		}

		GetAssignedIdentitiesResponse resultData = getAssignedIdentitiesResponseDocument
				.getGetAssignedIdentitiesResponse();
		if (resultData != null && resultData.getResult() != null) {
			for (com.novell.www.role.service.RoleAssignment roleAssignment : resultData.getResult()
					.getRoleassignmentArray()) {
				if (roleAssignment != null && roleAssignment.getAssignmentType() == RoleAssignmentType.USER_TO_ROLE) {
					if (roleAssignment.getExplicitIdentities() != null
							&& roleAssignment.getExplicitIdentities().getDnstringArray() != null
							&& roleAssignment.getExplicitIdentities().getDnstringArray().length > 0) {
						// There are identities attached to the role and we
						// can't remove the role
						String errMsg = "There are user identities attached to the Role and the Role cannot be removed. "
								+ "Please revoke all the identities under the Role by sending a RoleAssignment Delete-Request before deleting the Role";
						logger.warn(LOGTAG + errMsg);
						throw new ProviderException(errMsg);
					}
				}
			}
		}
		
		try {
			querySpec.setIdentityType("GROUP");
			getAssignedIdentitiesResponseDocument = RoleServiceClient.doRoleAssignedIdentitiesSearch(roleService,
					querySpec);
		} catch (RemoteException re) {
			String errMsg = "Error making web service calls doGetGroupSearch() to NetIQ during delete(). Error Message: "
					+ re.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, re);
		} catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
				String errMsg = "An error occurred querying group identities from NetIQ during Role deletion. Error Message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		} catch (EnterpriseFieldException efe) {
			String errMsg = "An error occurred setting the field values of the Role in Delete(). The exception is: "
					+ efe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, efe);
		}

		resultData = getAssignedIdentitiesResponseDocument.getGetAssignedIdentitiesResponse();
		if (resultData != null && resultData.getResult() != null) {
			for (com.novell.www.role.service.RoleAssignment roleAssignment : resultData.getResult()
					.getRoleassignmentArray()) {
				if (roleAssignment != null && roleAssignment.getAssignmentType() == RoleAssignmentType.GROUP_TO_ROLE) {
					if (roleAssignment.getExplicitIdentities() != null
							&& roleAssignment.getExplicitIdentities().getDnstringArray() != null
							&& roleAssignment.getExplicitIdentities().getDnstringArray().length > 0) {
						// There are identities attached to the role and we
						// can't remove the role
						String errMsg = "There are group identities attached to the Role and the Role cannot be removed. "
								+ "Please revoke all the identities under the Role by sending a RoleAssignment Delete-Request before deleting the Role";
						logger.warn(LOGTAG + errMsg);
						throw new ProviderException(errMsg);
					}
				}
			}
		}


		try {
			removeRolesResponseDocument = RoleServiceClient.doRemoveRole(roleService, role);
		}
		catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

				String errMsg = "An error occurred deleting Role object from NetIQ. Error Message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.warn(LOGTAG + errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}
		catch (RemoteException re) {
			String errMsg = "Error calling web service to NetIQ from delete(). Error Message: " + re.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, re);
		}

		if (removeRolesResponseDocument != null && removeRolesResponseDocument.getRemoveRolesResponse() != null
				&& removeRolesResponseDocument.getRemoveRolesResponse().getResult() != null
				&& removeRolesResponseDocument.getRemoveRolesResponse().getResult().getDnstringArray().length > 0) {

			aRoleDn = removeRolesResponseDocument.getRemoveRolesResponse().getResult().getDnstringArray(0).getDn();
			logger.info(LOGTAG + "In delete(), start to remove ResourceAssociation for " + role.getRoleDN());
			GetResourceAssociationsResponseDocument getResourceAssociationsResponseDocument = null;
			try {
				getResourceAssociationsResponseDocument = RoleServiceClient.doGetResourceAssociationsSearch(roleService,
						role.getRoleDN(), null);
			}
			catch (RemoteException re) {
				String errMsg = "Error making web service calls doGetResourceAssociationsSearch() during delete(). Error Message: "
						+ re.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, re);
			}
			catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

					String errMsg = "An error occurred querying ResourceAssociation object during delete(). Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.warn(LOGTAG + errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}

			if (getResourceAssociationsResponseDocument != null && getResourceAssociationsResponseDocument
					.getGetResourceAssociationsResponse().getResult().sizeOfResourceassociationArray() > 0) {
				for (ResourceAssociation resourceAssociation : getResourceAssociationsResponseDocument
						.getGetResourceAssociationsResponse().getResult().getResourceassociationArray()) {

					if (resourceAssociation != null && resourceAssociation.getEntityKey() != null
							&& resourceAssociation.getRole().equalsIgnoreCase(role.getRoleDN())) {
						String aResourceAssociationDn = resourceAssociation.getEntityKey();

						try {
							RoleServiceClient.doDeleteResourceAssociation(roleService, aResourceAssociationDn);
						}
						catch (RemoteException re) {
							String errMsg = "Error making web service calls doDeleteResourceAssociation() during delete(). Error Message: "
									+ re.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new ProviderException(errMsg, re);
						}
						catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
							if (nsee != null && nsee.getFaultMessage() != null
									&& nsee.getFaultMessage().getNrfServiceException() != null
									&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

								String errMsg = "An error occurred deleting ResourceAssociation object during delete(). Error Message: "
										+ nsee.getFaultMessage().getNrfServiceException().getReason();
								logger.warn(LOGTAG + errMsg);
								//throw new ProviderException(errMsg, nsee);
							}
						}
					}
				}
				logger.info(LOGTAG + "resourceDn retrieved: " + getResourceAssociationsResponseDocument
						.getGetResourceAssociationsResponse().getResult().getResourceassociationArray(0).getResource());
			}

			if (role != null && role.getResource() != null && !role.getResource().isEmpty()) {
				@SuppressWarnings("unchecked")
				Iterator<Resource> iterator = role.getResource().iterator();
				while (iterator.hasNext()) {
					Resource resource = iterator.next();
					if (resource.getResourceDN() != null && resource.getResourceDN().length() > 0) {

						logger.info(LOGTAG + "identifier.getValue(): " + resource.getResourceDN());
						GetResourceAssociationsResponseDocument getResourceAssociationResponseDocument = null;
						try {
							getResourceAssociationResponseDocument = RoleServiceClient
									.doGetResourceAssociationsSearch(roleService, null, resource.getResourceDN());
						}
						catch (RemoteException re) {

							String errMsg = "Error making web service calls doGetResourceAssociationsSearch() for resource during delete(). Error Message: "
									+ re.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new ProviderException(errMsg, re);
						}
						catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
							// dump the stack trace just in case 
							// there's something of interest that might help us
							// figure out the root cause of this situation.
							nsee.printStackTrace();
							
							if (nsee != null && nsee.getFaultMessage() != null
									&& nsee.getFaultMessage().getNrfServiceException() != null
									&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

								String errMsg = "An error occurred querying ResourceAssociation object for resource during delete(). Error Message: "
										+ nsee.getFaultMessage().getNrfServiceException().getReason();
								logger.warn(LOGTAG + errMsg);
								
								// TJ 6/25/2021:  Enter a re-try loop because this is an unexpected
								// and unknown situation.  Generally, when this fails, 
								// we re-deprovision and it just works so we're trying
								// to do that here since we really don't know when or 
								// why it's failing in the first place.
								logger.warn(LOGTAG + "RoleServiceClient.doGetResourceAssociationsSearch "
										+ "failed for an unexpected reason, "
										+ "entering re-try loop...");
								boolean keepTrying=true;
								int retryCount = 1;
								while (keepTrying) {
									try {
										logger.info(LOGTAG + "RoleServiceClient.doGetResourceAssociationsSearch attempt: " + retryCount);
										getResourceAssociationResponseDocument = RoleServiceClient
												.doGetResourceAssociationsSearch(roleService, null, resource.getResourceDN());
										// if it works, we want to stop looping
										keepTrying = false;
									}
									catch (Exception e) {
										// if we've tried more than maximumRetries
										// we'll stop trying and throw an exception
										if (retryCount >= maximumRetries) {
											keepTrying = false;

											logger.fatal(LOGTAG 
												+ "RoleServiceClient.doGetResourceAssociationsSearch "
												+ "re-try loop DID NOT work after " 
												+ retryCount + " attempts.  See "
												+ "the stack trace in the log "
												+ "above this message for clues "
												+ "as to the root case.  "
												+ "Processing cannot continue");
											throw new ProviderException(errMsg, nsee);
										}
										else {
											try {
												// sleep for however long we're configured to (default is 500 millis)
												Thread.sleep(retrySleepTimeInMillis);
											}
											catch (InterruptedException e1) {
											}
										}
										retryCount++;
									}
								}
								logger.info(LOGTAG + "RoleServiceClient.doGetResourceAssociationsSearch "
									+ "re-try loop finally worked after " 
									+ retryCount + " attempt(s).  "
									+ "Processing will continue.");
								// END re-try loop
							}
						}

						boolean isRoleAndResouceAssociated = false;
						if (getResourceAssociationResponseDocument != null
								&& getResourceAssociationResponseDocument.getGetResourceAssociationsResponse()
										.getResult().sizeOfResourceassociationArray() > 0) {
							for (ResourceAssociation resourceAssociation : getResourceAssociationResponseDocument
									.getGetResourceAssociationsResponse().getResult().getResourceassociationArray()) {

								if (resourceAssociation != null && resourceAssociation.getEntityKey() != null
										&& !resourceAssociation.getRole().equalsIgnoreCase(role.getRoleDN())) {
									isRoleAndResouceAssociated = true;
									break;
								}
							}
						}

						if (!isRoleAndResouceAssociated) {
							try {
								ResourceServiceClient.doRemoveResource(resourceService, resource.getResourceDN());
							}
							catch (RemoteException re) {
								String errMsg = "Error making web service calls to NetIQ from delete(). Error Message: "
										+ re.getMessage();
								logger.error(LOGTAG + errMsg);
								throw new ProviderException(errMsg, re);
							}
							catch (NrfServiceExceptionException nsee) {
								if (nsee != null && nsee.getFaultMessage() != null
										&& nsee.getFaultMessage().getNrfServiceException() != null
										&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

									String errMsg = "An error occurred deleting Resource object from NetIQ. Error Message: "
											+ nsee.getFaultMessage().getNrfServiceException().getReason();
									logger.warn(LOGTAG + errMsg);
									if (!StringUtil.isResourceExist(
											nsee.getFaultMessage().getNrfServiceException().getReason())) {
										throw new ProviderException(errMsg, nsee);
									}
								}
							}
						}
					}
				}
			}
		}

		if (aRoleDn != null && aRoleDn.length() > 0) {
		    try {
				responseMoa.setRoleDN(aRoleDn);
			} catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred setting the field values of the Role. The exception is: "
						+ efe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, efe);
			}
			responseMoa = (Role) waitForCompletion(responseMoa, MessageControl.DELETE_MSG_ACTION.getName(), null,
					roleService, resourceService);
			logger.info("Role.Delete-Request complete.");
		}
		else {
			responseMoa = null;
		}

		try {
			roleService._getServiceClient().cleanupTransport();
			resourceService._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return responseMoa;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ProviderProcessingResponse update(Role newRole, Role idmRole) throws ProviderException {

		logger.info(LOGTAG + "In update(). Ready to execute Update-Request.");
		List<String> idmResourceDnList = new ArrayList<String>();
		List<String> newResourceDnList = new ArrayList<String>();
		List<SavedChanges<String, String>> changedList = new ArrayList<SavedChanges<String, String>>();
		ProviderProcessingResponse providerProcessingResponse = new ProviderProcessingResponse();
		TransactionScopeManager transactionManager = new TransactionScopeManager();
		List<String> tempIdmResourceDnList = null;

		RoleServiceStub roleService = null;
		ResourceServiceStub resourceService = null;

		try {
			roleService = getRoleServiceStub(getServiceUserId(), getServicePassword(), getRoleServiceEndpoint(),
					getConnectionTimeout(), getSocketTimeout());
			if (roleService != null) {
				resourceService = getResourceServiceStub(getServiceUserId(), getServicePassword(),
						getResourceServiceEndpoint(), getConnectionTimeout(), getSocketTimeout());
			}
		}
		catch (RemoteException re) {
			String errMsg = "The Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		for (Resource idmResource : (List<Resource>) (Object) idmRole.getResource()) {
			idmResourceDnList.add(idmResource.getResourceDN());
		}

		tempIdmResourceDnList = new ArrayList<String>(idmResourceDnList);
		for (Resource newResource : (List<Resource>) (Object) newRole.getResource()) {
			newResourceDnList.add(newResource.getResourceDN());
		}

		idmResourceDnList.removeIf(newResourceDnList::contains);
		logger.info("newResource before:" + newResourceDnList.size());

		newResourceDnList.removeIf(tempIdmResourceDnList::contains);
		for (String newResourceDn : newResourceDnList) {
			logger.info("newResource after: " + newResourceDn);
		}

		for (String idmResourceDn : idmResourceDnList) {
			logger.info("idmResource after: " + idmResourceDn);
		}

		if (idmResourceDnList.size() > 0) {

			for (String idmResourceDn : idmResourceDnList) {
				GetResourceAssociationsResponseDocument getResourceAssociationsResponseDocument = null;
				try {
					getResourceAssociationsResponseDocument = RoleServiceClient
							.doGetResourceAssociationsSearch(roleService, null, idmResourceDn);
				}
				catch (RemoteException re) {
					String errMsg = "Error making web service calls doGetResourceAssociationsSearch() during delete(). Error Message: "
							+ re.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg, re);
				}
				catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
					if (nsee != null && nsee.getFaultMessage() != null
							&& nsee.getFaultMessage().getNrfServiceException() != null
							&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

						String errMsg = "An error occurred querying ResourceAssociation object during delete(). Error Message: "
								+ nsee.getFaultMessage().getNrfServiceException().getReason();
						logger.error(LOGTAG + errMsg);
						throw new ProviderException(errMsg, nsee);
					}
				}

				if (getResourceAssociationsResponseDocument != null && getResourceAssociationsResponseDocument
						.getGetResourceAssociationsResponse().getResult().sizeOfResourceassociationArray() > 0) {

					for (ResourceAssociation resourceAssociation : getResourceAssociationsResponseDocument
							.getGetResourceAssociationsResponse().getResult().getResourceassociationArray()) {
						if (resourceAssociation != null && resourceAssociation.getEntityKey() != null
								&& resourceAssociation.getRole() != null) {
							String aResourceAssociationDn = resourceAssociation.getEntityKey();
							logger.info(LOGTAG + "resource association: " + resourceAssociation.getEntityKey());

							if (resourceAssociation.getRole().equals(idmRole.getRoleDN())) {
								try {
									RoleServiceClient.doDeleteResourceAssociation(roleService, aResourceAssociationDn);
								}
								catch (RemoteException re) {
									String errMsg = "Error making web service calls doDeleteResourceAssociation() during delete(). Error Message: "
											+ re.getMessage();
									logger.error(LOGTAG + errMsg);
									throw new ProviderException(errMsg, re);
								}
								catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
									if (nsee != null && nsee.getFaultMessage() != null
											&& nsee.getFaultMessage().getNrfServiceException() != null
											&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

										String errMsg = "An error occurred deleting ResourceAssociation object during delete(). Error Message: "
												+ nsee.getFaultMessage().getNrfServiceException().getReason();
										logger.error(LOGTAG + errMsg);
										//throw new ProviderException(errMsg, nsee);
									}
								}
							}
						}
					}
				}
			}
		}

		if (newResourceDnList.size() > 0) {
			// do add resource association
			CreateResourceAssociationResponseDocument createResourceAssociationResponseDocument = null;
			for (String newResourceDn : newResourceDnList) {
				try {
					createResourceAssociationResponseDocument = RoleServiceClient
							.doResourceAssociationCreate(roleService, newRole.getRoleDN(), newResourceDn);
				}
				catch (RemoteException re) {
					String errMsg = "Error making web service calls to NetIQ. Error Message: " + re.getMessage();
					logger.error(LOGTAG + errMsg);
					if (changedList.size() > 0) {
						transactionManager.rollback(changedList, roleService, resourceService);
					}
					throw new ProviderException(errMsg, re);
				}
				catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
					String errMsg = "Server error from NetIQ during update(), please check log for more detail. Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.error(LOGTAG + errMsg);
					if (changedList.size() > 0) {
						transactionManager.rollback(changedList, roleService, resourceService);
					}
					throw new ProviderException(errMsg, nsee);
				}
				if (createResourceAssociationResponseDocument != null
						&& createResourceAssociationResponseDocument.getCreateResourceAssociationResponse() != null
						&& createResourceAssociationResponseDocument.getCreateResourceAssociationResponse()
								.getResult() != null) {
					String aResourceAssociationDn = createResourceAssociationResponseDocument
							.getCreateResourceAssociationResponse().getResult().getEntityKey();
					changedList.add(new SavedChanges<String, String>(IdmValues.RESOURCE_ASSOCIATION.getName(),
							aResourceAssociationDn));
				}
			}
		}
		providerProcessingResponse.setSavedChanges(changedList);
		providerProcessingResponse.setRoleService(roleService);
		providerProcessingResponse.setResourceService(resourceService);

		try {
			roleService._getServiceClient().cleanupTransport();
			resourceService._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return providerProcessingResponse;
	}

	private <T> Object waitForCompletion(Object object, String action, List<SavedChanges<String, String>> changedList,
			RoleServiceStub roleService, ResourceServiceStub resourceService) throws ProviderException {
        
		logger.info(LOGTAG + "In waitForCompletion()" + action);
		boolean isCompleted = false;
		long startTime = System.currentTimeMillis();
		Role role = null;
		while (isCompleted == false) {
			// Throw an exception if maxWaitTime is exceeded.
			if (System.currentTimeMillis() - startTime > getMaxWaitTime()) {
				String errMsg = "Maximum wait time of " + getMaxWaitTime()
						+ " ms for completion of Role object has been "
						+ "exceeded. Processing RoleAssignment may still be in progress.";
				throw new ProviderException(errMsg);
			}
			if (object instanceof Role) {
			    if (((Role) object).getRoleDN() != null && ((Role) object).getRoleDN().length() > 0) {
					logger.info(LOGTAG + "Found roleDn: " + ((Role) object).getRoleDN());
				} else {
					String errMsg = "RoleDn value is empty.";
					throw new ProviderException(errMsg);
				}
				role = query(((Role) object).getRoleDN(), changedList, action, roleService, resourceService);
				if (role != null && role.getRoleDN() != null && role.getRoleDN().length() > 0) {
					logger.info(LOGTAG + "Found " + "role object creation or deletion completed.");
					if (action.equalsIgnoreCase(MessageControl.GENERATE_MSG_ACTION.toString())) {
						isCompleted = true;
						object = role;
						break;
					}
					else {
						// this is for waiting delete
						logger.info(LOGTAG + "Sleeping for " + getWaitInterval() + " ms...");
						try {
							Thread.sleep(getWaitInterval());
						}
						catch (InterruptedException swallowed) {
							logger.info(LOGTAG + "Thread interrupted, do nothing " + swallowed.getMessage());
						}
					}
				}
				else {
					if (action.equalsIgnoreCase(MessageControl.DELETE_MSG_ACTION.toString())) {
						isCompleted = true;
						// object = null;
						break;
					}
					else {
						try {
							Thread.sleep(getWaitInterval());
						}
						catch (InterruptedException swallowed) {
							logger.info(LOGTAG + "Thread interrupted, do nothing " + swallowed.getMessage());
						}
					}
				}
			}
		}
		return object;
	}

	/**
	 * 
	 * @param roleDN
	 * @param changedList
	 * @param action
	 * @param roleService
	 * @param resourceService
	 * @return
	 */
	private Role query(String roleDn, List<SavedChanges<String, String>> changedList, String action,
			RoleServiceStub roleService, ResourceServiceStub resourceService) throws ProviderException {

		logger.info(LOGTAG + "In query(). Ready to execute request.");
		TransactionScopeManager transactionManager = new TransactionScopeManager();
		GetRoleResponseDocument getRoleRsponseDocument = null;

		try {
			getRoleRsponseDocument = RoleServiceClient.doGetRoleSearchByDn(roleService, roleDn);
		}
		catch (RemoteException re) {
			if (changedList != null && changedList.size() > 0) {
				transactionManager.rollback(changedList, roleService, resourceService);
			}
			String errMsg = "An error occurred calling IDM NetIQ from query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
				if (changedList != null && changedList.size() > 0) {
					transactionManager.rollback(changedList, roleService, resourceService);
				}
				String errMsg = "A server error occurred retrieving Role object from IDM NetIQ during query(). Error message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}

		Role responseMoa = null;
		if (getRoleRsponseDocument != null && getRoleRsponseDocument.getGetRoleResponse() != null
				&& getRoleRsponseDocument.getGetRoleResponse().getResult() != null) {

			GetRoleResponse roleResultData = getRoleRsponseDocument.getGetRoleResponse();
			GetResourceAssociationsResponseDocument getResourceAssociationsResponseDocument = null;
			List<String> resourceDnList = null;
			try {
				responseMoa = this.getObject(Role.class);
			}
			catch (EnterpriseConfigurationObjectException ecoe) {
				if (changedList != null && changedList.size() > 0) {
					transactionManager.rollback(changedList, roleService, resourceService);
				}
				String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: "
						+ ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, ecoe);
			}
			if (roleResultData != null && roleResultData.getResult() != null) {
				try {
					if (roleResultData.getResult().getDescription() != null) {
						responseMoa.setRoleDescription(roleResultData.getResult().getDescription());
					}
					if (roleResultData.getResult().getName() != null) {
						responseMoa.setRoleName(roleResultData.getResult().getName());
					}

					for (CategoryKey categoryKey : roleResultData.getResult().getRoleCategoryKeys()
							.getCategorykeyArray()) {
						responseMoa.setRoleCategoryKey(categoryKey.getCategoryKey());
						break;
					}
					responseMoa.setRoleDN(roleDn);
					getResourceAssociationsResponseDocument = RoleServiceClient.doGetResourceAssociationsSearch(
							roleService, roleResultData.getResult().getEntityKey(), null);
				}
				catch (EnterpriseFieldException efe) {
					if (changedList != null && changedList.size() > 0) {
						transactionManager.rollback(changedList, roleService, resourceService);
					}
					String errMsg = "An error occurred setting the field values of the Role. The exception is: "
							+ efe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg, efe);
				}
				catch (RemoteException re) {
					if (changedList != null && changedList.size() > 0) {
						transactionManager.rollback(changedList, roleService, resourceService);
					}
					String errMsg = "Error making web service calls to NetIQ in query(). Error Message: "
							+ re.getMessage();
					logger.error(errMsg);
					throw new ProviderException(errMsg, re);
				}
				catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
					if (nsee != null && nsee.getFaultMessage() != null
							&& nsee.getFaultMessage().getNrfServiceException() != null
							&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
						String errMsg = "Server error in query() retrieving RoleAssociation object from NetIQ, please check log for more detail. Error Message: "
								+ nsee.getFaultMessage().getNrfServiceException().getReason();
						logger.error(errMsg);
						if (changedList != null && changedList.size() > 0) {
							transactionManager.rollback(changedList, roleService, resourceService);
						}
						throw new ProviderException(errMsg, nsee);
					}
				}

				resourceDnList = new ArrayList<String>();
				if (getResourceAssociationsResponseDocument != null && getResourceAssociationsResponseDocument
						.getGetResourceAssociationsResponse().getResult().sizeOfResourceassociationArray() > 0) {
					for (ResourceAssociation resourceAssociation : getResourceAssociationsResponseDocument
							.getGetResourceAssociationsResponse().getResult().getResourceassociationArray()) {
						if (resourceAssociation != null) {
							resourceDnList.add(resourceAssociation.getResource());
							logger.info(LOGTAG + "Resource added to the DN list: " + resourceAssociation.getResource());
						}
					}
					// getResourceAssociationsResponseDocument.getGetResourceAssociationsResponse().getResult().getResourceassociationArray();
					logger.info(LOGTAG + "resoucedn1 retrieved: "
							+ getResourceAssociationsResponseDocument.getGetResourceAssociationsResponse().getResult()
									.getResourceassociationArray(0).getResource());
				}
			}

			if (resourceDnList != null && !resourceDnList.isEmpty()) {
				for (String resourceDn : resourceDnList) {

					Resource resource = new Resource();
					List<Entitlement> list = new ArrayList<Entitlement>();
					GetResourceResponseDocument getResourceResponseDocument = null;
					try {
						getResourceResponseDocument = ResourceServiceClient.doResourceSearchByDN(resourceService,
								resourceDn);
					}
					catch (RemoteException re) {
						if (changedList != null && changedList.size() > 0) {
							transactionManager.rollback(changedList, roleService, resourceService);
						}
						String errMsg = "An error occurred calling IDM NetIQ from query(), please check the log for more detail "
								+ re.getMessage();
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, re);
					}
					catch (org.apache.axis2.client.NrfServiceExceptionException nsee) {
						if (nsee != null && nsee.getFaultMessage() != null
								&& nsee.getFaultMessage().getNrfServiceException() != null
								&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
							String errMsg = "A server error occurred retrieving Resource object from IDM NetIQ during query(). Error message: "
									+ nsee.getFaultMessage().getNrfServiceException().getReason();
							logger.error(errMsg);
							if (changedList != null && changedList.size() > 0) {
								transactionManager.rollback(changedList, roleService, resourceService);
							}
							throw new ProviderException(errMsg, nsee);
						}
					}

					GetResourceResponse getResourceResponse = getResourceResponseDocument.getGetResourceResponse();
					if (getResourceResponse != null && getResourceResponse.getResult() != null
							&& getResourceResponse.getResult().getEntityKey() != null) {
						String resourceName = getResourceResponse.getResult().getName();
						try {
							resource.setResourceName(resourceName);
							resource.setResourceDN(resourceDn);
							resource.setResourceDescription(getResourceResponse.getResult().getDescription());
							for (com.novell.www.resource.service.CategoryKey categoryKey : getResourceResponse
									.getResult().getResourceCategoryKeys().getCategorykeyArray()) {
								resource.setResourceCategoryKey(categoryKey.getCategoryKey());
								break;
							}
						}
						catch (EnterpriseFieldException efe) {
							if (changedList != null && changedList.size() > 0) {
								transactionManager.rollback(changedList, roleService, resourceService);
							}
							String errMsg = "An error occurred setting the field values of the Role. The exception is: "
									+ efe.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new ProviderException(errMsg, efe);
						}
						for (NrfEntitlementRef ref : getResourceResponse.getResult().getEntitlementRef()
								.getNrfentitlementrefArray()) {
							Entitlement aEntitlement = new Entitlement();
							try {

								logger.info("entitlementDn: " + ref.getEntitlementDn());
								if (ref.getEntitlementParameters() != null) {

									logger.info("entitlementGuid: "
											+ StringUtil.getUUIDFromString(ref.getEntitlementParameters()));
									String dnString = JsonUtils.convertJSONObjectToString(
											ref.getEntitlementParameters(), IdmValues.ID2.getName());
									if (dnString != null) {
										aEntitlement.setEntitlementDN(dnString);
									}

									if (aEntitlement.getEntitlementDN() != null
											&& aEntitlement.getEntitlementDN().length() > 0) {
										aEntitlement.setEntitlementGuid(JsonUtils.convertJSONObjectToString(
												ref.getEntitlementParameters(), IdmValues.ID.getName()));
									}
									else {
										aEntitlement.setEntitlementDN(JsonUtils.convertJSONObjectToString(
												ref.getEntitlementParameters(), IdmValues.ID.getName()));
									}
								}
							}
							catch (EnterpriseFieldException efe) {
								if (changedList != null && changedList.size() > 0) {
									transactionManager.rollback(changedList, roleService, resourceService);
								}
								String errMsg = "An error occurred setting the field values of the Role. The exception is: "
										+ efe.getMessage();
								logger.error(LOGTAG + errMsg);
								throw new ProviderException(errMsg, efe);
							}
							list.add(aEntitlement);
						}
						resource.setEntitlement(list.get(0));
					}
					responseMoa.addResource(resource);
				}
			}
		}
		else {
			String errDesc = null;
			if (action != null && action.equalsIgnoreCase(MessageControl.DELETE_MSG_ACTION.getName())) {
				errDesc = "The Role is no longer existed after query(). It's successfully removed from NetIQ. RoleDn: "
						+ roleDn;
			}
			else {
				errDesc = "The Role does not exist for the requested RoleDn: " + roleDn;
			}
			logger.info(LOGTAG + errDesc);
		}

		try {
			resourceService._getServiceClient().cleanupTransport();
			roleService._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return responseMoa;

	}

	@SuppressWarnings("unchecked")
	private <T extends XmlEnterpriseObjectImpl> T getObject(Class<T> aClass)
			throws EnterpriseConfigurationObjectException {
		return (T) getAppConfig().getObjectByType(aClass.getName());
	}

	/**
	 * Creates a Role Service stub for the remote object acts as a client's local
	 * proxy, so the caller can invoke a method on the client that will carry out
	 * the method call for the remote object. A multithreaded client will be set up
	 * in order to enable concurrent access.
	 * 
	 * @param userid
	 * @param password
	 * @param endpoint
	 * @param timeout
	 * @return
	 * @throws RemoteException
	 */
	private RoleServiceStub getRoleServiceStub(String userid, String password, String endpoint, int timeout,
			int soTimeout) throws RemoteException {

		RoleServiceStub service = null;
		EndpointReference targetEPR = new EndpointReference(endpoint);
		service = new RoleServiceStub();
		HttpTransportPropertiesImpl.Authenticator auth = new HttpTransportPropertiesImpl.Authenticator();
		auth.setUsername(userid);
		auth.setPassword(password);
		auth.setPreemptiveAuthentication(true);

		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		Options options = new Options();
		options.setProperty(HTTPConstants.AUTHENTICATE, auth);

		// options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, "true");
		options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, "false");
		// Set up concurrent access by creating a pooling manager to manage
		// connections.
		HttpConnectionManagerParams params = conmgr.getParams();

		conmgr.getParams().setDefaultMaxConnectionsPerHost(20);
		params = new HttpConnectionManagerParams();
		params.setMaxTotalConnections(20);
		params.setDefaultMaxConnectionsPerHost(20);
		conmgr.setParams(params);

		// conmgr.getParams().setDefaultMaxConnectionsPerHost(10);
		HttpClient client = new HttpClient(conmgr);
		options.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
		// options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Boolean.TRUE);
		options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Boolean.FALSE);
		options.setTo(targetEPR);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
		options.setTimeOutInMilliSeconds(timeout);
		options.setProperty(HTTPConstants.SO_TIMEOUT, timeout);
		options.setProperty(HTTPConstants.CONNECTION_TIMEOUT, soTimeout);
		service._getServiceClient().setOptions(options);

		return service;
	}

	/**
	 * Creates a Resource service stub for the remote object acts as a client's
	 * local proxy
	 * 
	 * The multithreaded is configured by setting a cached httpclient object when
	 * the service Stub is invoked before the actual requests are made.
	 * 
	 * @param userid
	 * @param password
	 * @param endpoint
	 * @param timeout
	 * @return
	 * @throws RemoteException
	 */
	private ResourceServiceStub getResourceServiceStub(String userid, String password, String endpoint, int timeout,
			int soTimeout) throws RemoteException {

		ResourceServiceStub service = null;
		EndpointReference targetEPR = new EndpointReference(endpoint);
		service = new ResourceServiceStub();
		HttpTransportPropertiesImpl.Authenticator auth = new HttpTransportPropertiesImpl.Authenticator();
		auth.setUsername(userid);
		auth.setPassword(password);
		auth.setPreemptiveAuthentication(true);

		Options options = new Options();
		options.setProperty(HTTPConstants.AUTHENTICATE, auth);

		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(10);
		HttpConnectionManagerParams params = conmgr.getParams();
		params = new HttpConnectionManagerParams();
		params.setMaxTotalConnections(20);
		params.setDefaultMaxConnectionsPerHost(20);
		conmgr.setParams(params);

		HttpClient client = new HttpClient(conmgr);
		options.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
		// comment out for now
		// options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Boolean.TRUE);
		options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Boolean.FALSE);
		options.setTo(targetEPR);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
		options.setTimeOutInMilliSeconds(timeout);
		options.setProperty(HTTPConstants.SO_TIMEOUT, soTimeout);
		options.setProperty(HTTPConstants.CONNECTION_TIMEOUT, timeout);
		service._getServiceClient().setOptions(options);

		return service;
	}

	public AppConfig getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(AppConfig appConfig) {
		this.appConfig = appConfig;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public String getServiceUserId() {
		return serviceUserId;
	}

	public void setServiceUserId(String serviceUserId) {
		this.serviceUserId = serviceUserId;
	}

	public String getServicePassword() {
		return servicePassword;
	}

	public void setServicePassword(String servicePassword) {
		this.servicePassword = servicePassword;
	}

	public String getRoleServiceEndpoint() {
		return roleServiceEndpoint;
	}

	public void setRoleServiceEndpoint(String roleServiceEndpoint) {
		this.roleServiceEndpoint = roleServiceEndpoint;
	}

	public String getResourceServiceEndpoint() {
		return resourceServiceEndpoint;
	}

	public void setResourceServiceEndpoint(String resourceServiceEndpoint) {
		this.resourceServiceEndpoint = resourceServiceEndpoint;
	}

	public String getCodeMapKey() {
		return codeMapKey;
	}

	public void setCodeMapKey(String codeMapKey) {
		this.codeMapKey = codeMapKey;
	}

	public void setIdvCodeMapKey(String idvCodeMapKey) {
		this.idvCodeMapKey = idvCodeMapKey;
	}

	public String getIdvCodeMapKey() {
		return idvCodeMapKey;
	}

	private static long waitInterval = 8000;

	public static long getWaitInterval() {
		return waitInterval;
	}

	public static void setWaitInterval(long waitInterval) {
		NetiqRoleProvisioningProvider.waitInterval = waitInterval;
	}

	public static long getMaxWaitTime() {
		return maxWaitTime;
	}

	public static void setMaxWaitTime(long maxWaitTime) {
		NetiqRoleProvisioningProvider.maxWaitTime = maxWaitTime;
	}

	public String getEadEntitlementDn() {
		return eadEntitlementDn;
	}

	public void setEadEntitlementDn(String eadEntitlementDn) {
		this.eadEntitlementDn = eadEntitlementDn;
	}

	public String getUmdEntitlementDn() {
		return umdEntitlementDn;
	}

	public void setUmdEntitlementDn(String umdEntitlementDn) {
		this.umdEntitlementDn = umdEntitlementDn;
	}

	public String getCategoryKey() {
		return categoryKey;
	}

	public void setCategoryKey(String categoryKey) {
		this.categoryKey = categoryKey;
	}

	public String getIdvEntitlementDn() {
		return idvEntitlementDn;
	}

	public void setIdvEntitlementDn(String idvEntitlementDn) {
		this.idvEntitlementDn = idvEntitlementDn;
	}

	public String getHmdEntitlementDn() {
		return hmdEntitlementDn;
	}

	public void setHmdEntitlementDn(String hmdEntitlementDn) {
		this.hmdEntitlementDn = hmdEntitlementDn;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

}
package org.rhedcloud.idm.service.provider.netiq;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.NrfServiceExceptionException;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ResourceServiceStub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.moa.XmlEnterpriseObjectImpl;
import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.ProviderProcessingResponse;
import org.rhedcloud.idm.service.provider.ResourceProvisioningProvider;
import org.rhedcloud.idm.service.provider.netiq.client.ResourceServiceClient;
import org.rhedcloud.idm.service.provider.netiq.util.IdmValues;
import org.rhedcloud.idm.service.util.ESBJsonObject;
import org.rhedcloud.idm.service.util.JsonUtils;
import org.rhedcloud.idm.service.util.MessageControl;
import org.rhedcloud.idm.service.util.PropertyNames;
import org.rhedcloud.idm.service.util.SavedChanges;
import org.rhedcloud.idm.service.util.StringUtil;

import com.novell.www.resource.service.CreateResourceResponseDocument;
import com.novell.www.resource.service.GetResourceResponse;
import com.novell.www.resource.service.GetResourceResponseDocument;
import com.novell.www.resource.service.NrfEntitlementRef;
import com.novell.www.resource.service.RemoveResourceResponseDocument;

import edu.emory.moa.jmsobjects.identity.v1_0.Entitlement;
import edu.emory.moa.jmsobjects.identity.v1_0.Resource;
import edu.emory.moa.objects.resources.v1_0.ResourceRequisition;

/**
 * 
 * This class actually implements the provider class
 * ResourceProvisioningProvider.
 *
 * @author RXING2
 */
public class NetiqResourceProvisioningProvider extends OpenEaiObject implements ResourceProvisioningProvider {
	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(NetiqResourceProvisioningProvider.class);
	private AppConfig appConfig;
	private String serviceUserId = "";
	private String servicePassword = "";
	private String codeMapKey = "";
	private static long maxWaitTime = 30000;
	private String resourceServiceEndpoint = "";
	private String eadEntitlementDn = "";
	private String hmdEntitlementDn = "";
	private String umdEntitlementDn = "";
	private String idvCodeMapKey = "";
	private String categoryKey = "";
	private String idvEntitlementDn = "";
	private int connectionTimeout = 12000;
	private int socketTimeout = 12000;
	private String LOGTAG = "[IdmResourceProvisioningProvider] ";

	/**
	 * @throws EnterpriseConfigurationObjectException
	 * @see ResourceProvisioingProvider.java
	 */
	public void init(AppConfig aConfig) throws ProviderException {

		logger.info(LOGTAG + "Initializing...");
		setAppConfig(aConfig);

		// Get the provider properties
		PropertyConfig pConfig = new PropertyConfig();
		try {
			pConfig = (PropertyConfig) aConfig.getObject("ResourceProvisioningProviderProperties");
			Properties props = pConfig.getProperties();
			setProperties(props);
		}
		catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from AppConfig: The exception is:";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, eoce);
		}

		if (getProperties() != null && !getProperties().isEmpty()) {

			// Set the web service endpoint that we should make calls
			// to. Otherwise, throw an exception
			if (getProperties().getProperty(PropertyNames.RESOURCE_WEB_SERVICE_ENDPOINT.toString()) != null) {
				setResourceServiceEndpoint(
						getProperties().getProperty(PropertyNames.RESOURCE_WEB_SERVICE_ENDPOINT.toString()));
				logger.info(LOGTAG + PropertyNames.RESOURCE_WEB_SERVICE_ENDPOINT.toString() + " = "
						+ getResourceServiceEndpoint());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.RESOURCE_WEB_SERVICE_ENDPOINT.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the user id to log into IDM with from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()) != null) {
				setServiceUserId(getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()));
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_USER_ID.toString() + " = " + getServiceUserId());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_USER_ID.toString()
						+ " not defined and is expected to be passed in via the appConfig ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.CODE_MAP_KEY.toString()) != null) {
				setCodeMapKey(getProperties().getProperty(PropertyNames.CODE_MAP_KEY.toString()));
				logger.info(LOGTAG + PropertyNames.CODE_MAP_KEY.toString() + " = " + getCodeMapKey());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.CODE_MAP_KEY.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.IDV_CODE_MAP_KEY.toString()) != null) {
				setIdvCodeMapKey(getProperties().getProperty(PropertyNames.IDV_CODE_MAP_KEY.toString()));
				logger.info(LOGTAG + PropertyNames.IDV_CODE_MAP_KEY.toString() + " = " + getIdvCodeMapKey());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.IDV_CODE_MAP_KEY.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the password to log into IDM with from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()) != null) {
				setServicePassword(getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()));
				// logger.info(LOGTAG +
				// PropertyNames.WEB_SERVICE_PASSWORD.toString() + " = " +
				// getServicePassword());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_PASSWORD.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.EAD_ENTITLEMENT_DN.toString()) != null) {
				setEadEntitlementDn(getProperties().getProperty(PropertyNames.EAD_ENTITLEMENT_DN.toString()));
				logger.info(LOGTAG + PropertyNames.EAD_ENTITLEMENT_DN.toString() + " = " + getEadEntitlementDn());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.EAD_ENTITLEMENT_DN.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.HMD_ENTITLEMENT_DN.toString()) != null) {
				setHmdEntitlementDn(getProperties().getProperty(PropertyNames.HMD_ENTITLEMENT_DN.toString()));
				logger.info(LOGTAG + PropertyNames.HMD_ENTITLEMENT_DN.toString() + " = " + getHmdEntitlementDn());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.HMD_ENTITLEMENT_DN.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.IDV_ENTITLEMENT_DN.toString()) != null) {
				setIdvEntitlementDn(getProperties().getProperty(PropertyNames.IDV_ENTITLEMENT_DN.toString()));
				logger.info(LOGTAG + PropertyNames.IDV_ENTITLEMENT_DN.toString() + " = " + getIdvEntitlementDn());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.IDV_ENTITLEMENT_DN.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.UMD_ENTITLEMENT_DN.toString()) != null) {
				setUmdEntitlementDn(getProperties().getProperty(PropertyNames.UMD_ENTITLEMENT_DN.toString()));
				logger.info(LOGTAG + PropertyNames.UMD_ENTITLEMENT_DN.toString() + " = " + getUmdEntitlementDn());
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.UMD_ENTITLEMENT_DN.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.WAIT_INTERVAL.toString()) != null) {
				setWaitInterval(Long.valueOf(getProperties().getProperty(PropertyNames.WAIT_INTERVAL.toString())));
				logger.info(LOGTAG + PropertyNames.WAIT_INTERVAL.toString() + " = " + waitInterval);
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.WAIT_INTERVAL.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.MAX_WAIT_TIME.toString()) != null) {
				setMaxWaitTime(Long.valueOf(getProperties().getProperty(PropertyNames.MAX_WAIT_TIME.toString())));
				logger.info(LOGTAG + PropertyNames.MAX_WAIT_TIME.toString() + " = " + maxWaitTime);
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.MAX_WAIT_TIME.toString()
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString()) != null) {
				setConnectionTimeout(
						Integer.parseInt(getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString())));
				logger.info(LOGTAG + PropertyNames.CONNECTION_TIMEOUT.toString() + " = " + getConnectionTimeout());
			}
			else {
				logger.info(
						LOGTAG + "OPTIONAL Property " + PropertyNames.CONNECTION_TIMEOUT.toString() + " not defined");
			}

			if (getProperties().getProperty(PropertyNames.SOCKET_TIMEOUT.toString()) != null) {
				setSocketTimeout(
						Integer.parseInt(getProperties().getProperty(PropertyNames.SOCKET_TIMEOUT.toString())));
				logger.info(LOGTAG + PropertyNames.SOCKET_TIMEOUT.toString() + " = " + socketTimeout);
			}
			else {
				logger.info(LOGTAG + "OPTIONAL Property " + PropertyNames.SOCKET_TIMEOUT.toString() + " not defined");
			}
		}
		else {
			String errMsg = "REQUIRED Properties have not been defined. Please define required properties within AppConfig.";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg);
		}
	}

	// @Override
	public ProviderProcessingResponse generate(ResourceRequisition resourceRequisition) throws ProviderException {

		logger.info(LOGTAG + "Ready to execute Generate-Request for Resource object.");
		CreateResourceResponseDocument createResourceResponseDoc = CreateResourceResponseDocument.Factory.newInstance();
		String aResourceDn = null;
		Resource resource = new Resource();
		Entitlement entitlement = new Entitlement();
		ProviderProcessingResponse providerProcessingResponse = new ProviderProcessingResponse();
		List<SavedChanges<String, String>> changedList = new ArrayList<SavedChanges<String, String>>();
		ResourceServiceStub resourceService = null;

		try {
			resourceService = getResourceServiceStub(getServiceUserId(), getServicePassword(),
					getResourceServiceEndpoint(), getConnectionTimeout(), getSocketTimeout());
		}
		catch (RemoteException re) {
			String errMsg = "The Resource Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		try {
			resource.setResourceDescription(resourceRequisition.getResourceDescription());
			resource.setResourceName(resourceRequisition.getResourceName());
			resource.setResourceCategoryKey(resourceRequisition.getResourceCategoryKey());
			if (resourceRequisition.getEntitlement().getEntitlementApplication()
					.equalsIgnoreCase(IdmValues.EAD.getName())) {

				logger.info(LOGTAG + "EntitlementApplication is: " + IdmValues.EAD.getName());
				String paramGuidValue = null;
				try {
					paramGuidValue = ResourceServiceClient.doGetCodeMapValue(resourceService,
							resourceRequisition.getEntitlement().getEntitlementDN(), getCodeMapKey());
				}
				catch (RemoteException re) {
					String errMsg = "Error making web service calls to NetIQ. Error Message: " + re.getMessage();
					logger.error(errMsg);
					throw new ProviderException(errMsg, re);
				}
				catch (NrfServiceExceptionException nsee) {
					if (nsee != null && nsee.getFaultMessage() != null
							&& nsee.getFaultMessage().getNrfServiceException() != null
							&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

						String errMsg = "Server error from NetIQ, please check log for more detail. Error Message: "
								+ nsee.getFaultMessage().getNrfServiceException().getReason();
						logger.error(errMsg);
						throw new ProviderException(errMsg, nsee);
					}
				}
				if (paramGuidValue != null && paramGuidValue.length() > 0) {

					logger.info(LOGTAG + "paramGuidValue: " + paramGuidValue);
					entitlement.setEntitlementGuid(paramGuidValue);
					entitlement.setEntitlementDN(getEadEntitlementDn());
					resource.setEntitlement(entitlement);
				}
				else {
					String errMsg = "The entitlement value was not found in the NetIQ CodeMap. Please contact IDM for additional assistance.";
					logger.error(errMsg);
					throw new ProviderException(errMsg);
				}
			}
			else if (resourceRequisition.getEntitlement().getEntitlementApplication()
					.equalsIgnoreCase(IdmValues.UMD.getName())) {

				logger.info(LOGTAG + "EntitlementApplication is: " + IdmValues.UMD.getName());
				if (resourceRequisition.getEntitlement().getEntitlementGuid().toLowerCase().chars()
						.filter(e -> e == '-').count() != 4
						&& resourceRequisition.getEntitlement().getEntitlementGuid().length() != 38) {

					String errMsg = "The entitlement GUID values passed in is invalid. Please verify in the request message the proper UMD entitlement GUID values are provided. ";
					logger.error(errMsg);
					throw new ProviderException(errMsg);
				}
				else {

					String convertedGuid = StringUtil
							.convertGuidFromString(resourceRequisition.getEntitlement().getEntitlementGuid());
					ESBJsonObject object = JsonUtils.convertStringToJSONObject(convertedGuid,
							resourceRequisition.getEntitlement().getEntitlementDN());

					entitlement.setEntitlementGuid(object.toJSONString());
					entitlement.setEntitlementDN(getUmdEntitlementDn());
					resource.setEntitlement(entitlement);
				}
			}
			else if (resourceRequisition.getEntitlement().getEntitlementApplication()
					.equalsIgnoreCase(IdmValues.HMD.getName())) {

				logger.info(LOGTAG + "EntitlementApplication is: " + IdmValues.HMD.getName());
				if (resourceRequisition.getEntitlement().getEntitlementGuid().toLowerCase().chars()
						.filter(e -> e == '-').count() != 4
						&& resourceRequisition.getEntitlement().getEntitlementGuid().length() != 38) {

					String errMsg = "The entitlement GUID values passed in is invalid. Please verify in the request message the proper HMD entitlement GUID values are provided. ";
					logger.error(errMsg);
					throw new ProviderException(errMsg);
				}
				else {

					String convertedGuid = StringUtil
							.convertGuidFromString(resourceRequisition.getEntitlement().getEntitlementGuid());
					ESBJsonObject object = JsonUtils.convertStringToJSONObject(convertedGuid,
							resourceRequisition.getEntitlement().getEntitlementDN());
					entitlement.setEntitlementGuid(object.toJSONString());
					entitlement.setEntitlementDN(getHmdEntitlementDn());
					resource.setEntitlement(entitlement);
				}
			}
			else if (resourceRequisition.getEntitlement().getEntitlementApplication()
					.equalsIgnoreCase(IdmValues.IDV.getName())) {

				logger.info(LOGTAG + "EntitlementApplication is: " + IdmValues.IDV.getName());
				String paramGuidValue = null;
				try {
					paramGuidValue = ResourceServiceClient.doGetCodeMapValue(resourceService,
							resourceRequisition.getEntitlement().getEntitlementDN(), getIdvCodeMapKey());
				}
				catch (RemoteException re) {
					String errMsg = "Error making web service calls to NetIQ. Error Message: " + re.getMessage();
					logger.error(errMsg);
					throw new ProviderException(errMsg, re);
				}
				catch (NrfServiceExceptionException nsee) {
					if (nsee != null && nsee.getFaultMessage() != null
							&& nsee.getFaultMessage().getNrfServiceException() != null
							&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

						String errMsg = "Server error from NetIQ, please check log for more detail. Error Message: "
								+ nsee.getFaultMessage().getNrfServiceException().getReason();
						logger.error(errMsg);
						throw new ProviderException(errMsg, nsee);
					}
				}

				if (paramGuidValue != null && paramGuidValue.length() > 0) {
					logger.info(LOGTAG + "paramGuidValue: " + paramGuidValue);
					entitlement.setEntitlementGuid(paramGuidValue);
					entitlement.setEntitlementDN(getIdvEntitlementDn());
					resource.setEntitlement(entitlement);
				}
				else {
					String errMsg = "The entitlement value for IDV codeMapKey " + getCodeMapKey()
							+ " was not found in the NetIQ CodeMap. Please contact IDM for additional assistance.";
					logger.error(errMsg);
					throw new ProviderException(errMsg);
				}
			}
			else {
				String errMsg = "The entitlement application values can only be EAD, IDV, HMD or UMD. Please verify in the request message the proper values are provided. ";
				logger.error(errMsg);
				throw new ProviderException(errMsg);
			}

		}
		catch (EnterpriseFieldException efe) {
			String errMsg = "An error occurred setting the field values of Resource object. The exception is: "
					+ efe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, efe);
		}

		try {
			createResourceResponseDoc = ResourceServiceClient.doResourceCreate(resourceService, resource);
			if ((createResourceResponseDoc != null && createResourceResponseDoc.getCreateResourceResponse() != null
					&& createResourceResponseDoc.getCreateResourceResponse().getResult() != null
					&& createResourceResponseDoc.getCreateResourceResponse().getResult().length() > 0)) {

				aResourceDn = createResourceResponseDoc.getCreateResourceResponse().getResult().toString();
				resource.setResourceDN(aResourceDn);
			}
		}
		catch (NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

				String errMsg = "Server error from NetIQ, please check log for more detail. Error Message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}
		catch (RemoteException re) {
			String errMsg = "Error making web service calls to NetIQ. Error Message: " + re.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, re);
		}
		catch (EnterpriseFieldException efe) {
			String errMsg = "An error occurred setting the field values of Resource object. The exception is: "
					+ efe.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, efe);
		}

		if (aResourceDn != null && aResourceDn.length() > 0) {

			changedList.add(new SavedChanges<String, String>(IdmValues.RESOURCE.getName(), aResourceDn));
			resource = (Resource) waitForCompletion(resource, MessageControl.GENERATE_MSG_ACTION.getName(), changedList,
					resourceService);
			providerProcessingResponse.setSavedChanges(changedList);
			providerProcessingResponse.setResponse(resource);
			providerProcessingResponse.setResourceService(resourceService);
			logger.info(LOGTAG + "Finished Querying Resource");
		}

		try {
			resourceService._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return providerProcessingResponse;
	}

	@Override
	public Resource delete(Resource resource) throws ProviderException {

		logger.info(LOGTAG + "identifier.getValue(): " + resource.getResourceDN());
		ResourceServiceStub resourceService = null;
		try {
			resourceService = getResourceServiceStub(getServiceUserId(), getServicePassword(),
					getResourceServiceEndpoint(), getConnectionTimeout(), getSocketTimeout());
		}
		catch (RemoteException re) {
			String errMsg = "The Resource Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		if (resource != null && resource.getResourceDN() != null) {
			RemoveResourceResponseDocument removeResourceResponseDocument = null;
			try {
				removeResourceResponseDocument = ResourceServiceClient.doRemoveResource(resourceService,
						resource.getResourceDN());
			}
			catch (RemoteException re) {
				String errMsg = "Error making web service calls to NetIQ from delete(). Error Message: "
						+ re.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, re);
			}
			catch (NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

					String errMsg = "An error occurred deleting Resource object from NetIQ. Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.warn(LOGTAG + errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}
			if (removeResourceResponseDocument != null
					&& removeResourceResponseDocument.getRemoveResourceResponse() != null) {
				resource = (Resource) waitForCompletion(resource, MessageControl.DELETE_MSG_ACTION.getName(), null,
						resourceService);
			}
		}

		try {
			resourceService._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}
		return resource;
	}

	public Resource query(String queryData, List<SavedChanges<String, String>> changedList,
			ResourceServiceStub resourceService) throws ProviderException {

		GetResourceResponseDocument getResourceResponseDocument = null;
		TransactionScopeManager transactionManager = new TransactionScopeManager();
		Resource responseMoa = null;

		try {
			responseMoa = this.getObject(Resource.class);
		}
		catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from AppConfig: The exception is:";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, eoce);
		}

		try {
			getResourceResponseDocument = ResourceServiceClient.doResourceSearchByDN(resourceService, queryData);
		}
		catch (RemoteException re) {
			String errMsg = "An error occurred calling IDM NetIQ from Resource query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		catch (org.apache.axis2.client.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
				String errMsg = "A server error occurred retrieving Resource object from IDM NetIQ during query(). Error message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.warn(LOGTAG + errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}

		List<Entitlement> list = new ArrayList<Entitlement>();
		GetResourceResponse getResourceResponse = getResourceResponseDocument.getGetResourceResponse();
		if (getResourceResponse != null && getResourceResponse.getResult() != null
				&& getResourceResponse.getResult().getEntityKey() != null) {

			String resourceName = getResourceResponse.getResult().getName();
			try {
				responseMoa.setResourceName(resourceName);
				responseMoa.setResourceDN(queryData);
				responseMoa.setResourceDescription(getResourceResponse.getResult().getDescription());
				for (com.novell.www.resource.service.CategoryKey categoryKey : getResourceResponse.getResult()
						.getResourceCategoryKeys().getCategorykeyArray()) {
					responseMoa.setResourceCategoryKey(categoryKey.getCategoryKey());
					break;
				}
			}
			catch (EnterpriseFieldException efe) {
				if (changedList != null && changedList.size() > 0) {
					transactionManager.rollback(changedList, null, resourceService);
				}
				String errMsg = "An error occurred setting the field values of the Resource. The exception is: "
						+ efe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, efe);
			}
			for (NrfEntitlementRef ref : getResourceResponse.getResult().getEntitlementRef()
					.getNrfentitlementrefArray()) {
				Entitlement aEntitlement = new Entitlement();
				try {

					logger.info("entitlementDn: " + ref.getEntitlementDn());
					if (ref.getEntitlementParameters() != null) {

						String dnString = JsonUtils.convertJSONObjectToString(ref.getEntitlementParameters(),
								IdmValues.ID2.getName());
						if (dnString != null) {
							aEntitlement.setEntitlementDN(dnString);
						}

						logger.info("entitlementGuid: " + StringUtil.getUUIDFromString(ref.getEntitlementParameters()));
						if (aEntitlement.getEntitlementDN() != null && aEntitlement.getEntitlementDN().length() > 0) {
							aEntitlement.setEntitlementGuid(JsonUtils
									.convertJSONObjectToString(ref.getEntitlementParameters(), IdmValues.ID.getName()));
						}
						else {
							aEntitlement.setEntitlementDN(JsonUtils
									.convertJSONObjectToString(ref.getEntitlementParameters(), IdmValues.ID.getName()));
						}
					}
				}
				catch (EnterpriseFieldException efe) {
					if (changedList != null && changedList.size() > 0) {
						transactionManager.rollback(changedList, null, resourceService);
					}
					String errMsg = "An error occurred setting the field values of the Resource. The exception is: "
							+ efe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg, efe);
				}
				list.add(aEntitlement);
			}
			responseMoa.setEntitlement(list.get(0));
		}

		try {
			resourceService._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}
		return responseMoa;
	}

	/**
	 * Creates a Resource service stub for the remote object acts as a client's
	 * local proxy
	 * 
	 * The multithreaded is configured by setting a cached httpclient object when
	 * the service Stub is invoked before the actual requests are made.
	 * 
	 * @param userid
	 * @param password
	 * @param endpoint
	 * @param timeout
	 * @return
	 * @throws RemoteException
	 */
	private ResourceServiceStub getResourceServiceStub(String userid, String password, String endpoint, int timeout,
			int socketTimeout) throws RemoteException {

		ResourceServiceStub service = null;
		EndpointReference targetEPR = new EndpointReference(endpoint);
		service = new ResourceServiceStub();
		HttpTransportPropertiesImpl.Authenticator auth = new HttpTransportPropertiesImpl.Authenticator();
		auth.setUsername(userid);
		auth.setPassword(password);
		auth.setPreemptiveAuthentication(true);

		Options options = new Options();
		options.setProperty(HTTPConstants.AUTHENTICATE, auth);

		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		HttpConnectionManagerParams params = conmgr.getParams();

		params = new HttpConnectionManagerParams();
		params.setMaxTotalConnections(20);
		params.setDefaultMaxConnectionsPerHost(20);
		conmgr.setParams(params);

		conmgr.getParams().setDefaultMaxConnectionsPerHost(10);
		HttpClient client = new HttpClient(conmgr);
		options.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
		options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Boolean.TRUE);
		options.setTo(targetEPR);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
		options.setTimeOutInMilliSeconds(timeout);
		options.setProperty(HTTPConstants.SO_TIMEOUT, socketTimeout);
		options.setProperty(HTTPConstants.CONNECTION_TIMEOUT, timeout);
		service._getServiceClient().setOptions(options);

		return service;
	}

	public Resource query(String queryData, List<SavedChanges<String, String>> changedList) throws ProviderException {

		GetResourceResponseDocument getResourceResponseDocument = null;
		TransactionScopeManager transactionManager = new TransactionScopeManager();
		Resource responseMoa = null;
		ResourceServiceStub resourceService = null;

		try {
			resourceService = getResourceServiceStub(getServiceUserId(), getServicePassword(),
					getResourceServiceEndpoint(), getConnectionTimeout(), getSocketTimeout());
		}
		catch (RemoteException re) {
			String errMsg = "The Resource Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		try {
			responseMoa = this.getObject(Resource.class);
		}
		catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from AppConfig: The exception is:";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, eoce);
		}

		try {
			getResourceResponseDocument = ResourceServiceClient.doResourceSearchByDN(resourceService, queryData);
		}
		catch (RemoteException re) {
			String errMsg = "An error occurred calling IDM NetIQ from Resource query(), please check the log for more detail";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		catch (org.apache.axis2.client.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {
				String errMsg = "A server error occurred retrieving Resource object from IDM NetIQ during query(). Error message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.warn(LOGTAG + errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}

		List<Entitlement> list = new ArrayList<Entitlement>();
		GetResourceResponse getResourceResponse = getResourceResponseDocument.getGetResourceResponse();
		if (getResourceResponse != null && getResourceResponse.getResult() != null
				&& getResourceResponse.getResult().getEntityKey() != null) {

			String resourceName = getResourceResponse.getResult().getName();
			try {
				responseMoa.setResourceName(resourceName);
				responseMoa.setResourceDN(queryData);
				responseMoa.setResourceDescription(getResourceResponse.getResult().getDescription());
				for (com.novell.www.resource.service.CategoryKey categoryKey : getResourceResponse.getResult()
						.getResourceCategoryKeys().getCategorykeyArray()) {
					responseMoa.setResourceCategoryKey(categoryKey.getCategoryKey());
					break;
				}
			}
			catch (EnterpriseFieldException efe) {
				if (changedList != null && changedList.size() > 0) {
					transactionManager.rollback(changedList, null, resourceService);
				}
				String errMsg = "An error occurred setting the field values of the Resource. The exception is: "
						+ efe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg, efe);
			}
			for (NrfEntitlementRef ref : getResourceResponse.getResult().getEntitlementRef()
					.getNrfentitlementrefArray()) {
				Entitlement aEntitlement = new Entitlement();
				try {

					logger.info("entitlementDn: " + ref.getEntitlementDn());
					if (ref.getEntitlementParameters() != null) {

						String dnString = JsonUtils.convertJSONObjectToString(ref.getEntitlementParameters(),
								IdmValues.ID2.getName());
						if (dnString != null) {
							aEntitlement.setEntitlementDN(dnString);
						}

						logger.info("entitlementGuid: " + StringUtil.getUUIDFromString(ref.getEntitlementParameters()));
						if (aEntitlement.getEntitlementDN() != null && aEntitlement.getEntitlementDN().length() > 0) {
							aEntitlement.setEntitlementGuid(JsonUtils
									.convertJSONObjectToString(ref.getEntitlementParameters(), IdmValues.ID.getName()));
						}
						else {
							aEntitlement.setEntitlementDN(JsonUtils
									.convertJSONObjectToString(ref.getEntitlementParameters(), IdmValues.ID.getName()));
						}
					}
				}
				catch (EnterpriseFieldException efe) {
					if (changedList != null && changedList.size() > 0) {
						transactionManager.rollback(changedList, null, resourceService);
					}
					String errMsg = "An error occurred setting the field values of the Resource. The exception is: "
							+ efe.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg, efe);
				}
				list.add(aEntitlement);
			}
			responseMoa.setEntitlement(list.get(0));
		}
		return responseMoa;
	}

	/**
	 * 
	 * @param object
	 * @param action
	 * @return
	 * @throws ProviderException
	 */
	private <T> Object waitForCompletion(Object object, String action, List<SavedChanges<String, String>> changedList,
			ResourceServiceStub resourceService) throws ProviderException {

		boolean isCompleted = false;
		long startTime = System.currentTimeMillis();
		Resource resource = null;
		while (isCompleted == false) {

			// Throw an exception if maxWaitTime is exceeded.
			if (System.currentTimeMillis() - startTime > getMaxWaitTime()) {
				String errMsg = "Maximum wait time of " + getWaitInterval()
						+ " ms for completion of Resource object has been "
						+ "exceeded. Processing Resource may still be in progress.";
				throw new ProviderException(errMsg);
			}

			if (object instanceof Resource) {
				resource = query(((Resource) object).getResourceDN(), changedList);
				if (resource != null && resource.getResourceDN() != null && resource.getResourceDN().length() > 0) {
					logger.info(LOGTAG + "Found " + "Resource creation or deletion completed.");
					if (action.equalsIgnoreCase(MessageControl.GENERATE_MSG_ACTION.toString())) {
						object = resource;
						isCompleted = true;
						break;
					}
					else {
						// this is for waiting delete
						logger.info(LOGTAG + "Sleeping for " + getWaitInterval() + " ms...");
						try {
							Thread.sleep(getWaitInterval());
						}
						catch (InterruptedException swallowed) {
							logger.info(LOGTAG + "Thread interrupted, do nothing " + swallowed.getMessage());
						}
					}
				}
				else {
					if (action.equalsIgnoreCase(MessageControl.DELETE_MSG_ACTION.toString())) {
						isCompleted = true;
						logger.info(LOGTAG + "Resource is deleted successfully. ");
						break;
					}
					else {
						// this is for waiting create
						try {
							Thread.sleep(getWaitInterval());
						}
						catch (InterruptedException swallowed) {
							logger.info(LOGTAG + "Thread interrupted, do nothing " + swallowed.getMessage());
						}
					}
				}
			}
		}
		return object;
	}

	/**
	 * Converts oEAI object type to XmlEnterpriseObject
	 * 
	 * @param aClass
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 */
	@SuppressWarnings("unchecked")
	private <T extends XmlEnterpriseObjectImpl> T getObject(Class<T> aClass)
			throws EnterpriseConfigurationObjectException {
		return (T) getAppConfig().getObjectByType(aClass.getName());
	}

	public String getCodeMapKey() {
		return codeMapKey;
	}

	public void setCodeMapKey(String codeMapKey) {
		this.codeMapKey = codeMapKey;
	}

	private static long waitInterval = 8000;

	public static long getWaitInterval() {
		return waitInterval;
	}

	public static void setWaitInterval(long waitInterval) {
		NetiqResourceProvisioningProvider.waitInterval = waitInterval;
	}

	public static long getMaxWaitTime() {
		return maxWaitTime;
	}

	public static void setMaxWaitTime(long maxWaitTime) {
		NetiqResourceProvisioningProvider.maxWaitTime = maxWaitTime;
	}

	public AppConfig getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(AppConfig appConfig) {
		this.appConfig = appConfig;
	}

	// public ResourceServiceStub getResourceService() {
	// return resourceService;
	// }

	// public void setResourceService(ResourceServiceStub resourceService) {
	// this.resourceService = resourceService;
	// }

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public String getServiceUserId() {
		return serviceUserId;
	}

	public void setServiceUserId(String serviceUserId) {
		this.serviceUserId = serviceUserId;
	}

	public String getServicePassword() {
		return servicePassword;
	}

	public void setServicePassword(String servicePassword) {
		this.servicePassword = servicePassword;
	}

	public String getResourceServiceEndpoint() {
		return resourceServiceEndpoint;
	}

	public void setResourceServiceEndpoint(String resourceServiceEndpoint) {
		this.resourceServiceEndpoint = resourceServiceEndpoint;
	}

	public String getEadEntitlementDn() {
		return eadEntitlementDn;
	}

	public void setEadEntitlementDn(String eadEntitlementDn) {
		this.eadEntitlementDn = eadEntitlementDn;
	}

	public String getUmdEntitlementDn() {
		return umdEntitlementDn;
	}

	public void setUmdEntitlementDn(String umdEntitlementDn) {
		this.umdEntitlementDn = umdEntitlementDn;
	}

	public String getIdvCodeMapKey() {
		return idvCodeMapKey;
	}

	public void setIdvCodeMapKey(String idvCodeMapKey) {
		this.idvCodeMapKey = idvCodeMapKey;
	}

	public String getCategoryKey() {
		return categoryKey;
	}

	public void setCategoryKey(String categoryKey) {
		this.categoryKey = categoryKey;
	}

	public String getIdvEntitlementDn() {
		return idvEntitlementDn;
	}

	public void setIdvEntitlementDn(String idvEntitlementDn) {
		this.idvEntitlementDn = idvEntitlementDn;
	}

	public String getHmdEntitlementDn() {
		return hmdEntitlementDn;
	}

	public void setHmdEntitlementDn(String hmdEntitlementDn) {
		this.hmdEntitlementDn = hmdEntitlementDn;
	}
}
package org.rhedcloud.idm.service.provider.aws;

import org.apache.logging.log4j.Logger;

import com.amazonaws.services.ssoadmin.AWSSSOAdminClient;
import com.amazonaws.services.ssoadmin.model.AccountAssignmentOperationStatus;
import com.amazonaws.services.ssoadmin.model.DeleteAccountAssignmentResult;
import com.amazonaws.services.ssoadmin.model.DescribeAccountAssignmentDeletionStatusRequest;
import com.amazonaws.services.ssoadmin.model.DescribeAccountAssignmentDeletionStatusResult;
import com.amazonaws.services.ssoadmin.model.StatusValues;

public class AccountAssignmentDeleteWaiter implements Runnable {

	private static final String LOGTAG = "[DeleteWaiter] ";
	private final AWSSSOAdminClient clientSSO;
	private String accountAssignmentDeletionRequestId;
	private final long timeout;
	public AccountAssignmentOperationStatus accountAssignmentDeletionStatus;
	public RuntimeException runtimeException;
	private final String instanceArn;
	private final Logger logger;
	public long runningTime;


	public AccountAssignmentDeleteWaiter(String accountAssignmentDeletionRequestId, AWSSSOAdminClient clientSSO, long timeout, String instanceArn, Logger logger) {
		super();
		this.clientSSO = clientSSO;
		this.timeout = timeout;
		this.instanceArn = instanceArn;	
		this.accountAssignmentDeletionRequestId = accountAssignmentDeletionRequestId;
		this.logger = logger;
	}

	@Override
	public void run() {

		long start = System.currentTimeMillis(); 

		DescribeAccountAssignmentDeletionStatusResult daadsS;
		DescribeAccountAssignmentDeletionStatusRequest daadsQ = new DescribeAccountAssignmentDeletionStatusRequest()
				.withInstanceArn(instanceArn)
				.withAccountAssignmentDeletionRequestId(accountAssignmentDeletionRequestId);
		try {
			do {
				Thread.yield();
				daadsS = clientSSO.describeAccountAssignmentDeletionStatus(daadsQ);
				daadsS.getAccountAssignmentDeletionStatus().getStatus();
				runningTime = System.currentTimeMillis() - start;
				accountAssignmentDeletionStatus = daadsS.getAccountAssignmentDeletionStatus();
				logger.debug(LOGTAG+" accountAssignmentDeletionStatus ="+accountAssignmentDeletionStatus);
			} while (StatusValues.IN_PROGRESS.toString().equals(accountAssignmentDeletionStatus.getStatus())
					&& runningTime < timeout);
		} catch (RuntimeException e) {
			logger.error(LOGTAG+"describeAccountAssignmentDeletionStatus ERROR "+e);
			runtimeException = e;
		}			
	}
}

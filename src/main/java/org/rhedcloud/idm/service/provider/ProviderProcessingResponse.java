package org.rhedcloud.idm.service.provider;

import java.util.List;

import org.apache.axis2.client.ResourceServiceStub;
import org.rhedcloud.idm.service.util.SavedChanges;

import edu.emory.idm.service.axis2.RoleServiceStub;

/**
 * This class represents a response from processing provider service methods. It
 * is used as the need that we not only return business object to the command
 * class but also track all the calls made just in case the rollback is needed.
 * 
 * @author RXING2
 *
 */
public class ProviderProcessingResponse {

	/** Represents success of the operation */
	private boolean success = true;
	private Object response;
	private List<SavedChanges<String, String>> savedChanges;
	private ResourceServiceStub resourceService = null;
	private RoleServiceStub roleService = null;

	public ResourceServiceStub getResourceService() {
		return resourceService;
	}

	public void setResourceService(ResourceServiceStub resourceService) {
		this.resourceService = resourceService;
	}

	public RoleServiceStub getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleServiceStub roleService) {
		this.roleService = roleService;
	}

	public List<SavedChanges<String, String>> getSavedChanges() {
		return savedChanges;
	}

	public void setSavedChanges(List<SavedChanges<String, String>> savedChanges) {
		this.savedChanges = savedChanges;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean flag) {
		success = flag;
	}

}

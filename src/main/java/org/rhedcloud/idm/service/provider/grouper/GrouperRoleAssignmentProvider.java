package org.rhedcloud.idm.service.provider.grouper;

import java.io.StringReader;
import java.util.List;
import java.util.Properties;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.params.DefaultHttpParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.RoleAssignmentProvider;

import edu.emory.moa.jmsobjects.identity.v1_0.RoleAssignment;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentRequisition;

public class GrouperRoleAssignmentProvider extends OpenEaiObject implements RoleAssignmentProvider {

	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(GrouperRoleAssignmentProvider.class);
	AppConfig appConfig;
	private String LOGTAG = "[GrouperRoleAssignmentProvider] ";
	private String default_baseStem_IN_URL = "app%3Arhedcloud%3Aaws%3A";
	private String default_baseStem = "app:rhedcloud:aws:";
	private String baseStem_IN_URL = "app%3Arhedcloud%3Aaws%3A";
	private String baseStem = "app:rhedcloud:aws:";

	@Override
	public void init(AppConfig appConfig) throws ProviderException {
		info("init started...");
		this.appConfig = appConfig;
		try {
			Properties p = this.appConfig.getProperties("GrouperProperties");
			baseStem_IN_URL = p.getProperty("baseStem_IN_URL", default_baseStem_IN_URL);
			info("baseStem_IN_URL: " + baseStem_IN_URL);
			baseStem = p.getProperty("baseStem", default_baseStem);
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
		info("init complete...");
	}

	@Override
	public List<XmlEnterpriseObject> query(RoleAssignmentQuerySpecification queryData) throws ProviderException {
		// parse roleDn to get the account/group from "account:group" value passed
		// in
		try {
			info("actual query method - RoleAssignmentQuerySpecification passed in: " + queryData.toXmlString());
		} catch (XmlEnterpriseObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String roleDn = queryData.getRoleDN();
		String account = roleDn.substring(0, roleDn.indexOf(":"));
		String group = roleDn.substring(roleDn.indexOf(":") + 1);
		return getRoleAssignmentsForAccountAndGroup(account, group);
	}

	@Override
	public List<XmlEnterpriseObject> query(String queryData, String type) throws ProviderException {
		info("dummy query method - queryData and type passed in");
		info("queryData=" + queryData);
		info("type=" + type);
		
		try {
			GetMethod method = new GetMethod(RestClientSettings.URL + "/"
				+ WsRestType.xml.getWsLiteResponseContentType().name() + "/" + RestClientSettings.VERSION
				+ "/groups/" + baseStem_IN_URL + "[account]" + "%3A" + "[group]" + "/members");
			info("method's URI is: " + method.getURI());
		} catch (URIException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String responseRoleXml = "<RoleAssignment>\n"
				+ "			<RoleAssignmentType>USER_TO_ROLE</RoleAssignmentType>\n"
				+ "			<CauseIdentities/>\n" + "			<EffectiveDatetime>\n"
				+ "				<Year>2015</Year>\n" + "				<Month>02</Month>\n"
				+ "				<Day>01</Day>\n" + "				<Hour>11</Hour>\n"
				+ "				<Minute>12</Minute>\n" + "				<Second>13</Second>\n"
				+ "				<SubSecond>0</SubSecond>\n" + "				<Timezone>America/New_York</Timezone>\n"
				+ "			</EffectiveDatetime>\n" + "			<ExplicitIdentityDNs>\n"
				+ "				<DistinguishedName>cn=P4877359,ou=Users,ou=Data,o=EmoryDev (dummy grouper provider, queryData and type passed in)</DistinguishedName>\n"
				+ "			</ExplicitIdentityDNs>\n"
				+ "			<RoleDN>cn=RGR_AWS-308833937534-Administrator,cn=Level10,cn=RoleDefs,cn=RoleConfig,cn=AppConfig,cn=UserApplication,cn=DRIVERSET01,ou=Servers,o=EmoryDev</RoleDN>\n"
				+ "		</RoleAssignment>";
		try {
			RoleAssignment moa = (RoleAssignment) appConfig.getObject("RoleAssignment.v1_0");
			moa.buildObjectFromXmlString(responseRoleXml);
			List<XmlEnterpriseObject> results = new java.util.ArrayList<XmlEnterpriseObject>();
			results.add(moa);
			return results;
		}
		catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
		catch (XmlEnterpriseObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
	}

	@Override
	public RoleAssignment generate(RoleAssignmentRequisition roleAssignmentRequisition, String msgAction)
			throws ProviderException {

		try {
			RoleAssignment moa = (RoleAssignment) appConfig.getObject("RoleAssignment.v1_0");
			String member = roleAssignmentRequisition.getIdentityDN();
			moa.setIdentityDN(member);
			moa.setExplicitIdentityDNs(moa.newExplicitIdentityDNs());
			moa.getExplicitIdentityDNs().addDistinguishedName(member);
			for (int i = 0; i < roleAssignmentRequisition.getRoleDNs().getDistinguishedNameLength(); i++) {
				String distinguishedName = roleAssignmentRequisition.getRoleDNs().getDistinguishedName(i);
				String account = null;
				String group = null;
				if (distinguishedName.indexOf(":") >= 0) {
					// normal processing
					account = distinguishedName.substring(0, distinguishedName.indexOf(":"));
					group = distinguishedName.substring(distinguishedName.indexOf(":") + 1);
					this.addMember(account, group, member);
					moa.setRoleDN(account + ":" + group);
				}
				else {
					// assigning a member to the central admin role (higher level)
					group = distinguishedName;
					this.addMember(null, group, member);
					moa.setRoleDN(group);
				}
			}
			return moa;
		}
		catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
		catch (EnterpriseFieldException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
	}

	@Override
	public RoleAssignment delete(RoleAssignment queryData) throws ProviderException {
		String member = queryData.getIdentityDN();
		for (int i = 0; i < queryData.getRoleDNs().getDistinguishedNameLength(); i++) {
			String distinguishedName = queryData.getRoleDNs().getDistinguishedName(i);
			String account = null;
			String group = null;
			if (distinguishedName.indexOf(":") >= 0) {
				account = distinguishedName.substring(0, distinguishedName.indexOf(":"));
				group = distinguishedName.substring(distinguishedName.indexOf(":") + 1);
			}
			else {
				group = distinguishedName;
			}
			this.deleteMember(account, group, member);
		}

		return queryData;
	}

	@Override
	public RoleAssignment query(RoleAssignment queryData) throws ProviderException {
		String responseRoleXml = "<RoleAssignment>\n"
				+ "			<RoleAssignmentType>USER_TO_ROLE</RoleAssignmentType>\n"
				+ "			<CauseIdentities/>\n" + "			<EffectiveDatetime>\n"
				+ "				<Year>2015</Year>\n" + "				<Month>02</Month>\n"
				+ "				<Day>01</Day>\n" + "				<Hour>11</Hour>\n"
				+ "				<Minute>12</Minute>\n" + "				<Second>13</Second>\n"
				+ "				<SubSecond>0</SubSecond>\n" + "				<Timezone>America/New_York</Timezone>\n"
				+ "			</EffectiveDatetime>\n" + "			<ExplicitIdentityDNs>\n"
				+ "				<DistinguishedName>cn=P4877359,ou=Users,ou=Data,o=EmoryDev (dummy grouper provider, RoleAssignment passed in)</DistinguishedName>\n"
				+ "			</ExplicitIdentityDNs>\n"
				+ "			<RoleDN>cn=RGR_AWS-308833937534-Administrator,cn=Level10,cn=RoleDefs,cn=RoleConfig,cn=AppConfig,cn=UserApplication,cn=DRIVERSET01,ou=Servers,o=EmoryDev</RoleDN>\n"
				+ "		</RoleAssignment>";
		try {
			info("dummy query method - RoleAssignment passed in " + queryData.toXmlString());
			RoleAssignment moa = (RoleAssignment) appConfig.getObject("RoleAssignment.v1_0");
			moa.buildObjectFromXmlString(responseRoleXml);
			return moa;
		}
		catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
		catch (XmlEnterpriseObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
	}

	@Override
	public boolean isUserRoleBeingGranted(Object object) throws ProviderException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<XmlEnterpriseObject> query(String userDn, String roleDn, String identityType) throws ProviderException {
		// parse roleDn to get the account/group from "account:group" value passed
		// in
		info("actual query method - userDn, roleDn and identityType passed in");
		info("userDn=" + userDn);
		info("roleDn=" + roleDn);
		info("identityType=" + identityType);
		String account = roleDn.substring(0, roleDn.indexOf(":"));
		String group = roleDn.substring(roleDn.indexOf(":") + 1);
		return getRoleAssignmentsForAccountAndGroup(account, group);

	}

	/**
	 * get members web service with REST
	 * 
	 * @param wsSampleRestType is the type of rest (xml, xhtml, etc)
	 */
	public List<XmlEnterpriseObject> getRoleAssignmentsForAccountAndGroup(String account, String group) {

		info("getRoleAssignmentsForAccountAndGroup...begin.  Looking for all members of the " + group + " group in the "
				+ account + " account.");

		List<XmlEnterpriseObject> roleAssignments = new java.util.ArrayList<XmlEnterpriseObject>();
		try {
			HttpClient httpClient = new HttpClient();

			DefaultHttpParams.getDefaultParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(0, false));

			// URL e.g. http://localhost:8093/grouper-ws/servicesRest/v1_3_000/...
			// NOTE: aStem:aGroup urlencoded substitutes %3A for a colon
			// GET *baseURL*/groups/app%3Arhedcloud%3Aaws%3A123456789%3Aadmin

			GetMethod method = new GetMethod(RestClientSettings.URL + "/"
					+ WsRestType.xml.getWsLiteResponseContentType().name() + "/" + RestClientSettings.VERSION
					+ "/groups/" + baseStem_IN_URL + account + "%3A" + group + "/members");
			info("method's URI is: " + method.getURI());

			httpClient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(RestClientSettings.USER,
					RestClientSettings.PASS);

			// no keep alive so response if easier to indent for tests
			method.setRequestHeader("Connection", "close");

			// e.g. localhost and 8093
			httpClient.getState().setCredentials(new AuthScope(null, -1), defaultcreds);

			httpClient.executeMethod(method);

			// make sure a request came back
			Header successHeader = method.getResponseHeader("X-Grouper-success");
			String successString = successHeader == null ? null : successHeader.getValue();
			if (StringUtils.isBlank(successString)) {
				throw new RuntimeException("Web service did not even respond!");
			}
			boolean success = "T".equals(successString);
			String resultCode = method.getResponseHeader("X-Grouper-resultCode").getValue();

			String response = RestClientSettings.responseBodyAsString(method);
//			info("RAW Response from web service is: " + response);

			// convert to object (from xhtml, xml, json, etc)
			info("parsing the result from the ws...");

			/*
			 * <?xml version="1.0" encoding="UTF-8"?> <WsGetMembersLiteResult>
			 * <resultMetadata> <resultCode>SUCCESS</resultCode> <resultMessage>Success for:
			 * clientVersion: 2.4.0, wsGroupLookups: Array size: 1: [0]:
			 * WsGroupLookup[pitGroups=[],groupName=app:rhedcloud:aws:123456789:admin] ,
			 * memberFilter: All, includeSubjectDetail: false, actAsSubject: null,
			 * fieldName: null, subjectAttributeNames: null , paramNames: , params: null ,
			 * sourceIds: null , pointInTimeFrom: null, pointInTimeTo: null, pageSize: null,
			 * pageNumber: null, sortString: null, ascending: null</resultMessage>
			 * <success>T</success> </resultMetadata> <responseMetadata> <resultWarnings/>
			 * <millis>80</millis> <serverVersion>2.4.0</serverVersion> </responseMetadata>
			 * <wsGroup> <extension>admin</extension> <typeOfGroup>group</typeOfGroup>
			 * <displayExtension>admin</displayExtension> <description>admin role for this
			 * account</description>
			 * <displayName>app:rhedcloud:aws:123456789:admin</displayName>
			 * <name>app:rhedcloud:aws:123456789:admin</name>
			 * <uuid>5c4e8fda28cd478fb98b6e35af808e2f</uuid> <idIndex>10029</idIndex>
			 * </wsGroup> <wsSubjects> <WsSubject> <resultCode>SUCCESS</resultCode>
			 * <success>T</success> <id>jj</id> <sourceId>rhedcloud</sourceId> </WsSubject>
			 * </wsSubjects> </WsGetMembersLiteResult>
			 */
			Element root = null;
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(new StringReader(response));
			root = doc.getRootElement();
			Element eResultMetadata = root.getChild("resultMetadata");
			String metadataResultCode = eResultMetadata.getChildText("resultCode");
			String resultMessage = eResultMetadata.getChildText("resultMessage");
			Element eSubjects = root.getChild("wsSubjects");
			if (eSubjects != null && eSubjects.getChildren() != null) {
				info("There were " + eSubjects.getChildren().size() + " subjects returned");
				for (int i = 0; i < eSubjects.getChildren().size(); i++) {
					Element eSubject = (Element) eSubjects.getChildren().get(i);
					String subjectId = eSubject.getChildText("id");
					info("Subject ID: " + subjectId);
					try {
						RoleAssignment moa = (RoleAssignment) appConfig.getObject("RoleAssignment.v1_0");
						moa.setRoleDN(account + ":" + group);
						moa.setIdentityDN(subjectId);
						roleAssignments.add(moa);
					}
					catch (EnterpriseConfigurationObjectException e) {
						e.printStackTrace();
						throw new ProviderException(e.getMessage(), e);
					}
				}
			}
			else {
				info("No role assignments found for " + account + ":" + group);
			}

			// see if request worked or not
			if (!success) {
				throw new RuntimeException(
						"Bad response from web service: resultCode: " + metadataResultCode + ", " + resultMessage);
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		return roleAssignments;
	}

	public void addMember(String account, String group, String member) {

		info("[addMember] account: " + account);
		info("[addMember] group: " + group);
		info("[addMember] member: " + member);
		
		try {
			HttpClient httpClient = new HttpClient();

			DefaultHttpParams.getDefaultParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(0, false));

			// URL e.g. http://localhost:8093/grouper-ws/servicesRest/v1_3_000/...
			// NOTE: aStem:aGroup urlencoded substitutes %3A for a colon
			PutMethod method = null;
			
			if (account != null) {
				method = new PutMethod(RestClientSettings.URL + "/"
						+ WsRestType.xml.getWsLiteResponseContentType().name() + "/" + RestClientSettings.VERSION
						+ "/groups/" + baseStem_IN_URL + account + "%3A" + group + "/members/" + member);
			}
			else {
				method = new PutMethod(RestClientSettings.URL + "/"
						+ WsRestType.xml.getWsLiteResponseContentType().name() + "/" + RestClientSettings.VERSION
						+ "/groups/" + baseStem_IN_URL + group + "/members/" + member);
			}

			httpClient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(RestClientSettings.USER,
					RestClientSettings.PASS);

			// no keep alive so response if easier to indent for tests
			method.setRequestHeader("Connection", "close");
			info("method's URI is: " + method.getURI());

			// e.g. localhost and 8093
			httpClient.getState().setCredentials(new AuthScope(null, -1), defaultcreds);

			httpClient.executeMethod(method);

			// make sure a request came back
			Header successHeader = method.getResponseHeader("X-Grouper-success");
			String successString = successHeader == null ? null : successHeader.getValue();
			if (StringUtils.isBlank(successString)) {
				throw new RuntimeException("Web service did not even respond!");
			}
			boolean success = "T".equals(successString);
			String resultCode = method.getResponseHeader("X-Grouper-resultCode").getValue();

			String response = RestClientSettings.responseBodyAsString(method);
			info("response from addMember: " + response);

			// see if request worked or not
			if (!success) {
				throw new RuntimeException("Bad response from web service: resultCode: " + resultCode);
			}

		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public void deleteMember(String account, String group, String member) {

		try {
			HttpClient httpClient = new HttpClient();

			DefaultHttpParams.getDefaultParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(0, false));

			// URL e.g. http://localhost:8093/grouper-ws/servicesRest/v1_3_000/...
			// NOTE: aStem:aGroup urlencoded substitutes %3A for a colon
			String s = null;
			if (account != null) {
				s = account + "%3A" + group;
			}
			else {
				s = group;
			}
			DeleteMethod method = new DeleteMethod(
					RestClientSettings.URL + "/" + WsRestType.xml.getWsLiteResponseContentType().name() + "/"
							+ RestClientSettings.VERSION + "/groups/" + baseStem_IN_URL + s + "/members/" + member);

			httpClient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(RestClientSettings.USER,
					RestClientSettings.PASS);

			// no keep alive so response if easier to indent for tests
			method.setRequestHeader("Connection", "close");
			info("method's URI is: " + method.getURI());

			// e.g. localhost and 8093
			httpClient.getState().setCredentials(new AuthScope(null, -1), defaultcreds);

			httpClient.executeMethod(method);

			// make sure a request came back
			Header successHeader = method.getResponseHeader("X-Grouper-success");
			String successString = successHeader == null ? null : successHeader.getValue();
			if (StringUtils.isBlank(successString)) {
				throw new RuntimeException("Web service did not even respond!");
			}
			boolean success = "T".equals(successString);
			String resultCode = method.getResponseHeader("X-Grouper-resultCode").getValue();

			String response = RestClientSettings.responseBodyAsString(method);
			info("response from deleteMember: " + response);

			// see if request worked or not
			if (!success) {
				throw new RuntimeException("Bad response from web service: resultCode: " + resultCode);
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * @see edu.internet2.middleware.grouper.ws.samples.types.WsSampleRest#validType(edu.internet2.middleware.grouper.ws.WsRestType.types.WsSampleRestType)
	 */
	public boolean validType(WsRestType wsSampleRestType) {
		// dont allow http params
		return !WsRestType.http_json.equals(wsSampleRestType);
	}

	private void info(String message) {
		logger.info(LOGTAG + " - " + message);
	}
}

package org.rhedcloud.idm.service.provider.aws;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Datetime;
import org.rhedcloud.idm.service.provider.AwsProvisioningProvider;
import org.rhedcloud.idm.service.provider.ProviderException;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.IamRole;
import com.amazon.aws.moa.objects.resources.v1_0.IamRoleQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.InlinePolicy;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.AttachRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.AttachRolePolicyResult;
import com.amazonaws.services.identitymanagement.model.AttachUserPolicyRequest;
import com.amazonaws.services.identitymanagement.model.AttachedPolicy;
import com.amazonaws.services.identitymanagement.model.CreateRoleRequest;
import com.amazonaws.services.identitymanagement.model.CreateRoleResult;
import com.amazonaws.services.identitymanagement.model.DeleteRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteRoleRequest;
import com.amazonaws.services.identitymanagement.model.DeleteRoleResult;
import com.amazonaws.services.identitymanagement.model.DetachRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.GetRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.GetRolePolicyResult;
import com.amazonaws.services.identitymanagement.model.GetRoleRequest;
import com.amazonaws.services.identitymanagement.model.GetRoleResult;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesResult;
import com.amazonaws.services.identitymanagement.model.ListRolePoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListRolePoliciesResult;
import com.amazonaws.services.identitymanagement.model.ListRolesRequest;
import com.amazonaws.services.identitymanagement.model.ListRolesResult;
import com.amazonaws.services.identitymanagement.model.PutRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.PutRolePolicyResult;
import com.amazonaws.services.identitymanagement.model.Tag;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.amazonaws.services.securitytoken.model.Credentials;


/**
 * The class implements generate, query and delete actions for Policy message object.
 * 
 * @author tcerven
 * @version 1.0
 *
 */
public class IamRoleProvider extends OpenEaiObject implements AwsProvisioningProvider {

	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(IamRoleProvider.class);
	AppConfig appConfig;
	private String LOGTAG = "[IamRoleProvider] ";
	private String accessKeyId;
	private String secretKey;
	private String roleArnPattern;
	private int roleAssumptionDurationSeconds = 0;
	private String samlIdpName = null;
	private String samlIssuer = null;
	private int maxSessionDurationSeconds = 0;
	int maxResults = 100;

	/**
	 * Build an AmazonIdentityManagement client connected to the correct account with the correct role
	 * @param accountId account
	 * @return client
	 */
	private AmazonIdentityManagement buildIamClient(String accountId) {
		// Build the roleArn of the role to assume from the base ARN and
		// the account number in the query spec.
		String roleArn = roleArnPattern.replace("ACCOUNT_NUMBER", accountId);

		// Instantiate a basic credential provider
		BasicAWSCredentials creds = new BasicAWSCredentials(accessKeyId, secretKey);
		AWSStaticCredentialsProvider cp = new AWSStaticCredentialsProvider(creds);

		// Create the STS client
		AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.standard()
				.withRegion("us-east-1")
				.withCredentials(cp)
				.build();

		// Assume the appropriate role in the appropriate account.
		AssumeRoleRequest assumeRequest = new AssumeRoleRequest()
				.withRoleArn(roleArn)
				.withDurationSeconds(roleAssumptionDurationSeconds)
				.withRoleSessionName("AwsAccountService");

		AssumeRoleResult assumeResult = sts.assumeRole(assumeRequest);
		Credentials credentials = assumeResult.getCredentials();

		// Instantiate a credential provider
		BasicSessionCredentials temporaryCredentials = new BasicSessionCredentials(credentials.getAccessKeyId(),
				credentials.getSecretAccessKey(), credentials.getSessionToken());
		AWSStaticCredentialsProvider sessionCreds = new AWSStaticCredentialsProvider(temporaryCredentials);

		// Create the IAM client
		return AmazonIdentityManagementClientBuilder.standard()
				.withRegion("us-east-1")
				.withCredentials(sessionCreds)
				.build();
	}


	@SuppressWarnings("unused")
	@Override
	public void init(AppConfig appConfig) throws ProviderException {

		this.appConfig = appConfig;
		try {
			IamRole iamrole = (IamRole) appConfig.getObject("IamRole.v1_0");
			setProperties(this.appConfig.getProperties("IamRoleProviderProperties"));
		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg = "Can't load IamRole.v1_0";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg,e);
		}

		accessKeyId = getProperties().getProperty("accessKeyId");
		logger.info(LOGTAG + "accessKeyId is: " + accessKeyId);

		secretKey = getProperties().getProperty("secretKey");
		logger.info(LOGTAG + "secretKey is: present");

		roleArnPattern = getProperties().getProperty("roleArnPattern");
		logger.info(LOGTAG + "roleArnPattern is: " + roleArnPattern);

		roleAssumptionDurationSeconds = Integer.parseInt(getProperties().getProperty("roleAssumptionDurationSeconds"));
		logger.info(LOGTAG + "roleAssumptionDurationSeconds is: " + roleAssumptionDurationSeconds);

		samlIdpName = getProperties().getProperty("samlIdpName");
		logger.info(LOGTAG + "samlIdpName is: " + samlIdpName);

		samlIssuer = getProperties().getProperty("samlIssuer");
		logger.info(LOGTAG + "samlIssuer is: " + samlIssuer);

		if (getProperties().getProperty("maxSessionDurationSeconds") != null) {
			maxSessionDurationSeconds = Integer.parseInt(getProperties().getProperty("maxSessionDurationSeconds"));
		} else {
			maxSessionDurationSeconds = 43_200;
		}
		logger.info(LOGTAG + "maxSessionDurationSeconds is: " + maxSessionDurationSeconds);


		if (getProperties().getProperty("maxResults") != null) {
			maxResults = Integer.parseInt(getProperties().getProperty("maxResults"));
		} else {
			maxResults = 100;
		}
		logger.info(LOGTAG + "maxResults is: " + maxResults);


		logger.info(LOGTAG + "Initialization complete.");

	}

	private void deleteIamRole(IamRole iamrole) throws ProviderException {

		try {

			AmazonIdentityManagement iam = buildIamClient(iamrole.getAccountId());

			ListRolePoliciesRequest listRolePoliciesRequest = new ListRolePoliciesRequest()
					.withRoleName(iamrole.getName());
			ListRolePoliciesResult listRolePoliciesResult;
			do {
				listRolePoliciesResult = iam.listRolePolicies(listRolePoliciesRequest);

				for (String policyName : listRolePoliciesResult.getPolicyNames()) {
					DeleteRolePolicyRequest deleteRolePolicyRequest = new DeleteRolePolicyRequest()
							.withRoleName(iamrole.getName())
							.withPolicyName(policyName);
					iam.deleteRolePolicy(deleteRolePolicyRequest);
					logger.info(LOGTAG+"Deleted policy named "+policyName);
				}

				listRolePoliciesRequest.setMarker(listRolePoliciesResult.getMarker());
			} while (listRolePoliciesResult.isTruncated());


			ListAttachedRolePoliciesRequest listAttachedRolePoliciesRequest = new ListAttachedRolePoliciesRequest()
					.withRoleName(iamrole.getName());
			ListAttachedRolePoliciesResult listAttachedRolePoliciesResult;
			do {
				listAttachedRolePoliciesResult = iam.listAttachedRolePolicies(listAttachedRolePoliciesRequest);

				for (AttachedPolicy attachedPolicy : listAttachedRolePoliciesResult.getAttachedPolicies()) {
					DetachRolePolicyRequest detachRolePolicyRequest = new DetachRolePolicyRequest()
							.withRoleName(iamrole.getName())
							.withPolicyArn(attachedPolicy.getPolicyArn());
					iam.detachRolePolicy(detachRolePolicyRequest);
					logger.info(LOGTAG+"Detached policy named "+attachedPolicy.getPolicyName());
				}

				listAttachedRolePoliciesRequest.setMarker(listAttachedRolePoliciesResult.getMarker());
			} while (listAttachedRolePoliciesResult.isTruncated());

			DeleteRoleRequest deleteRoleRequest = new DeleteRoleRequest()
					.withRoleName(iamrole.getName());

			@SuppressWarnings("unused")
			DeleteRoleResult deleteRoleResult = iam.deleteRole(deleteRoleRequest);
			logger.info(LOGTAG+"IamRole deleted. ");

		} catch (RuntimeException e) {
			String errMsg = "An error occurred trying to delete the IamRole. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);       	      			
		}
	}

	private IamRole createIamRole(IamRole iamrole) throws ProviderException {

		try {

			CreateRoleRequest createRoleRequest = new CreateRoleRequest()
					.withRoleName(iamrole.getName())
					.withDescription(iamrole.getDescription())
					.withPath(iamrole.getPath()==null?"/":iamrole.getPath())
					.withAssumeRolePolicyDocument(iamrole.getAssumeRolePolicyDocument());

			if (iamrole.getMaxSessionDuration() != null) {
				try {
					createRoleRequest.setMaxSessionDuration(Integer.parseInt(iamrole.getMaxSessionDuration()));
				} catch (NumberFormatException e) {
					String errMsg = "MaxSessionDuration is not an integer or null: " +
							e.getMessage();
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg);       	      								
				}
			}
		
			
			AttachRolePolicyRequest a;

			if (iamrole.getTag().size()>0) {
				List<com.amazonaws.services.identitymanagement.model.Tag> amzTags = new ArrayList<>();
				@SuppressWarnings("unchecked")
				List<com.amazon.aws.moa.objects.resources.v1_0.Tag> moaTags = iamrole.getTag();
				for (com.amazon.aws.moa.objects.resources.v1_0.Tag moaTag: moaTags) {
					amzTags.add(
							new com.amazonaws.services.identitymanagement.model.Tag()
							.withKey(moaTag.getKey())
							.withValue(moaTag.getValue()));
				}
				createRoleRequest.setTags(amzTags);
			}
			AmazonIdentityManagement client = buildIamClient(iamrole.getAccountId());
			CreateRoleResult createRoleResult = client.createRole(createRoleRequest);
			
			for (String arn: (List<String>) iamrole.getManagedPolicyArn()) {
				AttachRolePolicyRequest arpq = new AttachRolePolicyRequest()
						.withPolicyArn(arn)
						.withRoleName(iamrole.getName());
				client.attachRolePolicy(arpq);
			}
			
			for (InlinePolicy inlinePolicy: (List<InlinePolicy>)iamrole.getInlinePolicy()) {
				PutRolePolicyRequest prpq = new PutRolePolicyRequest()
						.withRoleName(iamrole.getName())
						.withPolicyName(inlinePolicy.getName())
						.withPolicyDocument(inlinePolicy.getInlinePolicyDocument());
				client.putRolePolicy(prpq);
			}

			iamrole.setArn(createRoleResult.getRole().getArn());
			iamrole.setPath(createRoleResult.getRole().getPath());
			iamrole.setMaxSessionDuration(createRoleResult.getRole().getMaxSessionDuration()+"");
			iamrole.setCreateDatetime(new Datetime(createRoleResult.getRole().getCreateDate()));

			return iamrole;

		} catch (RuntimeException | EnterpriseFieldException e) {
			e.printStackTrace();
			String errMsg = "An error occurred trying to create the iamrole. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg,e);       	      			
		}

	}

	@Override
	public ActionableEnterpriseObject generate(XmlEnterpriseObject __, String msgAction)
			throws ProviderException {

		throw new ProviderException("Generate not implemented for IamRole. Use Create instead.");

	}

	@Override
	public ActionableEnterpriseObject delete(ActionableEnterpriseObject iamrole, String deleteAction) throws ProviderException {

		deleteIamRole((IamRole) iamrole);
		return iamrole;

	}

	@Override
	public ActionableEnterpriseObject update(ActionableEnterpriseObject naeo, XmlEnterpriseObject baeo) throws ProviderException {
		throw new ProviderException("Update not implemented for IamRole. Use Delete/Generate instead.");
	}


	@Override
	public List<ActionableEnterpriseObject> query(XmlEnterpriseObject querySpec1) throws ProviderException {
		IamRoleQuerySpecification querySpec = (IamRoleQuerySpecification) querySpec1;		
		try {

			Boolean withTags = Boolean.parseBoolean(querySpec.getWithTags());

			AmazonIdentityManagement iam = buildIamClient(querySpec.getAccountId());

			List<ActionableEnterpriseObject> results = new ArrayList<>();
			ListRolesResult listRolesResult;
			ListRolesRequest listRolesRequest  = new ListRolesRequest()
					.withMaxItems(maxResults);		
			do {
				listRolesResult = iam.listRoles(listRolesRequest);
				logger.info(LOGTAG+"Roles retrieved is "+listRolesResult.getRoles().size());
				listRolesRequest.setMarker(listRolesResult.getMarker());

				for (com.amazonaws.services.identitymanagement.model.Role candidate: listRolesResult.getRoles()) {

					String candidateName = candidate.getRoleName();
					logger.debug(LOGTAG+"Candidate rolename is "+candidate.getRoleName());
					if (querySpec.getName() != null 
							&& !candidate.getRoleName().equals(querySpec.getName())) {
						logger.debug(LOGTAG+"Candidate skipped because role name is not the same as the one in the query spec");
						continue;
					}
					if (querySpec.getPath() != null 
							&& !candidate.getPath().equals(querySpec.getPath())) {
						logger.debug(LOGTAG+"Candidate skipped because role path is not the same as the one in the query spec");
						continue;
					}
					String candidateArn = candidate.getArn();

					IamRole role = (IamRole) appConfig.getObject("IamRole.v1_0");
					role.setAccountId(querySpec.getAccountId());
					role.setName(candidate.getRoleName());
					role.setArn(candidateArn);
					role.setDescription(candidate.getDescription());
					role.setPath(candidate.getPath());
					role.setMaxSessionDuration(candidate.getMaxSessionDuration()+"");
					role.setCreateDatetime(new Datetime(candidate.getCreateDate()));
					role.setAssumeRolePolicyDocument(candidate.getAssumeRolePolicyDocument());

					// attached policies
					ListAttachedRolePoliciesRequest larpq = new ListAttachedRolePoliciesRequest()
							.withRoleName(role.getName());
					ListAttachedRolePoliciesResult larps;
					do {
						larps = iam.listAttachedRolePolicies(larpq);
						larpq.setMarker(larps.getMarker());
						for (AttachedPolicy attached:larps.getAttachedPolicies()) {
							role.addManagedPolicyArn(attached.getPolicyArn());
						}
					} while (larps.getMarker() != null);

					//inline policies
					ListRolePoliciesRequest lrpq = new ListRolePoliciesRequest()
							.withRoleName(role.getName());

					ListRolePoliciesResult lrps;
					do {
						lrps = iam.listRolePolicies(lrpq);
						lrpq.setMarker(lrps.getMarker());
						for (String pname:lrps.getPolicyNames()) {

							GetRolePolicyRequest grpq = new GetRolePolicyRequest()
									.withPolicyName(pname)
									.withRoleName(role.getName());

							GetRolePolicyResult grps = iam.getRolePolicy(grpq);

							InlinePolicy ip = role.newInlinePolicy();
							ip.setName(pname);
							ip.setInlinePolicyDocument(grps.getPolicyDocument());
							role.addInlinePolicy(ip);
						}
					} while (larps.getMarker() != null);

					if (withTags) {
						GetRoleRequest getRoleRequest = new GetRoleRequest()
								.withRoleName(candidateName);
						GetRoleResult getRoleResult = iam.getRole(getRoleRequest);
						role.setTag(awsTags2moaTags(getRoleResult.getRole().getTags(), role));
					}

					results.add(role);

					if (querySpec.getName() != null) {
						break;
					}

				}
				if (querySpec.getName() != null && results.size() == 1) {
					logger.debug(LOGTAG+"Found desired role");
					break;
				}

			} while ( listRolesResult.getMarker() != null );

			return results;

		} catch (RuntimeException | EnterpriseFieldException | EnterpriseConfigurationObjectException /*| EnterpriseObjectQueryException */e) {
			e.printStackTrace();
			String errMsg = "An error occurred: " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg,e);       	      			
		}	
	}

	private  List<com.amazon.aws.moa.objects.resources.v1_0.Tag> awsTags2moaTags(List<Tag> awstags, IamRole moarole) throws ProviderException {
		List<com.amazon.aws.moa.objects.resources.v1_0.Tag> moatags = new ArrayList<>();
		for (Tag awstag:awstags) {
			com.amazon.aws.moa.objects.resources.v1_0.Tag moatag = moarole.newTag();
			try {
				moatag.setKey(awstag.getKey());
				moatag.setValue(awstag.getValue());
				moatags.add(moatag);
			} catch (EnterpriseFieldException e) {
				throw new ProviderException("EnterpriseFieldException thrown",e);
			}
		}
		return moatags;
	}

	@Override
	public ActionableEnterpriseObject create(ActionableEnterpriseObject iamrole) throws ProviderException {

		return createIamRole((IamRole) iamrole);

	}
}

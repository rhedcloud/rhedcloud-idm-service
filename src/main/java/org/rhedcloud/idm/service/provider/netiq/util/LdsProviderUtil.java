package org.rhedcloud.idm.service.provider.netiq.util;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.util.Strings;
import org.apache.directory.ldap.client.api.DefaultLdapConnectionFactory;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapConnectionPool;
import org.apache.directory.ldap.client.api.ValidatingPoolableLdapConnectionFactory;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.XmlEnterpriseObject;
import org.rhedcloud.idm.service.provider.ProviderException;

import edu.emory.moa.jmsobjects.identity.v1_0.RoleAssignment;
import edu.emory.moa.objects.resources.v1_0.ExplicitIdentityDNs;

public final class LdsProviderUtil {
	private static final Logger logger = OpenEaiObject.logger;
	private static LdapConnectionPool ldapConnectionPool;



	public static LdapConnection getLdapConnection() throws LdapException {
		return ldapConnectionPool.getConnection();
	}

	public static void releaseLdapConnection(LdapConnection connection) {
		try {
			if (connection != null)
				ldapConnectionPool.releaseConnection(connection);
		} catch (LdapException e) {
			logger.error("Error releasing LDAP connection back to pool", e);
		}
	}

	public static synchronized void closeLdapPool() {
		if (ldapConnectionPool != null) {
			try {
				ldapConnectionPool.close();
			} catch (Exception e) {
				logger.error("Error while closing LDAP connection pool", e);
			}
			ldapConnectionPool = null;
		}
	}
	public static synchronized void initLdapPool(String ldapHost, int ldapPort, String adminDn, String adminPassword) {
		if (ldapConnectionPool != null) {
			return; // already initialized
		}

		LdapConnectionConfig config = new LdapConnectionConfig();
		config.setLdapHost(ldapHost);
		config.setLdapPort(ldapPort);
		config.setName(adminDn);
		config.setCredentials(adminPassword);
		//config.setTrustManagers(new NoVerificationTrustManager());
		config.setUseTls(true);

		DefaultLdapConnectionFactory factory = new DefaultLdapConnectionFactory(config);
		factory.setTimeOut(30000L); // default

		// optional, values below are defaults
		GenericObjectPool.Config poolConfig = new GenericObjectPool.Config();
		poolConfig.lifo = true;
		poolConfig.maxActive = 8;
		poolConfig.maxIdle = 8;
		poolConfig.maxWait = -1L;
		poolConfig.minEvictableIdleTimeMillis = 1000L * 60L * 30L;
		poolConfig.minIdle = 0;
		poolConfig.numTestsPerEvictionRun = 3;
		poolConfig.softMinEvictableIdleTimeMillis = -1L;
		poolConfig.testOnBorrow = false;
		poolConfig.testOnReturn = false;
		poolConfig.testWhileIdle = false;
		poolConfig.timeBetweenEvictionRunsMillis = -1L;
		poolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;

		ldapConnectionPool = new LdapConnectionPool(new ValidatingPoolableLdapConnectionFactory(factory), poolConfig);
	}
}

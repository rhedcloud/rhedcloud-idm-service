package org.rhedcloud.idm.service.provider.netiq.client;

import edu.emory.idm.service.axis2.RoleServiceStub;
import edu.emory.moa.jmsobjects.identity.v1_0.Role;
import edu.emory.moa.jmsobjects.identity.v1_0.RoleAssignment;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentRequisition;
import edu.emory.moa.objects.resources.v1_0.RoleRequisition;

import java.rmi.RemoteException;
import org.apache.axis2.client.NrfServiceExceptionException;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;

import com.novell.www.role.service.CategoryKeyArray;
import com.novell.www.role.service.CreateResourceAssociationRequest;
import com.novell.www.role.service.CreateResourceAssociationRequestDocument;
import com.novell.www.role.service.CreateResourceAssociationResponseDocument;
import com.novell.www.role.service.CreateRoleRequest;
import com.novell.www.role.service.CreateRoleRequestDocument;
import com.novell.www.role.service.CreateRoleResponseDocument;
import com.novell.www.role.service.DNString;
import com.novell.www.role.service.DNStringArray;
import com.novell.www.role.service.DeleteResourceAssociationRequest;
import com.novell.www.role.service.DeleteResourceAssociationRequestDocument;
import com.novell.www.role.service.DeleteResourceAssociationResponseDocument;
import com.novell.www.role.service.FindRoleByExampleWithOperatorRequest;
import com.novell.www.role.service.FindRoleByExampleWithOperatorRequestDocument;
import com.novell.www.role.service.FindRoleByExampleWithOperatorResponse;
import com.novell.www.role.service.FindRoleByExampleWithOperatorResponseDocument;
import com.novell.www.role.service.GetAssignedIdentitiesRequest;
import com.novell.www.role.service.GetAssignedIdentitiesRequestDocument;
import com.novell.www.role.service.GetAssignedIdentitiesResponseDocument;
import com.novell.www.role.service.GetGroupRequest;
import com.novell.www.role.service.GetGroupRequestDocument;
import com.novell.www.role.service.GetGroupResponseDocument;
import com.novell.www.role.service.GetResourceAssociationsRequest;
import com.novell.www.role.service.GetResourceAssociationsRequestDocument;
import com.novell.www.role.service.GetResourceAssociationsResponseDocument;
import com.novell.www.role.service.GetRoleAssignmentRequestStatusByIdentityTypeRequest;
import com.novell.www.role.service.GetRoleAssignmentRequestStatusByIdentityTypeRequestDocument;
import com.novell.www.role.service.GetRoleAssignmentRequestStatusByIdentityTypeResponseDocument;
import com.novell.www.role.service.GetRoleRequest;
import com.novell.www.role.service.GetRoleRequestDocument;
import com.novell.www.role.service.GetRoleResponseDocument;
import com.novell.www.role.service.GetUserRequest;
import com.novell.www.role.service.GetUserRequestDocument;
import com.novell.www.role.service.GetUserResponseDocument;
import com.novell.www.role.service.IdentityType;
import com.novell.www.role.service.LocalizedValue;
import com.novell.www.role.service.LocalizedValueArray;
import com.novell.www.role.service.RemoveRolesRequest;
import com.novell.www.role.service.RemoveRolesRequestDocument;
import com.novell.www.role.service.RemoveRolesResponseDocument;
import com.novell.www.role.service.RequestRolesAssignmentRequest;
import com.novell.www.role.service.RequestRolesAssignmentRequestDocument;
import com.novell.www.role.service.RequestRolesAssignmentResponseDocument;
import com.novell.www.role.service.ResourceAssociation;
import com.novell.www.role.service.RoleAssignmentActionType;
import com.novell.www.role.service.RoleAssignmentRequest;
import com.novell.www.role.service.RoleAssignmentType;
import com.novell.www.role.service.RoleRequest;

import org.rhedcloud.idm.service.provider.netiq.util.IdmValues;

/**
 * This web service Soap Client provides wrappers to allow local methods to
 * access NetIQ-IDM web services.
 *
 * @author RXING2 June 8, 2017
 */
public class RoleServiceClient {

	private static Logger logger = OpenEaiObject.logger;
	private static final String LOGTAG = "[RoleServiceClient] ";

	/**
	 *
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static RequestRolesAssignmentResponseDocument doRoleAssignmentGrant(RoleServiceStub service,
			RoleAssignmentRequisition queryData)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doRoleAssignmentRequest");
		RequestRolesAssignmentRequestDocument requestDoc = RequestRolesAssignmentRequestDocument.Factory.newInstance();
		RequestRolesAssignmentRequest requestAssignRequest = requestDoc.addNewRequestRolesAssignmentRequest();
		RoleAssignmentRequest assignRequest = requestAssignRequest.addNewAssignRequest();
		logger.info(LOGTAG + "RoleAssignmentActionType: " + queryData.getRoleAssignmentActionType());
		assignRequest.setActionType(RoleAssignmentActionType.GRANT);
		if (queryData.getRoleAssignmentType().equalsIgnoreCase("USER_TO_ROLE")) {
			assignRequest.setAssignmentType(RoleAssignmentType.USER_TO_ROLE);
		}
		else {
			logger.info("Assign Group To Role: " + queryData.getRoleAssignmentType());
			assignRequest.setAssignmentType(RoleAssignmentType.GROUP_TO_ROLE);
		}
		assignRequest.setIdentity(queryData.getIdentityDN());
		assignRequest.setReason(queryData.getReason());
		DNStringArray dnStringArray = DNStringArray.Factory.newInstance();
		DNString dnstring = DNString.Factory.newInstance();
		dnstring.setDn(queryData.getRoleDNs().getDistinguishedName(0));
		DNString[] dnarraynew = new DNString[1];
		dnarraynew[0] = dnstring;
		dnStringArray.setDnstringArray(dnarraynew);
		assignRequest.setRoles(dnStringArray);
		RequestRolesAssignmentResponseDocument responseDoc = null;
		responseDoc = service.requestRolesAssignment(requestDoc);
		return responseDoc;
	}

	/**
	 *
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static RequestRolesAssignmentResponseDocument doRoleAssignmentRevoke(RoleServiceStub service,
			RoleAssignment queryData) throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doRoleAssignmentRevoke()");
		RequestRolesAssignmentRequestDocument requestDoc = RequestRolesAssignmentRequestDocument.Factory.newInstance();
		RequestRolesAssignmentRequest requestAssignRequest = requestDoc.addNewRequestRolesAssignmentRequest();
		RoleAssignmentRequest assignRequest = requestAssignRequest.addNewAssignRequest();
		logger.info(LOGTAG + "RoleAssignmentActionType: " + queryData.getRoleAssignmentActionType());
		assignRequest.setActionType(RoleAssignmentActionType.REVOKE);
		if (queryData.getRoleAssignmentType().equalsIgnoreCase("USER_TO_ROLE")) {
			assignRequest.setAssignmentType(RoleAssignmentType.USER_TO_ROLE);
		}
		else {
			assignRequest.setAssignmentType(RoleAssignmentType.GROUP_TO_ROLE);
		}
		// assignRequest.setIdentity(queryData.getIdentityDN());
		assignRequest.setIdentity(queryData.getExplicitIdentityDNs().getDistinguishedName(0));
		assignRequest.setReason(queryData.getReason());
		DNStringArray dnStringArray = DNStringArray.Factory.newInstance();
		DNString dnstring = DNString.Factory.newInstance();
		dnstring.setDn(queryData.getRoleDNs().getDistinguishedName(0));
		DNString[] dnarraynew = new DNString[1];
		dnarraynew[0] = dnstring;
		dnStringArray.setDnstringArray(dnarraynew);
		assignRequest.setRoles(dnStringArray);
		RequestRolesAssignmentResponseDocument responseDoc = null;
		responseDoc = service.requestRolesAssignment(requestDoc);
		return responseDoc;
	}

	/**
	 *
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws NrfServiceExceptionException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static GetRoleAssignmentRequestStatusByIdentityTypeResponseDocument doRoleAssignmentSearchByType(
			RoleServiceStub service, RoleAssignmentQuerySpecification queryData) throws RemoteException,
			NrfServiceExceptionException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doRoleAssignmentSearchByType()");
		GetRoleAssignmentRequestStatusByIdentityTypeRequestDocument requestDoc = GetRoleAssignmentRequestStatusByIdentityTypeRequestDocument.Factory
				.newInstance();
		GetRoleAssignmentRequestStatusByIdentityTypeRequest queryRequest = requestDoc
				.addNewGetRoleAssignmentRequestStatusByIdentityTypeRequest();
		queryRequest.setIdentityDN(queryData.getRoleDN());
		if (queryData.getIdentityType().equalsIgnoreCase(IdmValues.USER_TYPE.getName())) {
			queryRequest.setIdentityType(IdentityType.USER);
		}
		else {
			queryRequest.setIdentityType(IdentityType.GROUP);
		}
		GetRoleAssignmentRequestStatusByIdentityTypeResponseDocument responseDoc = GetRoleAssignmentRequestStatusByIdentityTypeResponseDocument.Factory
				.newInstance();
		responseDoc = service.getRoleAssignmentRequestStatusByIdentityType(requestDoc);
		return responseDoc;
	}

	/**
	 * Retrieves all identities assigned to a role
	 *
	 * @param service
	 * @param queryData
	 * @return
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 * @throws Exception
	 */
	public static GetAssignedIdentitiesResponseDocument doRoleAssignedIdentitiesSearch(RoleServiceStub service,
			RoleAssignmentQuerySpecification queryData)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doRoleAssignedIdentitiesSearch()");
		GetAssignedIdentitiesRequestDocument requestDoc = GetAssignedIdentitiesRequestDocument.Factory.newInstance();
		GetAssignedIdentitiesRequest queryRequest = requestDoc.addNewGetAssignedIdentitiesRequest();
		if (queryData.getIdentityType().equalsIgnoreCase(IdmValues.USER_TYPE.getName())) {
			queryRequest.setIdentityType(IdentityType.USER);
		}
		else {
			queryRequest.setIdentityType(IdentityType.GROUP);
		}
		queryRequest.setRoleDN(queryData.getRoleDN());
		queryRequest.setDirectAssignOnly(true);
		GetAssignedIdentitiesResponseDocument responseDoc = GetAssignedIdentitiesResponseDocument.Factory.newInstance();
		responseDoc = service.getAssignedIdentities(requestDoc);
		return responseDoc;
	}

	/**
	 * Retrieves all the roles assigned to an identity
	 *
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws NrfServiceExceptionException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 * @throws Exception
	 */
	public static GetUserResponseDocument doGetUserSearch(RoleServiceStub service, String queryData)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doGetUserSearch()");
		GetUserRequestDocument requestDoc = GetUserRequestDocument.Factory.newInstance();
		GetUserRequest queryRequest = requestDoc.addNewGetUserRequest();
		queryRequest.setUserDN(queryData);
		GetUserResponseDocument responseDoc = GetUserResponseDocument.Factory.newInstance();
		responseDoc = service.getUser(requestDoc);
		return responseDoc;
	}

	/**
	 *
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static GetGroupResponseDocument doGetGroupSearch(RoleServiceStub service, String queryData)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doGetGroupSearch()");
		GetGroupRequestDocument requestDoc = GetGroupRequestDocument.Factory.newInstance();
		GetGroupRequest queryRequest = requestDoc.addNewGetGroupRequest();
		queryRequest.setGroupDN(queryData);
		GetGroupResponseDocument responseDoc = GetGroupResponseDocument.Factory.newInstance();
		responseDoc = service.getGroup(requestDoc);
		return responseDoc;
	}

	/**
	 *
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static GetRoleResponseDocument doGetRoleSearchByDn(RoleServiceStub service, String queryData)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doGetRoleSearchByDn()");
		GetRoleRequestDocument requestDoc = GetRoleRequestDocument.Factory.newInstance();
		GetRoleRequest request = requestDoc.addNewGetRoleRequest();
		request.setRoleDN(queryData);
		GetRoleResponseDocument responseDoc = GetRoleResponseDocument.Factory.newInstance();
		responseDoc = service.getRole(requestDoc);
		return responseDoc;
	}

	
	/**
	 *
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static FindRoleByExampleWithOperatorResponseDocument doFindRoleByExampleWithOperator(RoleServiceStub service, String queryData)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doFindRoleByExampleWithOperator()");
		FindRoleByExampleWithOperatorRequestDocument requestDoc = FindRoleByExampleWithOperatorRequestDocument.Factory.newInstance();
		FindRoleByExampleWithOperatorRequest queryRequest = requestDoc.addNewFindRoleByExampleWithOperatorRequest();
		com.novell.www.role.service.Role role = com.novell.www.role.service.Role.Factory.newInstance();
		role.setName(queryData);
		queryRequest.setRole(role);
		return service.findRoleByExampleWithOperator(requestDoc);
	}
	/**
	 *
	 * @param service
	 * @param roleDn
	 * @param resourceDn
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static GetResourceAssociationsResponseDocument doGetResourceAssociationsSearch(RoleServiceStub service,
			String roleDn, String resourceDn)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doGetUserSearch()");
		GetResourceAssociationsRequestDocument requestDoc = GetResourceAssociationsRequestDocument.Factory
				.newInstance();
		GetResourceAssociationsRequest request = requestDoc.addNewGetResourceAssociationsRequest();
		if (roleDn != null) {
			DNString roleDnstring = DNString.Factory.newInstance();
			roleDnstring.setDn(roleDn);
			request.setRoleDn(roleDnstring);
		}
		if (resourceDn != null) {
			DNString resourceDnString = DNString.Factory.newInstance();
			resourceDnString.setDn(resourceDn);
			request.setResourceDn(resourceDnString);
		}
		GetResourceAssociationsResponseDocument getResourceAssociociationResponseDoc = GetResourceAssociationsResponseDocument.Factory
				.newInstance();
		getResourceAssociociationResponseDoc = service.getResourceAssociations(requestDoc);
		if (getResourceAssociociationResponseDoc != null
				&& getResourceAssociociationResponseDoc.getGetResourceAssociationsResponse() != null
				&& getResourceAssociociationResponseDoc.getGetResourceAssociationsResponse().getResult()
						.sizeOfResourceassociationArray() > 0) {
			logger.info(LOGTAG + "resoucedn retrieved: " + getResourceAssociociationResponseDoc
					.getGetResourceAssociationsResponse().getResult().getResourceassociationArray(0).getResource());
		}
		else {
			logger.info(LOGTAG + "ResourceAssociationDn not found.");
		}
		return getResourceAssociociationResponseDoc;
	}

	/**
	 *
	 * @param roleService
	 * @param resourceAssociationDn
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static DeleteResourceAssociationResponseDocument doDeleteResourceAssociation(RoleServiceStub roleService,
			String resourceAssociationDn)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doDeleteResourceAssociation()");
		DeleteResourceAssociationRequestDocument requestDoc = DeleteResourceAssociationRequestDocument.Factory
				.newInstance();
		DeleteResourceAssociationRequest request = requestDoc.addNewDeleteResourceAssociationRequest();
		DNString dnstring = DNString.Factory.newInstance();
		dnstring.setDn(resourceAssociationDn);
		request.setResourceAssociationDn(dnstring);
		DeleteResourceAssociationResponseDocument responseDoc = DeleteResourceAssociationResponseDocument.Factory
				.newInstance();
		responseDoc = roleService.deleteResourceAssociation(requestDoc);
		return responseDoc;
	}

	/**
	 *
	 * @param roleService
	 * @param roleRequisition
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static CreateRoleResponseDocument doRoleCreate(RoleServiceStub roleService, RoleRequisition roleRequisition)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doRoleGeneration()");
		CreateRoleRequestDocument requestDoc = CreateRoleRequestDocument.Factory.newInstance();
		CreateRoleRequest request = requestDoc.addNewCreateRoleRequest();

		RoleRequest roleRequest = RoleRequest.Factory.newInstance();
		roleRequest.setDescription(roleRequisition.getRoleDescription());
		roleRequest.setName(roleRequisition.getRoleName());
		roleRequest.setRoleLevel(10);

		CategoryKeyArray categoryKeys = CategoryKeyArray.Factory.newInstance();
		categoryKeys.addNewCategorykey().setCategoryKey(roleRequisition.getRoleCategoryKey());
		roleRequest.setRoleCategoryKeys(categoryKeys);

		request.setRole(roleRequest);
		CreateRoleResponseDocument responseDoc = CreateRoleResponseDocument.Factory.newInstance();
		responseDoc = roleService.createRole(requestDoc);
		return responseDoc;
	}

	/**
	 *
	 * @param roleService
	 * @param roleDn
	 * @param resourceDn
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static CreateResourceAssociationResponseDocument doResourceAssociationCreate(RoleServiceStub roleService,
			String roleDn, String resourceDn)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doCreateResourceAssociation()");
		CreateResourceAssociationRequestDocument requestDoc = CreateResourceAssociationRequestDocument.Factory
				.newInstance();
		CreateResourceAssociationRequest request = requestDoc.addNewCreateResourceAssociationRequest();
		ResourceAssociation resourceAssociation = ResourceAssociation.Factory.newInstance();
		resourceAssociation.setResource(resourceDn);
		resourceAssociation.setRole(roleDn);

		LocalizedValueArray localizedValueArray = LocalizedValueArray.Factory.newInstance();
		LocalizedValue localizedValue = LocalizedValue.Factory.newInstance();
		localizedValue.setLocale(IdmValues.EN.getName());
		localizedValue.setValue("Provisioning");
		LocalizedValue[] localizedValues = new LocalizedValue[1];
		localizedValues[0] = localizedValue;
		localizedValueArray.setLocalizedvalueArray(localizedValues);
		resourceAssociation.setLocalizedDescriptions(localizedValueArray);

		request.setResourceAssociation(resourceAssociation);
		CreateResourceAssociationResponseDocument createResourceAssociationResponseDocument = CreateResourceAssociationResponseDocument.Factory
				.newInstance();
		createResourceAssociationResponseDocument = roleService.createResourceAssociation(requestDoc);

		if (createResourceAssociationResponseDocument != null
				&& createResourceAssociationResponseDocument.getCreateResourceAssociationResponse() != null
				&& createResourceAssociationResponseDocument.getCreateResourceAssociationResponse().getResult()
						.getRole().length() > 0) {
			logger.info(LOGTAG + "ResourceAssociationDn retrieved: " + createResourceAssociationResponseDocument
					.getCreateResourceAssociationResponse().getResult().getEntityKey());
		}
		else {
			String msg = "ResourceAssociationDn not retrieved. ";
			logger.info(LOGTAG + msg);
		}
		return createResourceAssociationResponseDocument;
	}

	/**
	 *
	 * @param roleService
	 * @param role
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static RemoveRolesResponseDocument doRemoveRole(RoleServiceStub roleService, Role role)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doRemoveRole()");
		RemoveRolesRequestDocument requestDoc = RemoveRolesRequestDocument.Factory.newInstance();
		RemoveRolesRequest request = requestDoc.addNewRemoveRolesRequest();

		DNStringArray dnStringArray = DNStringArray.Factory.newInstance();
		DNString dnstring = DNString.Factory.newInstance();
		dnstring.setDn(role.getRoleDN());
		DNString[] dnarraynew = new DNString[1];
		dnarraynew[0] = dnstring;
		dnStringArray.setDnstringArray(dnarraynew);
		request.setRoleDns(dnStringArray);

		RemoveRolesResponseDocument removeRolesResponseDocument = RemoveRolesResponseDocument.Factory.newInstance();
		removeRolesResponseDocument = roleService.removeRoles(requestDoc);
		if (removeRolesResponseDocument != null && removeRolesResponseDocument.getRemoveRolesResponse() != null
				&& removeRolesResponseDocument.getRemoveRolesResponse().getResult() != null) {
			logger.info(LOGTAG + "roleDN removed in doRemoveRole(): "
					+ removeRolesResponseDocument.getRemoveRolesResponse().getResult().getDnstringArray(0).getDn());
		}
		return removeRolesResponseDocument;
	}

	/**
	 *
	 * @param roleService
	 * @param dn
	 * @return
	 * @throws RemoteException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 */
	public static RemoveRolesResponseDocument doRemoveRole(RoleServiceStub roleService, String dn)
			throws RemoteException, edu.emory.idm.service.axis2.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doRemoveRole()");
		RemoveRolesRequestDocument requestDoc = RemoveRolesRequestDocument.Factory.newInstance();
		RemoveRolesRequest request = requestDoc.addNewRemoveRolesRequest();

		DNStringArray dnStringArray = DNStringArray.Factory.newInstance();
		DNString dnstring = DNString.Factory.newInstance();
		dnstring.setDn(dn);
		DNString[] dnarraynew = new DNString[1];
		dnarraynew[0] = dnstring;
		dnStringArray.setDnstringArray(dnarraynew);
		request.setRoleDns(dnStringArray);

		RemoveRolesResponseDocument removeRolesResponseDocument = RemoveRolesResponseDocument.Factory.newInstance();
		removeRolesResponseDocument = roleService.removeRoles(requestDoc);
		if (removeRolesResponseDocument != null && removeRolesResponseDocument.getRemoveRolesResponse() != null
				&& removeRolesResponseDocument.getRemoveRolesResponse().getResult() != null) {
			logger.info(LOGTAG + "roleDN removed: "
					+ removeRolesResponseDocument.getRemoveRolesResponse().getResult().getDnstringArray(0).getDn());
		}
		return removeRolesResponseDocument;
	}
}

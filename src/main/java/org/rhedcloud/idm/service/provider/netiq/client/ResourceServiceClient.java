package org.rhedcloud.idm.service.provider.netiq.client;

import edu.emory.moa.jmsobjects.identity.v1_0.Resource;

import java.rmi.RemoteException;

import org.apache.axis2.client.NrfServiceExceptionException;
import org.apache.axis2.client.ResourceServiceStub;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;

import com.novell.www.resource.service.CreateResourceRequest;
import com.novell.www.resource.service.CreateResourceRequestDocument;
import com.novell.www.resource.service.CreateResourceResponseDocument;
import com.novell.www.resource.service.GetCodeMapValuesRequest;
import com.novell.www.resource.service.GetCodeMapValuesRequestDocument;
import com.novell.www.resource.service.GetCodeMapValuesResponseDocument;
import com.novell.www.resource.service.GetResourceRequest;
import com.novell.www.resource.service.GetResourceRequestDocument;
import com.novell.www.resource.service.GetResourceResponseDocument;
import com.novell.www.resource.service.NrfEntitlementRef;
import com.novell.www.resource.service.NrfEntitlementRefArray;
import com.novell.www.resource.service.RemoveResourceRequest;
import com.novell.www.resource.service.RemoveResourceRequestDocument;
import com.novell.www.resource.service.RemoveResourceResponseDocument;

import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.netiq.util.IdmValues;

import com.novell.www.resource.service.CategoryKeyArray;
import com.novell.www.resource.service.CodeMapValue;

/**
 * This web service Soap Client provides wrappers to allow local methods to
 * access NetIQ-IDM web services.
 * 
 * @author RXING2
 */
public class ResourceServiceClient {

	private static Logger logger = OpenEaiObject.logger;
	private static final String LOGTAG = "[ResourceServiceClient] ";

	/**
	 * 
	 * @param service
	 * @param queryData
	 * @return
	 * @throws RemoteException
	 * @throws org.apache.axis2.client.NrfServiceExceptionException
	 */
	public static GetResourceResponseDocument doResourceSearchByDN(ResourceServiceStub service, String queryData)
			throws RemoteException, org.apache.axis2.client.NrfServiceExceptionException {

		GetResourceRequestDocument requestDoc = GetResourceRequestDocument.Factory.newInstance();
		GetResourceResponseDocument responseDoc = GetResourceResponseDocument.Factory.newInstance();
		GetResourceRequest request = requestDoc.addNewGetResourceRequest();
		request.setResourceDn(queryData);
		responseDoc = service.getResource(requestDoc);
		return responseDoc;
	}

	/**
	 * 
	 * @param resourceService
	 * @param resource
	 * @return
	 * @throws RemoteException
	 * @throws NrfServiceExceptionException
	 * @throws org.apache.axis2.client.NrfServiceExceptionException
	 */
	public static CreateResourceResponseDocument doResourceCreate(ResourceServiceStub resourceService,
			Resource resource)
			throws RemoteException, NrfServiceExceptionException, org.apache.axis2.client.NrfServiceExceptionException {
		logger.info(LOGTAG + "In doResourceGeneration() method");
		CreateResourceRequestDocument requestDoc = CreateResourceRequestDocument.Factory.newInstance();
		CreateResourceRequest request = requestDoc.addNewCreateResourceRequest();
		com.novell.www.resource.service.Resource nrfResource = com.novell.www.resource.service.Resource.Factory
				.newInstance();
		logger.info(LOGTAG + "resource.getEntitlement" + resource.getEntitlement().getEntitlementGuid());
		NrfEntitlementRefArray nrfEntitlementRefArray = NrfEntitlementRefArray.Factory.newInstance();
		NrfEntitlementRef nrfEntitlementref = NrfEntitlementRef.Factory.newInstance();
		String aEntitlementDn = resource.getEntitlement().getEntitlementDN();
		nrfEntitlementref.setEntitlementDn(aEntitlementDn);
		nrfEntitlementref.setEntitlementParameters(resource.getEntitlement().getEntitlementGuid());
		nrfEntitlementref.setSrc(IdmValues.UA.getName());

		NrfEntitlementRef[] nrfEntitlementRefs = new NrfEntitlementRef[1];
		nrfEntitlementRefs[0] = nrfEntitlementref;
		nrfEntitlementRefArray.setNrfentitlementrefArray(nrfEntitlementRefs);
		nrfResource.setEntitlementRef(nrfEntitlementRefArray);
		CategoryKeyArray categoryKeyArray = CategoryKeyArray.Factory.newInstance();

		if (resource.getResourceCategoryKey() != null && resource.getResourceCategoryKey().length() > 0) {
			categoryKeyArray.addNewCategorykey().setCategoryKey(resource.getResourceCategoryKey());
		}
		else {
			// System.out.println("setting here 2");
			categoryKeyArray.addNewCategorykey().setCategoryKey(IdmValues.GROUP.getName());
		}
		nrfResource.setDescription(resource.getResourceDescription());
		nrfResource.setName(resource.getResourceName());
		nrfResource.setEntitlementRef(nrfEntitlementRefArray);
		nrfResource.setResourceCategoryKeys(categoryKeyArray);
		request.setResource(nrfResource);

		CreateResourceResponseDocument responseDoc = CreateResourceResponseDocument.Factory.newInstance();
		responseDoc = resourceService.createResource(requestDoc);

		return responseDoc;
	}

	/**
	 * 
	 * @param resourceService
	 * @param entitlementDn
	 * @param codeMapKey
	 * @return
	 * @throws RemoteException
	 * @throws NrfServiceExceptionException
	 * @throws ProviderException
	 */
	public static String doGetCodeMapValue(ResourceServiceStub resourceService, String entitlementDn, String codeMapKey)
			throws RemoteException, NrfServiceExceptionException, ProviderException {

		GetCodeMapValuesRequestDocument requestDoc = GetCodeMapValuesRequestDocument.Factory.newInstance();
		GetCodeMapValuesRequest request = requestDoc.addNewGetCodeMapValuesRequest();
		request.setCodeMapKey(codeMapKey);
		GetCodeMapValuesResponseDocument responseDoc = GetCodeMapValuesResponseDocument.Factory.newInstance();
		responseDoc = resourceService.getCodeMapValues(requestDoc);
		String paramValue = null;
		if (responseDoc != null && responseDoc.getGetCodeMapValuesResponse() != null
				&& responseDoc.getGetCodeMapValuesResponse().getResult() != null) {

			if (responseDoc.getGetCodeMapValuesResponse().getResult().getCodemapvalueArray().length > 0) {
				for (CodeMapValue codeMapValue : responseDoc.getGetCodeMapValuesResponse().getResult()
						.getCodemapvalueArray()) {
					if (codeMapValue.getDisplayName().equalsIgnoreCase(entitlementDn)) {
						paramValue = codeMapValue.getParamValue();
						break;
					}
				}
				if (paramValue == null) {
					String errMsg = "The entitlement value for codeMapKey " + codeMapKey
							+ " was not found in the NetIQ CodeMap. Please contact IDM Admin for additional assistance.";
					logger.error(errMsg);
					throw new ProviderException(errMsg);
				}
			}
		}
		else {
			String errMsg = "There is no match for the CodeMapKey passed in. Please verify Code Map Key is the correct value";
			logger.error(errMsg);
			throw new ProviderException(errMsg);
		}
		return paramValue;
	}

	/**
	 * 
	 * @param resourceService
	 * @param resourceDn
	 * @return
	 * @throws RemoteException
	 * @throws NrfServiceExceptionException
	 * @throws org.apache.axis2.client.NrfServiceExceptionException
	 */
	public static RemoveResourceResponseDocument doRemoveResource(ResourceServiceStub resourceService,
			String resourceDn)
			throws RemoteException, NrfServiceExceptionException, org.apache.axis2.client.NrfServiceExceptionException {

		logger.info(LOGTAG + "In doRemoveResource()");
		RemoveResourceRequestDocument requestDoc = RemoveResourceRequestDocument.Factory.newInstance();
		RemoveResourceRequest request = requestDoc.addNewRemoveResourceRequest();
		com.novell.www.resource.service.DNString dnString = com.novell.www.resource.service.DNString.Factory
				.newInstance();
		dnString.setDn(resourceDn);
		request.setResourceDn(dnString);
		RemoveResourceResponseDocument responseDoc = RemoveResourceResponseDocument.Factory.newInstance();
		responseDoc = resourceService.removeResource(requestDoc);
		return responseDoc;
	}
}

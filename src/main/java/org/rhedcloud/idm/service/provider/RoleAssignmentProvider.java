package org.rhedcloud.idm.service.provider;

import java.util.List;
import org.openeai.config.AppConfig;
import org.openeai.moa.XmlEnterpriseObject;
import edu.emory.moa.jmsobjects.identity.v1_0.RoleAssignment;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentRequisition;

public interface RoleAssignmentProvider {

	public void init(AppConfig aConfig) throws ProviderException;

	public List<XmlEnterpriseObject> query(RoleAssignmentQuerySpecification queryData) throws ProviderException;

	public List<XmlEnterpriseObject> query(String queryData, String type) throws ProviderException;

	public RoleAssignment generate(RoleAssignmentRequisition roleAssignmentRequisition, String msgAction)
			throws ProviderException;

	public RoleAssignment delete(RoleAssignment queryData) throws ProviderException;

	RoleAssignment query(RoleAssignment queryData) throws ProviderException;

	public boolean isUserRoleBeingGranted(Object object) throws ProviderException;

	public List<XmlEnterpriseObject> query(String userDn, String roleDn, String identityType) throws ProviderException;
}

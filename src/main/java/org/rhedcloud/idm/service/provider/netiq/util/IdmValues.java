package org.rhedcloud.idm.service.provider.netiq.util;

public enum IdmValues {

	EAD("EAD"), IDV("IDV"), HMD("HMD"), EN("en"), GROUP("group"), DEFAULT("default"), ID("ID"), ID2("ID2"), UMD("UMD"),
	RESOURCE("Resource"), ROLE("Role"), USER_TYPE("USER"), GROUP_TYPE("GROUP"), USER_TO_ROLE("USER_TO_ROLE"),
	GROUP_TO_ROLE("GROUP_TO_ROLE"), RESOURCE_ASSOCIATION("ResourceAssociation"), UA("UA");

	private final String name;

	public String getName() {
		return name;
	}

	private IdmValues(String s) {
		this.name = s;
	}

	public String toString() {
		return name;
	}
}

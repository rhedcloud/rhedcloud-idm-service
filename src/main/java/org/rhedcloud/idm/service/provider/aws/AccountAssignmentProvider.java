package org.rhedcloud.idm.service.provider.aws;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.transport.RequestService;
import org.rhedcloud.idm.service.provider.AwsProvisioningProvider;
import org.rhedcloud.idm.service.provider.ProviderException;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountAssignment;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.IdentityCenterUser;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.PermissionSet;
import com.amazon.aws.moa.objects.resources.v1_0.AccountAssignmentQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.AccountAssignmentRequisition;
import com.amazon.aws.moa.objects.resources.v1_0.EmailAddress;
import com.amazon.aws.moa.objects.resources.v1_0.IdentityCenterUserQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.NameInfo;
import com.amazon.aws.moa.objects.resources.v1_0.PhoneNumber;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.identitystore.AWSIdentityStoreClient;
import com.amazonaws.services.identitystore.AWSIdentityStoreClientBuilder;
import com.amazonaws.services.identitystore.model.DescribeGroupRequest;
import com.amazonaws.services.identitystore.model.DescribeGroupResult;
import com.amazonaws.services.identitystore.model.DescribeUserRequest;
import com.amazonaws.services.identitystore.model.DescribeUserResult;
import com.amazonaws.services.identitystore.model.Filter;
import com.amazonaws.services.identitystore.model.ListGroupsRequest;
import com.amazonaws.services.identitystore.model.ListGroupsResult;
import com.amazonaws.services.identitystore.model.ResourceNotFoundException;
import com.amazonaws.services.ssoadmin.AWSSSOAdminClient;
import com.amazonaws.services.ssoadmin.AWSSSOAdminClientBuilder;
import com.amazonaws.services.ssoadmin.model.CreateAccountAssignmentRequest;
import com.amazonaws.services.ssoadmin.model.CreateAccountAssignmentResult;
import com.amazonaws.services.ssoadmin.model.DeleteAccountAssignmentRequest;
import com.amazonaws.services.ssoadmin.model.DeleteAccountAssignmentResult;
import com.amazonaws.services.ssoadmin.model.DescribeAccountAssignmentCreationStatusRequest;
import com.amazonaws.services.ssoadmin.model.ListAccountAssignmentsRequest;
import com.amazonaws.services.ssoadmin.model.ListAccountAssignmentsResult;
import com.amazonaws.services.ssoadmin.model.StatusValues;

import edu.emory.moa.jmsobjects.identity.v1_0.DirectoryPerson;
import edu.emory.moa.objects.resources.v1_0.DirectoryPersonQuerySpecification;

public class AccountAssignmentProvider extends OpenEaiObject implements AwsProvisioningProvider {

	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(AccountAssignmentProvider.class);
	AppConfig appConfig;
	private String LOGTAG = "[AccountAssignmentProvider] ";
	private String instanceArn;
	private String identityStoreId;
	private String accessKeyId;
	private String secretKey;
	private AWSIdentityStoreClient clientIS;
	private AWSSSOAdminClient clientSSO;
	private long millisBetweenStatusRequests;
	private int maxStatusRequests=5;
	private boolean createUserIfNotFound;
	Properties awsUtilsProps;
	private ProducerPool producerPool;
	private int requestTimeoutIntervalInMillis = 30_000;
	private boolean ignoreP2PException = false; //set to true for local debugging

	@SuppressWarnings("unused")
	@Override
	public void init(AppConfig appConfig) throws ProviderException {
		logger.info(LOGTAG+"init started...");
		this.appConfig = appConfig;

		try {
			AccountAssignment __ = (AccountAssignment) appConfig.getObject("AccountAssignment.v1_0");
			AccountAssignmentRequisition ___ = (AccountAssignmentRequisition) appConfig.getObject("AccountAssignmentRequisition.v1_0");
			IdentityCenterUser ____ = (IdentityCenterUser) appConfig.getObject("IdentityCenterUser.v1_0");
			IdentityCenterUserQuerySpecification _____ = 
					(IdentityCenterUserQuerySpecification) appConfig.getObject("IdentityCenterUserQuerySpecification.v1_0");

			setProperties(this.appConfig.getProperties("AccountAssignmentProviderProperties"));
			awsUtilsProps = appConfig.getProperties("AwsProviderUtilsProperties");
			createUserIfNotFound = Boolean.parseBoolean(getProperties().getProperty("createUserIfNotFound"));
			logger.info(LOGTAG + "createUserIfNotFound is: " + createUserIfNotFound);
			if (createUserIfNotFound) {
				DirectoryPerson ______ = (DirectoryPerson) appConfig.getObject("DirectoryPerson.v1_0");
				DirectoryPersonQuerySpecification _______ = 
						(DirectoryPersonQuerySpecification) appConfig.getObject("DirectoryPersonQuerySpecification.v1_0");
				try {
					producerPool = (ProducerPool) appConfig.getObject("DirectoryRequestService");
				} catch (EnterpriseConfigurationObjectException e) {
					if (ignoreP2PException ) {
						e.printStackTrace();
						System.out.println(LOGTAG+"P2P exception ignored");
					} else {
						throw e;
					}
				}
			}

		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg = "Can't load one or more of the following objects:"
					+" AccountAssignment.v1_0, AccountAssignmentRequisition.v1_0,"
					+" IdentityCenterUser.v1_0, IdentityCenterUserQuerySpecification.v1_0, DirectoryRequestService";
			if (createUserIfNotFound) errmsg += ",DirectoryPerson.v1_0 or DirectoryPersonQuerySpecification.v1_0";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg,e);
		}
		// Get properties.
		logger.info(LOGTAG + "Getting provider properties...");

		accessKeyId = getProperties().getProperty("accessKeyId");
		logger.info(LOGTAG + "accessKeyId is: " + accessKeyId);

		secretKey = getProperties().getProperty("secretKey");
		logger.info(LOGTAG + "secretKey is: present");

		identityStoreId = getProperties().getProperty("identityStoreId");
		logger.info(LOGTAG + "identityStoreId is: " + identityStoreId);

		instanceArn = getProperties().getProperty("instanceArn");
		logger.info(LOGTAG + "instanceArn is: " + instanceArn);

		millisBetweenStatusRequests = Long.parseLong(getProperties().getProperty("millisBetweenStatusRequests","1000"));
		logger.info(LOGTAG + "millisBetweenStatusRequests is: " + millisBetweenStatusRequests);

		maxStatusRequests = Integer.parseInt(getProperties().getProperty("maxStatusRequests","10"));
		logger.info(LOGTAG + "maxStatusRequests is: " + maxStatusRequests);


		logger.info(LOGTAG + "AwsProviderUtilsProps =  " + awsUtilsProps);

		AWSStaticCredentialsProvider creds = new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, secretKey));

		// Instantiate an AWS client builder
		AWSIdentityStoreClientBuilder builderIS = AWSIdentityStoreClientBuilder.standard().withCredentials(creds);
		// Initialize the AWS client
		logger.info(LOGTAG+"Initializing AWSIdentityStoreClient...");
		clientIS = (AWSIdentityStoreClient) builderIS.build();
		logger.info(LOGTAG+"AWSIdentityStoreClient initialized.");

		// Instantiate an AWS client builder
		AWSSSOAdminClientBuilder builderIAM = AWSSSOAdminClientBuilder.standard().withCredentials(creds);
		// Initialize the AWS client
		logger.info(LOGTAG+"Initializing AWSSSOAdminClient...");
		clientSSO = (AWSSSOAdminClient) builderIAM.build();
		logger.info(LOGTAG+"AWSSSOAdminClient initialized.");

	}

	private String getPermissionSetArn(String permissionSetName) throws ProviderException {

		logger.info(LOGTAG+"permissionSetName="+permissionSetName);

		//Check for the ARN is stored in the config file with the name as the key to speed up retrival of oft used perm sets
		String permissionSetArn = getProperties().getProperty(permissionSetName);		
		if (permissionSetArn != null) {
			return permissionSetArn;
		}

		List<ActionableEnterpriseObject> permissionSets = AwsProviderUtils.getPermissionSet(
				permissionSetName, 
				clientSSO, 
				appConfig, 
				instanceArn,
				false, false, false);

		if (permissionSets.size() == 0) {
			String msg="The permissionSet named '"+permissionSetName
					+"' is not found in the AWS IDM instanceArn "+instanceArn;
			logger.error(LOGTAG+msg);
			throw new ProviderException(msg);			
		}

		if (permissionSets.size() > 1) {
			logger.warn(LOGTAG+"There are "+permissionSets.size()+" permission sets named "+permissionSetName
					+" which should not happen. Using the first");
		}

		return ((PermissionSet) permissionSets.get(0)).getArn();
	}

	private void deleteAccountAssignment(AccountAssignment aa) throws ProviderException {
		logger.info(LOGTAG+"deleteAccountAssignment for aa="+aa);

		try {

			DeleteAccountAssignmentRequest deletereq = new DeleteAccountAssignmentRequest()
					.withInstanceArn(instanceArn)
					.withPermissionSetArn(getPermissionSetArn(aa.getPermissionSetName()))
					.withPrincipalId(aa.getPrincipalId())
					.withPrincipalType(aa.getPrincipalType())
					.withTargetId(aa.getTargetId())
					.withTargetType(aa.getTargetType());	

			DeleteAccountAssignmentResult deleterep = clientSSO.deleteAccountAssignment(deletereq);
			logger.info(LOGTAG+"Reply from delete account assignment: "+deleterep);


			String requestId = deleterep.getAccountAssignmentDeletionStatus().getRequestId();
			AccountAssignmentDeleteWaiter aadw = new AccountAssignmentDeleteWaiter(requestId, clientSSO, requestTimeoutIntervalInMillis, instanceArn, logger);
			Thread t = new Thread(aadw);
			t.start();
			t.join();

			if (aadw.runtimeException != null) {
				String errMsg = "Account assignment deletion status returned a RuntimeException: "+aadw.runtimeException;
				logger.error(LOGTAG+errMsg);
				throw new ProviderException(errMsg);
			}

			if (StatusValues.IN_PROGRESS.toString().equals(aadw.accountAssignmentDeletionStatus.getStatus()) ) {
				String 	errMsg = "Delete account assignment did not complete in the allowed time. Request ID = "
						+deleterep.getAccountAssignmentDeletionStatus().getRequestId();
				logger.error(LOGTAG+errMsg);
				throw new ProviderException(errMsg);
			}			

			if (!StatusValues.SUCCEEDED.toString().equals(aadw.accountAssignmentDeletionStatus.getStatus()) ) {
				String errMsg = "Account assignment deletion status returned: "+aadw.accountAssignmentDeletionStatus.getStatus();
				logger.error(LOGTAG+errMsg);
				throw new ProviderException(errMsg);
			}			

			logger.info(LOGTAG+"AccountAssignment deleted in "+aadw.runningTime+" ms.");

		} catch (RuntimeException e) {
			String errMsg = "An error occurred trying to create the account assignment. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);       	      			
		} catch (InterruptedException e) {
			String errmsg="InterruptedException thrown";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		}
	}


	private AccountAssignment createAccountAssignment(AccountAssignmentRequisition aar) throws ProviderException {
		logger.info(LOGTAG+"createAccountAssignment for aar="+aar);

		validatePrincipalType(aar.getPrincipalType());
		validateTargetType(aar.getTargetType());

		AccountAssignment aa = null;

		try {
			aa = (AccountAssignment) appConfig.getObject("AccountAssignment.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg="AccountAssignment.v1_0 is not found in the config.";
			logger.fatal(LOGTAG+errmsg);
			throw new ProviderException(errmsg);	
		}
		String permissionSetArn = getPermissionSetArn(aar.getPermissionSetName());

		String principalId = null;

		//Compute the principal ID based on the principal type

		if ("USER".equals(aar.getPrincipalType())) {
			String ppId = aar.getPublicId();

			logger.info(LOGTAG+"ppId is "+ppId); // e.g. "P1234567"

			IdentityCenterUserQuerySpecification querySpec;
			try {
				querySpec = (IdentityCenterUserQuerySpecification) appConfig.getObject("IdentityCenterUserQuerySpecification.v1_0");
				querySpec.setUserName(ppId);
			} catch (EnterpriseConfigurationObjectException e) {
				String errmsg="IdentityCenterUser.v1_0 or IdentityCenterUserQuerySpecification is not found in the config.";
				logger.fatal(LOGTAG+errmsg);
				throw new ProviderException(errmsg);	
			} catch (EnterpriseFieldException e) {
				logger.fatal(LOGTAG+e.getMessage());
				throw new ProviderException("Can't set username for query",e);	
			}

			List<ActionableEnterpriseObject> results = AwsProviderUtils.getIdentityCenterUser(
					querySpec, clientIS, appConfig, identityStoreId, awsUtilsProps);	
			if (results.size() == 0 ) {
				if (createUserIfNotFound) {
					logger.info(LOGTAG+"Creating new user with username = "+ ppId+"  -");
					IdentityCenterUser newicu = null;
					try {
						DirectoryPerson dp = (DirectoryPerson) appConfig.getObject("DirectoryPerson.v1_0");
						DirectoryPersonQuerySpecification dpq = 
								(DirectoryPersonQuerySpecification) appConfig.getObject("DirectoryPersonQuerySpecification.v1_0");
						dpq.setKey(ppId);
						@SuppressWarnings("unchecked")
						List<DirectoryPerson> dps = dp.query(dpq, getDSProducer(LOGTAG));
						if (dps.size() > 1) {
							String errMsg = "DirectoryPerson query returned more than one. Using the first to create the user";
							logger.warn(LOGTAG+errMsg);
						}
						if (dps.size() == 0) {
							String errMsg = "Can't find a DirectoryPerson for key = "+ppId;
							logger.error(LOGTAG+errMsg);
							throw new ProviderException(errMsg);
						}
						dp = dps.get(0); 
						logger.info(LOGTAG+"Found Directory Person: "+dp);

						//change this to use the AwsProviderUtils
						newicu = (IdentityCenterUser) appConfig.getObject("IdentityCenterUser.v1_0");
						newicu.setUserName(ppId);
						newicu.setType(null2unknown(dp.getType()));
						NameInfo nameInfo = newicu.newNameInfo();
						nameInfo.setGivenName(null2unknown(dp.getFirstMiddle()));
						nameInfo.setFormattedName(null2unknown(dp.getFullName()));
						nameInfo.setFamilyName(null2unknown(dp.getLastName()));
						nameInfo.setHonorificSuffix(null2unknown(dp.getSuffix()));
						newicu.setNameInfo(nameInfo);
						newicu.setDisplayName(null2unknown(dp.getFullName()));
						newicu.setTitle(null2unknown(dp.getTitle()));
						if (dp.getEmail() != null) {
							EmailAddress emailAddress = newicu.newEmailAddress();
							emailAddress.setValue(null2unknown(dp.getEmail().getEmailAddress()));
							emailAddress.setType(null2unknown(dp.getEmail().getType()));
							emailAddress.setPrimary("Primary".equals(null2unknown(emailAddress.getType()))+"");
							newicu.addEmailAddress(emailAddress);
						}
						if (dp.getDirectoryPhone() != null) {
							PhoneNumber phoneNumber = newicu.newPhoneNumber();
							phoneNumber.setValue(null2unknown(dp.getDirectoryPhone()));
							newicu.addPhoneNumber(phoneNumber);
						}
						newicu = AwsProviderUtils.createIdentityCenterUser(newicu, appConfig, awsUtilsProps, identityStoreId, clientIS);
						principalId = newicu.getUserId();
						logger.info(LOGTAG+"New user created: "+newicu.toXmlString());
					} catch (EnterpriseConfigurationObjectException e) {
						logger.fatal(LOGTAG+e.getMessage());
						throw new ProviderException("Can't load DirectoryPerson.v1_0 or DirectoryPerson.v1_0",e);	
					} catch (EnterpriseFieldException e) {
						logger.fatal(LOGTAG+e.getMessage());
						throw new ProviderException("Can't set key for query",e);	
					} catch (EnterpriseObjectQueryException e) {
						logger.error(LOGTAG+e.getMessage());
						throw new ProviderException("Can't query for directory person",e);	
					} catch (XmlEnterpriseObjectException e) {
						e.printStackTrace();
					} catch (ProviderException e) {
						if (e.getCause() != null && e.getCause().getMessage() != null && e.getCause().getMessage().startsWith("Duplicate unique attribute value for emails.value")) {
							// this user might have already been created by a previous account assignment create. Requery to find out.
							try {
								logger.warn(LOGTAG+"This user may have already been created. Querying for user...");
								IdentityCenterUserQuerySpecification icuq = (IdentityCenterUserQuerySpecification) appConfig.getObject("IdentityCenterUserQuerySpecification.v1_0");
								icuq.setUserName(newicu.getUserName());
								List<ActionableEnterpriseObject> candidates = AwsProviderUtils.getIdentityCenterUser(icuq, clientIS, appConfig, identityStoreId, awsUtilsProps);
								if (candidates.size() == 0) {
									String errmsg="An error occurred trying to create the IdentityCenterUser. The user appeared to have already been created with the same"
											+" email.value as desired but it is not returned in a query. Will throw original exception.";
									logger.error(LOGTAG+errmsg);
								} else {
									IdentityCenterUser candidate = (IdentityCenterUser) candidates.get(0);
									if (candidate.getUserName().equals(AwsProviderUtils.composeUserName(newicu.getUserName(), awsUtilsProps))) {
										principalId = candidate.getUserId();
										logger.info(LOGTAG+"New user was already created: "+candidate);
									} else {
										String errmsg="candidate="+candidate.getUserName()+" newicu="+AwsProviderUtils.composeUserName(newicu.getUserName(), awsUtilsProps)+" "+
												"An error occurred trying to create the IdentityCenterUser. The user appeared to have already been created with the same"
												+" email.value as desired but it is not the same user that was returned in a query. Will throw original exception.";
										logger.error(LOGTAG+errmsg);
									}
								}
							} catch (EnterpriseConfigurationObjectException | EnterpriseFieldException e1) {
								String errmsg="An error occurred trying to create the IdentityCenterUser. Exception thrown";
								logger.error(LOGTAG+errmsg+": ",e1.getMessage());
								throw new ProviderException(errmsg,e1);								
							}
						} else {
							e.printStackTrace();
							String errMsg = "An error occurred trying to create the IdentityCenterUser. " +
									e.getMessage();
							logger.error(LOGTAG + errMsg);
							throw new ProviderException(errMsg,e); 
						}
					}
				} else {
					String errMsg = "No user found in identity store for username = " + ppId+"  -";
					logger.error(LOGTAG + errMsg);
					throw new ProviderException(errMsg); 
				}
			} else {
				principalId = ((IdentityCenterUser) results.get(0)).getUserId();
				logger.info(LOGTAG+"principalId is "+principalId);
			}
		}
		if ("GROUP".equals(aar.getPrincipalType())) {
			String groupName = aar.getPublicId();

			logger.info(LOGTAG+"groupName is "+groupName);

			Filter[] filters = {new Filter()
					.withAttributePath("DisplayName")
					.withAttributeValue(groupName)};

			ListGroupsRequest lgreq = new ListGroupsRequest()
					.withIdentityStoreId(identityStoreId)
					.withFilters(filters);

			ListGroupsResult lgrep = clientIS.listGroups(lgreq);
			if (lgrep.getGroups().size() == 0 ) {
				String errMsg = "No group found in identity store for group name = " + groupName;
				logger.error(LOGTAG + errMsg);
				throw new ProviderException(errMsg);       	      				
			}
			principalId = lgrep.getGroups().get(0).getGroupId();
		}

		logger.info(LOGTAG+"principalId is "+principalId);

		try {
			CreateAccountAssignmentRequest createreq = new CreateAccountAssignmentRequest()
					.withInstanceArn(instanceArn)
					.withPermissionSetArn(permissionSetArn)
					.withPrincipalId(principalId)
					.withPrincipalType(aar.getPrincipalType()) 
					.withTargetId(aar.getTargetId())
					.withTargetType("AWS_ACCOUNT"); //TODO remove hardcode when adding support 


			CreateAccountAssignmentResult createrep = clientSSO.createAccountAssignment(createreq);
			logger.info(LOGTAG+"Reply from create account assignment: "+createrep);

			String status = createrep.getAccountAssignmentCreationStatus().getStatus();
			if (!"IN_PROGRESS".equals(status)) {
				logger.warn(LOGTAG+"Unexpected status, "+status+", returned from create request. Continuing..");
			} else {
				logger.info(LOGTAG+"Create IN_PROGRESS. Waiting for completion.");
			}

			DescribeAccountAssignmentCreationStatusRequest request = new DescribeAccountAssignmentCreationStatusRequest()
					.withAccountAssignmentCreationRequestId(createrep.getAccountAssignmentCreationStatus().getRequestId())
					.withInstanceArn(instanceArn);

			String requestId = createrep.getAccountAssignmentCreationStatus().getRequestId();
			AccountAssignmentCreateWaiter aacw = new AccountAssignmentCreateWaiter(requestId, clientSSO, requestTimeoutIntervalInMillis, instanceArn, logger);
			Thread t = new Thread(aacw);
			t.start();
			t.join();

			if (aacw.runtimeException != null) {
				String errMsg = "Account assignment creation status returned a RuntimeException: "+aacw.runtimeException;
				logger.error(LOGTAG+errMsg);
				throw new ProviderException(errMsg);
			}

			if (StatusValues.IN_PROGRESS.toString().equals(aacw.accountAssignmentCreationStatus.getStatus()) ) {
				String 	errMsg = "Delete account assignment did not complete in the allowed time. Request ID = "
						+createrep.getAccountAssignmentCreationStatus().getRequestId();
				logger.error(LOGTAG+errMsg);
				throw new ProviderException(errMsg);
			}			

			if (!StatusValues.SUCCEEDED.toString().equals(aacw.accountAssignmentCreationStatus.getStatus()) ) {
				String errMsg = "Account assignment deletion status returned: "+aacw.accountAssignmentCreationStatus;
				logger.error(LOGTAG+errMsg);
				throw new ProviderException(errMsg);
			}			

			logger.info(LOGTAG+"Create account assignment has "+aacw.accountAssignmentCreationStatus.getStatus());
			logger.info(LOGTAG+"AccountAssignment created in "+aacw.runningTime+" ms.");

			aa.setPublicId(aar.getPublicId());
			aa.setPrincipalId(aacw.accountAssignmentCreationStatus.getPrincipalId());
			aa.setPrincipalType(aar.getPrincipalType()); 
			aa.setPermissionSetName(aar.getPermissionSetName());
			aa.setTargetId(aar.getTargetId());
			aa.setTargetType("AWS_ACCOUNT"); //TODO remove hardcode when adding support 

			return aa;

		} catch (RuntimeException e) {
			e.printStackTrace();
			String errMsg = "An error occurred trying to create the account assignment. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg,e);       	      			
		} catch (EnterpriseFieldException e) {
			String errmsg="EnterpriseFieldException thrown";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		} catch (InterruptedException e) {
			String errmsg="InterruptedException thrown";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		}

	}

	public List<ActionableEnterpriseObject> queryAccountAssignment(AccountAssignmentQuerySpecification aaq, boolean withGhosts) throws ProviderException {
		logger.info(LOGTAG+"queryAccountAssignment for aaq="+aaq);

		if (aaq.getTargetId()==null) {
			String errmsg="TargetId is required";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg);						
		}
		if (aaq.getPermissionSetName()==null) {
			String errmsg="The permission set is required for an account assignment query. ";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg);										
		}
		logger.info(LOGTAG+"aaq.getPermissionSetName()="+aaq.getPermissionSetName());
		logger.info(LOGTAG+"permissionSetName="+aaq.getPermissionSetName());

		//Don't optimize next stmt because getProperty(null) throws NullPointerException 
		String permissionSetArn = aaq.getPermissionSetName()==null 
				? null
						: getProperties().getProperty(aaq.getPermissionSetName());
		logger.info(LOGTAG+"permissionSetArn="+permissionSetArn);
		if (permissionSetArn == null) {
			List<ActionableEnterpriseObject> permissionSets = AwsProviderUtils.getPermissionSet(
					aaq.getPermissionSetName(), 
					clientSSO, 
					appConfig, 
					instanceArn,
					false, false, false);
			if (permissionSets.size() == 0) {
				return new ArrayList<ActionableEnterpriseObject>();
			}
			permissionSetArn = ((PermissionSet) permissionSets.get(0)).getArn();			
		}

		try {
			if (aaq.getPrincipalType()!=null) validatePrincipalType(aaq.getPrincipalType());

			if (aaq.getTargetType()==null) aaq.setTargetType("AWS_ACCOUNT");
			validateTargetType(aaq.getTargetType());

		}  catch (EnterpriseFieldException e) {
			String errmsg="EnterpriseFieldException thrown";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		}

		try {


			ListAccountAssignmentsRequest laareq = new ListAccountAssignmentsRequest()
					.withInstanceArn(instanceArn)
					.withAccountId(aaq.getTargetId())
					.withPermissionSetArn(permissionSetArn);

			ListAccountAssignmentsResult laarep = clientSSO.listAccountAssignments(laareq);
			logger.info(LOGTAG+"Reply from query account assignment: "+laarep);

			List<ActionableEnterpriseObject> results = new ArrayList<>();

			for (com.amazonaws.services.ssoadmin.model.AccountAssignment candidate: laarep.getAccountAssignments()) {

				String principalName = null;

				if (aaq.getPrincipalType()!=null) {
					if (!candidate.getPrincipalType().equals(aaq.getPrincipalType())) {
						continue;
					}
				}

				if ("USER".equals(candidate.getPrincipalType())) {
					DescribeUserRequest dureq = new DescribeUserRequest()
							.withIdentityStoreId(identityStoreId)
							.withUserId(candidate.getPrincipalId());
					DescribeUserResult durep=null;
					try { 
						durep = clientIS.describeUser(dureq);
						// check that the username returned = the one in the queryspec
						if (aaq.getPublicId() != null 
								&& !aaq.getPublicId().equals(
										AwsProviderUtils.decomposeUserName(durep.getUserName()
												,awsUtilsProps))) {
							continue;
						}
						principalName = AwsProviderUtils.decomposeUserName(durep.getUserName(),awsUtilsProps);					
					} catch (ResourceNotFoundException e) {
						if (withGhosts) {
							principalName = "RIP_"+candidate.getPrincipalType()+"_"+candidate.getPrincipalId();
						} else {
							String errMsg = "No user found in identity store for userId = " + candidate.getPrincipalId()+". Skipping.";
							logger.warn(LOGTAG + errMsg);
							continue;  
						}
					}
				}

				if ("GROUP".equals(candidate.getPrincipalType())) {
					DescribeGroupRequest dgreq = new DescribeGroupRequest()
							.withIdentityStoreId(identityStoreId)
							.withGroupId(candidate.getPrincipalId());
					DescribeGroupResult dgrep=null;
					try { 
						dgrep = clientIS.describeGroup(dgreq);
						if (aaq.getPublicId() != null 
								&& !aaq.getPublicId().equals(dgrep.getDisplayName())) {
							continue;
						}
						principalName = dgrep.getDisplayName();					
					} catch (ResourceNotFoundException e) {
						if (withGhosts) {
							principalName = "RIP_"+candidate.getPrincipalType()+"_"+candidate.getPrincipalId();
						} else {
							String errMsg = "No group found in identity store for groupId = " + candidate.getPrincipalId()+". ";
							logger.warn(LOGTAG + errMsg);
							continue;  
						}
					}
				}

				logger.info(LOGTAG+"principalName is "+principalName);

				AccountAssignment aa = (AccountAssignment) appConfig.getObject("AccountAssignment.v1_0"); 

				aa.setPublicId(principalName);
				aa.setPrincipalId(candidate.getPrincipalId());
				aa.setPrincipalType(candidate.getPrincipalType()); 
				aa.setPermissionSetName(aaq.getPermissionSetName());
				aa.setTargetId(aaq.getTargetId());
				aa.setTargetType("AWS_ACCOUNT"); //TODO remove hardcode when adding support 
				results.add(aa);

				if (aaq.getPublicId()==null || "".equals(aaq.getPublicId())) {
					continue;
				}

				if (principalName.equals(aaq.getPublicId())) {
					break;
				}

			}

			return results;

		}  catch (EnterpriseFieldException e) {
			String errmsg="EnterpriseFieldException thrown";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			

		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg="Can't get AccountAssignmentObject from the config. ";
			logger.error(LOGTAG+errmsg+": ",e.getMessage());
			throw new ProviderException(errmsg,e);			
		}

	}

	private void validatePrincipalType(String type) throws ProviderException {
		if (!"USER".equals(type) && !"GROUP".equals(type)) {
			String errmsg=type + " is an unsupported PrincipalType. ";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg);
		}		
	}

	private void validateTargetType(String type) throws ProviderException {
		if (!"AWS_ACCOUNT".equals(type)) {
			String errmsg=type + " is an unsupported TargetType. ";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg);
		}		
	}


	@Override
	public ActionableEnterpriseObject generate(XmlEnterpriseObject accountAssignmentRequisition, String msgAction)
			throws ProviderException {

		AccountAssignment accountAssignment = createAccountAssignment((AccountAssignmentRequisition)accountAssignmentRequisition);

		return accountAssignment;

	}

	@Override
	public ActionableEnterpriseObject delete(ActionableEnterpriseObject accountAssignment, String deleteAction) throws ProviderException {

		deleteAccountAssignment((AccountAssignment) accountAssignment);

		return accountAssignment;

	}

	@Override
	public List<ActionableEnterpriseObject> query(XmlEnterpriseObject querySpec) throws ProviderException {
		return queryAccountAssignment((AccountAssignmentQuerySpecification) querySpec, true);
	}

	@Override
	public ActionableEnterpriseObject create(ActionableEnterpriseObject aeo) throws ProviderException {
		throw new ProviderException("Create not implemented for AccountAssignment. Use Generate instead.");
	}

	@Override
	public ActionableEnterpriseObject update(ActionableEnterpriseObject aeo, XmlEnterpriseObject baeo) throws ProviderException {
		throw new ProviderException("Update not implemented for AccountAssingment. Use Delete/Generate instead.");
	}

	private RequestService getDSProducer(String LOGTAG) throws ProviderException {
		try {
			PointToPointProducer p2p =
					(PointToPointProducer) producerPool.getProducer();
			p2p.setRequestTimeoutInterval(requestTimeoutIntervalInMillis);
			if (!p2p.isStarted()) p2p.startProducer();
			return (RequestService)p2p;
		}
		catch (JMSException jmse) {
			String errMsg = "An error occurred getting a producer " +
					"from the pool. The exception is: " + jmse.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg, jmse);
		}
	}

	private String null2unknown(String s) { return s==null?"unknown":s; }
}

package org.rhedcloud.idm.service.provider;

import java.util.List;
import org.openeai.config.AppConfig;
import org.rhedcloud.idm.service.util.SavedChanges;

import edu.emory.moa.jmsobjects.identity.v1_0.Resource;
import edu.emory.moa.objects.resources.v1_0.ResourceRequisition;

public abstract interface ResourceProvisioningProvider {
	void init(AppConfig appConfig) throws ProviderException;

	Resource delete(Resource resource) throws ProviderException;

	Resource query(String queryData, List<SavedChanges<String, String>> changedList) throws ProviderException;

	ProviderProcessingResponse generate(ResourceRequisition resourceRequisition) throws ProviderException;
}

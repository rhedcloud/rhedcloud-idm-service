package org.rhedcloud.idm.service.provider.aws;

import org.apache.logging.log4j.Logger;

import com.amazonaws.services.ssoadmin.AWSSSOAdminClient;
import com.amazonaws.services.ssoadmin.model.AccountAssignmentOperationStatus;
import com.amazonaws.services.ssoadmin.model.DescribeAccountAssignmentCreationStatusRequest;
import com.amazonaws.services.ssoadmin.model.DescribeAccountAssignmentCreationStatusResult;
import com.amazonaws.services.ssoadmin.model.StatusValues;

public class AccountAssignmentCreateWaiter implements Runnable {

	private static final String LOGTAG = "[CreateWaiter] ";
	private final AWSSSOAdminClient clientSSO;
	private String accountAssignmentCreationRequestId;
	private final long timeout;
	public AccountAssignmentOperationStatus accountAssignmentCreationStatus;
	public RuntimeException runtimeException;
	private final String instanceArn;
	private final Logger logger;
	public long runningTime;


	public AccountAssignmentCreateWaiter(String accountAssignmentCreationRequestId, AWSSSOAdminClient clientSSO, long timeout, String instanceArn, Logger logger) {
		super();
		this.clientSSO = clientSSO;
		this.timeout = timeout;
		this.instanceArn = instanceArn;	
		this.accountAssignmentCreationRequestId = accountAssignmentCreationRequestId;
		this.logger = logger;
	}

	@Override
	public void run() {

		long start = System.currentTimeMillis(); 

		DescribeAccountAssignmentCreationStatusResult daacsS;
		DescribeAccountAssignmentCreationStatusRequest daacsQ = new DescribeAccountAssignmentCreationStatusRequest()
				.withInstanceArn(instanceArn)
				.withAccountAssignmentCreationRequestId(accountAssignmentCreationRequestId);
		try {
			do {
				Thread.yield();
				daacsS = clientSSO.describeAccountAssignmentCreationStatus(daacsQ);
				daacsS.getAccountAssignmentCreationStatus().getStatus();
				runningTime = System.currentTimeMillis() - start;
				accountAssignmentCreationStatus = daacsS.getAccountAssignmentCreationStatus();
				logger.debug(LOGTAG+" accountAssignmentCreationStatus ="+accountAssignmentCreationStatus);
			} while (StatusValues.IN_PROGRESS.toString().equals(accountAssignmentCreationStatus.getStatus())
					&& runningTime < timeout);
		} catch (RuntimeException e) {
			logger.error(LOGTAG+"describeAccountAssignmentCreationStatus ERROR "+e);
			runtimeException = e;
		}			
	}
}

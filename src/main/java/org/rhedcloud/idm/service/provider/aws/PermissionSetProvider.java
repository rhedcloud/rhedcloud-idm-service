package org.rhedcloud.idm.service.provider.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Datetime;
import org.rhedcloud.idm.service.provider.AwsProvisioningProvider;
import org.rhedcloud.idm.service.provider.ProviderException;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.PermissionSet;
import com.amazon.aws.moa.objects.resources.v1_0.PermissionSetQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.PermissionSetRequisition;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ssoadmin.AWSSSOAdminClient;
import com.amazonaws.services.ssoadmin.AWSSSOAdminClientBuilder;
import com.amazonaws.services.ssoadmin.model.AttachCustomerManagedPolicyReferenceToPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.AttachManagedPolicyToPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.CreatePermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.CreatePermissionSetResult;
import com.amazonaws.services.ssoadmin.model.CustomerManagedPolicyReference;
import com.amazonaws.services.ssoadmin.model.DeleteInlinePolicyFromPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.DeletePermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.DeletePermissionSetResult;
import com.amazonaws.services.ssoadmin.model.DetachCustomerManagedPolicyReferenceFromPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.DetachManagedPolicyFromPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.ProvisionPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.ProvisionPermissionSetResult;
import com.amazonaws.services.ssoadmin.model.ProvisionTargetType;
import com.amazonaws.services.ssoadmin.model.PutInlinePolicyToPermissionSetRequest;
import com.amazonaws.services.ssoadmin.model.StatusValues;


/**
 * The class implements generate, update, query and delete actions for PermissionSet message object.
 * 
 * @author tcerven
 * @version 1.0
 *
 */
public class PermissionSetProvider extends OpenEaiObject implements AwsProvisioningProvider {

	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(PermissionSetProvider.class);
	AppConfig appConfig;
	private final String LOGTAG = "[PermissionSetProvider] ";
	private String instanceArn;
	private String accessKeyId;
	private String secretKey;
	private AWSSSOAdminClient clientSSO;
	private Integer maxThreads = 10;
	private ArrayList<String> reservedNames = new ArrayList<>();


	@SuppressWarnings("unused")
	@Override
	public void init(AppConfig appConfig) throws ProviderException {
		logger.info(LOGTAG+"init started...");
		this.appConfig = appConfig;
		PermissionSetQuerySpecification psq;
		PermissionSet ps;
		try {
			ps  = (PermissionSet) appConfig.getObject("PermissionSet.v1_0");
			PermissionSetRequisition psr = (PermissionSetRequisition) appConfig.getObject("PermissionSetRequisition.v1_0");
			psq = (PermissionSetQuerySpecification) appConfig.getObject("PermissionSetQuerySpecification.v1_0");
			setProperties(this.appConfig.getProperties("PermissionSetProviderProperties"));
		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg = "Can't load PermissionSet.v1_0 or PermissionSetRequisition.v1_0 or PermissionSetQuerySpecification.v1_0";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg,e);
		}

		accessKeyId = getProperties().getProperty("accessKeyId");
		logger.info(LOGTAG + "accessKeyId is: " + accessKeyId);

		secretKey = getProperties().getProperty("secretKey");
		logger.info(LOGTAG + "secretKey is: present");

		instanceArn = getProperties().getProperty("instanceArn");
		logger.info(LOGTAG + "instanceArn is: " + instanceArn);

		AWSStaticCredentialsProvider creds = new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, secretKey));
		AWSSSOAdminClientBuilder builderSSOAdmin = AWSSSOAdminClientBuilder.standard().withCredentials(creds);
		logger.info(LOGTAG+"Initializing AWSSSOAdminClient...");
		clientSSO = (AWSSSOAdminClient) builderSSOAdmin.build();
		logger.info(LOGTAG+"AWSSSOAdminClient initialized.");

		reservedNames.add("Administrator");
		reservedNames.add("CentralAdministrator");
		reservedNames.add("SecurityIR");
		reservedNames.add("Auditor");

		AwsProviderUtils.useCache = Boolean.parseBoolean(getProperties().getProperty("useCache"));
		logger.info(LOGTAG + "useCache is: " + AwsProviderUtils.useCache);
		if (AwsProviderUtils.useCache) {
			long cacheRefreshIntervalInMillis = 24 * 60 * 60 * 1000;
			try {
				cacheRefreshIntervalInMillis = Long.parseLong(getProperties().getProperty("cacheRefreshIntervalInMillis"));
				logger.info(LOGTAG+"Using cache refresh rate of "+cacheRefreshIntervalInMillis+" ms");
			} catch (NumberFormatException e) {
				logger.warn(LOGTAG+"Using default cache refresh rate of "+cacheRefreshIntervalInMillis+" ms");
			}
			new AwsProviderUtils().new CacheFreshener(clientSSO, appConfig, instanceArn, cacheRefreshIntervalInMillis).start();
		}
	}
	
	private void deletePermissionSet(PermissionSet ps, String deleteAction) throws ProviderException {

		if (ps.getName().startsWith("RHEDcloud")
				|| reservedNames.contains(ps.getName())) {
			String errMsg = ps.getName() + " is a reserved PermissionSet and can't be deleted.";
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);       	      			
		}


		try {

			if ("purge".equalsIgnoreCase(deleteAction)) {
				AwsProviderUtils.deleteAllAccountAssignmentsForPermissionSet(ps, clientSSO, instanceArn, maxThreads);
			}

			DeletePermissionSetRequest deletereq = new DeletePermissionSetRequest()
					.withInstanceArn(instanceArn)
					.withPermissionSetArn(ps.getArn());

			DeletePermissionSetResult deleterep = clientSSO.deletePermissionSet(deletereq);
			logger.info(LOGTAG+"Reply from delete PermissionSet: "+deleterep);
			logger.info(LOGTAG+"Permission set deleted. ");

		} catch (RuntimeException e) {
			String errMsg = "An error occurred trying to delete the permission set. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			e.printStackTrace();
			throw new ProviderException(errMsg);       	      			
		}
		if (AwsProviderUtils.useCache) {
			AwsProviderUtils.cachePsByName.remove(ps.getName());
			AwsProviderUtils.cachePsByArn.remove(ps.getArn());
			logger.info(LOGTAG + "Removed "+ps.getName()+" from cache");
		}
	}

	private PermissionSet createPermissionSet(PermissionSetRequisition psr) throws ProviderException {
		
		if (psr.getName().startsWith("RHEDcloud")
				|| reservedNames.contains(psr.getName())) {
			String errMsg = psr.getName() + " is a reserved PermissionSet and can't be created.";
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);       	      			
		}

		PermissionSet permissionSet = null;

		try {
			permissionSet = (PermissionSet) appConfig.getObject("PermissionSet.v1_0");
		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg="PermissionSet.v1_0 is not found in the config.";
			logger.fatal(LOGTAG+errmsg);
			throw new ProviderException(errmsg);	
		}

		try {

			String permissionSetName = psr.getName();
			String permissionSetDescription = psr.getDescription();
			String sessionDuration = psr.getSessionDuration();
			logger.info(LOGTAG+"creating PermissionSet named "+psr.getName()
			+" with session duration "+sessionDuration);

			CreatePermissionSetRequest createpmreq = new CreatePermissionSetRequest()
					.withInstanceArn(instanceArn)
					.withName(permissionSetName)
					.withDescription(permissionSetDescription)
					.withSessionDuration(sessionDuration);//.withSessionDuration("PT12H");

			if (psr.getTag().size()>0) {
				List<com.amazonaws.services.ssoadmin.model.Tag> amzTags = new ArrayList<>();
				@SuppressWarnings("unchecked")
				List<com.amazon.aws.moa.objects.resources.v1_0.Tag> moaTags = psr.getTag();
				for (com.amazon.aws.moa.objects.resources.v1_0.Tag moaTag: moaTags) {
					amzTags.add(
							new com.amazonaws.services.ssoadmin.model.Tag()
							.withKey(moaTag.getKey())
							.withValue(moaTag.getValue()));
				}
				createpmreq.setTags(amzTags);
			}

			CreatePermissionSetResult createpmres = clientSSO.createPermissionSet(createpmreq);
			logger.info(LOGTAG+"Reply from create permission set: "+createpmres);

			com.amazonaws.services.ssoadmin.model.PermissionSet awspermissionset = createpmres.getPermissionSet();

			// Add customer managed policies
			List<CustomerManagedPolicyReference> cmprs = new ArrayList<>();

			for (String cmp : (List<String>)psr.getCustomerManagedPolicyReference()) {
				if (!cmp.startsWith("/")) {
					String errmsg="CustomerManagedPolicy must start with a '/'.";
					logger.error(LOGTAG+errmsg);
					throw new ProviderException(errmsg);	
				}
				String[] parts = cmp.split("/");
				StringBuilder path = new StringBuilder();
				path.append("/");
				if (parts.length > 2 ) {
					for (int i = 1; i < parts.length - 1; i++) {
						path.append(parts[i]).append("/");
					}
				}

				cmprs.add(
						new CustomerManagedPolicyReference()
						.withName(parts[parts.length-1])
						.withPath(path.toString()));
			}

			// Add aws managed policies
			for (CustomerManagedPolicyReference cmpr: cmprs) {
				logger.info(LOGTAG+"Attaching customer managed policy reference: "+cmpr.getName());

				AttachCustomerManagedPolicyReferenceToPermissionSetRequest attachreq = 
						new AttachCustomerManagedPolicyReferenceToPermissionSetRequest()
						.withInstanceArn(instanceArn)
						.withPermissionSetArn(awspermissionset.getPermissionSetArn())
						.withCustomerManagedPolicyReference(cmpr);

				clientSSO.attachCustomerManagedPolicyReferenceToPermissionSet(attachreq);				
			}
			
			// Add aws managed policies
			@SuppressWarnings("unused")
			List<String> arns= new ArrayList<>();

			for (String arn : (List<String>)psr.getManagedPolicyArn()) {
				logger.info(LOGTAG+"Attaching managed policy arn: "+arn);

				AttachManagedPolicyToPermissionSetRequest attachreq = 
						new AttachManagedPolicyToPermissionSetRequest()
						.withInstanceArn(instanceArn)
						.withPermissionSetArn(awspermissionset.getPermissionSetArn())
						.withManagedPolicyArn(arn);

				clientSSO.attachManagedPolicyToPermissionSet(attachreq);				
			}
			
			// Add inline policy
			if (psr.getInlinePolicyDocument() != null) {
				PutInlinePolicyToPermissionSetRequest q = new PutInlinePolicyToPermissionSetRequest()
						.withInlinePolicy(psr.getInlinePolicyDocument())
						.withInstanceArn(instanceArn)
						.withPermissionSetArn(awspermissionset.getPermissionSetArn());
				clientSSO.putInlinePolicyToPermissionSet(q);
			}
			
			permissionSet.setName(awspermissionset.getName());
			permissionSet.setDescription(awspermissionset.getDescription());
			permissionSet.setArn(awspermissionset.getPermissionSetArn());
			permissionSet.setSessionDuration(awspermissionset.getSessionDuration());
			permissionSet.setRelayState(awspermissionset.getRelayState());
			permissionSet.setCreateDatetime(new Datetime(awspermissionset.getCreatedDate()));
			permissionSet.setCustomerManagedPolicyReference(psr.getCustomerManagedPolicyReference());
			permissionSet.setManagedPolicyArn(psr.getManagedPolicyArn());
			
			if (AwsProviderUtils.useCache) {
				PermissionSet result = null;
				try {
					result = (PermissionSet) permissionSet.clone();
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
				AwsProviderUtils.cachePsByName.put(result.getName(), result.getArn());
				AwsProviderUtils.cachePsByArn.put(result.getArn(),result);
			}

			return permissionSet;

		} catch (RuntimeException e) {
			String errMsg = "An error occurred trying to create the permission set. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg,e);       	      			
		} catch (EnterpriseFieldException e) {
			String errmsg="EnterpriseFieldException thrown";
			logger.error(LOGTAG+errmsg+": "+e.getMessage());
			throw new ProviderException(errmsg,e);			
		}

	}

	@Override
	public ActionableEnterpriseObject generate(XmlEnterpriseObject requisition, String msgAction)
			throws ProviderException {

		ActionableEnterpriseObject aeo = createPermissionSet((PermissionSetRequisition) requisition);
		return aeo;

	}

	@Override
	public ActionableEnterpriseObject delete(ActionableEnterpriseObject permissionSet, String deleteAction) throws ProviderException {

		deletePermissionSet((PermissionSet) permissionSet, deleteAction);
		return permissionSet;

	}

	@Override
	public List<ActionableEnterpriseObject> query(XmlEnterpriseObject querySpec) throws ProviderException {
		logger.info(LOGTAG+"queryspec = "+querySpec);
		PermissionSetQuerySpecification psquery = (PermissionSetQuerySpecification) querySpec;
		Boolean withTags = Boolean.parseBoolean(psquery.getWithTags());
		Boolean withPolicies = Boolean.parseBoolean(psquery.getWithPolicies());
		Boolean withProvisionedAccountIds = Boolean.parseBoolean(psquery.getWithProvisionedAccountIds());

		return AwsProviderUtils.getPermissionSet(
				psquery.getName(), 
				clientSSO, 
				appConfig, 
				instanceArn, withTags, withPolicies, withProvisionedAccountIds);
	}

	@Override
	public ActionableEnterpriseObject create(ActionableEnterpriseObject aeo) throws ProviderException {
		throw new ProviderException("Create not implemented for PermissionSet. Use Generate instead.");
	}

	@Override
	public ActionableEnterpriseObject update(ActionableEnterpriseObject naeo, XmlEnterpriseObject baeo) throws ProviderException {
		naeo.setBaseline(baeo);

		PermissionSet ps = (PermissionSet) naeo;
		if (ps.getName().startsWith("RHEDcloud")
				|| reservedNames.contains(ps.getName())) {
			String errMsg = ps.getName() + " is a reserved PermissionSet and can't be updated.";
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);       	      			
		}
		
		List<ActionableEnterpriseObject> currentPermissionSets = AwsProviderUtils.getPermissionSet(
				ps.getName(), 
				clientSSO, 
				appConfig, 
				instanceArn, 
				false,
				true, false);		
		@SuppressWarnings("unchecked")
		List<String> baselineRefs = ((PermissionSet) ps.getBaseline()).getCustomerManagedPolicyReference();
		@SuppressWarnings("unchecked")
		List<String> baselineArns = ((PermissionSet) ps.getBaseline()).getManagedPolicyArn();
		String baselineInline = denull(((PermissionSet) ps.getBaseline()).getInlinePolicyDocument());
		@SuppressWarnings("unchecked")
		List<String> newRefs = ps.getCustomerManagedPolicyReference();
		@SuppressWarnings("unchecked")
		List<String> newArns = ps.getManagedPolicyArn();
		String newInline = denull(ps.getInlinePolicyDocument());
		List<String> currentRefs = null;
		List<String> currentArns = null;
		String currentInline = null;
		
		//this block determines if the baseline is stale or the ps is not found
		if (currentPermissionSets.size() == 1) {		
			currentRefs = ((PermissionSet) currentPermissionSets.get(0)).getCustomerManagedPolicyReference();
			if (currentRefs.size() == baselineRefs.size()) {
				for (String ref: baselineRefs) {
					if (!currentRefs.contains(ref)) {
						String errmsg="Baseline is stale: customer managed policy reference, "+ref+", not in current list of references -";
						throw new ProviderException(errmsg);			
					}
				}
			} else {
				String errmsg="Baseline is stale: the number of customer managed policy references, "+baselineRefs.size()+", is different than in current list of references -";
				throw new ProviderException(errmsg);							
			}
			currentArns = ((PermissionSet) currentPermissionSets.get(0)).getManagedPolicyArn();
			if (currentArns.size() == baselineArns.size()) {
				for (String arn: baselineArns) {
					if (!currentArns.contains(arn)) {
						String errmsg="Baseline is stale: managed policy arn, "+arn+", not in current list of arns -";
						throw new ProviderException(errmsg);			
					}
				}
			} else {
				String errmsg="Baseline is stale: the number of managed policy arns, "+baselineArns.size()+", is different than in current list of arns -";
				throw new ProviderException(errmsg);							
			}
			currentInline = denull(((PermissionSet) currentPermissionSets.get(0)).getInlinePolicyDocument());
			if ( !currentInline.equals(baselineInline) ) {
				String errmsg="Baseline is stale: the policy document, '"+baselineInline+"', is different from the current, '"+currentInline+"' -";
				throw new ProviderException(errmsg);
			}
		} else if (currentPermissionSets.size() == 0) {
			String errmsg="PermissionSet not found -";
			throw new ProviderException(errmsg);						
		} else {
			String errmsg="More than one PermissionSet found that match the name -";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg);									
		}
		
		//this block detaches customer managed policy references
		for(String ref: currentRefs) {
			if (!newRefs.contains(ref)) {
				String name = ref.substring(ref.lastIndexOf("/")+1);
				String path = ref.substring(0,ref.lastIndexOf("/")+1);
				CustomerManagedPolicyReference cmpr = new CustomerManagedPolicyReference()
						.withName(name)
						.withPath(path);
		
				try {
					DetachCustomerManagedPolicyReferenceFromPermissionSetRequest req = new 
							DetachCustomerManagedPolicyReferenceFromPermissionSetRequest()
							.withInstanceArn(instanceArn)
							.withCustomerManagedPolicyReference(cmpr)
							.withPermissionSetArn(ps.getArn());
					logger.info(LOGTAG+"Detaching customer policy ref, '"+cmpr+"' from permission set, '"+ps.getName()+"'");
					clientSSO.detachCustomerManagedPolicyReferenceFromPermissionSet(req);
				} catch (RuntimeException e) {
					String errmsg="Can't detach Customer Managed Policy Reference From PermissionSet -"+e.getMessage();
					logger.error(LOGTAG+errmsg+": "+e.getMessage());
					throw new ProviderException(errmsg,e);														
				}
			}
		}
		
		//this block detaches managed policy references
		for(String arn: currentArns) {
			if (!newArns.contains(arn)) {
				try {
					DetachManagedPolicyFromPermissionSetRequest req = new DetachManagedPolicyFromPermissionSetRequest()
							.withInstanceArn(instanceArn)
							.withManagedPolicyArn(arn)
							.withPermissionSetArn(ps.getArn());
					logger.info(LOGTAG+"Detaching managed policy, '"+arn+"' from permission set, '"+ps.getName()+"'");
					clientSSO.detachManagedPolicyFromPermissionSet(req);
				} catch (RuntimeException e) {
					String errmsg="Can't detach Managed Policy From PermissionSet -"+e.getMessage();
					logger.error(LOGTAG+errmsg+": "+e.getMessage());
					throw new ProviderException(errmsg,e);														
				}
			}
		}
		
		//this block deletes the inline policy
		try {
			if (newInline.equals("null") && !currentInline.equals("null")) {
		
			DeleteInlinePolicyFromPermissionSetRequest req = new DeleteInlinePolicyFromPermissionSetRequest()
					.withInstanceArn(instanceArn)
					.withPermissionSetArn(ps.getArn());
			logger.info(LOGTAG+"Deleting inline policy from permission set, '"+ps.getName()+"'");
			clientSSO.deleteInlinePolicyFromPermissionSet(req);			
		}
		} catch (RuntimeException e) {
			String errmsg="Can't delete Inline Policy From PermissionSet -"+e.getMessage();
			logger.error(LOGTAG+errmsg+": "+e.getMessage());
			throw new ProviderException(errmsg,e);														
		}
		
		//this block attaches customer managed policy references
		for(String ref: newRefs) {
			if (!currentRefs.contains(ref)) {
				String name = ref.substring(ref.lastIndexOf("/")+1);
				String path = ref.substring(0,ref.lastIndexOf("/")+1);
				CustomerManagedPolicyReference cmpr = new CustomerManagedPolicyReference()
						.withName(name)
						.withPath(path);
				try {
					AttachCustomerManagedPolicyReferenceToPermissionSetRequest req = new
							AttachCustomerManagedPolicyReferenceToPermissionSetRequest()
							.withInstanceArn(instanceArn)
							.withCustomerManagedPolicyReference(cmpr)
							.withPermissionSetArn(ps.getArn());
					logger.info(LOGTAG+"Attaching policy arn "+ps.getArn()+" to permission set, '"+ps.getName()+"'");
					clientSSO.attachCustomerManagedPolicyReferenceToPermissionSet(req);
				} catch (RuntimeException e) {
					String errmsg="Can't attach Customer Managed Policy Arn to PermissionSet -"+e.getMessage();
					logger.error(LOGTAG+errmsg+": "+e.getMessage());
					throw new ProviderException(errmsg,e);														
				}
			}
		}
		
		//this block attaches managed policy references
		for(String arn: newArns) {
			if (!currentArns.contains(arn)) {
				try {
					AttachManagedPolicyToPermissionSetRequest req = new
							AttachManagedPolicyToPermissionSetRequest()
							.withInstanceArn(instanceArn)
							.withManagedPolicyArn(arn)
							.withPermissionSetArn(ps.getArn());
					logger.info(LOGTAG+"Attaching policy arn="+arn+" to permission set, '"+ps.getName()+"'");
					clientSSO.attachManagedPolicyToPermissionSet(req);
				} catch (RuntimeException e) {
					String errmsg="Can't attach Managed Policy Reference to PermissionSet -"+e.getMessage();
					logger.error(LOGTAG+errmsg+": "+e.getMessage());
					throw new ProviderException(errmsg,e);														
				}
			}
		}
		
		//this block creates an inline policy in the permission set
		if(!newInline.equals("null") && !newInline.equals(currentInline)) {
			try {
				PutInlinePolicyToPermissionSetRequest req = new PutInlinePolicyToPermissionSetRequest()
						.withInstanceArn(instanceArn)
						.withInlinePolicy(newInline)
						.withPermissionSetArn(ps.getArn());
				logger.info(LOGTAG+"Attaching inline policy to permission set, '"+ps.getName()+"'");
				clientSSO.putInlinePolicyToPermissionSet(req);
			} catch (RuntimeException e) {
				String errmsg="Can't attach inline policy to PermissionSet -"+e.getMessage();
				logger.error(LOGTAG+errmsg+": "+e.getMessage());
				throw new ProviderException(errmsg,e);														
			}
		}
		
		//update the provisioned accounts: 
		ProvisionPermissionSetRequest ppsq = new ProvisionPermissionSetRequest()
				.withInstanceArn(instanceArn)
				.withTargetType(ProvisionTargetType.ALL_PROVISIONED_ACCOUNTS)
				.withPermissionSetArn(ps.getArn());
		ProvisionPermissionSetResult ppss = clientSSO.provisionPermissionSet(ppsq);
		if (ppss.getPermissionSetProvisioningStatus().getStatus().equals(StatusValues.FAILED.toString())) {
			logger.warn(LOGTAG+"Failed to provision accounts. Reason: "+ppss.getPermissionSetProvisioningStatus().getFailureReason()+" -");
		}
		
		if (AwsProviderUtils.useCache) {
			PermissionSet result = null;
			try {
				result = (PermissionSet) ps.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			AwsProviderUtils.cachePsByName.put(result.getName(), result.getArn());
			AwsProviderUtils.cachePsByArn.put(result.getArn(),result);
		}
			
		return ps;
	}

	private String denull(String s) {
		return s == null || s.equals("") ? "null" : s;
	}
 }





package org.rhedcloud.idm.service.provider.aws;

import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;
import org.rhedcloud.idm.service.provider.AwsProvisioningProvider;
import org.rhedcloud.idm.service.provider.ProviderException;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.IdentityCenterUser;
import com.amazon.aws.moa.objects.resources.v1_0.IdentityCenterUserQuerySpecification;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.identitystore.AWSIdentityStoreClient;
import com.amazonaws.services.identitystore.AWSIdentityStoreClientBuilder;
import com.amazonaws.services.identitystore.model.DeleteUserRequest;
import com.amazonaws.services.identitystore.model.DeleteUserResult;


/**
 * The class implements generate, query and delete actions for PermissionSet message object.
 * 
 * @author tcerven
 * @version 1.0
 *
 */
public class IdentityCenterUserProvider extends OpenEaiObject implements AwsProvisioningProvider {

	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(IdentityCenterUserProvider.class);
	AppConfig appConfig;
	private String LOGTAG = "[IdentityCenterUserProvider] ";
	private String accessKeyId;
	private String secretKey;
	private AWSIdentityStoreClient clientIS;
	private String identityStoreId;
	private Properties awsUtilsProps;

	@SuppressWarnings("unused")
	@Override
	public void init(AppConfig appConfig) throws ProviderException {
		logger.info(LOGTAG+"init started...");
		this.appConfig = appConfig;
		
		try {
			IdentityCenterUser __ = (IdentityCenterUser) appConfig.getObject("IdentityCenterUser.v1_0");
			IdentityCenterUserQuerySpecification _____ = 
					(IdentityCenterUserQuerySpecification) appConfig.getObject("IdentityCenterUserQuerySpecification.v1_0");
			setProperties(this.appConfig.getProperties("IdentityCenterUserProviderProperties"));
			awsUtilsProps = appConfig.getProperties("AwsProviderUtilsProperties");
		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg = "Can't load IdentityCenterUser.v1_0 or IdentityCenterUserQuerySpecification.v1_0"
					+" or IdentityCenterUserProviderProperties or AwsProviderUtilsProperties";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg,e);
		}

		accessKeyId = getProperties().getProperty("accessKeyId");
		logger.info(LOGTAG + "accessKeyId is: " + accessKeyId);
		if (accessKeyId == null ) {
			String errmsg = "accessKeyId property not found in appConfig ";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg);			
		}

		secretKey = getProperties().getProperty("secretKey");
		if (secretKey == null ) {
			String errmsg = "secretKey property not found in appConfig ";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg);			
		}
		logger.info(LOGTAG + "secretKey is: present");
		
		identityStoreId = getProperties().getProperty("identityStoreId");
		logger.info(LOGTAG + "identityStoreId is: " + identityStoreId);		
		if (identityStoreId == null ) {
			String errmsg = "identityStoreId property not found in appConfig ";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg);			
		}
		
		logger.info(LOGTAG + "AwsProviderUtilsProps =  " + awsUtilsProps);
				
		AWSStaticCredentialsProvider creds = new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, secretKey));

		// Initialize the AWS client
		// Instantiate an AWS client builder
		AWSIdentityStoreClientBuilder builderIAM = AWSIdentityStoreClientBuilder.standard().withCredentials(creds);
		// Initialize the AWS client
		logger.info(LOGTAG+"Initializing AWSIdentityStoreClient...");
		clientIS = (AWSIdentityStoreClient) builderIAM.build();
		logger.info(LOGTAG+"AWSIdentityStoreClient initialized.");

	}

	private void deleteIdentityCenterUser(IdentityCenterUser icu) throws ProviderException {

		try {

			DeleteUserRequest deletereq = new DeleteUserRequest()
					.withIdentityStoreId(identityStoreId)
					.withUserId(icu.getUserId());

			DeleteUserResult deleterep = clientIS.deleteUser(deletereq);
			logger.info(LOGTAG+"Reply from delete IdentityCenterUser: "+deleterep);
			logger.info(LOGTAG+"IdentityCenterUser deleted. ");

		} catch (RuntimeException e) {
			String errMsg = "An error occurred trying to delete the IdentityCenterUser. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);       	      			
		}
	}



	@Override
	public ActionableEnterpriseObject generate(XmlEnterpriseObject requisition, String msgAction)
			throws ProviderException {

		throw new ProviderException("Generate not implemented for IdentityCenterUser. Use Create instead.");

	}

	@Override
	public ActionableEnterpriseObject delete(ActionableEnterpriseObject identityCenterUser, String deleteAction) throws ProviderException {

		deleteIdentityCenterUser((IdentityCenterUser) identityCenterUser);
		return identityCenterUser;

	}

	@Override
	public List<ActionableEnterpriseObject> query(XmlEnterpriseObject querySpec) throws ProviderException {
		logger.info(LOGTAG+"queryspec = "+querySpec);
		return AwsProviderUtils.getIdentityCenterUser(
				(IdentityCenterUserQuerySpecification) querySpec, 
				clientIS, 
				appConfig, 
				identityStoreId,
				awsUtilsProps);
	}
	
	@Override
	public ActionableEnterpriseObject create(ActionableEnterpriseObject aeo) throws ProviderException {
		return AwsProviderUtils.createIdentityCenterUser(
				(IdentityCenterUser)aeo, 
				appConfig, 
				awsUtilsProps, 
				identityStoreId, 
				clientIS);
	}
	
	@Override
	public ActionableEnterpriseObject update(ActionableEnterpriseObject aeo, XmlEnterpriseObject baeo) throws ProviderException {
		throw new ProviderException("Update not implemented for IdentityCenterUser. Use Delete/Generate instead.");
	}



}

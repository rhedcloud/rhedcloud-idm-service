package org.rhedcloud.idm.service.provider;

import java.util.List;

import org.openeai.config.AppConfig;
import org.rhedcloud.idm.service.util.SavedChanges;

import edu.emory.moa.jmsobjects.identity.v1_0.Role;
import edu.emory.moa.objects.resources.v1_0.RoleRequisition;

public abstract interface RoleProvisioningProvider {

	void init(AppConfig appConfig) throws ProviderException;

	ProviderProcessingResponse generate(RoleRequisition roleRequisition) throws ProviderException;

	Role delete(Role role) throws ProviderException;

	Role query(String roleDn, List<SavedChanges<String, String>> changedList, String fromAction)
			throws ProviderException;

	ProviderProcessingResponse update(Role newRole, Role idmRole) throws ProviderException;
}

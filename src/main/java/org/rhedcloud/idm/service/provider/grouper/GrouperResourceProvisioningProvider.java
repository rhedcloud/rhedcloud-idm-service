package org.rhedcloud.idm.service.provider.grouper;

import java.util.List;

import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.ProviderProcessingResponse;
import org.rhedcloud.idm.service.provider.ResourceProvisioningProvider;
import org.rhedcloud.idm.service.util.SavedChanges;

import edu.emory.moa.jmsobjects.identity.v1_0.Resource;
import edu.emory.moa.objects.resources.v1_0.ResourceRequisition;

public class GrouperResourceProvisioningProvider extends OpenEaiObject implements ResourceProvisioningProvider {

	AppConfig appConfig;

	@Override
	public void init(AppConfig appConfig) throws ProviderException {
		this.appConfig = appConfig;
	}

	@Override
	public Resource delete(Resource resource) throws ProviderException {
		return resource;
	}

	@Override
	public Resource query(String queryData, List<SavedChanges<String, String>> changedList) throws ProviderException {
		Resource moa;
		String responseRoleXml = "<Resource>\n" + 
				"				<ResourceDN>cn=MDSG_AWS-134285882893-Administrator,cn=ResourceDefs,cn=RoleConfig,cn=AppConfig,cn=UserApplication,cn=DRIVERSET01,ou=Servers,o=EmoryDev</ResourceDN>\n" + 
				"				<ResourceName>driverCode</ResourceName>\n" + 
				"				<ResourceDescription>Provisions members to group Administrator on MS LDS University Connector (dummy grouper provider)</ResourceDescription>\n" + 
				"				<ResourceCategoryKey>aws</ResourceCategoryKey>\n" + 
				"				<Entitlement>\n" + 
				"					<EntitlementDN>CN=AWS-Admin-8,OU=AWS,DC=entdev,DC=emory,DC=edu</EntitlementDN>\n" + 
				"					<EntitlementGuid>{451f4d2c-0aac-4ad9-92b9-ca655091d8a7}</EntitlementGuid>\n" + 
				"					<EntitlementApplication>EAD</EntitlementApplication>\n" + 
				"				</Entitlement>\n" + 
				"			</Resource>";
		try {
			moa = (Resource) appConfig.getObject("Resource.v1_0");
			moa.buildObjectFromXmlString(responseRoleXml);
		}
		catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
		catch (XmlEnterpriseObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
		return moa;
	}

	@Override
	public ProviderProcessingResponse generate(ResourceRequisition resourceRequisition) throws ProviderException {
		ProviderProcessingResponse providerProcessingResponse = new ProviderProcessingResponse();
		String responseRoleXml = "<Resource>\n" + 
				"				<ResourceDN>cn=MDSG_AWS-134285882893-Administrator,cn=ResourceDefs,cn=RoleConfig,cn=AppConfig,cn=UserApplication,cn=DRIVERSET01,ou=Servers,o=EmoryDev</ResourceDN>\n" + 
				"				<ResourceName>driverCode</ResourceName>\n" + 
				"				<ResourceDescription>Provisions members to group Administrator on MS LDS University Connector (dummy grouper provider)</ResourceDescription>\n" + 
				"				<ResourceCategoryKey>aws</ResourceCategoryKey>\n" + 
				"				<Entitlement>\n" + 
				"					<EntitlementDN>CN=AWS-Admin-8,OU=AWS,DC=entdev,DC=emory,DC=edu</EntitlementDN>\n" + 
				"					<EntitlementGuid>{451f4d2c-0aac-4ad9-92b9-ca655091d8a7}</EntitlementGuid>\n" + 
				"					<EntitlementApplication>EAD</EntitlementApplication>\n" + 
				"				</Entitlement>\n" + 
				"			</Resource>";
		try {
			Resource moa = (Resource) appConfig.getObject("Resource.v1_0");
			moa.buildObjectFromXmlString(responseRoleXml);
			providerProcessingResponse.setResponse(moa);
		}
		catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
		catch (XmlEnterpriseObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
		return providerProcessingResponse;
	}

}

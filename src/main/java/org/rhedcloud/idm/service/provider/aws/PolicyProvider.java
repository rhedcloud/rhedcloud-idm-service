package org.rhedcloud.idm.service.provider.aws;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Datetime;
import org.rhedcloud.idm.service.provider.AwsProvisioningProvider;
import org.rhedcloud.idm.service.provider.ProviderException;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Policy;
import com.amazon.aws.moa.objects.resources.v1_0.PolicyQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.PolicyRequisition;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.CreatePolicyRequest;
import com.amazonaws.services.identitymanagement.model.CreatePolicyResult;
import com.amazonaws.services.identitymanagement.model.CreatePolicyVersionRequest;
import com.amazonaws.services.identitymanagement.model.DeletePolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeletePolicyResult;
import com.amazonaws.services.identitymanagement.model.DeletePolicyVersionRequest;
import com.amazonaws.services.identitymanagement.model.ListPolicyVersionsRequest;
import com.amazonaws.services.identitymanagement.model.ListPolicyVersionsResult;
import com.amazonaws.services.identitymanagement.model.PolicyVersion;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.amazonaws.services.securitytoken.model.Credentials;


/**
 * The class implements generate, query and delete actions for Policy message object.
 * 
 * @author tcerven
 * @version 1.0
 *
 */
public class PolicyProvider extends OpenEaiObject implements AwsProvisioningProvider {

	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(PolicyProvider.class);
	AppConfig appConfig;
	private String LOGTAG = "[PolicyProvider] ";
	private String accessKeyId;
	private String secretKey;
	private String roleArnPattern;
	private int roleAssumptionDurationSeconds = 0;
	private String samlIdpName = null;
	private String samlIssuer = null;
	private int maxSessionDurationSeconds = 0;
	int maxResults = 100;

	/**
	 * Build an AmazonIdentityManagement client connected to the correct account with the correct role
	 * @param accountId account
	 * @return client
	 */
	private AmazonIdentityManagement buildIamClient(String accountId) {
		// Build the roleArn of the role to assume from the base ARN and
		// the account number in the query spec.
		String roleArn = roleArnPattern.replace("ACCOUNT_NUMBER", accountId);

		// Instantiate a basic credential provider
		BasicAWSCredentials creds = new BasicAWSCredentials(accessKeyId, secretKey);
		AWSStaticCredentialsProvider cp = new AWSStaticCredentialsProvider(creds);

		// Create the STS client
		AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.standard()
				.withRegion("us-east-1")
				.withCredentials(cp)
				.build();

		// Assume the appropriate role in the appropriate account.
		AssumeRoleRequest assumeRequest = new AssumeRoleRequest()
				.withRoleArn(roleArn)
				.withDurationSeconds(roleAssumptionDurationSeconds)
				.withRoleSessionName("AwsAccountService");

		AssumeRoleResult assumeResult = sts.assumeRole(assumeRequest);
		Credentials credentials = assumeResult.getCredentials();

		// Instantiate a credential provider
		BasicSessionCredentials temporaryCredentials = new BasicSessionCredentials(credentials.getAccessKeyId(),
				credentials.getSecretAccessKey(), credentials.getSessionToken());
		AWSStaticCredentialsProvider sessionCreds = new AWSStaticCredentialsProvider(temporaryCredentials);

		// Create the IAM client
		return AmazonIdentityManagementClientBuilder.standard()
				.withRegion("us-east-1")
				.withCredentials(sessionCreds)
				.build();
	}


	@SuppressWarnings("unused")
	@Override
	public void init(AppConfig appConfig) throws ProviderException {

		this.appConfig = appConfig;
		try {
			Policy moapolicy = (Policy) appConfig.getObject("Policy.v1_0");
			PolicyRequisition policyRequisition = (PolicyRequisition) appConfig.getObject("PolicyRequisition.v1_0");
			PolicyQuerySpecification policyQuerySpec = (PolicyQuerySpecification) appConfig.getObject("PolicyQuerySpecification.v1_0");
			setProperties(this.appConfig.getProperties("PolicyProviderProperties"));
		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg = "Can't load Policy.v1_0 or PolicyRequisition.v1_0 or PolicyQuerySpecification.v1_0";
			logger.error(LOGTAG+errmsg);
			throw new ProviderException(errmsg,e);
		}

		accessKeyId = getProperties().getProperty("accessKeyId");
		logger.info(LOGTAG + "accessKeyId is: " + accessKeyId);

		secretKey = getProperties().getProperty("secretKey");
		logger.info(LOGTAG + "secretKey is: present");

		roleArnPattern = getProperties().getProperty("roleArnPattern");
		logger.info(LOGTAG + "roleArnPattern is: " + roleArnPattern);

		roleAssumptionDurationSeconds = Integer.parseInt(getProperties().getProperty("roleAssumptionDurationSeconds"));
		logger.info(LOGTAG + "roleAssumptionDurationSeconds is: " + roleAssumptionDurationSeconds);

		samlIdpName = getProperties().getProperty("samlIdpName");
		logger.info(LOGTAG + "samlIdpName is: " + samlIdpName);

		samlIssuer = getProperties().getProperty("samlIssuer");
		logger.info(LOGTAG + "samlIssuer is: " + samlIssuer);

		if (getProperties().getProperty("maxSessionDurationSeconds") != null) {
			maxSessionDurationSeconds = Integer.parseInt(getProperties().getProperty("maxSessionDurationSeconds"));
		} else {
			maxSessionDurationSeconds = 43_200;
		}
		logger.info(LOGTAG + "maxSessionDurationSeconds is: " + maxSessionDurationSeconds);


		if (getProperties().getProperty("maxResults") != null) {
			maxResults = Integer.parseInt(getProperties().getProperty("maxResults"));
		} else {
			maxResults = 100;
		}
		logger.info(LOGTAG + "maxResults is: " + maxResults);


		logger.info(LOGTAG + "Initialization complete.");

	}

	private void deletePolicy(Policy policy) throws ProviderException {

		try {

			DeletePolicyRequest deletePolicyRequest = new DeletePolicyRequest()
					.withPolicyArn(policy.getArn());

			DeletePolicyResult deletePolicyResult = buildIamClient(policy.getAccountId()).deletePolicy(deletePolicyRequest);
			logger.info(LOGTAG+"Policy deleted. ");

		} catch (RuntimeException e) {
			String errMsg = "An error occurred trying to delete the policy. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);       	      			
		}
	}

	/**
	 * The only thing that will be updated is the policy document
	 */
	private Policy updatePolicyDocument(Policy policy) throws ProviderException {
		String policyDocument = policy.getPolicyDocument();
		if (policyDocument == null) {
			String errMsg = "The policy document is required to update the policy.";
			throw new ProviderException(errMsg);       	      						
		}
		String policyArn = policy.getArn();
		try {
			
			//delete any versions that are not the default
			
			List<PolicyVersion> versions = new ArrayList<>();
			ListPolicyVersionsRequest listPolicyVersionsRequest = new ListPolicyVersionsRequest()
					.withPolicyArn(policyArn);
			AmazonIdentityManagement iamClient = buildIamClient(policy.getAccountId());
			do {
				ListPolicyVersionsResult result = iamClient.listPolicyVersions(listPolicyVersionsRequest);
				for (PolicyVersion version: result.getVersions()) {
					versions.add(version);
				}
				listPolicyVersionsRequest.setMarker(result.getMarker());
			} while (listPolicyVersionsRequest.getMarker() != null);
			
			logger.info(LOGTAG+"This policy has "+versions.size()+" non default version(s) to delete before updating,");
			
			for (PolicyVersion version: versions) {
				if (!version.isDefaultVersion()) {
					DeletePolicyVersionRequest req = new DeletePolicyVersionRequest()
							.withPolicyArn(policyArn)
							.withVersionId(version.getVersionId());
					iamClient.deletePolicyVersion(req);
					logger.info(LOGTAG+"Deleted version ID "+version.getVersionId());
				}
			}

			CreatePolicyVersionRequest createPolicyVersionRequest = new CreatePolicyVersionRequest()
					.withPolicyArn(policyArn)
					.withPolicyDocument(policyDocument)
					.withSetAsDefault(true);
			buildIamClient(policy.getAccountId()).createPolicyVersion(createPolicyVersionRequest);

			return policy;
		} catch (RuntimeException e) {
			String errMsg = "An error occurred trying to update the policy document. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg);       	      			
		}

	}

	private Policy createPolicy(PolicyRequisition policyRequisition) throws ProviderException {

		try {

			Policy moapolicy = (Policy) appConfig.getObject("Policy.v1_0");


			CreatePolicyRequest createPolicyRequest = new CreatePolicyRequest()
					.withPolicyName(policyRequisition.getName())
					.withDescription(policyRequisition.getDescription())
					.withPath(policyRequisition.getPath()==null?"/":policyRequisition.getPath())
					.withPolicyDocument(policyRequisition.getPolicyDocument());

			if (policyRequisition.getTag().size()>0) {
				List<com.amazonaws.services.identitymanagement.model.Tag> amzTags = new ArrayList<>();
				@SuppressWarnings("unchecked")
				List<com.amazon.aws.moa.objects.resources.v1_0.Tag> moaTags = policyRequisition.getTag();
				for (com.amazon.aws.moa.objects.resources.v1_0.Tag moaTag: moaTags) {
					amzTags.add(
							new com.amazonaws.services.identitymanagement.model.Tag()
							.withKey(moaTag.getKey())
							.withValue(moaTag.getValue()));
				}
				createPolicyRequest.setTags(amzTags);
			}

			CreatePolicyResult createPolicyResult = buildIamClient(policyRequisition.getAccountId()).createPolicy(createPolicyRequest);

			moapolicy.setAccountId(policyRequisition.getAccountId());
			moapolicy.setName(createPolicyResult.getPolicy().getPolicyName());
			moapolicy.setArn(createPolicyResult.getPolicy().getArn());
			moapolicy.setDescription(createPolicyResult.getPolicy().getDescription());
			moapolicy.setPath(createPolicyResult.getPolicy().getPath());
			moapolicy.setAttachmentCount(createPolicyResult.getPolicy().getAttachmentCount()+"");
			moapolicy.setDefaultVersionId(createPolicyResult.getPolicy().getDefaultVersionId());
			moapolicy.setIsAttachable(createPolicyResult.getPolicy().getIsAttachable()+"");
			moapolicy.setPermissionsBoundaryUsageCount(createPolicyResult.getPolicy().getPermissionsBoundaryUsageCount()+"");
			moapolicy.setTag(AwsProviderUtils.awsIamTags2moaTags(createPolicyResult.getPolicy().getTags(),moapolicy.newTag()));
			moapolicy.setLastUpdateDatetime(new Datetime(createPolicyResult.getPolicy().getUpdateDate()));
			moapolicy.setCreateDatetime(new Datetime(createPolicyResult.getPolicy().getCreateDate()));

			return moapolicy;

		} catch (RuntimeException | EnterpriseFieldException e) {
			e.printStackTrace();
			String errMsg = "An error occurred trying to create the policy. " +
					e.getMessage();
			logger.error(LOGTAG + errMsg);
			throw new ProviderException(errMsg,e);       	      			
		} catch (EnterpriseConfigurationObjectException e) {
			String errmsg="Policy.v1_0 is not found in the config.";
			logger.fatal(LOGTAG+errmsg);
			throw new ProviderException(errmsg);	
		}

	}

	@Override
	public ActionableEnterpriseObject generate(XmlEnterpriseObject policyRequisition, String msgAction)
			throws ProviderException {

		Policy policy = createPolicy((PolicyRequisition) policyRequisition);
		return policy;

	}

	@Override
	public ActionableEnterpriseObject delete(ActionableEnterpriseObject policy, String deleteAction) throws ProviderException {

		deletePolicy((Policy) policy);
		return policy;

	}

	@Override
	public List<ActionableEnterpriseObject> query(XmlEnterpriseObject querySpec1) throws ProviderException {
		PolicyQuerySpecification querySpec = (PolicyQuerySpecification) querySpec1;
		return AwsProviderUtils.getPolicy(querySpec1, buildIamClient(querySpec.getAccountId()), appConfig);

	}

	@Override
	public ActionableEnterpriseObject create(ActionableEnterpriseObject aeo) throws ProviderException {
		throw new ProviderException("Create not implemented for Policy. Use Generate instead.");
	}
	
	@Override
	public ActionableEnterpriseObject update(ActionableEnterpriseObject newaeo, XmlEnterpriseObject baselineAeo) throws ProviderException {
		return updatePolicyDocument((Policy)newaeo);
	}

}

package org.rhedcloud.idm.service.provider;

import java.util.List;

import org.openeai.config.AppConfig;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Policy;
import com.amazon.aws.moa.objects.resources.v1_0.PolicyQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.PolicyRequisition;



public interface AwsProvisioningProvider {

	public void init(AppConfig aConfig) throws ProviderException;

	public ActionableEnterpriseObject generate(XmlEnterpriseObject requisition, String msgAction)
			throws ProviderException;

	public ActionableEnterpriseObject delete(ActionableEnterpriseObject aeo, String deleteAction) throws ProviderException;
	
	public ActionableEnterpriseObject create(ActionableEnterpriseObject aeo) throws ProviderException;
	
	public ActionableEnterpriseObject update(ActionableEnterpriseObject newaeo, XmlEnterpriseObject baselineAeo) throws ProviderException;

	public List<ActionableEnterpriseObject> query(XmlEnterpriseObject querySpec) throws ProviderException;


}

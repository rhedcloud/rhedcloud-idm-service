/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/
package org.rhedcloud.idm.service.provider.netiq;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectImpl;
import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.RoleAssignmentProvider;
import org.rhedcloud.idm.service.provider.netiq.client.RoleServiceClient;
import org.rhedcloud.idm.service.provider.netiq.util.IdmValues;
import org.rhedcloud.idm.service.provider.netiq.util.LdsProviderUtil;
import org.rhedcloud.idm.service.util.CalendarUtil;
import org.rhedcloud.idm.service.util.MessageControl;
import org.rhedcloud.idm.service.util.PropertyNames;

import com.novell.www.role.service.DNString;
import com.novell.www.role.service.FindRoleByExampleWithOperatorResponseDocument;
import com.novell.www.role.service.GetAssignedIdentitiesResponse;
import com.novell.www.role.service.GetAssignedIdentitiesResponseDocument;
import com.novell.www.role.service.GetGroupResponse;
import com.novell.www.role.service.GetGroupResponseDocument;
import com.novell.www.role.service.GetUserResponse;
import com.novell.www.role.service.GetUserResponseDocument;
import com.novell.www.role.service.RequestRolesAssignmentResponse;
import com.novell.www.role.service.RequestRolesAssignmentResponseDocument;
import com.novell.www.role.service.Role;
import com.novell.www.role.service.RoleAssignmentType;

import edu.emory.idm.service.axis2.NrfServiceExceptionException;
import edu.emory.idm.service.axis2.RoleServiceStub;
import edu.emory.moa.jmsobjects.identity.v1_0.RoleAssignment;
import edu.emory.moa.objects.resources.v1_0.ExplicitIdentityDNs;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.RoleAssignmentRequisition;
import edu.emory.moa.objects.resources.v1_0.RoleDNs;

/**
 * This class actually implements the provider class RoleAssignmentProvider.
 * 
 * @author RXING2
 *
 */
public class NetiqRoleAssignmentProvider extends OpenEaiObject implements RoleAssignmentProvider {
	private static Logger logger = org.apache.logging.log4j.LogManager.getLogger(NetiqRoleAssignmentProvider.class);
	private AppConfig m_appConfig;
	private static int socketTimeout = 22000;
	private static String serviceUserId = "";
	private static String servicePassword = "";
	private static long waitInterval = 8000;
	private static long maxWaitTime = 30000;
	private static String searchServiceEndpoint = "";
	private static int connectionTimeout = 12000;
	private String LOGTAG = "[IdmRoleAssignmentProvider] ";
	private String peopleOu;

	/**
	 * @throws EnterpriseConfigurationObjectException
	 * @see RoleAssignmentProvider.java
	 */
	public void init(AppConfig aConfig) throws ProviderException {

		logger.info(LOGTAG + "Initializing...");
		setM_appConfig(aConfig);

		// Get the provider properties
		PropertyConfig pConfig = new PropertyConfig();
		try {
			pConfig = (PropertyConfig) aConfig.getObject("RoleAssignmentProviderProperties");
			Properties props = pConfig.getProperties();
			setProperties(props);
		}
		catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a PropertyConfig object from AppConfig: The exception is:";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, eoce);
		}

		if (getProperties() != null && !getProperties().isEmpty()) {

			// Set the web service endpoint that we should make calls
			// to. Otherwise, throw an exception
			if (getProperties().getProperty(PropertyNames.ROLE_WEB_SERVICE_ENDPOINT.toString()) != null) {
				setSearchServiceEndpoint(
						getProperties().getProperty(PropertyNames.ROLE_WEB_SERVICE_ENDPOINT.toString()));
				logger.info(
						LOGTAG + PropertyNames.ROLE_WEB_SERVICE_ENDPOINT.toString() + " = " + searchServiceEndpoint);
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.ROLE_WEB_SERVICE_ENDPOINT.toString()
				+ " not defined and is expected to be passed in via the Query-Request message ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// Set the user id to log into IDM and LDS with from config. If
			// it's not there, throw an error.
			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()) != null) {
				setServiceUserId(getProperties().getProperty(PropertyNames.WEB_SERVICE_USER_ID.toString()));
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_USER_ID.toString() + " = " + serviceUserId);
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_USER_ID.toString()
				+ " not defined and is expected to be passed in via the Query-Request message ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()) != null) {
				setServicePassword(getProperties().getProperty(PropertyNames.WEB_SERVICE_PASSWORD.toString()));
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.WEB_SERVICE_PASSWORD.toString()
				+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty("ldapUserId") != null) {
				ldapUserId = getProperties().getProperty("ldapUserId");
				logger.info(LOGTAG + PropertyNames.WEB_SERVICE_USER_ID.toString() + " = " + serviceUserId);
			}
			else {
				String errMsg = " REQUIRED Property ldapUserId" 
						+ " not defined and is expected to be passed in via the Query-Request message ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty("ldapPassword") != null) {
				ldapPassword = getProperties().getProperty("ldapPassword");
			}
			else {
				String errMsg = " REQUIRED Property ldapPassword" 
						+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}


			if (getProperties().getProperty("LdapHost") != null) {
				ldapHost = getProperties().getProperty("LdapHost");
			}
			else {
				String errMsg = " REQUIRED Property LdapHost not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty("peopleOu") != null) {
				peopleOu = getProperties().getProperty("peopleOu");
			}
			else {
				String errMsg = " REQUIRED Property peopleOu not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty("LdapPort") != null) {
				String ldapPortString = getProperties().getProperty("LdapPort");
				try {
					ldapPort = Integer.parseInt(ldapPortString);
				} catch (NumberFormatException e) {
					String errMsg = " REQUIRED Property LdapPort is not an Integer ";
					logger.info(LOGTAG + errMsg);
					throw new ProviderException(errMsg);					
				}
			}
			else {
				String errMsg = " REQUIRED Property LdapPort not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.WAIT_INTERVAL.toString()) != null) {
				setWaitInterval(Long.valueOf(getProperties().getProperty(PropertyNames.WAIT_INTERVAL.toString())));
				logger.info(LOGTAG + PropertyNames.WAIT_INTERVAL.toString() + " = " + waitInterval);
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.WAIT_INTERVAL.toString()
				+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			if (getProperties().getProperty(PropertyNames.MAX_WAIT_TIME.toString()) != null) {
				setMaxWaitTime(Long.valueOf(getProperties().getProperty(PropertyNames.MAX_WAIT_TIME.toString())));
				logger.info(LOGTAG + PropertyNames.MAX_WAIT_TIME.toString() + " = " + maxWaitTime);
			}
			else {
				String errMsg = " REQUIRED Property " + PropertyNames.MAX_WAIT_TIME.toString()
				+ " not defined and is expected to be passed in via the AppConfig file ";
				logger.info(LOGTAG + errMsg);
				throw new ProviderException(errMsg);
			}

			// If the parameter is not set up, use the Axis default
			// timeout
			if (getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString()) != null) {
				setConnectionTimeout(
						Integer.parseInt(getProperties().getProperty(PropertyNames.CONNECTION_TIMEOUT.toString())));
				logger.info(LOGTAG + PropertyNames.CONNECTION_TIMEOUT.toString() + " = " + connectionTimeout);
			}
			else {
				logger.info(
						LOGTAG + "OPTIONAL Property " + PropertyNames.CONNECTION_TIMEOUT.toString() + " not defined");
			}
			// If the parameter is not set up, use the Axis default
			// timeout
			if (getProperties().getProperty(PropertyNames.SOCKET_TIMEOUT.toString()) != null) {
				setSocketTimeout(
						Integer.parseInt(getProperties().getProperty(PropertyNames.SOCKET_TIMEOUT.toString())));
				logger.info(LOGTAG + PropertyNames.SOCKET_TIMEOUT.toString() + " = " + socketTimeout);
			}
			else {
				logger.info(LOGTAG + "OPTIONAL Property " + PropertyNames.SOCKET_TIMEOUT.toString() + " not defined");
			}
		}
		else {
			String errMsg = "REQUIRED Properties have not been defined. Please define required properties within AppConfig.";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg);
		}
	}

	/**
	 * 
	 * 
	 * @param queryData
	 * @return
	 * @throws ProviderException
	 * @throws edu.emory.idm.service.axis2.NrfServiceExceptionException
	 * @see org.rhedcloud.idm.service.provider.RoleAssignmentProvider#create(com.netiq.moa.jmsobjects.identity.v1_0.RoleAssignment)
	 */
	@Override
	public RoleAssignment generate(RoleAssignmentRequisition queryData, String msgAction) throws ProviderException {
		{
			// Dispatch the request to the host
			logger.info(LOGTAG + "In executeRoleServiceRequest(). Ready to execute request.");
			RoleServiceStub service = null;
			try {
				service = getRoleServiceStub(getServiceUserId(), getServicePassword(), getSearchServiceEndpoint(),
						getConnectionTimeout(), getSocketTimeout());
			}
			catch (RemoteException re) {
				String errMsg = "The Service stub is not started successfully. The error message is: ";
				logger.error(LOGTAG + errMsg + re.getMessage());
				throw new ProviderException(errMsg, re);
			}

			RequestRolesAssignmentResponseDocument responseDocument = null;
			String aRoleDn = null;
			RoleAssignment roleAssignment = null;
			try {
				responseDocument = RoleServiceClient.doRoleAssignmentGrant(service, queryData);
			}
			catch (RemoteException re) {
				String errMsg = "An error occurred retrieving an object from idm client during doRoleAssignmentGrant() "
						+ re.getMessage();
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, re);
			}
			catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

					String errMsg = "Server error from NetIQ during create() RoleAssignment, please check log for more detail. Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.warn(errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}

			// Process the response data
			RequestRolesAssignmentResponse resultData = responseDocument.getRequestRolesAssignmentResponse();
			try {
				aRoleDn = getRoleAssignmentDNResponse(resultData);
			}
			catch (EnterpriseConfigurationObjectException epco) {
				String errMsg = "An error occurred retrieving an object from idm client during doRoleAssignmentGrant()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, epco);
			}
			catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred retrieving an object from idm client during doRoleAssignmentGrant()";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, efe);
			}

			// If response data aRoleDn is returned, the data is accepted by the
			// NetIq however we need confirm the creation process is complete
			if (aRoleDn != null && aRoleDn.length() > 0) {
				RoleAssignment newRoleAssignment = new RoleAssignment();
				try {
					newRoleAssignment.setIdentityDN(queryData.getIdentityDN());
					RoleDNs aRoleDNs = new RoleDNs();
					aRoleDNs.setDistinguishedName(0, queryData.getRoleDNs().getDistinguishedName(0));
					newRoleAssignment.setRoleDNs(aRoleDNs);
					newRoleAssignment.setRoleAssignmentType(queryData.getRoleAssignmentType());
					newRoleAssignment.setRoleDN(queryData.getRoleDNs().getDistinguishedName(0));
				}
				catch (EnterpriseFieldException efe) {
					String errMsg = "An error occurred retrieving an object from idm client during doRoleAssignmentGrant()";
					logger.error((LOGTAG + errMsg));
				}

				roleAssignment = waitForCompletion(newRoleAssignment, msgAction, service);
			}

			try {
				service._getServiceClient().cleanupTransport();
			}
			catch (AxisFault e) {
				String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
				logger.error(errMsg);
			}

			return roleAssignment;
		}
	}

	/**
	 * 
	 * 
	 * @param queryData
	 * @return
	 * @throws ProviderException
	 * @see org.rhedcloud.idm.service.provider.RoleAssignmentProvider#delete(com.netiq.moa.jmsobjects.identity.v1_0.RoleAssignment)
	 */
	@Override
	public RoleAssignment delete(RoleAssignment queryData) throws ProviderException {

		// Dispatch the request to the host
		logger.info(LOGTAG + "In executeRoleServiceRequest(). Ready to execute request.");
		RoleServiceStub service = null;
		RequestRolesAssignmentResponseDocument responseDocument = null;
		String aRoleDn = null;
		RoleAssignment roleAssignment = null;

		try {
			service = getRoleServiceStub(getServiceUserId(), getServicePassword(), getSearchServiceEndpoint(),
					getConnectionTimeout(), getSocketTimeout());
		}
		catch (RemoteException re) {
			String errMsg = "The Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		try {
			responseDocument = RoleServiceClient.doRoleAssignmentRevoke(service, queryData);
		}
		catch (RemoteException re) {
			String errMsg = "An error occurred retrieving an object from idm client during delete()";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

				String errMsg = "Server error from NetIQ during delete() RoleAssignment, please check log for more detail. Error Message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.warn(errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}

		// Process the response data
		RequestRolesAssignmentResponse resultData = responseDocument.getRequestRolesAssignmentResponse();
		try {
			aRoleDn = getRoleAssignmentDNResponse(resultData);
		}
		catch (EnterpriseConfigurationObjectException ecoe) {
			String errMsg = "An error occurred retrieving an object from idm client during doRoleAssignmentGrant()";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, ecoe);
		}
		catch (EnterpriseFieldException efe) {
			String errMsg = "An error occurred retrieving an object from idm client during doRoleAssignmentGrant()";
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, efe);
		}

		if (aRoleDn != null && aRoleDn.length() > 0) {
			roleAssignment = waitForCompletion(queryData, MessageControl.DELETE_MSG_ACTION.toString(), service);
		}

		try {
			service._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return roleAssignment;
	}

	/**
	 * 
	 * 
	 * @param queryData
	 * @return
	 * @throws ProviderException
	 * @see org.rhedcloud.idm.service.provider.RoleAssignmentProvider#query(com.netiq.moa.objects.resources.v1_0.RoleAssignmentQuerySpecification)
	 */
	@Override
	public List<XmlEnterpriseObject> query(RoleAssignmentQuerySpecification queryData) throws ProviderException {

		// Dispatch the request to the host
		logger.info(LOGTAG + "In query(). Ready to execute request. Query data is "+queryData);

		if ("ECS_GROUP".equals(queryData.getIdentityType())) { //TODO: Soft code "ECS_QUERY"
			logger.info(LOGTAG + "Processing ECS_GROUP Query for '"+queryData.getRoleDN()+"'");
			return processLdsGroupQuery(queryData);
		}

		RoleServiceStub service = null;
		GetAssignedIdentitiesResponseDocument responseDocument = null;
		try {
			service = getRoleServiceStub(getServiceUserId(), getServicePassword(), getSearchServiceEndpoint(),
					getConnectionTimeout(), getSocketTimeout());
		}
		catch (RemoteException re) {
			String errMsg = "The Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		String dn = queryData.getRoleDN();
		if (dn !=null) {
			if (dn.indexOf("endPoint=")>0) {
				ArrayList<XmlEnterpriseObject> results = new ArrayList<XmlEnterpriseObject>();
				String rq = dn.split(",")[0];
				logger.info(LOGTAG +"Looking up role for "+rq);
				try {
					FindRoleByExampleWithOperatorResponseDocument rDoc = RoleServiceClient.doFindRoleByExampleWithOperator(service, rq);
					logger.info(LOGTAG +"Number of roles to query for assignments is "+rDoc.getFindRoleByExampleWithOperatorResponse().getResult().getRoleArray().length);
					for (Role role:rDoc.getFindRoleByExampleWithOperatorResponse().getResult().getRoleArray()) {
						//NetIQRoleAssignmentProvider.query 
						RoleAssignmentQuerySpecification qd = (RoleAssignmentQuerySpecification) queryData.clone();
						String entityKey = role.getEntityKey();
						qd.setRoleDN(entityKey);
						qd.setIdentityType("USER");
						qd.setDirectAssignOnly("true");
						logger.info(LOGTAG +"Getting role assignments for role "+entityKey);
						List<XmlEnterpriseObject> intresults = query(qd);
						logger.info("Found "+intresults.size()+" role assignments");
						results.addAll(intresults);
					}
					return results;
				} catch (RemoteException | NrfServiceExceptionException | CloneNotSupportedException | EnterpriseFieldException e) {
					e.printStackTrace();
					throw new ProviderException("",e);
				}
			}
		}

		try {
			responseDocument = RoleServiceClient.doRoleAssignedIdentitiesSearch(service, queryData);
		}
		catch (RemoteException re) {
			String errMsg = "An error occurred retrieving an object from idm client during doRoleAssignmentGrant() "
					+ re.getMessage();
			logger.error((LOGTAG + errMsg));
			throw new ProviderException(errMsg, re);
		}
		catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
			if (nsee != null && nsee.getFaultMessage() != null
					&& nsee.getFaultMessage().getNrfServiceException() != null
					&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

				String errMsg = "Server error from NetIQ during query() RoleAssignment, please check log for more detail. Error Message: "
						+ nsee.getFaultMessage().getNrfServiceException().getReason();
				logger.warn(errMsg);
				throw new ProviderException(errMsg, nsee);
			}
		}

		// Process the response data
		List<XmlEnterpriseObject> responseMoa = null;
		if (responseDocument != null) {

			GetAssignedIdentitiesResponse resultData = responseDocument.getGetAssignedIdentitiesResponse();
			if (resultData != null && resultData.getResult() != null) {
				logger.info(LOGTAG + "Size of DnstringArray for Identity Search: "
						+ resultData.getResult().getRoleassignmentArray().length);
				try {
					responseMoa = getRoleAssignmentResponse(resultData);
				}
				catch (EnterpriseConfigurationObjectException ecoe) {
					String errMsg = "An error occurred retrieving an object from idm client during doRoleAssignmentGrant()";
					logger.error((LOGTAG + errMsg));
					throw new ProviderException(errMsg, ecoe);
				}
				catch (EnterpriseFieldException efe) {
					String errMsg = "An error occurred retrieving an object from idm client during doRoleAssignmentGrant()";
					logger.error((LOGTAG + errMsg));
					throw new ProviderException(errMsg, efe);
				}
			}
		}

		try {
			service._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return responseMoa;
	}

	/**
	 * 
	 * 
	 * @param queryData
	 * @return
	 * @throws ProviderException
	 * @see org.rhedcloud.idm.service.provider.RoleAssignmentProvider#query(java.lang.String)
	 */
	@Override
	public List<XmlEnterpriseObject> query(String queryData, String identityType) throws ProviderException {

		// Dispatch the request to the host
		logger.info(LOGTAG + "In query(). Ready to execute doGetUserSearch() request.");
		RoleServiceStub service = null;
		GetUserResponseDocument responseDocument = null;
		GetGroupResponseDocument groupResponseDocument = null;
		List<XmlEnterpriseObject> responseMoa = null;

		try {
			service = getRoleServiceStub(getServiceUserId(), getServicePassword(), getSearchServiceEndpoint(),
					getConnectionTimeout(), getSocketTimeout());
		}
		catch (RemoteException re) {
			String errMsg = "The Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		if (identityType.equalsIgnoreCase(IdmValues.USER_TYPE.getName())) {
			try {
				responseDocument = RoleServiceClient.doGetUserSearch(service, queryData);
			}
			catch (RemoteException re) {
				String errMsg = "An error occurred retrieving an object from idm client during query()"
						+ re.getMessage();
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, re);
			}
			catch (Exception e) {
				String errMsg = "A server error occurred retrieving an object from IDM NetIQ during delete(), please check the log for more detail";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, e);
			}

			// Process the response data
			if (responseDocument != null) {

				GetUserResponse resultData = responseDocument.getGetUserResponse();
				if (resultData != null && resultData.getResult() != null
						&& resultData.getResult().getRoleAssignments() != null) {
					logger.info(LOGTAG + "Size of DnstringArray1: "
							+ resultData.getResult().getRoleAssignments().getRoleassignmentArray().length);
					try {
						responseMoa = getRoleAssignmentResponse(resultData);
					}
					catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an object from idm client during query()";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, ecoe);
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred retrieving an object from idm client during query()";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, efe);
					}
				}
			}
		}
		else if (identityType.equalsIgnoreCase(IdmValues.GROUP_TYPE.getName())) {
			try {
				groupResponseDocument = RoleServiceClient.doGetGroupSearch(service, queryData);
			}
			catch (RemoteException re) {
				String errMsg = "An error occurred retrieving an object from idm client quering group during query()"
						+ re.getMessage();
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, re);
			}
			catch (Exception e) {
				String errMsg = "A server error occurred retrieving a group object from IDM NetIQ during query(), please check the log for more detail";
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, e);
			}

			// Process the response data
			if (groupResponseDocument != null) {

				GetGroupResponse resultData = groupResponseDocument.getGetGroupResponse();
				if (resultData != null && resultData.getResult() != null
						&& resultData.getResult().getRoleAssignments() != null) {
					logger.info(LOGTAG + "Size of DnstringArray for Group query: "
							+ resultData.getResult().getRoleAssignments().getRoleassignmentArray().length);
					try {
						responseMoa = getRoleAssignmentResponse(resultData);
					}
					catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an object from idm client during query()";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, ecoe);
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred retrieving an object from idm client during query()";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, efe);
					}
				}
			}
		}

		try {
			service._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}
		return responseMoa;
	}

	/**
	 * 
	 * 
	 * @param queryData
	 * @return
	 * @throws ProviderException
	 * @see org.rhedcloud.idm.service.provider.RoleAssignmentProvider#query(java.lang.String)
	 */
	@Override
	public RoleAssignment query(RoleAssignment queryData) throws ProviderException {

		// Dispatch the request to the host
		logger.info(LOGTAG + "In executeRoleServiceRequest(). Ready to execute request.");
		RoleServiceStub service = null;
		RoleAssignment responseMoa = null;
		GetUserResponseDocument responseDocument = null;
		GetGroupResponseDocument groupResponseDocument = null;
		String dn = null;

		try {
			service = getRoleServiceStub(getServiceUserId(), getServicePassword(), getSearchServiceEndpoint(),
					getConnectionTimeout(), getSocketTimeout());
		}
		catch (RemoteException re) {
			String errMsg = "The Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		if (queryData.getExplicitIdentityDNs() != null
				&& queryData.getExplicitIdentityDNs().getDistinguishedName(0) != null
				&& queryData.getExplicitIdentityDNs().getDistinguishedName(0).length() > 0) {
			dn = queryData.getExplicitIdentityDNs().getDistinguishedName(0);
		}
		else {
			dn = queryData.getIdentityDN();
		}

		if (queryData.getRoleAssignmentType().equalsIgnoreCase(IdmValues.USER_TO_ROLE.getName())) {

			logger.info(LOGTAG + "In query() user dn: " + dn);
			try {
				responseDocument = RoleServiceClient.doGetUserSearch(service, dn);
			}
			catch (RemoteException re) {
				String errMsg = "An error occurred retrieving an user object from idm client during query()"
						+ re.getMessage();
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, re);
			}
			catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

					String errMsg = "An error occurred querying ResourceAssignment User object during query. Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.error(errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}

			// Process the response data
			if (responseDocument != null) {
				GetUserResponse resultData = responseDocument.getGetUserResponse();
				if (resultData != null && resultData.getResult() != null
						&& resultData.getResult().getRoleAssignments() != null) {
					logger.info(LOGTAG + "Size of User DnstringArray: "
							+ resultData.getResult().getRoleAssignments().getRoleassignmentArray().length);
					try {
						responseMoa = getRoleAssignmentCompleteResponse(resultData, queryData);
					}
					catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an User object from idm client during query(querydata)";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, ecoe);
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred retrieving an User object from idm client during query(querydata)";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, efe);
					}
				}
			}
		}
		else {

			logger.info(LOGTAG + "In query() for group dn: " + dn);
			try {
				groupResponseDocument = RoleServiceClient.doGetGroupSearch(service, dn);
			}
			catch (RemoteException re) {
				String errMsg = "An error occurred retrieving an Group object from idm client during query(querydata)"
						+ re.getMessage();
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, re);
			}
			catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

					String errMsg = "An error occurred querying ResourceAssignment group object during query(querydata). Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.error(errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}

			// Process the response data
			if (groupResponseDocument != null) {
				GetGroupResponse resultData = groupResponseDocument.getGetGroupResponse();
				if (resultData != null && resultData.getResult() != null
						&& resultData.getResult().getRoleAssignments() != null) {
					logger.info(LOGTAG + "Size of Group DnstringArray: "
							+ resultData.getResult().getRoleAssignments().getRoleassignmentArray().length);
					try {
						responseMoa = getRoleAssignmentGroupCompleteResponse(resultData, queryData);
					}
					catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an object from idm client during query(querydata)";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, ecoe);
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred retrieving an object from idm client during query(querydata)";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, efe);
					}
				}
			}
		}

		try {
			service._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return responseMoa;
	}

	/**
	 * Maps and transforms the result data to a list of oEAI enterprise objects,
	 * returns the oEAI objects to the command class for messaging related
	 * processing.
	 * 
	 * @param resultData
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 * @throws EnterpriseFieldException
	 */
	private List<XmlEnterpriseObject> getRoleAssignmentResponse(Object object)
			throws EnterpriseConfigurationObjectException, EnterpriseFieldException {

		List<XmlEnterpriseObject> roleAssignmentObjects = new ArrayList<XmlEnterpriseObject>();
		if (object instanceof GetAssignedIdentitiesResponse) {

			for (com.novell.www.role.service.RoleAssignment roleAssignment : ((GetAssignedIdentitiesResponse) object)
					.getResult().getRoleassignmentArray()) {
				// Create our return object and make sure it's clear of data.
				RoleAssignment roleAssignmentObject = this.getObject(RoleAssignment.class);
				if (roleAssignment != null) {
					if (roleAssignment.getAssignmentType() != null) {
						roleAssignmentObject.setRoleAssignmentType(roleAssignment.getAssignmentType().toString());
					}
					if (roleAssignment.getEffectiveDate() != null) {
						roleAssignmentObject
						.setEffectiveDatetime(CalendarUtil.convertDatetime(roleAssignment.getEffectiveDate()));
					}
					if (roleAssignment.getRole() != null) {
						RoleDNs roleDNs = new RoleDNs();
						roleDNs.setDistinguishedName(0, roleAssignment.getRole());
						roleAssignmentObject.setRoleDN(roleAssignment.getRole());
						roleAssignmentObject.setRoleDNs(roleDNs);
					}
					if (roleAssignment.getExplicitIdentities() != null
							&& roleAssignment.getExplicitIdentities().getDnstringArray() != null) {
						ExplicitIdentityDNs dns = new ExplicitIdentityDNs();
						for (DNString dnstring : roleAssignment.getExplicitIdentities().getDnstringArray()) {
							if (dnstring != null && dnstring.getDn() != null && dnstring.getDn().length() > 0) {
								dns.addDistinguishedName(dnstring.getDn());
							}
						}
						roleAssignmentObject.setExplicitIdentityDNs(dns);
					}
				}
				roleAssignmentObjects.add(roleAssignmentObject);
			}
		}
		else if (object instanceof GetUserResponse) {

			for (com.novell.www.role.service.RoleAssignment roleAssignment : ((GetUserResponse) object).getResult()
					.getRoleAssignments().getRoleassignmentArray()) {

				RoleAssignment roleAssignmentObject = this.getObject(RoleAssignment.class);
				if (roleAssignment != null && roleAssignment.getAssignmentType() == RoleAssignmentType.USER_TO_ROLE) {

					if (roleAssignment.getAssignmentType() != null) {
						roleAssignmentObject.setRoleAssignmentType(roleAssignment.getAssignmentType().toString());
					}
					if (roleAssignment.getEffectiveDate() != null) {
						roleAssignmentObject
						.setEffectiveDatetime(CalendarUtil.convertDatetime(roleAssignment.getEffectiveDate()));
					}
					if (roleAssignment.getRole() != null) {
						roleAssignmentObject.setRoleDN(roleAssignment.getRole());
						RoleDNs roleDNs = new RoleDNs();
						roleDNs.setDistinguishedName(0, roleAssignment.getRole());
						roleAssignmentObject.setRoleDNs(roleDNs);
					}
					if (roleAssignment.getExplicitIdentities() != null
							&& roleAssignment.getExplicitIdentities().getDnstringArray() != null) {
						ExplicitIdentityDNs dns = new ExplicitIdentityDNs();
						for (DNString dnstring : roleAssignment.getExplicitIdentities().getDnstringArray()) {
							if (dnstring != null && dnstring.getDn() != null && dnstring.getDn().length() > 0) {
								dns.addDistinguishedName(dnstring.getDn());
							}
						}
						roleAssignmentObject.setExplicitIdentityDNs(dns);
					}
					roleAssignmentObjects.add(roleAssignmentObject);
				}
			}
		}
		else if (object instanceof GetGroupResponse) {
			for (com.novell.www.role.service.RoleAssignment roleAssignment : ((GetGroupResponse) object).getResult()
					.getRoleAssignments().getRoleassignmentArray()) {

				RoleAssignment roleAssignmentObject = this.getObject(RoleAssignment.class);
				if (roleAssignment != null && roleAssignment.getAssignmentType() == RoleAssignmentType.GROUP_TO_ROLE) {

					if (roleAssignment.getAssignmentType() != null) {
						roleAssignmentObject.setRoleAssignmentType(roleAssignment.getAssignmentType().toString());
					}
					if (roleAssignment.getEffectiveDate() != null) {
						roleAssignmentObject
						.setEffectiveDatetime(CalendarUtil.convertDatetime(roleAssignment.getEffectiveDate()));
					}
					if (roleAssignment.getRole() != null) {
						roleAssignmentObject.setRoleDN(roleAssignment.getRole());
						RoleDNs roleDNs = new RoleDNs();
						roleDNs.setDistinguishedName(0, roleAssignment.getRole());
						roleAssignmentObject.setRoleDNs(roleDNs);
					}
					if (roleAssignment.getExplicitIdentities() != null
							&& roleAssignment.getExplicitIdentities().getDnstringArray() != null) {
						ExplicitIdentityDNs dns = new ExplicitIdentityDNs();
						for (DNString dnstring : roleAssignment.getExplicitIdentities().getDnstringArray()) {
							if (dnstring != null && dnstring.getDn() != null && dnstring.getDn().length() > 0) {
								dns.addDistinguishedName(dnstring.getDn());
							}
						}
						roleAssignmentObject.setExplicitIdentityDNs(dns);
					}
					roleAssignmentObjects.add(roleAssignmentObject);
				}
			}
		}
		return roleAssignmentObjects;
	}

	static String ldapHost;
	static int	ldapPort;
	private static String ldapUserId;
	private static String ldapPassword;

	/**
	 * Uses the LDAP server for the source of RoleAssignments (instead of NetIQ) when the identity type is ECS_GROUP
	 * @param queryData
	 * @return
	 * @throws ProviderException
	 */
	List<XmlEnterpriseObject> processLdsGroupQuery(RoleAssignmentQuerySpecification queryData) throws ProviderException {
		long startTiming = System.currentTimeMillis();
		LdsProviderUtil.initLdapPool(ldapHost, ldapPort, ldapUserId, ldapPassword);
		LdapConnection connection = null;
		try {
			connection = LdsProviderUtil.getLdapConnection();

			String dn = queryData.getRoleDN();
			String[] memberObjectClassCn = {"cn","member","objectClass"};
			SearchScope searchScope = null;
			if (dn.trim().startsWith("CN=")) {
				searchScope = SearchScope.OBJECT;
			} else {
				searchScope = SearchScope.SUBTREE;
			}
			EntryCursor cursor = connection.search(dn, "(objectClass=group)", searchScope, 
					memberObjectClassCn);

			List<XmlEnterpriseObject> roleAssignments = new ArrayList<>();

			for (Entry ent : cursor) { 
				if (!ent.hasObjectClass("group")) { 
					throw new ProviderException("Entry is not a group");
				}

				String derivedCN = null; 
				if (searchScope == SearchScope.SUBTREE) {
						derivedCN = ent.get("cn").toString().replace("cn","CN").replace(':', '=');			
				}

				for (Attribute att : ent.getAttributes()) {
					if ("member".equals(att.getUpId())) {
						Iterator<Value<?>> members = att.iterator();
						while (members.hasNext()) {
							Value<?> member = members.next();
							// CN=jburk22,OU=People,DC=emory,DC=edu is an example of member.getValue()
							String memberDn = ((String) member.getValue()).split(",")[0]
									+ "," + peopleOu;
							String[] returningAttributes = {"serialNumber"};
							EntryCursor peopleCursor = connection.search(memberDn, "(objectClass=person)", SearchScope.OBJECT, 
									returningAttributes);
							for (Entry entPerson : peopleCursor) {
								for (Attribute attPerson : entPerson.getAttributes()) {
									if ("serialNumber".equals(attPerson.getUpId()) ) {
										String serialNumber = attPerson.getString();
										RoleAssignment roleAssignment;
										try {
											roleAssignment = (RoleAssignment) getM_appConfig().getObject("RoleAssignment.v1_0");
											if (derivedCN==null) {
												roleAssignment.setRoleDN(queryData.getRoleDN());
											} else {
												roleAssignment.setRoleDN(derivedCN+","+queryData.getRoleDN());
											}
											ExplicitIdentityDNs eidns = roleAssignment.newExplicitIdentityDNs();
											eidns.addDistinguishedName("CN="+serialNumber+","+memberDn);
											roleAssignment.setExplicitIdentityDNs(eidns);
											roleAssignments.add(roleAssignment);
										} catch (EnterpriseConfigurationObjectException | EnterpriseFieldException e) {
											throw new ProviderException("Caught error: ",e);
										}
										break;
									}
								}
							}
						}
					}
				}
			}

			return roleAssignments;

		} catch (LdapException e) {
			throw new ProviderException(e.getMessage(), e);
		} finally {
			LdsProviderUtil.releaseLdapConnection(connection);
			long duration = System.currentTimeMillis() - startTiming;
			logger.info(LOGTAG+" - LDAP Group query complete in " + duration + " ms.");
		}

	}

	/**
	 * Maps and transforms the result data to a list of oEAI enterprise objects,
	 * returns the oEAI objects to the command class for messaging related
	 * processing.
	 * 
	 * @param
	 * 
	 * @param resultData
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 * @throws EnterpriseFieldException
	 */
	private RoleAssignment getRoleAssignmentCompleteResponse(Object object, RoleAssignment queryData)
			throws EnterpriseConfigurationObjectException, EnterpriseFieldException {

		RoleAssignment roleAssignmentObject = null;
		ExplicitIdentityDNs dns = new ExplicitIdentityDNs();
		String aDn = null;
		for (com.novell.www.role.service.RoleAssignment roleAssignment : ((GetUserResponse) object).getResult()
				.getRoleAssignments().getRoleassignmentArray()) {
			if (roleAssignment.getRole().equalsIgnoreCase(queryData.getRoleDN())) {
				if (roleAssignment.getAssignmentType().toString().equalsIgnoreCase(IdmValues.USER_TO_ROLE.getName())) {
					roleAssignmentObject = this.getObject(RoleAssignment.class);
					if (roleAssignment != null) {
						if (roleAssignment.getAssignmentType() != null) {
							roleAssignmentObject.setRoleAssignmentType(roleAssignment.getAssignmentType().toString());
						}
						if (roleAssignment.getEffectiveDate() != null) {
							roleAssignmentObject.setEffectiveDatetime(
									CalendarUtil.convertDatetime(roleAssignment.getEffectiveDate()));
						}
						roleAssignmentObject.setRoleDN(roleAssignment.getRole());
						RoleDNs roleDNs = new RoleDNs();
						roleDNs.setDistinguishedName(0, roleAssignment.getRole());
						roleAssignmentObject.setRoleDNs(roleDNs);
						if (roleAssignment.getExplicitIdentities() != null
								&& roleAssignment.getExplicitIdentities().getDnstringArray() != null) {
							for (DNString dnstring : roleAssignment.getExplicitIdentities().getDnstringArray()) {
								if (dnstring != null && dnstring.getDn() != null && dnstring.getDn().length() > 0) {
									aDn = dnstring.getDn();
									// dns.addDistinguishedName(dnstring.getDn());
									break;
								}
							}
						}
					}

					dns.addDistinguishedName(aDn);
					roleAssignmentObject.setExplicitIdentityDNs(dns);
					logger.info(LOGTAG + "Matching record found ");
				}
			}
		}
		return roleAssignmentObject;
	}

	/**
	 * 
	 * @param object
	 * @param queryData
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 * @throws EnterpriseFieldException
	 */
	private RoleAssignment getRoleAssignmentGroupCompleteResponse(Object object, RoleAssignment queryData)
			throws EnterpriseConfigurationObjectException, EnterpriseFieldException {

		RoleAssignment roleAssignmentObject = null;
		ExplicitIdentityDNs dns = new ExplicitIdentityDNs();
		String aDn = null;

		for (com.novell.www.role.service.RoleAssignment roleAssignment : ((GetGroupResponse) object).getResult()
				.getRoleAssignments().getRoleassignmentArray()) {

			if (roleAssignment.getRole().equalsIgnoreCase(queryData.getRoleDN())) {
				if (roleAssignment.getAssignmentType().toString().equalsIgnoreCase(IdmValues.GROUP_TO_ROLE.getName())) {
					roleAssignmentObject = this.getObject(RoleAssignment.class);
					if (roleAssignment != null) {
						if (roleAssignment.getAssignmentType() != null) {
							roleAssignmentObject.setRoleAssignmentType(roleAssignment.getAssignmentType().toString());
						}
						if (roleAssignment.getEffectiveDate() != null) {
							roleAssignmentObject.setEffectiveDatetime(
									CalendarUtil.convertDatetime(roleAssignment.getEffectiveDate()));
						}
						roleAssignmentObject.setRoleDN(roleAssignment.getRole());
						RoleDNs roleDNs = new RoleDNs();
						roleDNs.setDistinguishedName(0, roleAssignment.getRole());
						roleAssignmentObject.setRoleDNs(roleDNs);
						if (roleAssignment.getExplicitIdentities() != null
								&& roleAssignment.getExplicitIdentities().getDnstringArray() != null) {
							for (DNString dnstring : roleAssignment.getExplicitIdentities().getDnstringArray()) {
								if (dnstring != null && dnstring.getDn() != null && dnstring.getDn().length() > 0) {
									aDn = dnstring.getDn();
									break;
								}
							}
						}
					}
				}

				dns.addDistinguishedName(aDn);
				roleAssignmentObject.setExplicitIdentityDNs(dns);
				logger.info(LOGTAG + "Matching record found for Group Search");
			}
		}
		return roleAssignmentObject;
	}

	/**
	 * Converts oEAI object type to XmlEnterpriseObject
	 * 
	 * @param aClass
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 */
	@SuppressWarnings("unchecked")
	private <T extends XmlEnterpriseObjectImpl> T getObject(Class<T> aClass)
			throws EnterpriseConfigurationObjectException {
		return (T) getM_appConfig().getObjectByType(aClass.getName());
	}

	/**
	 * Retrieves the roleDNs response string from the result set and returns to the
	 * calling method
	 * 
	 * @param resultData
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 * @throws EnterpriseFieldException
	 */
	private String getRoleAssignmentDNResponse(RequestRolesAssignmentResponse resultData)
			throws EnterpriseConfigurationObjectException, EnterpriseFieldException {

		String roleDN = null;
		for (DNString dnstring : resultData.getResult().getDnstringArray()) {
			// get the dnstring from the response in order to create our return
			// object.
			if (dnstring != null && dnstring.getDn() != null && dnstring.getDn().length() > 0) {
				roleDN = dnstring.getDn();
				break;
			}
		}
		return roleDN;
	}

	private RoleAssignment waitForCompletion(RoleAssignment queryData, String action, RoleServiceStub service)
			throws ProviderException {

		boolean isCompleted = false;
		long startTime = System.currentTimeMillis();
		RoleAssignment roleAssignment = null;
		while (isCompleted == false) {
			// Throw an exception if maxWaitTime is exceeded.
			if (System.currentTimeMillis() - startTime > getMaxWaitTime()) {
				String errMsg = "Maximum wait time of " + getMaxWaitTime()
				+ " ms for completion of RoleAssignment object has been "
				+ "exceeded. Processing RoleAssignment may still be in progress.";
				throw new ProviderException(errMsg);
			}
			roleAssignment = query(queryData, service);
			if (roleAssignment != null) {
				logger.info(LOGTAG + "Validate if roleAssignment creation or deletion is complete.");
				if (action.equalsIgnoreCase(MessageControl.GENERATE_MSG_ACTION.toString())) {
					isCompleted = true;
					try {
						roleAssignment.setRoleAssignmentActionType("grant");
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred retrieving an object from idm client in waitForCompletion()";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, efe);
					}
					break;
				}
				else {
					logger.info(LOGTAG + "Sleeping for " + getWaitInterval() + " ms...");
					try {
						Thread.sleep(getWaitInterval());
					}
					catch (InterruptedException swallowed) {
						logger.info("Thread interrupted, do nothing " + swallowed.getMessage());
					}
					isCompleted = false;
				}
			}
			else {
				if (action.equalsIgnoreCase(MessageControl.DELETE_MSG_ACTION.toString())) {
					isCompleted = true;
					break;
				}
				else {
					// this is for waiting create
					try {
						Thread.sleep(getWaitInterval());
					}
					catch (InterruptedException swallowed) {
						logger.info("Thread interrupted, do nothing " + swallowed.getMessage());
					}
				}
			}
		}
		return roleAssignment;
	}

	/**
	 * 
	 * @param queryData
	 * @param service
	 * @return
	 * @throws ProviderException
	 */
	private RoleAssignment query(RoleAssignment queryData, RoleServiceStub service) throws ProviderException {

		// Dispatch the request to the host
		logger.info(LOGTAG + "In executeRoleServiceRequest(). Ready to execute request.");
		GetUserResponseDocument responseDocument = null;
		GetGroupResponseDocument groupResponseDocument = null;
		RoleAssignment responseMoa = null;
		String dn = null;

		if (queryData.getExplicitIdentityDNs() != null
				&& queryData.getExplicitIdentityDNs().getDistinguishedName(0) != null
				&& queryData.getExplicitIdentityDNs().getDistinguishedName(0).length() > 0) {
			dn = queryData.getExplicitIdentityDNs().getDistinguishedName(0);
		}
		else {
			dn = queryData.getIdentityDN();
		}
		if (queryData.getRoleAssignmentType().equalsIgnoreCase(IdmValues.USER_TO_ROLE.getName())) {

			logger.info(LOGTAG + "In query() dn: " + dn);
			try {
				responseDocument = RoleServiceClient.doGetUserSearch(service, dn);
			}
			catch (RemoteException re) {
				String errMsg = "An error occurred retrieving an object from idm client during query()"
						+ re.getMessage();
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, re);
			}
			catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

					String errMsg = "An error occurred querying ResourceAssignment object during query(). Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.error(errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}

			// Process the response data
			if (responseDocument != null) {
				GetUserResponse resultData = responseDocument.getGetUserResponse();
				if (resultData != null && resultData.getResult() != null
						&& resultData.getResult().getRoleAssignments() != null) {
					logger.info(LOGTAG + "Size of User DnstringArray: "
							+ resultData.getResult().getRoleAssignments().getRoleassignmentArray().length);
					try {
						responseMoa = getRoleAssignmentCompleteResponse(resultData, queryData);
					}
					catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an object from idm client during query()";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, ecoe);
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred retrieving an object from idm client during query()";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, efe);
					}
				}
			}
		}
		else {
			logger.info(LOGTAG + "In query() for group dn: " + dn);
			try {
				groupResponseDocument = RoleServiceClient.doGetGroupSearch(service, dn);
			}
			catch (RemoteException re) {
				String errMsg = "An error occurred retrieving a group object from idm client during query()"
						+ re.getMessage();
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, re);
			}
			catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

					String errMsg = "An error occurred querying ResourceAssignment object during delete(). Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.error(errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}

			// Process the response data
			if (groupResponseDocument != null) {
				GetGroupResponse resultData = groupResponseDocument.getGetGroupResponse();
				if (resultData != null && resultData.getResult() != null
						&& resultData.getResult().getRoleAssignments() != null) {
					logger.info(LOGTAG + "Size of Group DnstringArray: "
							+ resultData.getResult().getRoleAssignments().getRoleassignmentArray().length);
					try {
						responseMoa = getRoleAssignmentGroupCompleteResponse(resultData, queryData);
					}
					catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an object from idm client during query()";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, ecoe);
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred retrieving an object from idm client during query()";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, efe);
					}
				}
			}
		}

		try {
			service._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return responseMoa;
	}

	@Override
	public boolean isUserRoleBeingGranted(Object object) throws ProviderException {

		RoleAssignment persistedRoleAssignment = new RoleAssignment();
		RoleAssignment queryData = new RoleAssignment();
		if (object instanceof RoleAssignmentRequisition) {
			try {
				queryData.setIdentityDN(((RoleAssignmentRequisition) object).getIdentityDN());
				RoleDNs aRoleDNs = new RoleDNs();
				aRoleDNs.setDistinguishedName(0,
						((RoleAssignmentRequisition) object).getRoleDNs().getDistinguishedName(0));
				queryData.setRoleDNs(aRoleDNs);
				queryData.setRoleAssignmentType(((RoleAssignmentRequisition) object).getRoleAssignmentType());
				queryData.setRoleDN(((RoleAssignmentRequisition) object).getRoleDNs().getDistinguishedName(0));
			}
			catch (EnterpriseFieldException efe) {
				String errMsg = "An error occurred retrieving an object from idm client during isUserRoleBeingGranted()";
				logger.error((LOGTAG + errMsg));
			}
		}
		else if (object instanceof RoleAssignment) {
			queryData = (RoleAssignment) object;
		}

		logger.info(LOGTAG + " Validate if the user has been being granted for the Role.");
		persistedRoleAssignment = query(queryData);
		if (persistedRoleAssignment != null) {
			return true;
		}
		return false;
	}

	@Override
	public List<XmlEnterpriseObject> query(String userDn, String roleDn, String identityType) throws ProviderException {

		// Dispatch the request to the host
		logger.info(LOGTAG + "In query() to search by userDn and roleDn. Ready to execute request.");
		RoleServiceStub service = null;
		GetUserResponseDocument responseDocument = null;
		GetGroupResponseDocument groupResponseDocument = null;
		List<XmlEnterpriseObject> responseMoa = null;
		String dn = userDn;

		try {
			service = getRoleServiceStub(getServiceUserId(), getServicePassword(), getSearchServiceEndpoint(),
					getConnectionTimeout(), getSocketTimeout());
		}
		catch (RemoteException re) {
			String errMsg = "The Service stub is not started successfully. The error message is: ";
			logger.error(LOGTAG + errMsg + re.getMessage());
			throw new ProviderException(errMsg, re);
		}

		logger.info(LOGTAG + "In query() by userDn and roleDn, userDn: " + dn);
		if (identityType.equalsIgnoreCase(IdmValues.USER_TYPE.getName())) {
			try {
				responseDocument = RoleServiceClient.doGetUserSearch(service, dn);
			}
			catch (RemoteException re) {
				String errMsg = "An error occurred retrieving an object from idm client during query(userdn, roledn, type)"
						+ re.getMessage();
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, re);
			}
			catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

					String errMsg = "An error occurred querying ResourceAssignment object during query(userdn, roledn, type). Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.error(errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}

			// Process the response data
			if (responseDocument != null) {

				GetUserResponse resultData = responseDocument.getGetUserResponse();
				if (resultData != null && resultData.getResult() != null
						&& resultData.getResult().getRoleAssignments() != null) {
					logger.info(LOGTAG + "Size of DnstringArray for querying idenities: "
							+ resultData.getResult().getRoleAssignments().getRoleassignmentArray().length);
					try {
						responseMoa = getRoleAssignmentUniqueResponse(resultData, roleDn);
					}
					catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an user object from idm client during query(userdn, roledn, user)";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, ecoe);
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred retrieving an user object from idm client during query(userdn, roledn, user)";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, efe);
					}
				}
			}
		}
		else {
			try {
				groupResponseDocument = RoleServiceClient.doGetGroupSearch(service, dn);
			}
			catch (RemoteException re) {
				String errMsg = "An error occurred retrieving an object from idm client during query (userdn, roledn, group)"
						+ re.getMessage();
				logger.error((LOGTAG + errMsg));
				throw new ProviderException(errMsg, re);
			}
			catch (edu.emory.idm.service.axis2.NrfServiceExceptionException nsee) {
				if (nsee != null && nsee.getFaultMessage() != null
						&& nsee.getFaultMessage().getNrfServiceException() != null
						&& nsee.getFaultMessage().getNrfServiceException().getReason() != null) {

					String errMsg = "An error occurred querying ResourceAssignment object during query(userdn, roledn, group). Error Message: "
							+ nsee.getFaultMessage().getNrfServiceException().getReason();
					logger.error(errMsg);
					throw new ProviderException(errMsg, nsee);
				}
			}

			// Process the response data
			if (groupResponseDocument != null) {

				GetGroupResponse resultData = groupResponseDocument.getGetGroupResponse();
				if (resultData != null && resultData.getResult() != null
						&& resultData.getResult().getRoleAssignments() != null) {
					logger.info(LOGTAG + "Size of DnstringArray for querying idenities of group: "
							+ resultData.getResult().getRoleAssignments().getRoleassignmentArray().length);
					try {
						responseMoa = getRoleAssignmentUniqueResponse(resultData, roleDn);
					}
					catch (EnterpriseConfigurationObjectException ecoe) {
						String errMsg = "An error occurred retrieving an user object from idm client during query(userdn, roledn, group)";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, ecoe);
					}
					catch (EnterpriseFieldException efe) {
						String errMsg = "An error occurred retrieving an user object from idm client during query(userdn, roledn, gropu)";
						logger.error((LOGTAG + errMsg));
						throw new ProviderException(errMsg, efe);
					}
				}
			}
		}

		try {
			service._getServiceClient().cleanupTransport();
		}
		catch (AxisFault e) {
			String errMsg = "Error occurred while shuting down oconnection manager. The connection manager can only close before thread exists";
			logger.error(errMsg);
		}

		return responseMoa;
	}

	/**
	 * Maps and transforms the result data to a list of oEAI enterprise objects,
	 * returns the oEAI objects to the command class for messaging related
	 * processing.
	 * 
	 * @param resultData
	 * @return
	 * @throws EnterpriseConfigurationObjectException
	 * @throws EnterpriseFieldException
	 */
	private List<XmlEnterpriseObject> getRoleAssignmentUniqueResponse(Object object, String roleDn)
			throws EnterpriseConfigurationObjectException, EnterpriseFieldException {

		List<XmlEnterpriseObject> roleAssignmentObjects = new ArrayList<XmlEnterpriseObject>();
		if (object instanceof GetUserResponse) {
			for (com.novell.www.role.service.RoleAssignment roleAssignment : ((GetUserResponse) object).getResult()
					.getRoleAssignments().getRoleassignmentArray()) {

				if (roleAssignment.getRole().equalsIgnoreCase(roleDn)) {
					RoleAssignment roleAssignmentObject = this.getObject(RoleAssignment.class);

					if (roleAssignment != null
							&& roleAssignment.getAssignmentType() == RoleAssignmentType.USER_TO_ROLE) {
						if (roleAssignment.getAssignmentType() != null) {
							roleAssignmentObject.setRoleAssignmentType(roleAssignment.getAssignmentType().toString());
						}
						if (roleAssignment.getEffectiveDate() != null) {
							roleAssignmentObject.setEffectiveDatetime(
									CalendarUtil.convertDatetime(roleAssignment.getEffectiveDate()));
						}
						if (roleAssignment.getRole() != null) {
							roleAssignmentObject.setRoleDN(roleAssignment.getRole());
							RoleDNs roleDNs = new RoleDNs();
							roleDNs.setDistinguishedName(0, roleAssignment.getRole());
							roleAssignmentObject.setRoleDNs(roleDNs);
						}
						if (roleAssignment.getExplicitIdentities() != null
								&& roleAssignment.getExplicitIdentities().getDnstringArray() != null) {
							ExplicitIdentityDNs dns = new ExplicitIdentityDNs();
							for (DNString dnstring : roleAssignment.getExplicitIdentities().getDnstringArray()) {
								if (dnstring != null && dnstring.getDn() != null && dnstring.getDn().length() > 0) {
									dns.addDistinguishedName(dnstring.getDn());
								}
							}
							roleAssignmentObject.setExplicitIdentityDNs(dns);
						}
						roleAssignmentObjects.add(roleAssignmentObject);
						break;
					}
				}
			}
		}
		else if (object instanceof GetGroupResponse) {
			for (com.novell.www.role.service.RoleAssignment roleAssignment : ((GetGroupResponse) object).getResult()
					.getRoleAssignments().getRoleassignmentArray()) {

				if (roleAssignment.getRole().equalsIgnoreCase(roleDn)) {
					RoleAssignment roleAssignmentObject = this.getObject(RoleAssignment.class);

					if (roleAssignment != null
							&& roleAssignment.getAssignmentType() == RoleAssignmentType.GROUP_TO_ROLE) {
						if (roleAssignment.getAssignmentType() != null) {
							roleAssignmentObject.setRoleAssignmentType(roleAssignment.getAssignmentType().toString());
						}
						if (roleAssignment.getEffectiveDate() != null) {
							roleAssignmentObject.setEffectiveDatetime(
									CalendarUtil.convertDatetime(roleAssignment.getEffectiveDate()));
						}
						if (roleAssignment.getRole() != null) {
							roleAssignmentObject.setRoleDN(roleAssignment.getRole());
							RoleDNs roleDNs = new RoleDNs();
							roleDNs.setDistinguishedName(0, roleAssignment.getRole());
							roleAssignmentObject.setRoleDNs(roleDNs);
						}
						if (roleAssignment.getExplicitIdentities() != null
								&& roleAssignment.getExplicitIdentities().getDnstringArray() != null) {
							ExplicitIdentityDNs dns = new ExplicitIdentityDNs();
							for (DNString dnstring : roleAssignment.getExplicitIdentities().getDnstringArray()) {
								if (dnstring != null && dnstring.getDn() != null && dnstring.getDn().length() > 0) {
									dns.addDistinguishedName(dnstring.getDn());
								}
							}
							roleAssignmentObject.setExplicitIdentityDNs(dns);
						}
						roleAssignmentObjects.add(roleAssignmentObject);
						break;
					}
				}
			}
		}
		return roleAssignmentObjects;
	}

	/**
	 * Creates a stub for the remote object acts as a client's local proxy, so the
	 * caller can invoke a method on the client that will carry out the method call
	 * for the remote object.
	 * 
	 * A multithreaded client will be set up in order to enable concurrent access.
	 * This can be configured by setting a cached httpclient object when the service
	 * Stub is invoked before the actual requests are made.
	 * 
	 * @param userid
	 * @param password
	 * @param endpoint
	 * @param timeout
	 * @return
	 * @throws RemoteException
	 */
	private RoleServiceStub getRoleServiceStub(String userid, String password, String endpoint, int timeout,
			int soTimeout) throws RemoteException {

		logger.info(LOGTAG + "getRoleServiceStub: start");
		RoleServiceStub service = null;
		EndpointReference targetEPR = new EndpointReference(endpoint);
		service = new RoleServiceStub();
		HttpTransportPropertiesImpl.Authenticator auth = new HttpTransportPropertiesImpl.Authenticator();
		auth.setUsername(userid);
		auth.setPassword(password);
		auth.setPreemptiveAuthentication(true);
		logger.info(LOGTAG + "getRoleServiceStub: authenticator created");

		Options options = new Options();
		options.setProperty(HTTPConstants.AUTHENTICATE, auth);

		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(10);
		HttpConnectionManagerParams params = conmgr.getParams();
		params = new HttpConnectionManagerParams();
		// params.setMaxTotalConnections(20);
		params.setMaxTotalConnections(2);
		params.setDefaultMaxConnectionsPerHost(50);
		conmgr.setParams(params);
		logger.info(LOGTAG + "getRoleServiceStub: connection manager created");

		HttpClient client = new HttpClient(conmgr);
		options.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
		options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Boolean.TRUE);
		options.setTo(targetEPR);
		logger.info(LOGTAG + "getRoleServiceStub: http client created");
		// call this and see if working rdx 06/05/2019

		options.setCallTransportCleanup(true);
		// end
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
		options.setTimeOutInMilliSeconds(timeout);
		options.setProperty(HTTPConstants.SO_TIMEOUT, soTimeout);
		options.setProperty(HTTPConstants.CONNECTION_TIMEOUT, timeout);
		service._getServiceClient().setOptions(options);
		logger.info(LOGTAG + "getRoleServiceStub: service client created");

		return service;
	}

	public static int getConnectionTimeout() {
		return connectionTimeout;
	}

	public static void setConnectionTimeout(int connectionTimeout) {
		NetiqRoleAssignmentProvider.connectionTimeout = connectionTimeout;
	}

	public static int getSocketTimeout() {
		return socketTimeout;
	}

	public static void setSocketTimeout(int socketTimeout) {
		NetiqRoleAssignmentProvider.socketTimeout = socketTimeout;
	}

	public static String getServiceUserId() {
		return serviceUserId;
	}

	public static void setServiceUserId(String serviceUserId) {
		NetiqRoleAssignmentProvider.serviceUserId = serviceUserId;
	}

	public static String getServicePassword() {
		return servicePassword;
	}

	public static void setServicePassword(String servicePassword) {
		NetiqRoleAssignmentProvider.servicePassword = servicePassword;
	}

	public static String getLdapUserId() {
		return ldapUserId;
	}

	public static void setLdapUserId(String ldapUserId) {
		NetiqRoleAssignmentProvider.ldapUserId = ldapUserId;
	}

	public static String getLdapPassword() {
		return ldapPassword;
	}

	public static void setldapPassword(String ldapPassword) {
		NetiqRoleAssignmentProvider.ldapPassword = ldapPassword;
	}

	public static String getSearchServiceEndpoint() {
		return searchServiceEndpoint;
	}

	public AppConfig getM_appConfig() {
		return this.m_appConfig;
	}

	public void setM_appConfig(AppConfig m_appConfig) {
		this.m_appConfig = m_appConfig;
	}

	public static void setSearchServiceEndpoint(String searchServiceEndpoint) {
		NetiqRoleAssignmentProvider.searchServiceEndpoint = searchServiceEndpoint;
	}

	public static long getWaitInterval() {
		return waitInterval;
	}

	public static void setWaitInterval(long waitInterval) {
		NetiqRoleAssignmentProvider.waitInterval = waitInterval;
	}

	public static long getMaxWaitTime() {
		return maxWaitTime;
	}

	public static void setMaxWaitTime(long maxWaitTime) {
		NetiqRoleAssignmentProvider.maxWaitTime = maxWaitTime;
	}
}

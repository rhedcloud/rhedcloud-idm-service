package org.rhedcloud.idm.service.provider.grouper;

import java.io.StringReader;
import java.util.List;
import java.util.Properties;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.DefaultHttpParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.ProviderProcessingResponse;
import org.rhedcloud.idm.service.provider.RoleProvisioningProvider;
import org.rhedcloud.idm.service.util.SavedChanges;

import edu.emory.moa.jmsobjects.identity.v1_0.Role;
import edu.emory.moa.objects.resources.v1_0.RoleRequisition;
import edu.internet2.middleware.grouper.ws.coresoap.WsGroup;
import edu.internet2.middleware.grouper.ws.coresoap.WsGroupLookup;
import edu.internet2.middleware.grouper.ws.coresoap.WsGroupToSave;
import edu.internet2.middleware.grouper.ws.coresoap.WsStem;
import edu.internet2.middleware.grouper.ws.coresoap.WsStemLookup;
import edu.internet2.middleware.grouper.ws.coresoap.WsStemToSave;
import edu.internet2.middleware.grouper.ws.query.WsQueryFilterType;
import edu.internet2.middleware.grouper.ws.rest.group.WsRestFindGroupsLiteRequest;
import edu.internet2.middleware.grouper.ws.rest.group.WsRestGroupSaveRequest;
import edu.internet2.middleware.grouper.ws.rest.stem.WsRestStemSaveRequest;

public class GrouperRoleProvisioningProvider extends OpenEaiObject implements RoleProvisioningProvider {

	private AppConfig appConfig;
	private String default_baseStem_IN_URL = "app%3Arhedcloud%3Aaws%3A";
	private String default_baseStem = "app:rhedcloud:aws:";
	private String baseStem_IN_URL = "app%3Arhedcloud%3Aaws%3A";
	private String baseStem = "app:rhedcloud:aws:";
	private String LOGTAG = "[GrouperRoleProvider] ";

	public GrouperRoleProvisioningProvider() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(AppConfig aConfig) throws ProviderException {
		info("init started...");
		appConfig = aConfig;
		try {
			Properties p = appConfig.getProperties("GrouperProperties");
			
			baseStem_IN_URL = p.getProperty("baseStem_IN_URL", default_baseStem_IN_URL);
			info("baseStem_IN_URL: " + baseStem_IN_URL);
			
			baseStem = p.getProperty("baseStem", default_baseStem);
		} catch (EnterpriseConfigurationObjectException e) {
			e.printStackTrace();
			throw new ProviderException(e.getMessage(), e);
		}
		info("init complete...");
	}

	@Override
	public ProviderProcessingResponse generate(RoleRequisition roleRequisition) throws ProviderException {
		String tag = "[Role.Generate] ";
		ProviderProcessingResponse providerProcessingResponse = new ProviderProcessingResponse();
		String roleName = roleRequisition.getRoleName();
		info(tag + "roleName: " + roleName);
		
		boolean accountLevelRole = true;
		if (roleName.indexOf(":") >= 0) {
			// it's an account level role
			accountLevelRole = true;
		}
		else {
			// it's the central admin role at the base stem level
			accountLevelRole = false;
		}
		
		String account = null;
		String accountStemDescription = null;
		String group = null;
		String groupDescription = null;

		if (accountLevelRole) {
			account = roleName.substring(0, roleName.indexOf(":"));
			accountStemDescription = "RHEDcloud AWS Account " + account;
			group = roleName.substring(roleName.indexOf(":") + 1);
			groupDescription = group + " role for AWS Account " + account;
			info(tag + "account: " + account);
			info(tag + "group: " + roleName);

			try {
				// save the account first (always)
				long acctStemStart = System.currentTimeMillis();
				accountStemSave(account, accountStemDescription);
				long acctStemElapsed = System.currentTimeMillis() - acctStemStart;
				info("account stem creation elapsed time: " + acctStemElapsed + " millis");

				// now create role/group within the account
				long groupSaveStart = System.currentTimeMillis();
				groupSave(account, group, groupDescription);
				long groupSaveElapsed = System.currentTimeMillis() - groupSaveStart;
				info("group save elapsed time: " + groupSaveElapsed + " millis");
				info("overall elapsed time: " + (System.currentTimeMillis() - acctStemStart) + " mills");

				Role roleMoa = (Role) appConfig.getObject("Role.v1_0");
				roleMoa.setRoleDN(roleName);
				roleMoa.setRoleName(group);
				roleMoa.setRoleDescription(groupDescription);
				roleMoa.setRoleCategoryKey("aws");

				providerProcessingResponse.setResponse(roleMoa);
			}
			catch (EnterpriseConfigurationObjectException e) {
				e.printStackTrace();
				throw new ProviderException(e.getMessage(), e);
			}
			catch (EnterpriseFieldException e) {
				e.printStackTrace();
				throw new ProviderException(e.getMessage(), e);
			}
		}
		else {
			// it's the CentralAdmin role/group at the higher level
			groupDescription = "RHEDcloud AWS Central Admin Group";
			long groupSaveStart = System.currentTimeMillis();
			groupSave(null, roleName, groupDescription);
			long groupSaveElapsed = System.currentTimeMillis() - groupSaveStart;
			info("group stem creation elapsed time: " + groupSaveElapsed + " millis");

			try {
				Role roleMoa = (Role) appConfig.getObject("Role.v1_0");

				roleMoa.setRoleDN(roleName);
				roleMoa.setRoleName(roleName);
				roleMoa.setRoleDescription(groupDescription);
				roleMoa.setRoleCategoryKey("aws");

				providerProcessingResponse.setResponse(roleMoa);
			} catch (EnterpriseConfigurationObjectException e) {
				e.printStackTrace();
				throw new ProviderException(e.getMessage(), e);
			} catch (EnterpriseFieldException e) {
				e.printStackTrace();
				throw new ProviderException(e.getMessage(), e);
			}
		}
		
		return providerProcessingResponse;
	}

	@Override
	public Role delete(Role role) throws ProviderException {
		String roleDN = role.getRoleDN();
		String account = null;
		String group = null;
		if (roleDN.indexOf(":") >= 0) {
			account = roleDN.substring(0, roleDN.indexOf(":"));
			group = roleDN.substring(roleDN.indexOf(":") + 1);
		}
		else {
			account = roleDN.trim();
		}

		long groupStart = System.currentTimeMillis();
		if (group != null) {
			// have to delete the group first, or, we may need to do a query for all
			// the groups in this stem and delete them dynamically...
			groupDelete(account, group);
			long groupElapsed = System.currentTimeMillis() - groupStart;
			info("group delete elapsed time: " + groupElapsed + " millis");
		}
		else {
			// no group passed in, just delete the account stem
			// NOTE:  if someone calls this BEFORE all the groups have been
			// deleted, it will result in an error.  We could remedy this
			// by querying for all the groups within an account and delete them
			// dynamically but for now we'll just stick with this.
			long accountStemStart = System.currentTimeMillis();
			accountStemDelete(account);
			long accountStemElapsed = System.currentTimeMillis() - accountStemStart;
			info("account stem delete elapsed time: " + accountStemElapsed + " millis");
			info("total elapsed time: " + (System.currentTimeMillis() - groupStart) + " millis");
		}

		return role;
	}

	@Override
	public Role query(String roleDn, List<SavedChanges<String, String>> changedList, String fromAction)
			throws ProviderException {
		
		String roleDN = roleDn;
		String account = null;
		String group = null;
		if (roleDN.indexOf(":") > -1) {
			account = roleDN.substring(0, roleDN.indexOf(":"));
			group = roleDN.substring(roleDN.indexOf(":") + 1);
		}
		else {
			account = roleDN;
		}

		List<Role> roles = findGroups(account, group);
		info("got " + roles.size() + " Roles back from Grouper");
		
		if (roles.size() > 0) {
			return roles.get(0);
		}
		return null;
	}

	@Override
	public ProviderProcessingResponse update(Role newRole, Role baselineRole) throws ProviderException {
		ProviderProcessingResponse providerProcessingResponse = new ProviderProcessingResponse();
		String roleDN = newRole.getRoleDN();
		String group = newRole.getRoleName();
		String account = roleDN.substring(0, roleDN.indexOf(":"));
		String accountStemDescription = "RHEDcloud AWS Account " + account;
		String groupFromDN = roleDN.substring(roleDN.indexOf(":") + 1);
		String groupDescription = newRole.getRoleDescription();

		// TODO: if baseline roleDN is different than newRole roleDN, we'll have to
		// delete the old and then
		// add the new one i think.
		// TODO: what if the groupFromDN is different than the group (rolename)? is that
		// possible?
		if (!baselineRole.getRoleDN().equalsIgnoreCase(newRole.getRoleDN())) {
			// TODO: delete baselineRole
		}
		else {
			// just save newRole
			// save the account first (always)
			long acctStemStart = System.currentTimeMillis();
			accountStemSave(account, accountStemDescription);
			long acctStemElapsed = System.currentTimeMillis() - acctStemStart;
			info("account stem update elapsed time: " + acctStemElapsed + " millis");

			// now save role/group within the account
			long groupSaveStart = System.currentTimeMillis();
			groupSave(account, group, groupDescription);
			long groupSaveElapsed = System.currentTimeMillis() - groupSaveStart;
			info("group save elapsed time: " + groupSaveElapsed + " millis");
			info("overall elapsed time: " + (System.currentTimeMillis() - acctStemStart) + " mills");
		}
		return null;
	}

	public void accountStemSave(String account, String accountStemDescription) throws ProviderException {
		info("accountStemSave account: " + account);
		try {
			HttpClient httpClient = new HttpClient();

			DefaultHttpParams.getDefaultParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(0, false));

			// URL e.g. http://localhost:8093/grouper-ws/servicesRest/v1_3_000/...
			PostMethod method = new PostMethod(RestClientSettings.URL + "/" + RestClientSettings.VERSION + "/stems");

			httpClient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(RestClientSettings.USER,
					RestClientSettings.PASS);

			// no keep alive so response if easier to indent for tests
			method.setRequestHeader("Connection", "close");
			info("method's URI is: " + method.getURI());

			// e.g. localhost and 8093
			httpClient.getState().setCredentials(new AuthScope(null, -1), defaultcreds);
			info("set credentials...");

			// Make the body of the request, in this case with beans and marshaling, but you
			// can make
			// your request document in whatever language or way you want
			String stemBeingCreated = baseStem + account;
			info("stem being created: " + stemBeingCreated);
			WsRestStemSaveRequest stemSave = new WsRestStemSaveRequest();
			info("created stemSave...");

			WsStemToSave wsStemToSave = new WsStemToSave();
			wsStemToSave.setWsStemLookup(new WsStemLookup(stemBeingCreated, null));
			WsStem wsStem = new WsStem();
			wsStem.setDescription(accountStemDescription);
			wsStem.setDisplayExtension(account);
//			wsStem.setExtension("whateverStem");
			wsStem.setName(stemBeingCreated);
			wsStemToSave.setWsStem(wsStem);
			info("created wsStemToSave...");

			WsStemToSave[] wsStemToSaves = new WsStemToSave[] { wsStemToSave };

			stemSave.setWsStemToSaves(wsStemToSaves);
			info("set wsStemToSaves...");

			// get the xml / json / xhtml / paramString
			String requestDocument = WsRestType.xml.getWsLiteRequestContentType().writeString(stemSave);
			info("request document we're sending: " + requestDocument);

			// make sure right content type is in request (e.g. application/xhtml+xml
			String contentType = WsRestType.xml.getWsLiteRequestContentType().getContentType();

			method.setRequestEntity(new StringRequestEntity(requestDocument, contentType, "UTF-8"));

			info("method URI: " + method.getURI());
			info("method path: " + method.getPath());
			httpClient.executeMethod(method);

			// make sure a request came back
			Header successHeader = method.getResponseHeader("X-Grouper-success");
			info("successHeader: " + successHeader);
			String successString = successHeader == null ? null : successHeader.getValue();
			info("successString: " + successString);
			if (StringUtils.isBlank(successString)) {
				throw new ProviderException("Web service did not even respond!");
			}
			boolean success = "T".equals(successString);
			String resultCode = method.getResponseHeader("X-Grouper-resultCode").getValue();

			String response = RestClientSettings.responseBodyAsString(method);
			info("response from accountStemSave: " + response);
		}
		catch (Exception e) {
			logger.fatal(e.getMessage(), e);
			throw new ProviderException(e.getMessage());
		}

	}

	public void groupSave(String account, String group, String groupDescription) throws ProviderException {

		try {
			HttpClient httpClient = new HttpClient();

			DefaultHttpParams.getDefaultParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(0, false));

			// URL e.g. http://localhost:8093/grouper-ws/servicesRest/v1_3_000/...
			PostMethod method = new PostMethod(RestClientSettings.URL + "/" + RestClientSettings.VERSION + "/groups");

			httpClient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(RestClientSettings.USER,
					RestClientSettings.PASS);

			// no keep alive so response if easier to indent for tests
			method.setRequestHeader("Connection", "close");
			info("method's URI is: " + method.getURI());

			// e.g. localhost and 8093
			httpClient.getState().setCredentials(new AuthScope(null, -1), defaultcreds);

			// Make the body of the request, in this case with beans and marshaling, but you
			// can make
			// your request document in whatever language or way you want
			String groupBeingCreated = null;
			if (account != null) {
				groupBeingCreated = baseStem + account + ":" + group;
			}
			else {
				groupBeingCreated = baseStem + group;
			}
			
			WsRestGroupSaveRequest groupSave = new WsRestGroupSaveRequest();

			WsGroupToSave wsGroupToSave = new WsGroupToSave();
			wsGroupToSave.setWsGroupLookup(new WsGroupLookup(groupBeingCreated, null));
			WsGroup wsGroup = new WsGroup();
			wsGroup.setDescription(groupDescription);
			wsGroup.setDisplayExtension(group);
//			wsGroup.setExtension("whateverGroup");
			wsGroup.setName(groupBeingCreated);
			wsGroupToSave.setWsGroup(wsGroup);

			WsGroupToSave[] wsGroupToSaves = new WsGroupToSave[] { wsGroupToSave };

			groupSave.setWsGroupToSaves(wsGroupToSaves);

			// get the xml / json / xhtml / paramString
			String requestDocument = WsRestType.xml.getWsLiteRequestContentType().writeString(groupSave);
			info("Request being sent: " + requestDocument);

			// make sure right content type is in request (e.g. application/xhtml+xml
			String contentType = WsRestType.xml.getWsLiteRequestContentType().getContentType();

			method.setRequestEntity(new StringRequestEntity(requestDocument, contentType, "UTF-8"));

			httpClient.executeMethod(method);

			// make sure a request came back
			Header successHeader = method.getResponseHeader("X-Grouper-success");
			String successString = successHeader == null ? null : successHeader.getValue();
			if (StringUtils.isBlank(successString)) {
				throw new ProviderException("Web service did not even respond!");
			}
			boolean success = "T".equals(successString);
			String resultCode = method.getResponseHeader("X-Grouper-resultCode").getValue();

			String response = RestClientSettings.responseBodyAsString(method);
			info("Response from WS: " + response);

		}
		catch (Exception e) {
			throw new ProviderException(e.getMessage(), e);
		}

	}

	public void accountStemDelete(String account) throws ProviderException {

		try {
			HttpClient httpClient = new HttpClient();

			DefaultHttpParams.getDefaultParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(0, false));

			// URL e.g. http://localhost:8093/grouper-ws/servicesRest/v1_3_000/...
			// NOTE: aStem:aGroup urlencoded substitutes %3A for a colon
			DeleteMethod method = new DeleteMethod(
					RestClientSettings.URL + "/" + WsRestType.xml.getWsLiteResponseContentType().name() + "/"
							+ RestClientSettings.VERSION + "/stems/" + baseStem_IN_URL + account);
			info("Delete account method URI: " + method.getURI());

			httpClient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(RestClientSettings.USER,
					RestClientSettings.PASS);

			// no keep alive so response if easier to indent for tests
			method.setRequestHeader("Connection", "close");

			// e.g. localhost and 8093
			httpClient.getState().setCredentials(new AuthScope(null, -1), defaultcreds);

			httpClient.executeMethod(method);

			// make sure a request came back
			Header successHeader = method.getResponseHeader("X-Grouper-success");
			String successString = successHeader == null ? null : successHeader.getValue();
			if (StringUtils.isBlank(successString)) {
				throw new ProviderException("Web service did not even respond!");
			}
			boolean success = "T".equals(successString);
			String resultCode = method.getResponseHeader("X-Grouper-resultCode").getValue();

			String response = RestClientSettings.responseBodyAsString(method);
			info("Response from WS: " + response);

			// see if request worked or not
			if (!success) {
				throw new ProviderException("Bad response from web service: resultCode: " + resultCode);
			}
		}
		catch (Exception e) {
			throw new ProviderException(e.getMessage(), e);
		}

	}

	public void groupDelete(String account, String group) throws ProviderException {

		try {
			HttpClient httpClient = new HttpClient();

			DefaultHttpParams.getDefaultParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(0, false));

			// URL e.g. http://localhost:8093/grouper-ws/servicesRest/v1_3_000/...
			// NOTE: aStem:aGroup urlencoded substitutes %3A for a colon
			DeleteMethod method = new DeleteMethod(
					RestClientSettings.URL + "/" + WsRestType.xml.getWsLiteResponseContentType().name() + "/"
							+ RestClientSettings.VERSION + "/groups/" + baseStem_IN_URL + account + "%3A" + group);
			info("Delete group method URI: " + method.getURI());

			httpClient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(RestClientSettings.USER,
					RestClientSettings.PASS);

			// no keep alive so response if easier to indent for tests
			method.setRequestHeader("Connection", "close");

			// e.g. localhost and 8093
			httpClient.getState().setCredentials(new AuthScope(null, -1), defaultcreds);

			httpClient.executeMethod(method);

			// make sure a request came back
			Header successHeader = method.getResponseHeader("X-Grouper-success");
			String successString = successHeader == null ? null : successHeader.getValue();
			if (StringUtils.isBlank(successString)) {
				throw new ProviderException("Web service did not even respond!");
			}
			boolean success = "T".equals(successString);
			String resultCode = method.getResponseHeader("X-Grouper-resultCode").getValue();

			String response = RestClientSettings.responseBodyAsString(method);
			info("Response from WS: " + response);

			// see if request worked or not
			if (!success) {
				throw new ProviderException("Bad response from web service: resultCode: " + resultCode);
			}
		}
		catch (Exception e) {
			throw new ProviderException(e.getMessage(), e);
		}

	}

	public List<Role> findGroups(String account, String group) throws ProviderException {

		try {
			HttpClient httpClient = new HttpClient();

			DefaultHttpParams.getDefaultParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(0, false));

			// URL e.g. http://localhost:8093/grouper-ws/servicesRest/v1_3_000/...
			// NOTE: aStem:aGroup urlencoded substitutes %3A for a colon
			PostMethod method = new PostMethod(RestClientSettings.URL + "/" + RestClientSettings.VERSION + "/groups");
			info("Find group method URI: " + method.getURI());

			httpClient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(RestClientSettings.USER,
					RestClientSettings.PASS);

			// no keep alive so response if easier to indent for tests
			method.setRequestHeader("Connection", "close");

			// e.g. localhost and 8093
			httpClient.getState().setCredentials(new AuthScope(null, -1), defaultcreds);

			// Make the body of the request, in this case with beans and marshaling, but you
			// can make
			// your request document in whatever language or way you want
			WsRestFindGroupsLiteRequest findGroupsLite = new WsRestFindGroupsLiteRequest();

			findGroupsLite.setStemName(baseStem + account);
			findGroupsLite.setIncludeGroupDetail("true");
			if (group != null) {
				// find the group by the exact group name passed in
				findGroupsLite.setGroupName(group);
				findGroupsLite.setQueryFilterType(WsQueryFilterType.FIND_BY_GROUP_NAME_APPROXIMATE.name());
			}
			else {
				// all groups within a stem?
				// WsQueryFilterType.FIND_BY_STEM_NAME
				findGroupsLite.setQueryFilterType(WsQueryFilterType.FIND_BY_STEM_NAME.name());
			}

			// get the xml / json / xhtml / paramString
			String requestDocument = WsRestType.xml.getWsLiteRequestContentType().writeString(findGroupsLite);
			info("Request being sent: " + requestDocument);

			// make sure right content type is in request (e.g. application/xhtml+xml
			String contentType = WsRestType.xml.getWsLiteRequestContentType().getContentType();

			method.setRequestEntity(new StringRequestEntity(requestDocument, contentType, "UTF-8"));

			httpClient.executeMethod(method);

			// make sure a request came back
			Header successHeader = method.getResponseHeader("X-Grouper-success");
			String successString = successHeader == null ? null : successHeader.getValue();
			if (StringUtils.isBlank(successString)) {
				throw new ProviderException("Web service did not even respond!");
			}
			boolean success = "T".equals(successString);
			String resultCode = method.getResponseHeader("X-Grouper-resultCode").getValue();

			String response = RestClientSettings.responseBodyAsString(method);
			info("Response from WS: " + response);
			if (!success) {
				throw new ProviderException("Bad response from web service.  result code: " + resultCode);
			}
			
			/*
		    <WsFindGroupsResults>
		        <groupResults>
		            <WsGroup>
		                <extension>admin</extension>
		                <typeOfGroup>group</typeOfGroup>
		                <displayExtension>admin</displayExtension>
		                <description>admin role for AWS Account 123456789</description>
		                <displayName>app:rhedcloud:aws:123456789:admin</displayName>
		                <name>app:rhedcloud:aws:123456789:admin</name>
		                <uuid>5c4e8fda28cd478fb98b6e35af808e2f</uuid>
		                <detail>
		                    <hasComposite>F</hasComposite>
		                    <typeNames/>
		                    <createSubjectId>GrouperSystem</createSubjectId>
		                    <createTime>2020/02/20 17:10:34.782</createTime>
		                    <isCompositeFactor>F</isCompositeFactor>
		                    <modifySubjectId>GrouperSystem</modifySubjectId>
		                    <modifyTime>2020/02/27 22:11:53.735</modifyTime>
		                </detail>
		                <idIndex>10029</idIndex>
		            </WsGroup>
		            <WsGroup>
		                <extension>c_admin</extension>
		                <typeOfGroup>group</typeOfGroup>
		                <displayExtension>c_admin</displayExtension>
		                <description>central admin</description>
		                <displayName>app:rhedcloud:aws:123456789:c_admin</displayName>
		                <name>app:rhedcloud:aws:123456789:c_admin</name>
		                <uuid>ef04784838ca421c9cbd2a945643e633</uuid>
		                <detail>
		                    <hasComposite>F</hasComposite>
		                    <typeNames/>
		                    <createSubjectId>GrouperSystem</createSubjectId>
		                    <createTime>2020/02/20 17:14:23.579</createTime>
		                    <isCompositeFactor>F</isCompositeFactor>
		                    <modifySubjectId>GrouperSystem</modifySubjectId>
		                    <modifyTime>2020/02/20 17:14:24.470</modifyTime>
		                </detail>
		                <idIndex>10031</idIndex>
		            </WsGroup>
		        </groupResults>
		        <resultMetadata>
		            <resultCode>SUCCESS</resultCode>
		            <resultMessage>Success for: clientVersion: 2.4.0, wsQueryFilter:
		                WsQueryFilter[queryFilterType=FIND_BY_GROUP_NAME_APPROXIMATE,groupName=admin,stemName=app:rhedcloud:aws:123456789]
		                , includeGroupDetail: true, actAsSubject: null, paramNames: , params: null ,
		                wsGroupLookups: null</resultMessage>
		            <success>T</success>
		        </resultMetadata>
		        <responseMetadata>
		            <resultWarnings/>
		            <millis>248</millis>
		            <serverVersion>2.4.0</serverVersion>
		        </responseMetadata>
		    </WsFindGroupsResults>
			 */
			
			// build the role object(s) from the response
			List<Role> roles = new java.util.ArrayList<Role>();
			Element root = null;
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(new StringReader(response));
			root = doc.getRootElement();
			Element eGroupResults = root.getChild("groupResults");
			for (int i=0; i<eGroupResults.getChildren().size(); i++) {
				Element eWsGroup = (Element) eGroupResults.getChildren().get(i);
				String extension = eWsGroup.getChildText("extension");
				if (extension.equalsIgnoreCase(group)) {
					Role roleMoa = (Role) appConfig.getObject("Role.v1_0");
					roleMoa.setRoleDN(account + ":" + group);
					roleMoa.setRoleName(group);
					roleMoa.setRoleDescription(eWsGroup.getChildText("description"));
					roleMoa.setRoleCategoryKey("aws");
					roles.add(roleMoa);
				}
			}

			// see if request worked or not
			if (!success) {
				throw new ProviderException("Bad response from web service: resultCode: " + resultCode);
			}

			return roles;
		}
		catch (Exception e) {
			throw new ProviderException(e.getMessage(), e);
		}
	}

	private void info(String message) {
		logger.info(LOGTAG + " - " + message);
	}
}

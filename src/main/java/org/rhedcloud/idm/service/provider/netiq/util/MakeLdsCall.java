package org.rhedcloud.idm.service.provider.netiq.util;

import java.util.Iterator;
import java.util.Scanner;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.ldap.client.api.DefaultLdapConnectionFactory;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapConnectionPool;
import org.apache.directory.ldap.client.api.ValidatingPoolableLdapConnectionFactory;



public class MakeLdsCall {

	/*
	 * Password -> Sxdh3hhHCPlm34OpTsxY
adminDN -> CN=esb_group_svc,OU=Services,DC=emory,DC=edu
ldapHost -> ldsautheudev.service.emory.edu
ldapPort -> 389
dn -> CN=EmoryCloudAdmin,OU=381492275187,OU=AWS,DC=emory,DC=edu
	 */

	static String 
	ldapHost
	= "ldsautheudev.service.emory.edu"
	,adminDn
	= "CN=esb_group_svc,OU=Services,DC=emory,DC=edu"
	,adminPassword
	= "Sxdh3hhHCPlm34OpTsxY"	
	,dn
	//= "CN=EmoryCloudAdmin,OU=381492275187,OU=AWS,DC=emory,DC=edu"
			= "OU=381492275187,OU=AWS,DC=emory,DC=edu"
	,peopleOu
	= "OU=People,DC=emory,DC=edu";
	
	static int ldapPort=389;

	public static void main(String[] args) throws LdapException, CursorException {
		long startTiming = System.currentTimeMillis();
		LdapConnection connection = null;
		try {
			if (ldapHost == null) {
				System.out.print("Password -> ");
				try (Scanner in = new Scanner(System.in)) {
					adminPassword = in.nextLine();
					System.out.print("adminDN -> ");
					adminDn = in.nextLine();
					System.out.print("ldapHost -> ");
					ldapHost = in.nextLine();
					System.out.print("ldapPort -> ");
					ldapPort = Integer.parseInt(in.nextLine());
					System.out.print("dn -> ");
					dn = in.nextLine();
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} 
			}
			
			
			System.out.println("Connection in progress...");

			initLdapPool(ldapHost, ldapPort, adminDn, adminPassword);
			connection = ldapConnectionPool.getConnection();

			String[] groupAttributes = {"member"};
			EntryCursor cursor = connection.search(dn, "(objectClass=group)", SearchScope.SUBTREE,
					groupAttributes);

			for (Entry ent : cursor) {
				System.out.println("\tent.getDn()="+ent.getDn());
				
				for (Attribute att : ent.getAttributes()) {
					if ("member".equals(att.getUpId())) {
						Iterator<Value<?>> members = att.iterator();
						while (members.hasNext()) {
							Value<?> member = members.next();
							// CN=jburk22,OU=People,DC=emory,DC=edu is an example of member.getValue()
							String memberDn = ((String) member.getValue()).split(",")[0]
									+ "," + peopleOu;
							System.out.println("\t\tmemberDn="+memberDn);
							//EntryCursor peopleCursor = connection.search(memberDn, "(objectClass=person)", SearchScope.OBJECT, LdsProviderUtil.returningAttributes);
							String[] returningAttributes = {"serialNumber"};
							EntryCursor peopleCursor = connection.search(memberDn, "(objectClass=person)", SearchScope.OBJECT, 
									returningAttributes);
							for (Entry entPerson : peopleCursor) {
								for (Attribute attPerson : entPerson.getAttributes()) {
									if ("serialNumber".equals(attPerson.getUpId()) ) {
										String serialNumber = attPerson.getString();
										System.out.println("\t\t\tserialNumber: "+serialNumber);
										break;
									}
								}
							}
						}
						
					}
				}
				//break; // because there should only be one group returned from the search.
			}
		} finally {
			releaseLdapConnection(connection);
			long duration = System.currentTimeMillis() - startTiming;
			System.out.println(" - LDAP queries complete in " + duration + " ms.");
		}

	}

	public static void releaseLdapConnection(LdapConnection connection) {
		try {
			if (connection != null)
				ldapConnectionPool.releaseConnection(connection);
		} catch (LdapException e) {
			System.err.println("Error releasing LDAP connection back to pool:" + e);
		}
	}

	private static LdapConnectionPool ldapConnectionPool;

	public static synchronized void closeLdapPool() {
		if (ldapConnectionPool != null) {
			try {
				ldapConnectionPool.close();
			} catch (Exception e) {
				System.err.println("Error while closing LDAP connection pool "+ e);
			}
			ldapConnectionPool = null;
		}
	}
	public static synchronized void initLdapPool(String ldapHost, int ldapPort, String adminDn, String adminPassword) {
		if (ldapConnectionPool != null) {
			return; // already initialized
		}

		LdapConnectionConfig config = new LdapConnectionConfig();
		config.setLdapHost(ldapHost);
		config.setLdapPort(ldapPort);
		config.setName(adminDn);
		config.setCredentials(adminPassword);
		//config.setTrustManagers(new NoVerificationTrustManager());
		config.setUseTls(true);

		DefaultLdapConnectionFactory factory = new DefaultLdapConnectionFactory(config);
		factory.setTimeOut(30000L); // default

		// optional, values below are defaults
		GenericObjectPool.Config poolConfig = new GenericObjectPool.Config();
		poolConfig.lifo = true;
		poolConfig.maxActive = 8;
		poolConfig.maxIdle = 8;
		poolConfig.maxWait = -1L;
		poolConfig.minEvictableIdleTimeMillis = 1000L * 60L * 30L;
		poolConfig.minIdle = 0;
		poolConfig.numTestsPerEvictionRun = 3;
		poolConfig.softMinEvictableIdleTimeMillis = -1L;
		poolConfig.testOnBorrow = false;
		poolConfig.testOnReturn = false;
		poolConfig.testWhileIdle = false;
		poolConfig.timeBetweenEvictionRunsMillis = -1L;
		poolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;

		ldapConnectionPool = new LdapConnectionPool(new ValidatingPoolableLdapConnectionFactory(factory), poolConfig);
	}

}

package org.rhedcloud.idm.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.openeai.OpenEaiException;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.consumer.commands.RequestCommandImpl;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.EnterpriseObjectSyncException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Authentication;
import org.openeai.transport.SyncService;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;
import org.rhedcloud.idm.service.provider.AwsProvisioningProvider;
import org.rhedcloud.idm.service.provider.ProviderException;
import org.rhedcloud.idm.service.provider.ResourceProvisioningProvider;
import org.rhedcloud.idm.service.provider.RoleAssignmentProvider;
import org.rhedcloud.idm.service.provider.RoleProvisioningProvider;
import org.rhedcloud.idm.service.provider.aws.AccountAssignmentProvider;
import org.rhedcloud.idm.service.provider.aws.IamRoleProvider;
import org.rhedcloud.idm.service.provider.aws.IdentityCenterUserProvider;
import org.rhedcloud.idm.service.provider.aws.PermissionSetProvider;
import org.rhedcloud.idm.service.provider.aws.PolicyProvider;
import org.rhedcloud.idm.service.util.MessageControl;

/**
 * This request command handles Request messages for  AWS Identity Manager (SSO) support 
 * was added so that users could use the new account/role selection page, which allows 
 * them to get to the AWS console and provides them with temporary credentials for use 
 * with CLI for all the accounts and roles for which they belong. This will allow for 
 * the retirement of the TKI (Temportary Key Issuance) service and client apps. 
 * 
 * Also, support was added for custom roles which worked a little differently than
 * in IAM than it does in IDM.
 * 
 * NOTE: AWS IDM is not being used as an identity manager (IDM) like NetIQ or Grouper are
 * So far, only NetIQ or Grouper are supported as IDMs. The only thing AWS IDM 
 * is being used for is to support the .... page which provides both 
 * access to the AWS management console, and TKI. These are SSO features and 
 * AWS IDM is only being used for its SSO features.  
 * 
 * The provider classes for the AWS messages are handled a bit differently than 
 * the ones that handle the Resource, RoleAssignemnt, and Role messages. The latter 
 * are coded so that they can be swapped in or out depending on which IDM was in use. 
 * In contrast, the AWS message providers are hard-coded into this class because
 * they will not be swapped out for a different SSO like the IDM provider classes 
 * can be swapped out for NetIQ or Grouper.
 * 
 * AWS IDM support required the creation of 3 new message objects, 
 * AccountAssignment, PermissionSet and Policy. The IDM service now handles the Generate,
 * Query and Delete actions for the new message objects. 
 * 
 * The only known producers of these messages are the RHEDcloud Console and the 
 *  AWS Account Service
 * 
 * @author Tom Cervenka (tcerven@emory.edu)
 * @author Richard Xing (rxing2@emory.edu)
 * @author Steve Wheat (swheat@emory.edu)
 * 
 * @version 1.0 - 11 Feburary 2017
 * @version 1.1 - 20 Oct 2023
 */
public class AwsServiceRequestCommand extends RequestCommandImpl implements RequestCommand {
	private static String serviceUserId = "";
	private static String servicePassword = "";
	private static String searchServiceEndpoint = "";
	private static int connectionTimeout = 12000;
	private ProducerPool producerPool = null;
	private static int socketTimeout = 12000;
	private static String authUserId = null;
	private Document responseDoc = null;
	private boolean verbose = false;
	private RoleAssignmentProvider roleAssignmentProvider = null;
	private RoleProvisioningProvider roleProvisioningProvider = null;
	private ResourceProvisioningProvider resourceProvisioningProvider = null;
	private HashMap<String,AwsProvisioningProvider> awsProvisioningProviders = new HashMap<>();


	private static final String LOGTAG = "[" + AwsServiceRequestCommand.class.getSimpleName() + "]";;

	@SuppressWarnings({ "deprecation" })
	public AwsServiceRequestCommand(CommandConfig cConfig)
			throws InstantiationException, ProviderException, IllegalAccessException, ClassNotFoundException {
		super(cConfig);

		logger.info(LOGTAG + "AwsServiceRequestCommand is initializing...");
		// Initialize a command-specific logger if it exists.
		try {
			ProducerPool pool = (ProducerPool) getAppConfig().getObject("SyncPublisher");
			setProducerPool(pool);
		}
		catch (EnterpriseConfigurationObjectException eoce) {
			String errMsg = "Error retrieving a ProducerPool object " + "from AppConfig. The exception is: "
					+ eoce.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		try {
			PropertyConfig pConfig = (PropertyConfig) getAppConfig().getObject("GeneralProperties");
			setProperties(pConfig.getProperties());
		}
		catch (EnterpriseConfigurationObjectException ecoe) {
			String errMsg = "An error occurred retrieving a property config from AppConfig. The exception is: "
					+ ecoe.getMessage();
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(errMsg);
		}

		// Initialize response document.
		XmlDocumentReader xmlReader = new XmlDocumentReader();
		try {
			logger.debug(LOGTAG + "responseDocumentUri: " + getProperties().getProperty("responseDocumentUri"));
			responseDoc = xmlReader.initializeDocument(getProperties().getProperty("responseDocumentUri"),
					getOutboundXmlValidation());
			if (responseDoc == null) {
				String errMsg = LOGTAG + "Missing 'responseDocumentUri' "
						+ "property in the deployment descriptor.  Can't continue.";
				logger.fatal(LOGTAG + errMsg);
				throw new InstantiationException(errMsg);
			}
			setResponseDoc(responseDoc);
		}
		catch (XmlDocumentReaderException e) {
			String errMsg = "Error initializing the primed documents.";
			logger.fatal(LOGTAG + errMsg);
			throw new InstantiationException(e.getMessage());
		}

		Class<AccountAssignmentProvider> aa = AccountAssignmentProvider.class;
		awsProvisioningProviders.put("AccountAssignment",aa.newInstance());
		Class<PermissionSetProvider> ps = PermissionSetProvider.class;
		awsProvisioningProviders.put("PermissionSet",ps.newInstance());
		Class<PolicyProvider> po = PolicyProvider.class;
		awsProvisioningProviders.put("Policy",po.newInstance());
		Class<IamRoleProvider> ir = IamRoleProvider.class;
		awsProvisioningProviders.put("IamRole",ir.newInstance());
		Class<IdentityCenterUserProvider> icu = IdentityCenterUserProvider.class;
		awsProvisioningProviders.put("IdentityCenterUser",icu.newInstance());

		for(Entry<String, AwsProvisioningProvider> entry:awsProvisioningProviders.entrySet()) {
			try {
				entry.getValue().init(getAppConfig());
			} catch (ProviderException pe) {
				String errMsg = "Error initializing the "+entry.getKey()+" object from AppConfig: The exception is: "
						+ pe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new InstantiationException(errMsg);
			}
		}
	}

	@Override
	public final Message execute(int messageNumber, Message aMessage) throws CommandException {

		logger.debug(LOGTAG + "In execute()");
		long startTime = System.currentTimeMillis();
		Document requestFromEai = new Document();
		TextMessage msg = (TextMessage) aMessage;
		try {
			requestFromEai = initializeInput(messageNumber, aMessage);
			if (isVerbose()) {
				logger.info(LOGTAG + "Processing message: " + msg.getText());
			}
			msg.clearBody();
		}
		catch (JMSException je) {
			String errMsg = "Exception occurred processing input message in "
					+ "org.openeai.jms.consumer.commands.Command.  Exception is: " + je.getMessage();
			throw new CommandException(errMsg);
		}

		// Get Control area from XML object
		Element eControlArea = getControlArea(requestFromEai.getRootElement());
		String msgAction = this.getMessageAction(requestFromEai);
		String msgObject = this.getMessageObject(requestFromEai);
		String msgRelease = this.getMessageRelease(requestFromEai);

		// parse the text from the JMS message into a JDOM document.
		logger.debug(LOGTAG + "msgAction: " + this.getMessageAction(requestFromEai));
		logger.debug(LOGTAG + "msgType: " + this.getMessageType(requestFromEai));
		logger.debug(LOGTAG + "msgObject: " + this.getMessageObject(requestFromEai));
		logger.debug(LOGTAG + "msgRelease: " + this.getMessageRelease(requestFromEai));

		Element eAuthUserId = eControlArea.getChild("Sender").getChild("Authentication").getChild("AuthUserId");
		authUserId = eAuthUserId.getValue();

		Document responseXmlObject = (Document) getResponseXml(msgAction, msgObject, msgRelease).clone();


		/***********************************************************************************************************************************
		 * 
		 * AccountAssignment, PermissionSet, Policy, IamRole, IdentityCenterUser
		 * 		
		 ***********************************************************************************************************************************/


		if (awsProvisioningProviders.containsKey(msgObject)) {

			AwsProvisioningProvider provider = awsProvisioningProviders.get(msgObject);
			ActionableEnterpriseObject aeo = null;
			XmlEnterpriseObject querySpec = null;
			XmlEnterpriseObject requisition = null;
			ActionableEnterpriseObject moaobject = null, newMoaobject = null, baselineMoaobject = null;

			List<ActionableEnterpriseObject> aeos = null;


			try {
				aeo = (ActionableEnterpriseObject) getAppConfig()
						.getObject(msgObject + "." + generateRelease(msgRelease));

			} catch (EnterpriseConfigurationObjectException ecoe) {
				String errMsg = "Error retrieving an object from AppConfig. The exception is: " + ecoe.getMessage();
				logger.error(LOGTAG + errMsg);
				throw new CommandException(errMsg, ecoe);
			}

			try {

				if ("Generate".equalsIgnoreCase(msgAction)
						|| "Create".equalsIgnoreCase(msgAction)
						|| "Update".equalsIgnoreCase(msgAction)) {

					boolean isCreate = "Create".equalsIgnoreCase(msgAction);
					boolean isUpdate = "Update".equalsIgnoreCase(msgAction);
					boolean isGenerate = "Generate".equalsIgnoreCase(msgAction);

					logger.info(LOGTAG + " Start to process "+msgObject+"."+msgAction+"-Request. requestFromEai: "
							+ requestFromEai.toString());
					Element eRequisition = null;

					if (isGenerate) {
						eRequisition = requestFromEai.getRootElement().getChild("DataArea")
								.getChild(msgObject+"Requisition");
						if (eRequisition == null) {
							String errMsg = "Invalid element found in the Generate-Request message. This action expects a "+msgObject+"Requisition object";
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-665", eControlArea, msgRelease, msg, "error");
						}
					}

					Element eMsgObj = null;

					if (isCreate) {
						eMsgObj = requestFromEai.getRootElement().getChild("DataArea")
								.getChild("NewData")
								.getChild(msgObject);
						if (eMsgObj == null) {
							String errMsg = "Invalid element found in the Create-Request message. This action expects a "+msgObject+" object.";
							logger.error(LOGTAG+errMsg);
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-667", eControlArea, msgRelease, msg, "error");
						}
					}

					Element eNewData = null, eBaselineData = null;

					if (isUpdate) {
						eNewData = requestFromEai.getRootElement().getChild("DataArea")
								.getChild("NewData")
								.getChild(msgObject);
						eBaselineData = requestFromEai.getRootElement().getChild("DataArea")
								.getChild("BaselineData")
								.getChild(msgObject);						
						if (eBaselineData == null || eNewData == null ) {
							String errMsg = "Element(s) not found in the Update-Request message. This action expects NewData and BaselineData objects.";
							logger.error(LOGTAG+errMsg);
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-766", eControlArea, msgRelease, msg, "error");
						}
					}

					XMLOutputter outputter = new XMLOutputter();
					if (isGenerate) logger.info(LOGTAG + "Generate messages: " + outputter.outputString(eRequisition));
					if (isCreate) logger.info(LOGTAG + "Create messages: " + outputter.outputString(eMsgObj));
					if (isUpdate) { 
						logger.info(LOGTAG + "Baseline element: " + outputter.outputString(eBaselineData));
						logger.info(LOGTAG + "     New element: " + outputter.outputString(eNewData));
					}

					ActionableEnterpriseObject confirmedAeo = null;

					if (isGenerate) {
						requisition = (XmlEnterpriseObject) getAppConfig().getObject(msgObject+"Requisition." + generateRelease(msgRelease));
						requisition.buildObjectFromInput(eRequisition);
					}

					if (isCreate) {
						moaobject = (ActionableEnterpriseObject) getAppConfig().getObject(msgObject+"."+generateRelease(msgRelease));
						moaobject.buildObjectFromInput(eMsgObj);
					}

					if (isUpdate) {
						newMoaobject = (ActionableEnterpriseObject) getAppConfig().getObject(msgObject+"."+generateRelease(msgRelease));
						newMoaobject.buildObjectFromInput(eNewData);
						baselineMoaobject = (ActionableEnterpriseObject) getAppConfig().getObject(msgObject+"."+generateRelease(msgRelease));
						baselineMoaobject.buildObjectFromInput(eBaselineData);
					}

					if (isGenerate) {
						try {
							long generateStartTime = System.currentTimeMillis();
							confirmedAeo = provider.generate(requisition,msgAction);
							long generateTime = System.currentTimeMillis() - generateStartTime;
							logger.info(LOGTAG + "Generated "+msgObject+" in " + generateTime + " ms.");
						} catch (ProviderException pe) {
							String errMsg = "An error occurred generating "+msgObject+" in Generate-Request. The exception is: "
									+ pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-666", eControlArea, msgRelease, msg, "warn");
						}
					}

					if (isCreate) {
						try {
							long createStartTime = System.currentTimeMillis();
							confirmedAeo = provider.create(moaobject);
							long createTime = System.currentTimeMillis() - createStartTime;
							logger.info(LOGTAG + "Created "+msgObject+" in " + createTime + " ms.");
						} catch (ProviderException pe) {
							String errMsg = "An error occurred creating "+msgObject+" in Create-Request. The exception is: "
									+ pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-664", eControlArea, msgRelease, msg, "warn");
						}
					}

					if (isUpdate) {
						try {
							long updateStartTime = System.currentTimeMillis();
							confirmedAeo = provider.update(newMoaobject, baselineMoaobject);
							long updateTime = System.currentTimeMillis() - updateStartTime;
							logger.info(LOGTAG + "Updated "+msgObject+" in " + updateTime + " ms.");
						} catch (ProviderException pe) {
							String errMsg = "An error occurred updating "+msgObject+" in Update-Request. The exception is: "
									+ pe.getMessage();
							OpenEaiException e = new OpenEaiException(errMsg + msgObject);
							return getExceptionResponse(e, "IDMService-466", eControlArea, msgRelease, msg, "warn");
						}
					}

					// Issue a create or update sync 
					if (confirmedAeo != null) {
						try {
							MessageProducer producer = getProducerPool().getProducer();
							logger.info(LOGTAG + "Publishing "+msgObject+" Create/Update-Sync");
							Authentication auth = new Authentication();
							auth.setAuthUserId(authUserId);
							auth.setAuthUserSignature("none");
							confirmedAeo.setAuthentication(auth);
							confirmedAeo.createSync((SyncService) producer);
							logger.info(LOGTAG + "Published "+msgObject+".Create/Update-Sync message.");
						} catch (EnterpriseObjectSyncException | JMSException eose) {
							String errMsg = "An error occurred publishing the "+msgObject+".Create/Update-Sync message after generating.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(eose);
						}
					}

					responseXmlObject.getRootElement().getChild("DataArea").removeContent();
					logger.debug(LOGTAG + "In execute(). Completed returning oEAI object and publishingSync.");
					Element elementRole = null;
					try {
						elementRole = (Element) confirmedAeo.buildOutputFromObject();
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred serializing a "+msgObject+" object to an XML element. The exception is: "
								+ ele.getMessage();
						logger.error(LOGTAG + errMsg, ele);
						throw new CommandException(errMsg, ele);
					}
					responseXmlObject.getRootElement().getChild("DataArea").addContent(elementRole);
					String replyContents = buildReplyDocument(eControlArea, responseXmlObject);
					// Log execution time.
					long executionTime = System.currentTimeMillis() - startTime;
					logger.info(LOGTAG + "AwsServiceRequestCommand execution complete in " + executionTime + " ms.");
					// Return the response with status success.
					return getMessage(msg, replyContents);
				}

				if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					logger.debug(LOGTAG + " Start to process "+msgObject+".Delete-Request ");
					Element deleteElement = null;
					deleteElement = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.DELETE_DATA.toString())
							.getChild(msgObject);
					XMLOutputter outputter = new XMLOutputter();
					logger.info(LOGTAG + "Delete message: " + outputter.outputString(deleteElement));
					String deleteAction = null;
					deleteAction = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(MessageControl.DELETE_DATA.toString())
							.getChild("DeleteAction")
							.getAttribute("type").getValue();
					logger.info(LOGTAG + "Delete Action: " + deleteAction);

					if (deleteElement == null) {
						String errMsg = "Invalid element found in the Delete-Request message. This action expects a "+msgObject+" object";
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
					}

					try {
						aeo.buildObjectFromInput(deleteElement);
					} catch (EnterpriseLayoutException ele) {
						String errMsg = "An error occurred building the delete object from the DataArea element in the Delete-Request message. The exception is: "
								+ ele.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-666", eControlArea, msgRelease, msg, "error");
					}
					ActionableEnterpriseObject confirmedDelete = null;
					try {

						long deleteStartTime = System.currentTimeMillis();
						confirmedDelete = provider.delete(aeo,deleteAction);
						long deleteTime = System.currentTimeMillis() - deleteStartTime;
						logger.info(LOGTAG + "Deleted "+msgObject+" in " + deleteTime + " ms.");
					} catch (ProviderException pe) {
						String errMsg = "An error occurred deleting "+msgObject+" in Delete-Request. The exception is: "
								+ pe.getMessage();
						OpenEaiException e = new OpenEaiException(errMsg + msgObject);
						return getExceptionResponse(e, "IDMService-666", eControlArea, msgRelease, msg, "warn");
					}

					// Issue a delete sync only we confirm the role is deleted.
					if (confirmedDelete == null) {
						try {
							MessageProducer producer = getProducerPool().getProducer();
							logger.info(LOGTAG + "Publishing "+msgObject+" Delete-Sync");
							aeo.deleteSync("purge", (SyncService) producer);
							logger.info(LOGTAG + "Published "+msgObject+".Delete-Sync" + " message.");
						}
						catch (EnterpriseObjectSyncException | JMSException eose) {
							String errMsg = "An error occurred publishing the "+msgObject+".Delete-Sync message after revoking.";
							logger.error(LOGTAG + errMsg);
							throw new CommandException(eose);
						}
					}
				} 

				if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {

					logger.debug(LOGTAG + " Start to process "+msgObject+".Query-Request.");
					Element queryElement = null;
					queryElement = requestFromEai.getRootElement().getChild(MessageControl.DATA_AREA.toString())
							.getChild(msgObject+"QuerySpecification");
					XMLOutputter outputter = new XMLOutputter();

					logger.info(LOGTAG + "Query messages: " + outputter.outputString(queryElement));
					if (queryElement != null) {
						querySpec = (XmlEnterpriseObject) getAppConfig()
								.getObject(msgObject+"QuerySpecification." + generateRelease(msgRelease));
						querySpec.buildObjectFromInput(queryElement);
						logger.info(LOGTAG + "querySpec is build: " + querySpec);
					}

					aeos = provider.query(querySpec);		
				}

				Element responseDataArea = responseXmlObject.getRootElement().getChild(DATA_AREA);
				responseDataArea.removeContent();

				// Return the response with status success.
				// Log execution time.
				long executionTime = System.currentTimeMillis() - startTime;
				logger.info(
						LOGTAG + "AwsServiceRequestCommand execution complete in " + executionTime + " ms.");

				// Send response
				Element aeoElement = null;
				List<Element> elementList = new ArrayList<Element>();
				if (MessageControl.QUERY_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
					if (aeos != null && aeos.size() > 0) {
						for (XmlEnterpriseObject eObj : aeos) {
							aeoElement = getElementFromEnterpriseObject(eObj);
							elementList.add(aeoElement);
						}
						if (!elementList.isEmpty()) {
							for (Element element : elementList) {
								responseDataArea.addContent(element);
							}
						}
					}
				} else {
					if (MessageControl.DELETE_MSG_ACTION.toString().equalsIgnoreCase(msgAction)) {
						responseDoc.getRootElement().getChild("DataArea").removeContent();
						String replyContents = buildReplyDocument(eControlArea, responseDoc);
						// Return the response with status success.
						// Log execution time.
						executionTime = System.currentTimeMillis() - startTime;
						logger.info(
								LOGTAG + "AwsServiceRequestCommand execution complete in " + executionTime + " ms.");
						return getMessage(msg, replyContents);
					}
				}

				String responseMessage = buildReplyDocument(eControlArea, responseXmlObject);
				Message responseToEAI = getMessage(msg, responseMessage);
				// Log execution time.
				executionTime = System.currentTimeMillis() - startTime;
				logger.info(LOGTAG + "AwsServiceRequestCommand execution complete in " + executionTime + " ms.");
				return responseToEAI;

			}
			catch (OpenEaiException o) {
				logger.error("An error occurred starting the producer. The exception is: "
						+ o.getMessage());
				return getExceptionResponse(o, "IDMService-666", eControlArea, msgRelease, msg, "error");

			} catch (ProviderException e) {
				logger.error(LOGTAG+"An error occurred executing the query. The exception is: "
						+ e.getMessage());
				return getExceptionResponse(e, "IDMService-666", eControlArea, msgRelease, msg, "error");
			}


		} else {
			String errMsg = "The Aws service doesn't support the input message object. Verify that the sending application is sending appropriate message objects. Input message object: ";
			OpenEaiException e = new OpenEaiException(errMsg + msgObject);
			return getExceptionResponse(e, errMsg, eControlArea, msgRelease, msg, "error");
		}
	}

	/**
	 * Converts object to XmlEnterpriseObject to add to return replyDoc message in
	 * XML format.
	 * 
	 * @param eObject
	 * @return
	 * @throws OpenEaiException
	 */
	private <T> Element getElementFromEnterpriseObject(T eObject) throws OpenEaiException {

		if (!(eObject instanceof org.openeai.moa.XmlEnterpriseObject)) {
			String errMsg = "Error while attempting to convert a MOA Enterprise Object to JDOM Element. Input object not of type 'org.openeai.moa.XmlEnterpriseObject'.";
			throw new OpenEaiException(errMsg);
		}

		XmlEnterpriseObject enterpriseObj = (XmlEnterpriseObject) eObject;
		Element eOutput = null;
		if (enterpriseObj != null) {
			org.openeai.layouts.EnterpriseLayoutManager outElm = enterpriseObj
					.getOutputLayoutManager(MessageControl.XML_OUTPUT.toString());
			enterpriseObj.setOutputLayoutManager(outElm);
			eOutput = (Element) enterpriseObj.buildOutputFromObject();
		}
		return eOutput;
	}

	/**
	 * Creates and returns an XML object that is ready to receive input element
	 * data.
	 * 
	 * @param msgAction  - Action that this object is a response to
	 * @param msgObject  - The type of action this object responds to
	 * @param msgRelease - Version information.
	 * @return Document - Ready JDOM XML object
	 * @throws CommandException
	 */
	protected Document getResponseXml(final String msgAction, final String msgObject, final String msgRelease)
			throws CommandException {
		try {
			XmlEnterpriseObject xmlObject = (XmlEnterpriseObject) getAppConfig()
					.getObject(msgObject + "." + generateRelease(msgRelease));
			Document responseObject = null;
			responseObject = xmlObject.getProvideDoc();
			if (responseObject == null) {
				String errMsg = "Could not find an appropriate reply document for the following request: ";
				throw new CommandException(errMsg + msgObject + "-" + msgAction);
			}
			return responseObject;
		}
		catch (Exception e) {
			throw new CommandException(e.getMessage(), e);
		}
	}

	/**
	 * This method wraps the response exception passed in to the Error elements as
	 * specified in the OpenEAI protocal.
	 * 
	 * @param e
	 * @param errorId
	 * @param eControlArea
	 * @param msgRelease
	 * @param msg
	 * @return
	 * @throws CommandException
	 */
	protected Message getExceptionResponse(Exception e, String errorId, Element eControlArea, String msgRelease,
			TextMessage msg, String logLevel) throws CommandException {
		Document xmlObject = getResponseXml(MessageControl.PROVIDE_MSG_ACTION.toString(),
				eControlArea.getAttributeValue(MESSAGE_OBJECT), msgRelease);

		List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<org.openeai.moa.objects.resources.Error>();
		errors.add(buildError(e.getClass().getName(), "EaiException" + errorId, e.getMessage()));

		String responseContents = buildReplyDocumentWithErrors(eControlArea, xmlObject, errors);
		if (logLevel.equals("error")) {
			logger.error(LOGTAG + "Exception Thrown: \nException Message: " + e.getMessage() + "\nStack Trace: "
					+ this.getExceptionStackTrace(e));
		}
		else {
			logger.warn(LOGTAG + "Exception Thrown: \nException Message: " + e.getMessage());
		}
		return getMessage(msg, responseContents);
	}

	private String getExceptionStackTrace(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}

	public static int getConnectionTimeout() {
		return connectionTimeout;
	}

	public static void setConnectionTimeout(int connectionTimeout) {
		AwsServiceRequestCommand.connectionTimeout = connectionTimeout;
	}

	public static int getSocketTimeout() {
		return socketTimeout;
	}

	public static void setSocketTimeout(int socketTimeout) {
		AwsServiceRequestCommand.socketTimeout = socketTimeout;
	}

	protected static void setServicePassword(String password) {
		servicePassword = password;
	}

	public static String getServicePassword() {
		return servicePassword;
	}

	protected static void setServiceUserId(String id) {
		serviceUserId = id;
	}

	public static String getServiceUserId() {
		return serviceUserId;
	}

	protected static void setWebServiceEndpoint(String endpoint) {
		searchServiceEndpoint = endpoint;
	}

	protected static String getWebServiceEndpoint() {
		return searchServiceEndpoint;
	}

	public ProducerPool getProducerPool() {
		return this.producerPool;
	}

	public void setProducerPool(ProducerPool producerPool) {
		this.producerPool = producerPool;
	}

	public Document getResponseDoc() {
		return responseDoc;
	}

	public void setResponseDoc(Document responseDoc) {
		this.responseDoc = responseDoc;
	}

	public RoleAssignmentProvider getRoleAssignmentProvider() {
		return roleAssignmentProvider;
	}

	public RoleProvisioningProvider getRoleProvisioningProvider() {
		return roleProvisioningProvider;
	}

	public void setRoleProvisioningProvider(RoleProvisioningProvider roleProvisioningProvider) {
		this.roleProvisioningProvider = roleProvisioningProvider;
	}

	public ResourceProvisioningProvider getResourceProvisioningProvider() {
		return resourceProvisioningProvider;
	}

	public void setResourceProvisioningProvider(ResourceProvisioningProvider resourceProvisioningProvider) {
		this.resourceProvisioningProvider = resourceProvisioningProvider;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
}

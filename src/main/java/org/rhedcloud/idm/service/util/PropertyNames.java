package org.rhedcloud.idm.service.util;

public enum PropertyNames {

	CODE_MAP_KEY("codeMapKey"), 
	IDV_CODE_MAP_KEY("idvCodeMapKey"), 
	EAD_ENTITLEMENT_DN("eadEntitlementDn"),
	HMD_ENTITLEMENT_DN("hmdEntitlementDn"), 
	UMD_ENTITLEMENT_DN("umdEntitlementDn"),
	IDV_ENTITLEMENT_DN("idvEntitlementDn"), 
	CATEGORY_KEY("categoryKey"), 
	CONNECTION_TIMEOUT("connectionTimeout"),
	GENERAL_PROPERTIES("GeneralProperties"), 
	ROLE_WEB_SERVICE_ENDPOINT("roleWebServiceEndpoint"),
	RESOURCE_WEB_SERVICE_ENDPOINT("resourceWebServiceEndpoint"),
	ROLE_SERVICE_PROVIDER_CLASSNAME("roleServiceProviderClassName"),
	RESOURCE_SERVICE_PROVIDER_CLASSNAME("resourceServiceProviderClassName"),
	ROLE_ASSIGNMENT_PROVIDER_CLASSNAME("roleAssignmentProviderClassName"),
	ACCOUNT_ASSIGNMENT_PROVIDER_CLASSNAME("accountAssignmentProviderClassName"),
	PERMISSION_SET_PROVIDER_CLASSNAME("permissionSetProviderClassName"),
	SOCKET_TIMEOUT("socketTimeout"),
	WEB_SERVICE_NAME("webServiceName"),
	WAIT_INTERVAL("waitInterval"),
	MAX_WAIT_TIME("maxWaitTime"),
	WEB_SERVICE_USER_ID("webServiceUserId"),
	WEB_SERVICE_PASSWORD("webServicePassword"),
	POLICY_PROVIDER_CLASSNAME("policyProviderClassName");

	private final String name;

	public String getName() {
		return name;
	}

	private PropertyNames(String s) {
		this.name = s;
	}

	public String toString() {
		return name;
	}
}

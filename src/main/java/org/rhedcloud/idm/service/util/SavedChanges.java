package org.rhedcloud.idm.service.util;

/**
 * Make sure the class is generic enough that we can pass in object like Role or
 * Resource if needed even though we pass String object for now.
 * 
 * @author RXING2
 *
 * @param <T>
 * @param <U>
 */
public class SavedChanges<T, U> {
	public final T key;
	public final U value;

	public SavedChanges(T key, U value) {
		this.key = key;
		this.value = value;
	}
}

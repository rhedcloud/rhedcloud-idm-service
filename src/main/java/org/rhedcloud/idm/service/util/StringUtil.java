package org.rhedcloud.idm.service.util;

import java.math.BigInteger;
import java.util.UUID;

/**
 * Utility class that contains methods to format and search an input string
 * 
 * @author rxing2
 */
public class StringUtil {
	/**
	 * Removes the last occurrence of a ','
	 * 
	 * @param str
	 * @return
	 */
	public static String replaceLastChar(String str) {
		if (str.length() > 0 && str.charAt(str.length() - 1) == ',') {
			str = str.substring(0, str.length() - 1);
		}
		return str;
	}

	public static String splitString(String str) {
		// str = "Hi richard, how are you?,i have a question!,can you help me?";
		String[] ss = str.split(",");
		for (int i = 0; i < ss.length; i++) {
			// System.out.println(ss[i]);
		}
		return ss[0];
	}

	/**
	 * Uses the matches() method in the String class to search for the regular
	 * expression "is registered" against whole string text passed in. An invocation
	 * of this method yields exactly the same result as the expression
	 * Pattern.matches(regex, str).
	 * <P>
	 * Example 1: "registrant - 2296 - is registered" returns true
	 * <P>
	 * Example 2: "registration - 2374 - is registered." returns true
	 * <P>
	 * Example 3: "registrant - 1398 - is Registered" returns false
	 * <P>
	 * 
	 * @param string text
	 * @return true or false
	 */
	public static boolean isFilterMatch(String str) {
		if (str.matches(".*is registered.?$")) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * 
	 * @param str
	 * @param type
	 * @return
	 */
	public static String getRoleResourceDn(String str, String type) {
		String dn = null;
		if (type.equalsIgnoreCase("Role")) {
			if (str.matches("(?i).*Role already exists:.*")) {
				dn = str.trim().substring(str.indexOf("Role already exists:") + 20, str.length()).trim();
				return dn;
			}
		}
		else if (type.equalsIgnoreCase("Resource")) {
			if (str.matches("(?i).*Resource already exists:.*")) {
				dn = str.trim().substring(str.indexOf("Resource already exists:") + 24, str.length()).trim();
				return dn;
			}
		}
		return null;
	}

	public static boolean isResourceAlreadyExist(String str) {
		if (str.indexOf("Resource already exists") != -1) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public static boolean isResourceExist(String str) {
		if (str.indexOf("does not exist") != -1) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public static boolean isRoleExist(String str) {
		if (str.indexOf("The Role does not exist") != -1) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public static String convertUUIDFromString(String str) {

		if (str == null) {
			return null;
		}
		String parsedString = str.replaceAll("-", "");
		BigInteger biFirst = new BigInteger(parsedString.substring(0, 16), 16);
		BigInteger biSecond = new BigInteger(parsedString.substring(16, 32), 16);
		UUID uuid = new UUID(biSecond.longValue(), biFirst.longValue());
		return uuid.toString().replaceAll("-", "");
	}

	public static String convertGuidFromString(String str) {
		if (str == null) {
			return null;
		}
		String guid;
		guid = new StringBuffer().append(str.substring(7, 9)).append(str.substring(5, 7)).append(str.substring(3, 5))
				.append(str.substring(1, 3)).append(str.substring(12, 14)).append(str.substring(10, 12))
				.append(str.substring(17, 19)).append(str.substring(15, 17)).append(str.substring(20, 24))
				.append(str.substring(25, 37)).toString();
		return guid;
	}

	public static String getUUIDFromString(String str1) {

		String details = str1.replaceAll("\"", " ");
		String guidStr = null;
		if (details.contains("ID ")) {
			guidStr = details.substring(details.indexOf("ID ") + 5, details.indexOf("ID ") + 37);
		}
		return guidStr.trim();
	}
}

package org.rhedcloud.idm.service.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import org.openeai.moa.objects.resources.Datetime;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * This class contains methods that convert a string or date input to
 * XMLGreorianCalendar or other date and time formats.
 * 
 * @version 1.1 Added methods for Imodules Connector (2015-10-03).
 * @version 1.2 Added convertStringToCalendar and convertStringToDate methods
 *          for EirbRequest Connector (2016-05-23).
 * @version 1.3 Added convertCalToEaiDatetime methods for RoleService Connector
 *          (2017-02-13).
 * 
 * @author Rxing2
 */
public class CalendarUtil {
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	/**
	 * Validates a string input and converts it to XMLGreorianCalendar
	 * 
	 * @param string
	 * @return XMLGregorianCalendar
	 * @throws ParseException
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar stringTimeToXMLGregorianCalendar(String s)
			throws ParseException, DatatypeConfigurationException {
		XMLGregorianCalendar result = null;
		Date date = formatter.parse(s);
		GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
		gregorianCalendar.setTime(date);
		result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		return result;
	}

	/**
	 * Validates a date input and converts it to XMLGreorianCalendar
	 * 
	 * @param date
	 * @return XMLGregorianCalendar
	 * @throws ParseException
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar dateTimeToXMLGregorianCalendar(Date d)
			throws ParseException, DatatypeConfigurationException {
		XMLGregorianCalendar result = null;
		String stringDate = formatter.format(d);
		Date date = formatter.parse(stringDate);
		GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
		gregorianCalendar.setTime(date);
		result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		return result;
	}

	/**
	 * This method returns true if the checkTime falls between the startTime and
	 * endTime.
	 * 
	 * @param checkTime
	 * @param startTime
	 * @param endTime
	 * @return return true of checkTime is between startTime and endTime, otherwise
	 *         return false
	 */
	public static boolean isInRange(String checkTime, String startTime, String endTime) {
		return checkTime.compareTo(startTime) >= 0 && checkTime.compareTo(endTime) <= 0;
	}

	/**
	 * This method returns the current time value as a formatted string.
	 * 
	 * @return strDate
	 */
	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");
		java.util.Date now = new java.util.Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	/**
	 * This method parses the date string into a Date object
	 * 
	 * @param dateString
	 * @return
	 */
	public static Date convertStringToDate(String dateString) {
		Date formatteddate = null;
		dateString = "Thu Sep 28 20:29:30 JST 2000";
		DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
		try {
			formatteddate = df.parse(dateString);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return formatteddate;
	}

	/**
	 * This method converts the date string into a Calendar object
	 * 
	 * @param dateString
	 * @return
	 */
	public static Calendar convertStringToCalendar(String dateString) {

		DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
		Date formatteddate = null;
		Calendar cal = null;
		try {
			formatteddate = df.parse(dateString);
			cal = Calendar.getInstance();
			cal.setTime(formatteddate);
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return cal;
	}

	/**
	 * This method converts the date into a Calendar object
	 * 
	 * @param date
	 * @return
	 */
	public static Calendar convertDateToCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	/**
	 * This method converts a calendar date to an oEAI Datetime
	 * 
	 * @param calendar Datetime
	 * @return
	 */
	public static Datetime convertCalToEaiDatetime(Calendar calendar) {

		if (calendar == null) {
			return null;
		}

		Datetime eaiDatetime = new Datetime(calendar.getTime());
		TimeZone time = TimeZone.getDefault();

		eaiDatetime.setTimezone(time.toString());
		// eaiDatetime.setTimezone(calendar.getTimeZone().getID());

		eaiDatetime.setYear(Integer.toString(calendar.get(1)));
		eaiDatetime.setMonth(Integer.toString(calendar.get(2) + 1));
		eaiDatetime.setDay(Integer.toString(calendar.get(5)));
		eaiDatetime.setHour(Integer.toString(calendar.get(10)));
		eaiDatetime.setMinute(Integer.toString(calendar.get(12)));
		eaiDatetime.setSecond(Integer.toString(calendar.get(13)));
		eaiDatetime.setSubSecond(Integer.toString(calendar.get(14)));
		return eaiDatetime;
	}

	/**
	 * Handles the construction of openEAI datetimes
	 * 
	 * @param Calendar datetime returned by the web service
	 * @return an open EAI datetime
	 */
	public static Datetime convertDatetime(Calendar calendar) {
		if (calendar == null) {
			return null;
		}
		org.openeai.moa.objects.resources.Datetime eaiDatetime = new org.openeai.moa.objects.resources.Datetime();
		eaiDatetime.update(calendar);

		return eaiDatetime;
	}
}


package org.rhedcloud.idm.service.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.json.simple.JSONObject;
//import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.rhedcloud.idm.service.provider.netiq.util.IdmValues;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

/**
 * This class contains utilities methods for handling JSON-formatted text based
 * mostly on the use of the JSON-Simple and flexjson library.
 * 
 * @author rxing2
 *
 */
public class JsonUtils {

	/**
	 * Converts a String (holding a JSON String) using the JSON-simple parser to
	 * create the JSON-Simple object
	 * 
	 * @param json
	 * @param id
	 * @return
	 */
	public static void main(String[] args) {
		System.out.println("before calling");
		String json = "{\"ID\":\"7026170705e5ca4f83c248823dbfd961\",\"ID2\":\"CN=AWS-Admin-24,OU=AWS,DC=entdev,DC=emory,DC=edu\"}";
		String result = convertJSONObjectToString(json, "ID");
		System.out.println("result: " + result);

		// String json = "7026170705e5ca4f83c248823dbfd961";
		// String dn = "CN=AWS-Admin-24,OU=AWS,DC=entdev,DC=emory,DC=edu";
		// ESBJsonObject result = convertStringToJSONObject(json, dn);
		// System.out.println("result: "+result.toString());

	}

	public static String convertJSONObjectToString(String json, String id) {

		// String json =
		// "{\"ID\":\"7026170705e5ca4f83c248823dbfd961\",\"ID2\":\"CN=AWS-Admin-24,OU=AWS,DC=entdev,DC=emory,DC=edu\"}";
		JSONObject result = null;
		StringReader r = new StringReader(json);
		JSONParser jp = new JSONParser();
		try {
			result = (JSONObject) jp.parse(r);
		}
		catch (Throwable t) {
			// logger.info(t.getMessage());
		}
		r.close();
		String jsonString = null;
		jsonString = (String) result.get(id);
		return jsonString;
	}

	@SuppressWarnings("unchecked")
	public static ESBJsonObject convertStringToJSONObject(String guid, String dn) {

		ESBJsonObject object = new ESBJsonObject();
		object.put(IdmValues.ID.getName(), guid);
		object.put(IdmValues.ID2.getName(), dn);
		return object;
	}

	/**
	 * Converts a JSON String (reader) into a corresponding object of the desired
	 * class using the flexjson library.
	 * 
	 * @param rd        reader holding json string
	 * @param thisclass the object's desired class
	 * @return an object of the desired class
	 */
	public static <T> T convertJsonResponse(StringReader rd, Class<T> thisclass) {
		T jsonObject = new JSONDeserializer<T>().deserialize(rd, thisclass);
		return jsonObject;
	}

	/**
	 * Converts an input stream (holding a JSON String) into a corresponding object
	 * of the desired class.
	 * 
	 * @param inputStream
	 * @param thisclass
	 * @return
	 */
	public static <T> T convertJsonResponse(InputStream inputStream, Class<T> thisclass) {
		BufferedReader rd = null;
		T jsonObject = null;
		try {
			rd = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			jsonObject = new JSONDeserializer<T>().deserialize(rd, thisclass);
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			try {
				rd.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		return jsonObject;
	}

	/**
	 * Converts an input stream (holding a JSON String) into a list of objects of
	 * the desired class.
	 * 
	 * @param inputStream
	 * @param thisclass
	 * @return
	 */
	public static <T extends Object> List<T> convertJsonArrayResponse(StringReader rd, Class<T> thisclass) {
		return new JSONDeserializer<List<T>>().use(null, thisclass).deserialize(rd);
	}

	/**
	 * 
	 * @param rd
	 * @param thisclass
	 * @return
	 */
	public static <T extends Object> Object convertJsonObjectResponse(StringReader rd, Class<T> thisclass) {
		return new JSONDeserializer<Object>().use(null, thisclass).deserialize(rd);
	}

	public static <T extends Object> List<T> convertJsonArrayResponse(InputStream inputStream, Class<T> thisclass) {
		BufferedReader rd = null;

		List<T> jsonArray = null;
		try {
			rd = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			jsonArray = new JSONDeserializer<List<T>>().use("values", thisclass).deserialize(rd);
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			try {
				rd.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		return jsonArray;
	}

	/**
	 * Serializes an object using the flexjson library.
	 * 
	 * @param obj
	 * @return
	 */
	public static String serialize(Object obj) {
		return (new JSONSerializer()).serialize(obj);
	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static String serializeNoClass(Object obj) {
		return (new JSONSerializer()).exclude("*.class").serialize(obj);
	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static String deepSerializeNoClass(Object obj) {
		return (new JSONSerializer()).exclude("*.class").deepSerialize(obj);
	}
}
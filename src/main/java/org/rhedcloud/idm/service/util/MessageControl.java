package org.rhedcloud.idm.service.util;

public enum MessageControl {

	CREATE_MSG_ACTION("Create"),
	DATA_AREA("DataArea"),
	DELETE_DATA("DeleteData"),
	DELETE_MSG_ACTION("Delete"),
	GENERATE_MSG_ACTION("Generate"),
	NEW_DATA("NewData"),
	PROVIDE_MSG_ACTION("Provide"),
	QUERY_MSG_ACTION("Query"),
	REQUEST_MSG_TYPE("Request"),
	REPLY_MSG_TYPE("Reply"),
	RESOURCE_OBJECT("Resource"),
	RESOURCE_QUERY_SPECIFICATION("ResourceQuerySpecification"),
	RESOURCE_REQUISITION("ResourceRequisition"),
	ROLE_ASSIGNMENT("RoleAssignment"),
	ROLE_ASSIGNMENT_OBJECT("RoleAssignment"),
	ROLE_ASSIGNMENT_QUERY_SPECIFICATION("RoleAssignmentQuerySpecification"),
	ROLE_ASSIGNMENT_REQUISITION("RoleAssignmentRequisition"),
	ROLE_OBJECT("Role"),
	ROLE_QUERY_SPECIFICATION("RoleQuerySpecification"),
	ROLE_REQUISITION("RoleRequisition"),
	UPDATE_MSG_ACTION("Update"),
	XML_OUTPUT("xml");


	private final String name;

	public String getName() {
		return name;
	}

	private MessageControl(String s) {
		this.name = s;
	}

	public String toString() {
		return name;
	}
}

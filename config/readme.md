# Config folder files

Name | Description
----------------- | -------------
`hibernate-rtp.cfg.xml`  | Template used to configure hibernate. Contains placeholder values that need to be replaced prior to deployment.
`ResourceTaggingProfile-SampleData.xml`  | Used by the [CreateAll](../src/main/java/org/rhedcloud/rtp/app/CreateAll.java) demo app to populate the database with demo records
`rtp.properties` | Properties file used by the ServiceGen app to produce the hibernate mapping and WSDL files.
`service.properties` | Example properties file that can be used to run the service
`serviceAppConfig.xml` | Example configuration file that can be used to run the service
`serviceHibernate.properties` | Another example properties file that can be used to run the service
`serviceHibernateAppConfig.xml` | Example configuration file that can be used to run the service

